===GPT AI Power===
Contributors: senols
Tags: gpt, chatgpt, gpt3, ai, openai, content generator, product writer, dall-e, image generator, fine-tuner, ai training
Requires at least: 5.0.0
Tested up to: 6.1.1
Requires PHP: 7.4
Stable tag: 1.5.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
GPT AI Power: Complete AI Pack is a complete package for AI tools. Content Writer, Auto Content Writer, ChatGPT, Image Generator (DALL-E and Stable Diffusion), WooCommerce Product Writer, AI Training, SEO optimizer and hundreds of ready to use prompts.
 
== Description ==

Please read documentation here: <a href="https://gptaipower.com/">https://gptaipower.com/</a>

== Core Features ==
* Content Writer
* Auto Content Writer
* Bulk Content Writer
* Image Generator (DALL-E and Stable Diffusion 🚀🚀🚀)
* ChatGPT
* WooCommerce Product Writer
* AI Training
* Fine-Tuner
* Dataset Converter
* Embeddings! Customize your chat bot with embeddings - Integrated with Pinecone vector DB.
* Index builder for Embeddings! Convert all your pages, posts and products to embedding format with one click.
* PromptBase - hundreds of ready to use prompts
* GPT Forms - Design your own forms and embed them into your website
* Playground
* SEO Optimizer
* Title suggestion tool for posts, pages and products.
* GPT powered semantic search with Embeddings
* 38 langauge support
 
GPT AI Power is a complete AI package for WordPress. It generates content, images, and forms with customizable options. It includes AI training, Chat widget, WooCommerce integration, and is integrated in Posts. Enhance your website with this comprehensive solution.

https://www.youtube.com/watch?v=lmT_ctVA8iI

GPT AI Power is a comprehensive WordPress plugin that leverages the power of OpenAI's GPT-3 language model to generate high-quality content, images, and forms for your website. 

It features a Content Writer, Auto Content Writer, Bulk Content Writer, Image Generator (DALL-E and Stable Diffusion), WooCommerce Product Writer, hundreds of ready to use prompts and forms and AI Training capabilities. 

With 38 language support and customizable options for writing style, tone, and image generation, the plugin offers a wide range of possibilities to enhance your website content. 

The plugin is integrated in Posts and includes a Playground for testing.

Train your chat bot with your content and products.

https://www.youtube.com/watch?v=NPMLGwFQYrY

Design GPT based forms and prompts for your website.

https://www.youtube.com/watch?v=hetYOlR-ms4

== Benchmark Results ==

1. English, 10 heading + 1 image: 2 minute 14 seconds - 1,713 words 11,467 characters
2. English, 5 heading + 1 image: 1 minute 17 seconds - 1,007 words 6,974 characters
3. English, 5 heading (no image): 58 seconds - 959 words 6,367 characters
4. English, 3 heading + 1 image: 36 seconds - 688 words 4,878 characters

== Features ==

* Generate high-quality, longer articles using OpenAI's GPT-3 language model (text-davinci-003).
* Support for 23 different languages, including English, Spanish, French, German, Italian, Portuguese, Russian, Japanese, Korean, Chinese, Dutch, Indonesian, Turkish, Polish, Ukrainian, Arabic, Romanian, Greek, Czech, Bulgarian, Swedish and Hungarian.
* Customize the generated content with options for temperature, maximum tokens, top p, best of, frequency penalty, and presence penalty.
* Customize the generated content with options for writing style, including informative, descriptive, creative, narrative, persuasive, expository, reflective, argumentative, analytical, critical, evaluative, journalistic, technical and simple.
* Adjust the tone of the generated content with options for formal, neutral, assertive, cheerful, humorous, informal, inspirational, sarcastic, professional and skeptical.
* Automatically generate high-quality images to accompany the generated content using OpenAI's DALL-E engine.
* Add customizable introductions and conclusions to your generated content.
* Add customizable heading tags (h1, h2, h3, h4, h5, h6) to your content.
* Add customizable hyperlink and anchor text to your content.
* Easily manage and update your OpenAI API key from within the plugin's settings page.
* Save as a draft
* Track logs
* Edit, delete, sort or add new headings before generating content.
* Add Call to Action to your content.
* Integrated in Post and Page.
* Playground for making tests.
* Auto content writer.
* With the Bulk Editor tool, you can easily generate large amounts of content at once. There are three ways to use the Bulk Editor:
* Directly within the tool: Simply enter your desired titles and the Bulk Editor will generate content. Free plan users can generate up to 5 at a time.
* Bulk update via CSV: If you have a large amount of text stored in a CSV file, you can upload it to the Bulk Editor and generate content. Free plan users can generate up to 5 at a time.
* Bulk update via copy-paste: You can also use the Bulk Editor by simply copying and pasting your text into the tool, and then generate content. Free plan users can generate up to 5 at a time.
* Embed GPT chatGPT in front-end.
* Featured image.
* Art Styles.
* WooCommerce Integration: You can optimize your product title, description and short description with our plugin.
* Train your AI. Fine-Tune and create your own model.
* Convert your db to jsonl format for AI training.
* Image Generator with hundreds of configuration options using DALL-E and Stable Diffusion 🚀🚀🚀
* Chatbot tone and proffesion options.
* PromptBase - hundreds of ready to use prompts
* GPT Forms - Embed hundreds of GPT forms with shortcodes
* Embed Image Generator (both DALL-E and Stable Diffusion) in your website.
* Embeddings! Customize your chat bot with embeddings - Integrated with Pinecone vector DB.
* Title suggestion tool for posts, pages and products.
* Index builder for Embeddings! Convert all your pages, posts and products to embedding format with one click.
* GPT powered semantic search with Embeddings
* [PREMIUM] Ability to add keywords
* [PREMIUM] Ability to avoid certain keywords
* [PREMIUM] Make keywords bold
* [PREMIUM] Additional writing styles
* [PREMIUM] Additional writing tones
* [PREMIUM] Ability to Add Q&A
* [PREMIUM] Ability to schedule posts.

== Supported Languages ==
* Arabic
* Bulgarian
* Chinese
* Croatian
* Czech
* Danish
* Dutch
* English
* Estonian
* Filipino
* Finnish
* French
* German
* Greek
* Hebrew
* Hindi
* Hungarian
* Indonesian
* Italian
* Japanese
* Korean
* Latvian
* Lithuanian
* Malay
* Norwegian
* Polish
* Portuguese
* Romanian
* Russian
* Serbian
* Slovak
* Slovenian
* Spanish
* Swedish
* Thai
* Turkish
* Ukrainian
* Vietnamese

== Installation ==
 
1. Upload `gpt3-ai-content-generator.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Setup OpenAI API key.
4. Setup temperature, max tokens, best of, frequenct penalty.
5. Go to posts and create a "New post".
6. Enter title, for example: Tesla Electric cars.
7. Enter number of headings, for example: 7
8. Click generate.
9. Copy response and paste it in your text editor.


== Screenshots ==

1. Settings
2. Classic Editor
3. Block Editor
4. Benchmark Results
 
== Frequently Asked Questions ==
 
= What is GPT AI Power? =
GPT AI Power is a complete AI package for WordPress that generates high-quality content, images, and forms using OpenAI's GPT-3 technology.

= What are the core features of GPT AI Power? =
The core features include a Content Writer, Auto Content Writer, Bulk Content Writer, Image Generator, ChatGPT, WooCommerce Product Writer, AI Training, Fine-Tuner, Dataset Converter, PromptBase, GPT Forms and SEO Optimizer with support for 38 languages.  

= Can I customize the generated content? =
Yes, you can customize the generated content with options for writing style, tone, temperature, maximum tokens, and more.

= Does GPT AI Power support WooCommerce integration? =
Yes, GPT AI Power has WooCommerce integration, allowing you to optimize your product titles, descriptions, and short descriptions.

=Can I train the AI with my own data?=
Yes, you can train the AI using your own data with the Fine-Tuner and Dataset Converter. You can also create your own model and use it in the plugin.

=What is causing the timeout issue in the plugin?=
The timeout issue may be caused by a limit set on your web server, or by the use of a VPS, CDN, proxy, firewall, or Cloudflare.
To resolve the timeout issue, please contact your hosting provider and request an increase in the timeout limit.

=Is there a minimum timeout limit required for the plugin to work properly?=
The minimum timeout limit required for the plugin to work properly depends on the content settings, such as the number of headings, the language used, and the amount of additional content like an introduction or conclusion.
 
=I increased PHP max execution time, but I'm still getting a timeout issue. What should I do?=
The PHP max execution time setting determines the maximum amount of time a PHP script is allowed to run before it is terminated by the server. However, increasing this setting may not necessarily resolve a timeout issue if there are other factors involved, such as the size and complexity of the content being generated or other server-side configurations like VPS, CDN, proxy, firewall, or Cloudflare. In these cases, it may be necessary to increase the timeout limit set by your hosting provider in order to resolve the issue.

=I received the error message "You exceeded your current quota, please check your plan and billing details." What does this mean?=
This message is coming from OpenAI, not from our plugin. It means that you have reached the limit of your OpenAI API quota, which is determined by your plan and billing details. Our plugin works on a "bring your own API key" model, serving as a bridge between WordPress and OpenAI, so the issue is not related to the plugin itself. To resolve this issue, you'll need to check your OpenAI account and ensure that you have enough quota for your needs, or upgrade your plan if necessary.

== Changelog ==

= 1.5.6 =
* [Language]: Hungarian, Indonesian and Japanese language support added for Chat widget.
* [Fix]: Fixed "Act As" order in ChatGPT tab.
* [Fix]: Removed "Mean" style from chat widget.
* [Fix]: Changed placeholder for Search box from "Ask anything.." to "Search anything.."
* [Documentation]: Added a small guide for SearchGPT feature under How to Use tab.

= 1.5.5 =
* [New Feature]: GPT powered Semantic Search is here! You can now implement GPT powered search box in your website with a shortcode. You need to use Embeddings to activate this feature.
* [Language]: Hebrew language support added for Chat widget.
* [Fix]: Fixed chat widget responsive issue.

= 1.5.4 =
* [Language]: Croatian, Danish, Estonian, Finnish and Greek language support added for Chat widget.

= 1.5.3 =
* [Language]: Czech, Dutch, Polish and Turkish language support added for Chat widget.

= 1.5.2 =
* [New Feature]: I am very excited about this new feature :) It is called Index Builder. You can now convert all your pages, posts and products to embedding format and use it with your chat widget. It means your chat bot will know everything about your content with just one click.

= 1.5.1 =
* [Language]: Portuguese, Bulgarian and Italian language support added for Chat widget.

= 1.5 =
* [Fix]: Various fixes.
* [Fix]: Display a meaningful message when there is an empty response from OpenAI.

= 1.4.99 =
* [Fix]: Various fixes.
* [Fix]: Display a meaningful message when there is an empty response from OpenAI.

= 1.4.98 =
* [New Feature]: Title suggestion tool here! Head over to the posts, pages or product list and click on "Suggest Title" link. Choose one from the suggestion list and save it.
* [Enhancement]: Added author info to the PromptBase and GPT Forms.
* [Enhancement]: Restructured the codebase.
* [Fix]: Input fields aligned in Stable Diffusion page.
* [Fix]: Input field size fix in GPT Forms and Promptbase.
* [Fix]: Fixed min/max notice in GPT forms.
* [Fix]: Fixed dropdown custom field of GPT forms.
* [Fix]: "Say I dont know" instruction removed from chat widget. This strict rule will be added as an optional feature.
* [Fix]: Fixed styling issue with Image Generator shortcode for some specific themes.

= 1.4.97 =
* Video tutorials added for GPT forms, PromptBase and Embeddings.

= 1.4.96 =
* [New Feature]: Yoast SEO and All in One SEO integration. You can now automatically update those plugins meta description field.
* [Fix]: Fixed scrolling for large embedding content in modal window.
* [Enhancement]: Added car expert proffesion for chat widget.
* [Enhancement]: Added "Say I dont know" instruction for chat widget if the question is not in the current context.

= 1.4.95 =
* [New Feature]: * Embeddings here! Customize your chat bot with embeddings - Integrated with Pinecone vector DB.

= 1.4.94 =
* [Fix]: Removed session management and now using cookies instead. This will solve session issues.

= 1.4.93 =
* [Fix]: Fixed an issue related with session management.

= 1.4.92 =
* [Fix]: Fixed an issue related with session management.
* [Fix]: Fixed a CSS issue on GPT Forms.

= 1.4.91 =
* [Fix]: Fixed an issue on chat widget page.

= 1.4.90 =
* [Fix]: Removed API status page.

= 1.4.89 =
* [Fix]: Fixed a bug that was causing empty headings.

= 1.4.88 =
* [Fix]: Fixed a few more bugs to improve stability and user experience.

= 1.4.87 =
* [Fix]: Various fixes.

= 1.4.86 =
* [Fix]: Fixed set_time_limit(0) limit.

= 1.4.85 =
* [New languages]: Spanish, French and German support added for Chat bot. More languages are coming..

= 1.4.84 =
* [New Feature]: You can now embed Image Generator (both DALL-E and Stable Diffusion 🚀🚀🚀) in your website with shortcodes, so that your visitors can generate images!

= 1.4.83 =
* [New Feature]: Stable Diffusion is here! You can now generate images using the powerful Stable Diffusion engine. Go to Image Generator and switch to Stable Diffusion tab!
* [Fix]: PHP session issue fixed. Thanks James for reporting this bug!

= 1.4.82 =
* Fixed an issue in Image Generator module.

= 1.4.81 =
* Fixed an issue with Bulk tracking.

= 1.4.80 =
* [New Feature]: Added a new feature for ChatGPT so that you can now set number of conversation to remember.
* [Improvement]: ChatGPT content awareness improved.

= 1.4.79 =
* [New Feature]: You can now create and design your own prompt and embed it into your website.
* [New Feature]: You can now delete single or batch jobs in bulk module.
* [New Feature]: In the event of a bulk job failure, the system will automatically restart it after a specified time interval, with an attempt up to a maximum of x times. You can configure the frequency of job restarts and the maximum number of attempts in the Settings page.
* [Fix]: Occasionally, OpenAI was not responding with an image for certain prompts, resulting in an WPAICG_IMAGE tag appearing within the post. This issue has now been resolved.

= 1.4.78 =
* More prompts have been added to PromptBase.

= 1.4.77 =
* More prompts have been added to PromptBase.

= 1.4.76 =
* Now you can embed hundreds of GPT forms in your website and let your users interact with them. Go to PromptBase and get your shortcode :)

= 1.4.75 =
* We continue to lead the AI revolution in the WordPress community with our latest offering, PromptBase. With hundreds of ready-to-use prompts at your disposal, you will soon have the ability to embed them directly into your website, adding a whole new level of cool to your online presence. How cool is that :)

= 1.4.74 =
* More improvements and enhancements.

= 1.4.73 =
* More improvements and enhancements.

= 1.4.72 =
* Bulk editor improvements.

= 1.4.71 =
* Chat bot is now content aware. Dont forget to turn "Content Aware" option on.

= 1.4.70 =
* You can now set "Remember conversation" option to yes if you want your chat bot to remember the conversation with users.

= 1.4.69 =
* Chat widget improvements.

= 1.4.68 (25.01.2023) =
* New writing styles: Anecdotal, Sensory and Vivid.
* You can now specify heading tag for conclusion.
* Fixed an issue on the API Status page.

= 1.4.67 (25.01.2023) =
* You can now specify heading tag for introduction.

= 1.4.66 (25.01.2023) =
* You can specify heading tag for ToC title in the settings.
* Estonian language added.

= 1.4.65 (24.01.2023) =
* More improvements and fixes.

= 1.4.64 (23.01.2023) =
* More improvements, please update to get the best out of it :)

= 1.4.63 (22.01.2023) =
* Danish and Norwegian language support added.

= 1.4.62 (21.01.2023) =
* Tone and Act as added for ChatBot. Now you can give a tone and proffesion for your chat bot :)

= 1.4.61 (21.01.2023) =
* Fixed max token issue.

= 1.4.60 (21.01.2023) =
* Fixed an issue with Arabic prompt.

= 1.4.59 =
* Slovenian and Latvian language support added.

= 1.4.58 =
* Lithuanian, Serbian and Slovak language support added.
* Bold keyword issue fixed.

= 1.4.57 =
* Finnish support added.

= 1.4.56 =
* Fixed an issue to remove empty headings.

= 1.4.55 =
* Fixed a bug in Fine-Tune module.

= 1.4.54 =
* New langauge support: Croatian!
* Fixed a bug in Fine-Tune module.

= 1.4.53 =
* Additional fields for bulk editor.
* Fixed a bug in Fine-Tune module.

= 1.4.52 =
* [Image Generator]: Now you can edit alt, caption, title and description for Dall-E images and add them to you Media Library.

= 1.4.51 =
* Malay, Thai and Filipino language support added.

= 1.4.50 =
* [New Feature]: Data Converter: Now you can convert your Wordpress database (posts, pages, products) into jsonl format and train your AI with one click.
* [New Feature]: Data Editor: Now you can enter your data into our Data Editor and prepare your jsonl file and train your AI with one click.

= 1.4.49 =
* [Enhancement]: Couple of more features added in Image Generator.

= 1.4.48 =
* [New Feature]: Now you can add ChatGPT widget on your website and customize its style and look.
* [New Feature]: You can add Table of Contents (ToC) in your content.
* [New Feature]: Tones of new settings added into the Image Generator. Give it a try!

= 1.4.47 =
* Image generator added.

= 1.4.46 =
* Image generator added.

= 1.4.45 =
* Image generator added.

= 1.4.44 =
* Image generator added.

= 1.4.43 =
* More improvements and fixes.

= 1.4.42 =
* New writing tones, new langauges..

= 1.4.41 =
* [New Feature]: Now you can train and fine tune your AI!

= 1.4.40 =
* More improvements!

= 1.4.39 =
* More improvements!

= 1.4.38 =
* Fixes and improvements.

= 1.4.37 =
* Fixes and improvements.

= 1.4.36 =
* Fixes and improvements.

= 1.4.35 =
* Fixes and improvements.

= 1.4.34 =
* [Improvement]: The duration of content generation has been improved. Hopefully this will solve Cloudflare users 100 seconds timeout limit problem.
* [Fix]: A small issue with Chinese language support has been fixed.
* [Fix]: An issue with the cron job has been resolved.

= 1.4.33 =
* [New feature]: Bulk Editor added. With the Bulk Editor tool, you can easily generate large amounts of content at once. There are three ways to use the Bulk Editor: Directly within the tool: Simply enter your desired titles and the Bulk Editor will generate content. Free plan users can generate up to 5 at a time.
via CSV: If you have a large amount of text stored in a CSV file, you can upload it to the Bulk Editor and generate content. Free plan users can generate up to 5 at a time. Via copy-paste: You can also use the Bulk Editor by simply copying and pasting your text into the tool, and then generate content. Free plan users can generate up to 5 at a time.
* [New feature]: You can use "Single Content Writer" to generate single content.
* [New feature]: You can see the token usage, estimated cost, duration and word count for each batch.

= 1.4.32 =
* [New feature]: Playground added. Now you can write your own prompts.

= 1.4.31 =
* Bug fixes.

= 1.4.30 =
* [New feature]: Now you can save your content settings and use them as default.
* [New feature]: Now you can use content generator in Pages.
* [New feature]: Added documentation page link for each feature. 
* [Bug fix]: Conflict with WooCommerce plugin fixed.
* [Bug fix]: Saving settings issue fixed.

= 1.4.29 =
* Bug fixes.

= 1.4.28 =
* New writing style: Simple
* New writing tone: Professional
* New feature: Add Call-to-Action to your content.
* Bug fixes.


= 1.4.27 =
* A highly requested feature has been added: the ability to edit headings before generating content. You can now edit, delete, sort, or add new headings before generating content.
* Bug fixes.

= 1.4.26 =
* Six new languages have been added: Romanian, Greek, Czech, Bulgarian, Swedish, and Hungarian.
* A bug where saving posts in Gutenberg was not functioning properly has been fixed.

= 1.4.25 =
* Fixed draft/publish issue.

= 1.4.24 =
* Save Draft button added.
* Log page added.
* Bug fix.

= 1.4.23 =
* Added option to add a hyperlink and anchor text to their content.
* Bug fix.

= 1.4.22 =
* Bug fix.

= 1.4.21 =
* Added option for users to add a tagline to their generated content.

= 1.4.20 =
* We are constantly updating the plugin to make it much better and produce better results!

= 1.4.19 =
* Ability to select heading tag from h1 to h6.

= 1.4.18 =
* Arabic langauge support added.

= 1.4.17 =
* Bug fix.

= 1.4.16 =
* Two new language added: Polish, Ukrainian.

= 1.4.15 =
* Bug fix.

= 1.4.14 =
* Pro plan added with additional writing styles, tones and random keywords.

= 1.4.13 =
* Hindi langauge support added.
* Bug fix.

= 1.4.12 =
* Fix prompt design for Chinese, Japanese, Korean and Russian.

= 1.4.11 =
* Bug fix.

= 1.4.10 =
* Chinese prompt design fixed.

= 1.4.9 =
* Turkish language support added.
* Bug fix.

= 1.4.8 =
* 1 new writing style added: Technical.
* 1 new writing tone added: Skeptical.
* Bug fix.

= 1.4.7 =
* 4 new writing styles added: Analytical, Critical, Evaluative, Journalistic.
* Bug fix.

= 1.4.6 =
* Writing tones added: Formal, Informal, Neutral, Inspirational, Sarcastic, Assertive, Cheerful, Humorous.

= 1.4.5 =
* Two additional writing styles added: Argumentative and Reflective.

= 1.4.4 =
* Writing styles added.

= 1.4.3 =
* Bug fix.

= 1.4.2 =
* Timer added.
* Bug fix.

= 1.4.1 =
* Intro, conclusion option added.

= 1.4 =
* Language option added.
 
= 1.0.0 =
* First release.