<?php
namespace WPAICG;
if ( ! defined( 'ABSPATH' ) ) exit;
use Orhanerday\OpenAi\OpenAi;

if (!class_exists('\\WPAICG\\WPAICG_OpenAI')){
    class WPAICG_OpenAI
    {
        private static  $instance = null ;

        public static function get_instance()
        {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;
        }

        public function openai()
        {
            global $wpdb;
            $wpaicgTable = $wpdb->prefix . 'wpaicg';
            $sql = sprintf( 'SELECT * FROM ' . $wpaicgTable . ' where name="wpaicg_settings"' );
            $wpaicg_settings = $wpdb->get_row( $sql, ARRAY_A );
            if($wpaicg_settings && isset($wpaicg_settings['api_key']) && !empty($wpaicg_settings['api_key'])){
                $open_ai = new OpenAi($wpaicg_settings['api_key']);
                unset($wpaicg_settings['ID']);
                unset($wpaicg_settings['name']);
                unset($wpaicg_settings['added_date']);
                unset($wpaicg_settings['modified_date']);
                foreach($wpaicg_settings as $key=>$wpaicg_setting){
                    $open_ai->$key = $wpaicg_setting;
                }
                return $open_ai;
            }
            else return false;
        }
    }
}
