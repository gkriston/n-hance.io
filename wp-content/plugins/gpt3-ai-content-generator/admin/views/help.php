<?php
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="wrap">
    <h1>Help</h1>

    <h2>Installation</h2>
    <ol>
        <li>Enter your OpenAI API key. You can get it from <a href="https://beta.openai.com/" target="_blank">OpenAI</a></li>
        <li>Setup temperature, max tokens, best of, frequenct penalty. You can get more information about these parameters from <a href="https://beta.openai.com/docs/api-reference/completions/create" target="_blank">OpenAI</a></li>
    </ol>

    <h2>How to use the plugin</h2>
    <ol>
        <li>Go to Single Content Writer.</li>
        <li>Enter title, for example: Mobile Phones.</li>
        <li>Enter number of headings, for example: 7 (This will generate 7 headings)</li>
        <li>Click generate.</li>
        <li>Click save draft.</li>
    </ol>

    <h2>Parameters</h2>
    <ol>
        <li><b>Temperature:</b> The higher the temperature, the more random-looking the text. Lower temperature results in more predictable text.</li>
        <li><b>Max tokens:</b> The maximum number of tokens to generate. By default, this is 64.</li>
        <li><b>Best of:</b> The number of different completions to request. By default, this is 1.</li>
        <li><b>Frequenct penalty:</b> The value of the frequency penalty. By default, this is 0.0.</li>
        <li><b>Top p:</b> The cumulative probability threshold to use when sampling from candidates. By default, this is 1.0.</li>
        <li><b>Presence penalty:</b> The value of the presence penalty. By default, this is 0.0.</li>
    </ol>

    <h2>Documentation</h2>
    <p>For more information about the plugin, please visit <a href="https://gptaipower.com/category/docs/" target="_blank">here</a></p>

    <h2>Contact</h2>
    <p> If you have any questions, please contact me at <a href="mailto:senols@gmail.com">senols@gmail.com</a> or join our Discord community <a href="https://discord.gg/EtkpBZYY6v" target="_blank">here</a></p>

    <h2>Known Issues for Cloudflare Users</h2>
    <p>If you are encountering situations where the plugin appears to be unresponsive, even after waiting for several minutes. In some cases, this may be due to network settings that are preventing the plugin from receiving a response from OpenAI.</p>
    <p>Cloudflare have default connection timeout for <b>100 seconds</b>. This means that if you are using Cloudflareâ€™s free plan and do not receive all responses from OpenAI within 100 seconds, Cloudflare will timeout and you will not see any content being generated. Enterprise customers can increase this timeout up to <b>6000 seconds</b> through the Cloudflare API or by contacting customer support.</p>
    <p>To fix this issue, you have two options:
    <ul>
        <li>1. Try disabling any Cloudflare or proxy settings that you may have enabled on your website. This will allow the plugin to communicate directly with OpenAI, which should resolve the issue and allow the plugin to generate content as expected.</li>
        <li>2. Upgrade your Cloudflare plan to increase your timeout duration.</li>
    </ul>
    <p>Please read <a href="https://gptaipower.com/why-is-my-content-generation-process-taking-too-long/ target=_blank">this article</a> for more information.</p>
</div>
