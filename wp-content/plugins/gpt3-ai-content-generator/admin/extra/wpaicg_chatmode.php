<?php
if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="wrap fs-section">
    <h2 class="nav-tab-wrapper">
        <a href="javascript:void(0)" class="nav-tab nav-tab-active">Chat Mode</a>
    </h2>
    <div id="poststuff">
        <div id="fs_account">
            <div class="wpaicg-alert mb-5">
                <p>To add the chat bot to your website, please include the shortcode <code>[wpaicg_chatgpt]</code> in the desired location on your site. This will allow your users to interact with the bot directly from the frontend of your website. Please be aware that using the bot will consume your OPENAI tokens. Make sure to monitor your token usage and refill as needed to ensure that the bot remains operational for your users.</p>
            <p>If you'd like to use ChatBox widget instead of shortcode, please go to <b>Settings - ChatGPT</b> tab and configure your widget.</p>
            <p>Learn how you can teach your content to the chat bot: <u><b><a href="https://youtu.be/NPMLGwFQYrY" target="_blank">https://youtu.be/NPMLGwFQYrY</a></u></b></p>
            </div>
            <?php
            echo do_shortcode('[wpaicg_chatgpt]');
            ?>
        </div>
    </div>
</div>
