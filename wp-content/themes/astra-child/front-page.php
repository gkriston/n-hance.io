<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header(); ?>

</div>
<?php
    $lang = get_locale();
    $is_hun = false;
?>


<?php if ($is_hun) : ?>
    <section id="hero" class="landing">
        <div class="ast-container">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-box">
                    <h1 class="hero-title">Feladatpriorizálási keretrendszer a garantáltan eredményesebb élethez</h1>
                    <p class="hero-description">Felejstd el a teendőlistákat, amelyek csak arra jók, hogy túlhajszoljanak. Találj rá, mi az, ami igazán fontos számodra és dolgozz azokon feladatokon, amelyek valóban közelebb visznek téged a céljaidhoz.</p>
                    <div class="cta-button-holder">
<!--                        <a href="--><?php //echo site_url() . (is_user_logged_in() ? '/app' : '/wp-admin'); ?><!--" class="cta-button">KEZDJÜNK BELE</a>-->
                        <?php echo do_shortcode('[contact-form-7 id="177" title="Bétateszt feliratkozás"]'); ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 img-box">
                    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/n-hance-desktop-view.jpg">
                </div>
            </div>
            <div class="row achievable">
                <div class="col-lg-6 col-md-6">
                    <ul>
                        <li>Priorizáld a céljaidat, hogy minél hamarabb szintet léphess az életedben</li>
                        <li>Rendelj a listáidhoz célokat, hogy sose téveszd őket szem elől</li>
                        <li>Rendezd a listáidat, egymás mellé nyitva őket</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6">
                    <ul>
                        <li>Válogasd ki a legfontosabb feladatokat, hatás szerinti súlyozás alapján</li>
                        <li>Tervezd meg mi fér bele a napodba, a feladatokhoz társított időigény szerint</li>
                        <li>Szinkronizáld feladataidat a Google Calendarba</li>
                    </ul>
                </div>
            </div>
        </div>
        <img class="bullseye" src="<?php echo get_stylesheet_directory_uri();?>/assets/images/bullseye.svg">
    </section>
<div class="ast-container">
    <?php else : ?>
    <section id="hero" class="landing">
        <div class="ast-container">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-box">
                    <h1 class="hero-title">Framework for your mind, unavoidable higher results</h1>
                    <p class="hero-description">Forget unprioritized, stressful to-do lists and try the app which launches your productivity to the stratosphere by giving you guidance on what to focus on and helps you get smarter through making new knowledge engraving a daily habit.</p>
                    <div class="cta-button-holder">
<!--                        <a href="--><?php //echo site_url() . (is_user_logged_in() ? '/app' : '/wp-admin'); ?><!--" class="cta-button">GET MORE PRODUCTIVE NOW</a>-->
                        <?php echo do_shortcode('[signup_form_logic]'); ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 img-box">
<!--                    <img src="--><?php //echo get_stylesheet_directory_uri();?><!--/assets/images/n-hance-desktop-view.jpg">-->
                    <?php echo do_shortcode("[game_of_life]"); ?>
                </div>
            </div>
            <div class="row achievable">
                <div class="col-lg-6 col-md-6">
                    <ul>
                        <li>Prioritizing goals based on their importance for your life</li>
                        <li>Per list based goal setting to keep you on track</li>
                        <li>Side-by-side list views for easier overview and goal setting</li>
                        <li>Level up with action plans from our knowledge marketplace</li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6">
                    <ul>
                        <li>Priorization view with scoring panel to see what really matters</li>
                        <li>Automatic time estimate summing for better time-management</li>
                        <li>Customizable looks to make your to-do list personal</li>
                        <li>Smart integrations for easy task importing</li>
                    </ul>
                </div>
            </div>
        </div>
        <img class="bullseye" src="<?php echo get_stylesheet_directory_uri();?>/assets/images/bullseye.svg">
    </section>
    <div class="ast-container">
        <?php endif; ?>

        <script>
            let sound = document.getElementById('complete-sound'),
                noOfObstacles = 16,
                yearsLeft,
                rotationStep = Math.floor(1 / 80 * 360),
                yearsRotationDegree = Math.round(360 * (80 - yearsLeft) / 80) - 270,
                goal = 45,
                obstacleGenerator,
                splashTimer,
                yearsTimer,
                obstacleList = document.querySelector('#circle-of-life-game__obstacles'),
                obstacles = obstacleList.querySelectorAll('li');

            //start(false);

            function start(hideInstructions = true) {
                document.querySelector('#circle-of-life-game__wrapper').classList.add('timer');

                splashTimer = setInterval(() => {
                    let timer = document.getElementById('intro-splash-timer'),
                        time = Number(timer.innerHTML);

                    if (!time) {
                        document.querySelector('#circle-of-life-game__wrapper').classList.remove('timer');
                        timer.innerHTML = 3;
                        clearInterval(splashTimer);

                        yearsLeft = 45;
                        document.querySelector('#time-left-wrapper h2').innerHTML = yearsLeft;
                        document.querySelector('#points .counter').innerHTML = 0;
                        document.getElementById('target-wrapper').innerHTML = '?';
                        if (hideInstructions) document.getElementById('instructions').style.display = 'none';
                        document.querySelector('#time-left-wrapper').style = `transform: rotate(${90}deg)`;
                        document.querySelector('#circle-of-life-game__wrapper').classList.remove('influence');

                        clearInterval(obstacleGenerator);
                        clearInterval(yearsTimer);
                        obstacles.forEach(moveObstacle);
                        obstacleGenerator = setInterval(addNewObstacle, 500);

                        yearsTimer = setInterval(() => {
                            let counter = document.querySelector('#time-left .counter'),
                                distance = Math.round(document.querySelector('#circle-of-life-game').offsetWidth / 2) - 40;

                            if (!counter) return;
                            current = Number(counter.innerHTML);

                            if (current == 0) {
                                document.querySelector('#instructions h1').innerHTML = '<h1>GAME OVER</h1>';
                                document.getElementById('instructions').style.display = 'flex';
                                document.querySelector('#time-left').style = `transform: rotate(-270deg) translateX(${distance}px)`;
                                clearInterval(obstacleGenerator);
                                clearInterval(yearsTimer);
                                document.querySelector('#circle-of-life-game__wrapper').classList.remove('influence');
                            } else {
                                counter.innerHTML = current - 1;
                                yearsLeft--;

                                yearsRotationDegree = Math.round(360 * (80 - yearsLeft) / 80) - 270;
                                document.querySelector('#time-left').style = `transform: rotate(${yearsRotationDegree}deg) translateX(${distance}px)`;
                            }
                        }, 1000);
                    } else {
                        timer.innerHTML = time - 1;
                    }
                }, 1000);
            }

            function calculateRotationDegree() {
                return Math.round(360 * Math.random());
            }

            function addNewObstacle() {
                let obstacleCount = obstacleList.querySelectorAll('li').length;

                if (!document.getElementById('circle-of-life-game')) return;

                if (obstacleCount < noOfObstacles) {
                    setTimeout(() => {
                        let rand,
                            point,
                            obstacleHTML,
                            counter = document.querySelector('#points .counter'),
                            distractionIcons = [
                                'facebook.svg',
                                'messenger.svg',
                                'mail.svg',
                                'youtube.svg',
                                'video.svg',
                                'linkedin.svg',
                                'twitter.svg',
                            ];

                        if (!counter) return;

                        if (Number(counter.innerHTML) < Math.floor(goal / 2)) {
                            rand = getRandomIntInclusive(1,2);

                            switch (rand) {
                                case 1:
                                    let randDist = getRandomIntInclusive(0,6);
                                    obstacleHTML = `<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/${distractionIcons[randDist]}">`;
                                    point = -1;
                                    break;
                                case 2:
                                    obstacleHTML = '<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/n-letter.svg">';
                                    point = 1;
                                    break;
                            }
                        } else {
                            let gameWrapper = document.querySelector('#circle-of-life-game__wrapper');
                            if (!gameWrapper.classList.contains('influence')) gameWrapper.classList.add('influence');
                            document.getElementById('target-wrapper').innerHTML = '<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/goal-white.svg">';
                            rand = getRandomIntInclusive(1,3);

                            switch (rand) {
                                case 1:
                                    obstacleHTML = '<span>+</span>';
                                    point = 2;
                                    break;
                                case 2:
                                    obstacleHTML = '<span>0</span>';
                                    point = 0;
                                    break;
                                case 3:
                                    obstacleHTML = '<span>-</span>';
                                    point = -1;
                                    break;
                            }
                        }



                        document.getElementById('circle-of-life-game__obstacles').insertAdjacentHTML('beforeend', `<li data-point="${point}">${obstacleHTML}</li>`);
                        let newObstacle = obstacleList.querySelector('li:last-child');
                        moveObstacle(newObstacle);
                    }, Math.round(Math.random() * 1000));
                }
            }

            function moveObstacle(el) {
                let rotationDegree = calculateRotationDegree();

                el.style =  `transform: rotate(${rotationDegree}deg) translateX(180px);`;
                el.setAttribute('data-distance', 180);

                el.addEventListener('mouseenter', function(e) {
                    let point = Number(e.currentTarget.dataset.point),
                        counter = document.querySelector('#points .counter'),
                        current = Number(counter.innerHTML);
                    counter.innerHTML = current + point;

                    e.currentTarget.remove();
                    //sound.play();

                    if (current + point + 1 > goal) {
                        document.querySelector('#instructions h1').innerHTML = '<h1>YOU WIN</h1>';
                        document.getElementById('instructions').style.display = 'flex';
                        clearInterval(obstacleGenerator);
                        clearInterval(yearsTimer);
                        document.querySelector('#circle-of-life-game__wrapper').classList.remove('influence');
                    }
                });

                setInterval(() => {
                    let distance = el.dataset.distance;

                    if (distance < 0) {
                        el.remove();
                    } else {
                        //distance += getRandomIntInclusive(1, 3);
                        el.style =  `transform: rotate(${rotationDegree}deg) translateX(${--distance}px);`;
                        el.dataset.distance = distance;
                    }
                }, 16);
            }

            function getRandomIntInclusive(min, max) {
                min = Math.ceil(min);
                max = Math.floor(max);
                return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
            }
        </script>
        <?php get_footer(); ?>
