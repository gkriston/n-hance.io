<div id="circle-of-life-game__wrapper">
    <div id="circle-of-life-game">
        <div id="instructions">
            <h1>
                <span class="uc distractions"><?php echo __('Avoid distractions', 'astra-child'); ?></span><br>
                <span class="uc bg-orange calling"><?php echo __('Find your true calling with', 'astra-child'); ?></span><br>
                <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/n-hance-io.png"><br><?php echo __('Complete tasks that matter', 'astra-child'); ?>
                    <span class="plus-sign">+</span><br>
                <span class="font-normal complete-tasks"><?php echo __('to achieve it in time', 'astra-child'); ?></span>
            </h1>
            <div id="intro-splash-timer">5</div>
            <button onclick="start()"><?php echo __('Start', 'astra-child'); ?></button>
        </div>
        <div id="time-left">
            <div id="time-left-wrapper">
                <h2 class="counter mb-0">45</h2><?php echo __('years', 'astra-child'); ?>
                <div class="arrow-left"></div>
            </div>
        </div>
        <div id="game">
            <ul id="circle-of-life-game__obstacles">
            </ul>
            <div id="circle-of-life-game__inner-space"></div>
            <div id="circle-of-life-game__target">
                <div id="target-wrapper">
                    ?
                </div>
                <div id="points">
                    <div class="points-wrapper">
                        <div class="total">45/</div><div class="counter mb-0">0</div>
                    </div>
                    <div><?php echo __('points', 'astra-child'); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let sound = document.getElementById('complete-sound'),
        noOfObstacles = 16,
        yearsLeft,
        rotationStep = Math.floor(1 / 80 * 360),
        yearsRotationDegree = Math.round(360 * (80 - yearsLeft) / 80) - 270,
        goal = 45,
        obstacleGenerator,
        splashTimer,
        yearsTimer,
        obstacleList = document.querySelector('#circle-of-life-game__obstacles'),
        obstacles = obstacleList.querySelectorAll('li');

    //start(false);

    function start(hideInstructions = true) {
        document.querySelector('#circle-of-life-game__wrapper').classList.add('timer');

        splashTimer = setInterval(() => {
            let timer = document.getElementById('intro-splash-timer'),
                time = Number(timer.innerHTML);

            if (!time) {
                document.querySelector('#circle-of-life-game__wrapper').classList.remove('timer');
                timer.innerHTML = 3;
                clearInterval(splashTimer);

                yearsLeft = 45;
                document.querySelector('#time-left-wrapper h2').innerHTML = yearsLeft;
                document.querySelector('#points .counter').innerHTML = 0;
                document.getElementById('target-wrapper').innerHTML = '?';
                if (hideInstructions) document.getElementById('instructions').style.display = 'none';
                document.querySelector('#time-left-wrapper').style = `transform: rotate(${90}deg)`;
                document.querySelector('#circle-of-life-game__wrapper').classList.remove('influence');

                clearInterval(obstacleGenerator);
                clearInterval(yearsTimer);
                obstacles.forEach(moveObstacle);
                obstacleGenerator = setInterval(addNewObstacle, 500);

                yearsTimer = setInterval(() => {
                    let counter = document.querySelector('#time-left .counter'),
                        distance = Math.round(document.querySelector('#circle-of-life-game').offsetWidth / 2) - 40;

                    if (!counter) return;
                    current = Number(counter.innerHTML);

                    if (current == 0) {
                        document.querySelector('#instructions h1').innerHTML = '<h1><?php echo __('GAME OVER', 'astra-child'); ?></h1>';
                        document.getElementById('instructions').style.display = 'flex';
                        document.querySelector('#time-left').style = `transform: rotate(-270deg) translateX(${distance}px)`;
                        clearInterval(obstacleGenerator);
                        clearInterval(yearsTimer);
                        document.querySelector('#circle-of-life-game__wrapper').classList.remove('influence');
                    } else {
                        counter.innerHTML = current - 1;
                        yearsLeft--;

                        yearsRotationDegree = Math.round(360 * (80 - yearsLeft) / 80) - 270;
                        document.querySelector('#time-left').style = `transform: rotate(${yearsRotationDegree}deg) translateX(${distance}px)`;
                    }
                }, 1000);
            } else {
                timer.innerHTML = time - 1;
            }
        }, 1000);
    }

    function calculateRotationDegree() {
        return Math.round(360 * Math.random());
    }

    function addNewObstacle() {
        let obstacleCount = obstacleList.querySelectorAll('li').length;

        if (!document.getElementById('circle-of-life-game')) return;

        if (obstacleCount < noOfObstacles) {
            setTimeout(() => {
                let rand,
                    point,
                    obstacleHTML,
                    counter = document.querySelector('#points .counter'),
                    distractionIcons = [
                        'facebook.svg',
                        'messenger.svg',
                        'mail.svg',
                        'youtube.svg',
                        'video.svg',
                        'linkedin.svg',
                        'twitter.svg',
                    ];

                if (!counter) return;

                if (Number(counter.innerHTML) < Math.floor(goal / 2)) {
                    rand = getRandomIntInclusive(1,2);

                    switch (rand) {
                        case 1:
                            let randDist = getRandomIntInclusive(0,6);
                            obstacleHTML = `<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/${distractionIcons[randDist]}">`;
                            point = -1;
                            break;
                        case 2:
                            obstacleHTML = '<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/n-letter.svg">';
                            point = 1;
                            break;
                    }
                } else {
                    let gameWrapper = document.querySelector('#circle-of-life-game__wrapper');
                    if (!gameWrapper.classList.contains('influence')) gameWrapper.classList.add('influence');
                    document.getElementById('target-wrapper').innerHTML = '<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/svg-icons/goal-white.svg">';
                    rand = getRandomIntInclusive(1,3);

                    switch (rand) {
                        case 1:
                            obstacleHTML = '<span>+</span>';
                            point = 2;
                            break;
                        case 2:
                            obstacleHTML = '<span>0</span>';
                            point = 0;
                            break;
                        case 3:
                            obstacleHTML = '<span>-</span>';
                            point = -1;
                            break;
                    }
                }



                document.getElementById('circle-of-life-game__obstacles').insertAdjacentHTML('beforeend', `<li data-point="${point}">${obstacleHTML}</li>`);
                let newObstacle = obstacleList.querySelector('li:last-child');
                moveObstacle(newObstacle);
            }, Math.round(Math.random() * 1000));
        }
    }

    function moveObstacle(el) {
        let rotationDegree = calculateRotationDegree();

        el.style =  `transform: rotate(${rotationDegree}deg) translateX(180px);`;
        el.setAttribute('data-distance', 180);

        el.addEventListener('mouseenter', function(e) {
            let point = Number(e.currentTarget.dataset.point),
                counter = document.querySelector('#points .counter'),
                current = Number(counter.innerHTML);
            counter.innerHTML = current + point;

            e.currentTarget.remove();
            //sound.play();

            if (current + point + 1 > goal) {
                document.querySelector('#instructions h1').innerHTML = '<h1><?php echo __('YOU WIN', 'astra-child'); ?></h1>';
                document.getElementById('instructions').style.display = 'flex';
                clearInterval(obstacleGenerator);
                clearInterval(yearsTimer);
                document.querySelector('#circle-of-life-game__wrapper').classList.remove('influence');
            }
        });

        setInterval(() => {
            let distance = el.dataset.distance;

            if (distance < 0) {
                el.remove();
            } else {
                //distance += getRandomIntInclusive(1, 3);
                el.style =  `transform: rotate(${rotationDegree}deg) translateX(${--distance}px);`;
                el.dataset.distance = distance;
            }
        }, 16);
    }

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
    }
</script>