<?php
wp_enqueue_style( 'landing-styles', get_stylesheet_directory_uri() . '/assets/css/landing-styles.css',false,'1.0','all');

$GLOBALS['app_ver'] = '0.7.9.2';

function enqueue_app_scripts() {
    if (is_page_template('app.php')) {
        wp_register_script( 'app-js', trailingslashit( get_stylesheet_directory_uri() ) . 'assets/js/app.js' , [], $GLOBALS['app_ver'], true );
        wp_localize_script(
            'app-js',
            'appVariables',
            array(
                'templateUrl'        => get_stylesheet_directory_uri(),
                'userName'          => wp_get_current_user()->user_nicename,
                'currentUser'       => get_current_user_id(),
                'userOptions'       => get_user_meta(get_current_user_id(), 'nhance_options')[0],
                'ajax_url'         => admin_url( 'admin-ajax.php' ),
                'userLoginData'     => array(
                                        'user_email'    => wp_get_current_user()->user_email,
                                        'user_pass'     => hash('sha256', wp_get_current_user()->user_pass)
                                        )
            )
        );
        wp_enqueue_script( 'app-js' );
    }
}
add_action('wp_enqueue_scripts', 'enqueue_app_scripts');

register_meta('user', 'nhance_options', array(
    "type" => "object",
    "single" => true,
    "show_in_rest" => array(
        'schema' => array(
            'type'=> 'object',
            'properties' => array(
                'color_scheme' => array(
                    'type' => 'string'
                ),
                'workday_length' => array(
                    'type' => 'string'
                ),
                'sound_effect' => array(
                    'type' => 'string'
                ),
                'prio' => array(
                    'type' => 'string'
                ),
                'beta_features' => array(
                    'type' => 'string'
                ),
                'wheel_data' => array(
                    'type' => 'array'
                ),
                'language' => array(
                    'type' => 'string'
                ),
                'migrated' => array(
                    'type' => 'string'
                ),
                'first_run' => array(
                    'type' => 'string'
                ),
                'last_review_date' => array(
                    'type' => 'string'
                ),
                'mission_statement' => array(
                    'type' => 'string'
                ),
                'x_ray_mode' => array(
                    'type' => 'string'
                ),
            ),
            'additionalProperties' => true
        )
    ),
));

register_meta('user', 'nhance_backup', array(
    "type" => "object",
    "single" => true,
    "show_in_rest" => array(
        'schema' => array(
            'type'=> 'object',
            'additionalProperties' => true
        )
    ),
));

wp_register_script( 'user-js', get_theme_file_uri( './assets/js/update-user-meta-ajax.js' ), [ 'jquery' ], '', true );
wp_localize_script(
    'user-js',
    'wpApiSettings',
    array(
        'root'        => esc_url_raw( rest_url() ), // Rest api root
        'nonce'       => wp_create_nonce( 'wp_rest' ), // For auth
        'currentUser' => wp_get_current_user()->ID
    )
);
wp_enqueue_script( 'user-js' );


add_filter( 'login_redirect', 'ckc_login_redirect' );
function ckc_login_redirect() {
    // Change this to the url to Updates page.
    return home_url( '/app' );
}

add_filter( 'wp_nav_menu_items', 'themeprefix_login_logout_link', 10, 2 );

function themeprefix_login_logout_link( $items, $args  ) {
    if( $args->theme_location == 'primary' ) {
        $loginoutlink = wp_loginout( 'index.php', false );
        $items .= '<li class="menu-item">'. $loginoutlink .'</li>';
        return $items;
    }
    return $items;
}


// Ajax actions

function getlastbrowserid_callback() {
    global $wpdb;

    $user = $_POST['user'];

    $target_table = $user == 'creator' ? 'moduledata' : 'userdata';
    $sql = "SELECT randbrowserid FROM " . $target_table . " WHERE name = '" . $user . "'";

    $result = $wpdb->get_var($sql);

    echo $result;

    wp_die();
}
add_action( 'wp_ajax_getlastbrowserid', 'getlastbrowserid_callback' );
add_action( 'wp_ajax_nopriv_getlastbrowserid', 'getlastbrowserid_callback' );


function loadlistdatafromdb_callback() {
    global $wpdb;

    $user = $_POST['user'];
    $lists_requested = $_POST['lists'];

    $target_table = $user == 'creator' ? 'moduledata' : 'userdata';
    $sql = "SELECT id FROM " . $target_table . " WHERE name = '" . $user . "'";

    $user_id = $wpdb->get_var($sql);

    if ($user_id && $lists_requested == 'all') {
        $lists = $wpdb->get_results("SELECT * FROM nh_listdata WHERE user_id='" . $user_id . "'");
        $listsData = (object)[];

        foreach ($lists as $list) {
            $listsData->{$list->list_name} = json_decode($list->list_data);
        }

        echo json_encode($listsData);
    } else if ($user_id && $lists_requested != 'all') {
        $lists = $wpdb->get_results("SELECT * FROM nh_listdata WHERE user_id='" . $user_id . "'");
        $listsData = (object)[];

        foreach ($lists as $list) {
            if (in_array($list->list_name, $lists_requested)) {
                $listsData->{$list->list_name} = json_decode($list->list_data);
            }
        }

        echo json_encode($listsData);
    } else {
        $wpdb->insert(
            'userdata',
            array(
                'name' => $user
            ),
            array(
                '%s'
            )
        );
    }

    wp_die();
}
add_action( 'wp_ajax_loadlistdatafromdb', 'loadlistdatafromdb_callback' );
add_action( 'wp_ajax_nopriv_loadlistdatafromdb', 'loadlistdatafromdb_callback' );


function savelistdatatodb_callback() {
    global $wpdb;

    $user = $_POST['user'];
    $lists = json_decode(str_replace('\"', '"', $_POST['lists']));
    $remove = $_POST['remove'] === 'true' ? true : false;
    $target_table = $user == 'creator' ? 'moduledata' : 'userdata';

    // Check for new user and create it if doesn't exist
    $sql = "SELECT id FROM " . $target_table . " WHERE name = '" . $user . "'";
    $user_id = $wpdb->get_var($sql);

    if (!$user_id) {
        $wpdb->insert(
            'userdata',
            array(
                'name' => $user
            ),
            array(
                '%s'
            )
        );
    }


    $sql = "SELECT * FROM " . $target_table . " WHERE name = '" . $user . "'";
    $result = $wpdb->get_row($sql);

    if ($result->name) {
        $user_id = intval($result->id);

        foreach ($lists as $listname => $listdata) {
            echo $listname . ' ';
            $existing_list = $wpdb->get_row("SELECT * FROM nh_listdata WHERE user_id='" . $user_id . "' AND list_name='" . $listname . "'");

            if ($remove && $existing_list) {
                $wpdb->delete('nh_listdata', array('id' => $existing_list->id), array('%d'));
            } else if ($existing_list->list_name) {
                $wpdb->update(
                    'nh_listdata',
                    array(
                        'list_data' => json_encode($listdata)
                    ),
                    array('id' => $existing_list->id),
                    array('%s'),
                    array('%d')
                );
            } else {
                $wpdb->insert(
                    'nh_listdata',
                    array(
                        'list_name' => $listname,
                        'list_data' => json_encode($listdata),
                        'user_id' => $user_id
                    ),
                    array(
                        '%s',
                        '%s',
                        '%d'
                    )
                );
            }
        }
    }

    wp_die();
}
add_action( 'wp_ajax_savelistdatatodb', 'savelistdatatodb_callback' );
add_action( 'wp_ajax_savelistdatatodb', 'savelistdatatodb_callback' );

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );


function game_of_life_shortcode() {
    ob_start();
    get_template_part( 'game_of_life' );
    return ob_get_clean();
}
add_shortcode( 'game_of_life', 'game_of_life_shortcode' );

if ( ! current_user_can( 'manage_options' ) ) {
    add_filter( 'show_admin_bar', '__return_false' );
}

/**
 * Enable ACF 5 early access
 * Requires at least version ACF 4.4.12 to work
 */
define('ACF_EARLY_ACCESS', '5');


/**
 * Loads the child theme textdomain.
 */
function wpdocs_child_theme_setup() {
    load_child_theme_textdomain( 'astra', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'wpdocs_child_theme_setup' );

function my_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'my_login_logo_url' );


function enqueuing_login_scripts(){
    wp_enqueue_style('poppins', 'https://fonts.googleapis.com/css?family=Poppins&display=swap');

}

add_action( 'login_enqueue_scripts', 'enqueuing_login_scripts');


// Shortcode to output custom PHP in Elementor
function nh_elementor_shortcode( $atts ) {
    function get_new_accounts_from_today() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM wp_users");
    }
    function get_waitlist_length() {
        global $wpdb;
        return count($wpdb->get_results("SELECT * FROM wp_db7_forms WHERE form_post_id = '177'"));
    }

    $signup = shortcode_atts( array(
        'limit' => 50
    ), $atts );

    $entries = get_new_accounts_from_today();
    $signups_today = 0;

    foreach ($entries as $entry) {
        $start_of_today = strtotime('today');
        $start_of_tomorrow = strtotime('tomorrow', $start_of_today) - 1;
        $signup_time = strtotime($entry->user_registered);

        if ($signup_time > $start_of_today && $signup_time < $start_of_tomorrow) {
            $signups_today++;
        }

    }

    if (true) : ?>
<!--    if ($signups_today > $signup['limit']) : ?>-->
        <div class="cta-button-holder">
<!--            <form-widget ucid='utZhj4BTDIDnmbwSCndgSHrA7CE'></form-widget>-->
            <?php echo do_shortcode('[contact-form-7 id="177" title="Closed beta signup"]'); ?>
        </div>
        <div id="waitlist-count-notification" style="display: none">
            <p class="waitlist-length">There are <span class="count"><?php echo get_waitlist_length(); ?></span> people on the waitlist in front of you.</p>
            <p>We will notify you as soon as new spots open up in the beta program.</p>
            <p>Thank you for your understanding!</p>
        </div>

    <?php else : ?>
        <!-- Begin Mailchimp Signup Form -->
        <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
        <style type="text/css">
            /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
               We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
        </style>
        <div id="mc_embed_signup">
            <form action="https://n-hance.us6.list-manage.com/subscribe/post?u=0f3b99003a145987c33223f3d&amp;id=83f7fb6207" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    <h2 style="display:none">REGISTER FREE ACCOUNT</h2>
                    <div class="indicates-required" style="display:none"><span class="asterisk">*</span> indicates required</div>
                    <div class="mc-field-group">
                        <label for="mce-EMAIL"  style="display:none">Email Address  <span class="asterisk">*</span>
                        </label>
                        <input placeholder="Enter email address to get started..." type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                    </div>
                    <div class="mc-field-group input-group" style="display:none">
                        <strong>Beta test intensive (HU) </strong>
                        <ul><li><input type="checkbox" value="1" name="group[22518][1]" id="mce-group[22518]-22518-0"><label for="mce-group[22518]-22518-0">Beta test intensive (HU)</label></li>
                        </ul>
                    </div>
                    <div class="mc-field-group input-group" style="display:none">
                        <strong>Beta test (EN) </strong>
                        <ul><li><input type="checkbox" value="2" name="group[22522][2]" id="mce-group[22522]-22522-0" checked><label for="mce-group[22522]-22522-0">Beta test (EN)</label></li>
                        </ul>
                    </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none">Congrats! You're in!</div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0f3b99003a145987c33223f3d_83f7fb6207" tabindex="-1" value=""></div>
                    <div class="clear"><input type="submit" value="REGISTER FREE ACCOUNT" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
        </div>
        <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';fnames[5]='BIRTHDAY';ftypes[5]='birthday';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
        <!--End mc_embed_signup-->

        <form style="display: none" action="<?php echo get_site_url(); ?>/register" method="POST" id="register-redirect">
            <input type="hidden" id="mc-reg-email" name="mc-reg-email" value=""/>
        </form>
        <script>
            var mcSignUpForm = document.getElementById('mc-embedded-subscribe-form');

            mcSignUpForm.addEventListener('submit', (e) => {
                e.preventDefault();
                document.getElementById('mc-reg-email').value = document.getElementById('mce-EMAIL').value;
            });
            // select the target node
            var target = document.getElementById('mce-success-response');

            // create an observer instance
            var observer = new MutationObserver(function(mutations) {
                mutations.forEach(function(mutation) {
                    if (target.innerHTML === "Thank you for subscribing!") {
                        target.innerHTML = "Congrats! You're in!";

                        setTimeout(() => {
                            document.getElementById('register-redirect').submit();
                        }, 1000);
                    }
                });
            });

            // configuration of the observer:
            var config = { attributes: true, childList: true, characterData: true };

            // pass in the target node, as well as the observer options
            observer.observe(target, config);

            let wpcf7Elm = document.querySelector('.wpcf7');

            if (wpcf7Elm) {
                wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
                    document.getElementById('waitlist-count-notification').style.display = 'block';
                }, false );
            }
        </script>
    <?php endif;
}
add_shortcode( 'signup_form_logic', 'nh_elementor_shortcode');