const fetchApi = function(route, data, method) {
    if (!route) route = 'posts'
    if (!method) method = 'GET'

    return fetch(wpApiSettings.root + 'wp/v2/' + route, {
        method,
        credentials: 'same-origin',
        body: JSON.stringify(data),
        headers: {
            'X-WP-Nonce': wpApiSettings.nonce,
            'Content-Type': 'application/json'
        }
    }).catch(function(error) {
        console.log(error);
    });
};

function changeStyle(e) {
    let date = new Date();
    const uniqueStyles = ['dx-style', 'light-style', 'mastermind'];
    const userStyle = e.currentTarget.value;

    uniqueStyles.forEach(function(style) {
       if (style != 'dx-style') document.body.classList.remove(style);
    });

    document.body.classList.add(userStyle);
    userOptions.color_scheme = userStyle;
    setSiteStyle(userStyle);

    updateUserOptions();
}

function updateWorkdayLength(e) {
    userOptions.workday_length = e.currentTarget.value;
    updateUserOptions();
}

function toggleBetaFeatures(e) {
    userOptions.beta_features = e.currentTarget.value;
    document.body.classList.toggle('beta-features-off')
    saveUserOptions(userOptions);
}

function toggleXRayMode(e) {
    userOptions.x_ray_mode = e.currentTarget.value;
    if (userOptions.x_ray_mode == 'on') {
        document.body.classList.add('x-ray');
    } else {
        document.body.classList.remove('x-ray');
    }
    updateUserOptions();
}

function toggleImpactPopup(e) {
    userOptions.impact_popup = e.currentTarget.value;
    updateUserOptions();
}

function updateWheelRange(range) {
    userOptions.wheel_range = range;
    updateUserOptions();
}

function updateProgressPointGoal(e) {
    userOptions.baseline_progress_points = e.currentTarget.value;
    updateUserOptions();
}

function toggleDailyPlanningReminder(e) {
    userOptions.daily_planning_reminder = e.currentTarget.value;
    updateUserOptions();
}

function toggleSoundEffect(e) {
    userOptions.sound_effect = e.currentTarget.value;
    updateUserOptions();
}

function togglePrioViewOnStartup(e) {
    userOptions.prio = e.currentTarget.value;
    updateUserOptions();
}

function toggleLanguage(e) {
    userOptions.language = e.currentTarget.value;
    updateUserOptions()
    .then(function() {
       window.location.reload();
    });
}

function updateUserOptions() {
    updateWPUserOptions();
    return saveUserOptions(userOptions);
}

function updateWPUserBackup() {
    return fetchApi(
        `users/${appVariables.currentUser}`,
        {
            meta: {
                "nhance_backup": myLists
            }
        },
        'PUT'
    ).then(resp => {
        if (resp.ok) {
            return resp.json()
        }
        return false
    });
}

function updateWPUserOptions() {
    return fetchApi(
        `users/${appVariables.currentUser}`,
        {
            meta: {
                "nhance_options": userOptions
            }
        },
        'PUT'
    ).then(resp => {
        if (resp.ok) {
            return resp.json();
        }
        return false
    });
}
