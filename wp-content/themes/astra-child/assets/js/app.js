// Initialize unique list item ID counter
/**
 * Declare app variables
 */
let syncWatcher,
	timeTracker,
	commonLabels,
	learningModules,
	taskToBeMovedData,
	vueNoteApp,
	notesCalendar,
	compassHTML,
	firestoreWriteOrigin,
	dayviewCalendar,
	dayviewCalUpdateNeeded,
	quill,
	quillTargetElementId,
	quillTargetElementSelector,
	dbLists,
	isHun = false,
	userId = wpApiSettings.currentUser,
	dbLearningModules = {},
	elDataTransfer;

/**
 * - InitializeFirebase()
 * - setInitAppValues()
 * - loginUserAndLoadData()
 * <pre>
 * packed together in order to achieve the current order
 * </pre>
 * @returns {Promise<void>}
 */
async function appInitialization() {
	initializeFirebase();
	await autoLoginToFirebase();
	await setInitAppValues();
	setSiteStyle(userOptions.color_scheme);
	console.log(userOptions);
	// setLanguage(userOptions.language);
	document.body.classList.remove("hu");
	loginUserAndLoadData();
	dailyReviewReminder();
}

appInitialization();

/**
 * Make sure to load the corresponding useroptions and save it to firebase from
 * - firebase
 * - appVariables
 * - default
 * @returns {Promise<{x_ray_mode: string, daily_planning_reminder: string, list_categories: *[], beta_features: string, color_scheme: string, sound_effect: string, language: string, prio: string, impact_popup: string, workday_length: string, wheel_data: number[]}>} userOptions
 */
async function getOptimalUserOptions(){
	let defaultUserOptions = {
			color_scheme: 'dx-style',
			daily_planning_reminder: 'off',
			workday_length: '6',
			sound_effect: 'off',
			wheel_data: [3,3,3,3,3],
			baseline_progress_points: 1000,
			x_ray_mode: 'off',
			language: 'en',
			beta_features: 'off',
			prio: 'on',
			impact_popup: 'off',
			list_categories: []
		},
		firebaseUserOptions = await getUserOptions(),
		result;

	if (firebaseUserOptions){
		console.log("user options loaded from firestore");
		result = firebaseUserOptions
	} else if (appVariables.userOptions) {
		saveUserOptions(appVariables.userOptions)
		console.log("user options loaded from sql");
		result = appVariables.userOptions;
	} else {
		saveUserOptions(defaultUserOptions);
		console.log("default user options saved");
		result = defaultUserOptions;
	}
	// result['color_scheme'] = 'dx-style';
	return result;
}

/**
 * Load and set app values.
 * @Important Only execute after firebase login occured
 * @returns {Promise<void>}
 */
async function setInitAppValues(){
	debugMode = false;
	dateToday = moment().format('YYYY-MM-DD');
	taskDataSavingInProgress = false;
	listItemIdCounter = 1;
	myLists = {};
	clonedMyLists = {};
	compassData = {};
	trackedTime = 0;
	myPrinciples = {
		'Uncategorized': []
	};
	events = [];
	tasks = {};
	userOptions = await getOptimalUserOptions();
	setSiteStyle(userOptions.color_scheme);
	sitePath = window.location.pathname;
	appPath = sitePath.substring(0, sitePath.indexOf('app'));
	userName = appVariables.userName;
	// Needed for multi-device usage to avoid overwriting user data when logged in on multiple devices at the same time
	randomBrowserId = getCookie('randbrowserid');
	notes = {};
	notesFromDb = [];
	// Cached DOM elements should be here
	wordCloud = document.getElementById('word-cloud');
	dayviewCalData = [];
	defaultLists = {
		'Inbox': {
			'goal': '',
			'parentcategory': 'Capture',
			'simpleList': true,
			'order': 1,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		},
		'Ideas': {
			'goal': '',
			'parentcategory': 'Capture',
			'simpleList': false,
			'order': 2,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		},
		'Reflect': {
			'goal': '',
			'parentcategory': 'Capture',
			'simpleList': true,
			'order': 3,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		},
		'Today': {
			'goal': '',
			'parentcategory': 'Scheduled',
			'simpleList': false,
			'order': 4,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		},
		'Tomorrow': {
			'goal': '',
			'parentcategory': 'Scheduled',
			'simpleList': false,
			'order': 5,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		},
		'Later This Week': {
			'goal': '',
			'parentcategory': 'Scheduled',
			'simpleList': false,
			'order': 6,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		},
		'Next Week': {
			'goal': '',
			'parentcategory': 'Scheduled',
			'simpleList': false,
			'order': 7,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		},
		'Someday': {
			'goal': '',
			'parentcategory': 'Scheduled',
			'simpleList': false,
			'order': 11,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		}
	};
	quill;
	quillTargetElementId;
	quillTargetElementSelector;
	isThisTheFirstRun = false;

	if (userOptions.beta_features == 'on') document.body.classList.remove('beta-features-off')
	if (userOptions.x_ray_mode == 'off') document.body.classList.remove('x-ray')

	let rangeInput = document.querySelector('form[name="myform"] input[name="range1"]');
	rangeInput.value = userOptions.wheel_range || 3;

	document.querySelector('form[name="myform"] output[name="range1value"]').value = (rangeInput.valueAsNumber == 1 ? (isHun ? '1 év' : '1 year') : (rangeInput.valueAsNumber == 2 ?  (isHun ? '5 év' : '5 year') : (isHun ? 'Teljes élet' : 'Whole life')));
}

/**
 * Set the style of the site
 * @param userStyle
 */
function setSiteStyle(userStyle) {
	// const uniqueStyles = ['dx-style', 'wunderlist', 'mastermind', 'light-style'];
	// uniqueStyles.forEach(function(style) {
	// 	if (userStyle != 'light-style') document.body.classList.remove(style);
	// });
	let logoImage = document.querySelector('#nav-wrapper .logo');

	if (userStyle != 'dx-style') document.body.classList.add(userStyle);
	if (userStyle == 'mastermind') {
		logoImage.setAttribute('src', appVariables.templateUrl + '/assets/images/bpr-manager-logo.png');
	} else {
		logoImage.setAttribute('src', appVariables.templateUrl + '/assets/images/svg-icons/n-letter-orange.svg');
	}
}

// If there is no user cookie set than this is a new login or a new user
/**
 * login user and load data.
 * @Important Only execute after firebase useroptions loaded
 */
function loginUserAndLoadData() {
	if (userName) {

		let date = new Date();
		document.cookie = 'username=' + userName + '; expires=' + new Date(new Date().setMonth(date.getMonth() + 1)) + '; path=' + appPath;

		// We try to load the data based on the username
		let dbLoadPromise;

		if (!userOptions.listsMigratedToFirestore) {
			dbLoadPromise = new Promise((resolve) => {
				resolve((async () => {
					let sqlData = await loadlistdatafromdb(),
						data = isJson(sqlData) ? JSON.parse(sqlData): {},
						objKeys = Object.keys(data);

					userOptions.listsMigratedToFirestore = true;
					updateUserOptions();

					// Check if sql data matches firestore data
					if (objKeys.length > 0) {
						objKeys.forEach(list => {
							setDocumentData(`myLists/${list}`, data[list]);
						});

						return data;
					}

					return {};
				})());
			});
		} else {
			dbLoadPromise = getCollectionData('myLists');
		}

		let dataInsertPromise = dbLoadPromise.then(function (listsData) {
			let data;

			if (isJson(listsData)) {
				data = JSON.parse(listsData);
			} else if (typeof listsData == 'object') {
				data = listsData;
			}
			// If there is a match we load the stored lists
			if (data && typeof data === 'object' && Object.keys(listsData).length > 0) {

				// Empty arrays and objects are not passed to be stored in database through jQuery.post so we have to re-add them
				for (list in data) {
					let defaultCategories = ['Enhance', 'Must-Do', 'Optional'];

					if (!data[list].categories) {
						data[list].categories = {};
					}

					defaultCategories.forEach(function (cat) {
						if (!data[list].categories[cat]) {
							data[list].categories[cat] = [];
						}
					});
				}

				if (userName != 'creator') {
					myLists = data;
					insertMissingDefaultListsAndIndex();
					myLists = sortObjectByOrderIndex(myLists);
				} else {
					myLists = data;
				}

				// Check if old Monthly Tasks, Next Month and This Year lists exist and move their task to the Long Term list
				// let obsoleteLists = ['Monthly Tasks', 'Next Month', 'This Year'];
				//
				// obsoleteLists.forEach(listName => {
				// 	if (myLists[listName]) {
				// 		for (category in myLists[listName].categories) {
				// 			myLists[listName].categories[category].forEach(task => {
				// 				task.list = "Someday";
				// 				task.listId = "someday"
				// 				myLists['Someday'].categories[category].push(task)
				// 			})
				// 		}
				//
				// 		delete myLists[listName]
				// 		saveDataToDB([listName], true)
				// 	}
				// })
				// Move tasks to renamed lists
				// let renamedLists = [{
				// 		oldName: 'Daily Tasks',
				// 		newName: 'Today'
				// 	}, {
				// 		oldName: 'Weekly Tasks',
				// 		newName: 'This Week'
				// 	}, {
				// 		oldName: 'This Week',
				// 		newName: 'Later This Week'
				// 	},{
				// 		oldName: 'Long Term',
				// 		newName: 'Someday'
				// 	}];

				// renamedLists.forEach(renamedList => {
				// 	if (myLists[renamedList.oldName]) {
				// 		for (category in myLists[renamedList.oldName].categories) {
				// 			myLists[renamedList.oldName].categories[category].forEach(task => {
				// 				if (task.list == renamedList.oldName) {
				// 					task.list = renamedList.newName;
				// 					task.listId = toId(renamedList.newName)
				//
				// 					if (!myLists[renamedList.newName]) {
				// 						myLists[renamedList.newName] = myLists[renamedList.oldName]
				// 						myLists[renamedList.newName].categories = defaultLists[renamedList.newName].categories
				// 					}
				// 					myLists[renamedList.newName].categories[category].push(task)
				// 				}
				// 			})
				// 		}
				//
				// 		delete myLists[renamedList.oldName]
				// 		saveDataToDB([renamedList.oldName], true)
				// 	}
				// })

				initLists();
				document.querySelectorAll('.list-container').forEach(list => {
					Sortable.create(list, {
						group: 'shared',
						animation: 150,
						// setData: drag,
						onStart: drag,
						onEnd: drop,
						touchStartThreshold: 5,
						delayOnTouchOnly: 100
					});
				});
				if (userOptions.timeTracking && userOptions.timeTracking.trackingNow) continueTimeTracking();
				loadDayviewData();
				dailyBackup();
				loadNotes();
				loadCompassData();

				if (window.location.href.indexOf('action=import_list') > -1) {
					console.log('List import action');
					parseListSharingURL();
				}

				// Temporary switch for old users to turn on impact popup feature
				if (!userOptions.impact_popup) {
					userOptions.impact_popup = 'on';
				}

				// For old users we want to make sure that this option is stored, so default list rewrites can only happen upon confirmation
				if (!userOptions.initial_launch_completed) {
					userOptions.initial_launch_completed = true;
					updateUserOptions();
				}
			} else {
				// If there is no match we set up the default lists
				if (userName != 'creator') {
					if (userOptions.initial_launch_completed) {
						if (confirm('The default lists will be rewriten. Do you want to proceed?')) {
							myLists = defaultLists;
						}
					} else {
						myLists = defaultLists;
					}
					initLists();

					// Set the daily review date so the reminder will be active from the day after the current date
					if (userOptions.first_run == 'done') userOptions.last_review_date = moment().format('YYYY-MM-DD');
					if (!userOptions.initial_launch_completed) userOptions.initial_launch_completed = true;
					updateUserOptions();
					saveDataToDB();
				}

				loadNotesToCalendar();
				loadCompassData();
			}

			return true;
		});

		// The old data loading function was fetch based and the new one is with jQuery so the error handling differs
		dataInsertPromise.catch(onDataInsertionFailed);


	function onDataInsertionFailed() {
		let loadOverlay = document.getElementById('load-overlay');

		if (loadOverlay.classList.contains('hide')) {
			loadOverlay.classList.remove('hide');
			document.getElementById('db-save-error').classList.remove('hide');
		}

			// If there is no match we set up the default lists
			// if (userName != 'creator') {
			// 	myLists = defaultLists;
			//
			// 	saveDataToDB().then(function() {
			// 		setTimeout(function() {
			// 			saveCurrentBrowserId().then(initLists);
			// 		}, 1000);
			// 	});
			// }
			//
			// loadNotesToCalendar();
			console.error('error loading data');
		}

		// syncWatcher = setInterval(function () {
		// 	reloadOnSyncNeeded();
		// }, 15000);
	} else {
		console.log('There is no logged in user');
	}
}


function initLists(lists = myLists) {
	document.getElementById('load-overlay').classList.add('hide');
	document.getElementById('load-in-progress').classList.add('hide');

	listCategoryModule.init();
	// Set up default lists
	for (list in lists) {
		// In case of old DB structure we remap categories
		if (!lists[list].categories) {
			let categoriesOld = myLists[list];
			lists[list] = {
				'goal' : '',
				'categories': categoriesOld
			};
		}

		if (!lists[list].parentcategory) lists[list].parentcategory = 'Capture';

		let categories = lists[list].categories;
		var listNameAsId = toId(list);

		addListToDOM(listNameAsId, list, lists[list], lists[list].goal);
		listItemIdCounter = getNextAvailableId();
		initListCategories(categories, listNameAsId);
	}
	// Auto assign tasks to lists based on due date on startup (note: can cause bugs)
	//attachListsDataChangeListener();
	attachDefaultHandlers();
	updateUserOptions();
}

function initListCategories(categories, listNameAsId, sync) {
	for (category in categories) {
		if (categories[category].length > 0) {
			categories[category].forEach(function(el, i) {
				if (el) {
					var targetList = document.getElementById(listNameAsId).querySelector('.' + el.category);

					// Reset list id's (if necessary)
					// el.id = listItemIdCounter;

					initListElement(el);
				}

				// Fixing database by removing null items (will be saved automatically on any user interaction)
				if (el === null) {
					categories[category].splice(i, 1);
				}

			});
		}
	}

	if (!sync) {
		updateUserOptions();
	} else {
		attachDefaultHandlers(document.getElementById(listNameAsId));
	}
}

function createNewList(listName = '', listObject, isSimple = false, sync) {
	// Prompt for new username
	var list = listName != '' ? listName : prompt(isHun ? 'Kérlek add meg a lista nevét' : 'Please enter list name:', '').trim(),
		defaultListObject;

	// If no name given do nothing
	if (!list) return;

	// If list name already exists
	if (myLists[list]) {
		alert(isHun ? 'Már létezik ilyen nevű lista' : 'A list with the same name already exists');
		return;
	}

	// isSimple = confirm(isHun ? 'Egyszerű listát szeretnél létrehozni?' : 'Is this a simple list?');
	isSimple = false;

	// Default list subacategories
	if (userName != 'creator') {
		defaultListObject = {
			'goal' : '',
			'parentcategory': 'Uncategorized',
			'simpleList' : isSimple,
			'categories': {
				'Enhance': [],
				'Must-Do': [],
				'Optional': []
			}
		};
	} else {
		defaultListObject = {
			'goal' : '',
			'parentcategory': 'Uncategorized',
			'simpleList' : isSimple,
			'categories': {
				'Enhance': []
			}
		}
	}


	var listNameAsId = toId(list);

	myLists[list] = listObject ? listObject : defaultListObject;
	addListToDOM(listNameAsId, list, listObject ? listObject : defaultListObject);
	if (!sync) saveDataToDB([list]);

	if (userName == 'creator') {
		syncLearningModules();
	}

	document.querySelectorAll('#' + listNameAsId + ' .goal').forEach(function(el) {
		el.addEventListener('click', function(e) {
			addGoalHandler(e, list);
		});
	});

	document.querySelectorAll('#' + listNameAsId + ' .set-goal').forEach(function(el) {
		let goalWrapper = document.querySelector('#' + listNameAsId + ' .goal');

		if (!myLists[list].isGoalVisible) goalWrapper.classList.toggle('hide');

		if (goalWrapper.classList.contains('hide')) {
			myLists[list].isGoalVisible = false;
		} else {
			myLists[list].isGoalVisible = true;
		}

		saveDataToDB([list]);
	});

	attachDefaultHandlers(document.getElementById(listNameAsId));
}

// Initialize list element
function initListElement(data, sync) {
	// Create new list element
	let taskId = listItemIdCounter,
		listItem = document.createElement('li'),
		timeDataAvailable = Boolean(Number(data.time)),
		time,
		isSimpleListElement = data.targetList ? myLists[data.targetList.closest('.task-list-wrapper').dataset.list].simpleList : false;

	if (!timeDataAvailable && !isSimpleListElement && data.isFromUserInput) {
		time = 15;
	} else {
		time = isSimpleListElement ? 15 : Number(data.time);
	}

	// listItem.setAttribute('data-time', time);
	// listItem.style.height = Math.floor(listItem.dataset.time / 15 * 58) + 'px';


	// Use default list element template and set task name
	listItem.innerHTML = `
		<span class="done">
			<svg class="svg-icon" viewBox="0 0 140 140"><path d="M70,0.5c-38.6,0-70,31.4-70,70s31.4,70,70,70s70-31.4,70-70S108.6,0.5,70,0.5z M70,126.5c-30.9,0-56-25.1-56-56
		s25.1-56,56-56s56,25.1,56,56S100.9,126.5,70,126.5z"/><path class="checked-mark" d="M101.6,39.6L55.5,85.7L37.4,67.6l-9.9,9.9l28,28l56-56L101.6,39.6z"/></svg>
		</span>
		<div class="task-name">
			<div class="task-name__full">
				<span class="task-name__full__desc">${data.name}</span><span class="edit-task-settings">
					<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/edit-task-settings.svg"></span><div class="task-interactions-wrapper">
					<span class="sub-action-switch" onclick="showTaskSubActions(event)" href="#"><img class="svg-icon " src="${appVariables.templateUrl}/assets/images/svg-icons/more.svg"></span><div class="task-interactions">
						<span class="track-time" onclick="trackTime(event)">
							<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/time.svg">
						</span>
						<span class="identify-principle">
							<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/idea-2-dark.svg">
						</span>
						<span class="note">
							<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/note-icon.svg">
						</span>
						<span class="day">
							<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/calendar.svg">
						</span>
						<span class="pro-con">
							<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/plus-and-minus.svg">
						</span>
						<span class="del">
							<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon.svg">
						</span>
						<span class="search-notes-by-name" onclick="triggerNoteSearch(event)" data-task-name="${data.name}">
							<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/search-icon-thin.svg">
						</span>
					</div>
				</div>
			</div>
			<div class="task-date">
				<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/calendar.svg">
				<span>${data.day ? data.day : ''}</span>
			</div>
			<div class="task-note">
				<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/note-icon.svg">
			</div>
			<div class="task-principle">
				<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/idea-2-dark.svg">
			</div>
			<div class="task-tracked-time">
				<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/time.svg">
				<span>${data.trackedTime ? convertMinutesToTimeString(data.trackedTime) : ''}</span>
			</div>
			${listAppliedLabels(data)}
		</div>
<!--		<input type="number" min="0" max="10" class="effect" value="${data.effect ? data.effect : 1}">-->
<!--		<input type="number" min="0" max="10" class="ease" value="${data.ease ? data.ease : 0}">-->
<!--		<input type="number" class="prio" value="${data.prio ? data.prio : 0}" disabled>-->
		<span class="time">${convertMinutesToTimeString(time)}</span>`;

	listItem.setAttribute('data-id', (data.id ? data.id :listItemIdCounter));
	// Apply a uniuque ID to list element
	listItem.setAttribute('id', "draggable-list-item-" + (data.id ? data.id :listItemIdCounter));

	// Make list element draggable
	listItem.setAttribute('draggable', true);
	// Set list element class name(s)
	listItem.className = 'draggable i-' + (data.effect ? data.effect : 1);

	if (data.labels && data.labels.length > 0) data.labels.forEach(label => listItem.classList.add(toId(label)))


	// If target DOM element is specified then append the list item there
	if (data.targetList) {
		if (data.isFromUserInput) {
			data.targetList.insertAdjacentElement('afterbegin', listItem);
		} else {
			data.targetList.appendChild(listItem);
		}

		recalcListTimeSum(data.targetList);
	} else {
		// If there is no exact target specified than the element is loaded from the database so we can construct target from data
		var targetList = document.querySelector('#' + data.listId + ' .' + data.categoryClass)
		targetList.appendChild(listItem);
		recalcListTimeSum(targetList);
	}

	attachListElementEventHandlers(listItem);

	// if ID is not provided as argument than this is a newly created list item and it needs to be saved to the database
	if (!data.id) {
		let parentList = listItem.closest('.task-list-wrapper'),
			subList = listItem.closest('.list-container')

		var dataObj = {
			id: listItemIdCounter,
			list: parentList.dataset.list,
			listId: parentList.getAttribute('id'),
			category: data.category || subList.dataset.category,
			origin: parentList.dataset.list,
			categoryClass: data.categoryClass || subList.dataset.category.toLowerCase(),
			name: data.name,
			effect: data.effect || 1,
			ease: 0,
			time: time,
			creationDate: moment().format('YYYY-MM-DD'),
			prio: 0,
			sampleTask: data.sampleTask || '',
			note: data.note || '',
			day: data.day ? data.day : ''
		};

		// Update working memory
		myLists[dataObj.list].categories[dataObj.category].unshift(dataObj);

		if (data.day) {
			document.getElementById('draggable-list-item-' + listItemIdCounter).classList.add('scheduled');
		}

		if (data.note) {
			document.getElementById('draggable-list-item-' + listItemIdCounter).classList.add('has-note');
		}

		if (data.principle) {
			document.getElementById('draggable-list-item-' + listItemIdCounter).classList.add('has-principle');
		}

		if (data.procon) {
			document.getElementById('draggable-list-item-' + listItemIdCounter).classList.add('has-procon');
		}

		// Update database
		if (data.isFromUserInput && !sync) {
			saveDataToDB([dataObj.list]);
			listItem.querySelector('.edit-task-settings').click();

			showTaskAddMoveNotification(dataObj.list);
		}

		if (data.moduleInjection)  updateListItemCounter([dataObj.list]);

		// Init label menu
		// if (!listItem.closest('.task-list-wrapper').classList.contains('simple-list') && data.isFromUserInput) {
		// 	initLabelMenus();
		//
		// 	setTimeout(function() {
		// 		document.getElementById('label-menu-overlay').classList.remove('hidden');
		// 		document.getElementById('menu-toggler').classList.add('active');
		// 		document.getElementById('menu-toggler').dataset.targetId =  listItemIdCounter - 1;
		// 	},100);
		//
		// 	//document.body.classList.add('show-prio');
		// }

		// Increase unique ID counter
		listItemIdCounter = getNextAvailableId();
	} else {
		if (data.time == 0) {
			myLists[data.list].categories[data.category].forEach(function (el, i) {
				if (el.id == data.id) {
					el.time = time;
				}

			});
		}

		if (!data.effect || data.effect == 0) data.effect = 1;

		if (!data.creationDate) {
			myLists[data.list].categories[data.category].forEach(function (el, i) {
				if (el.id == data.id) {
					el.creationDate = moment().format('YYYY-MM-DD');
				}
			});
		}

		if (data.day) {
			document.getElementById('draggable-list-item-' + data.id).classList.add('scheduled');
		}

		if (data.note) {
			document.getElementById('draggable-list-item-' + data.id).classList.add('has-note');
		}

		if (data.principle) {
			document.getElementById('draggable-list-item-' + data.id).classList.add('has-principle');
		}

		if (data.procon) {
			document.getElementById('draggable-list-item-' + data.id).classList.add('has-procon');
		}

		if (data.trackedTime) {
			document.getElementById('draggable-list-item-' + data.id).classList.add('time-recorded');
		}

		if (data.creationDate && moment().diff(data.creationDate, 'days') > 30) listItem.classList.add('old');

		if (data.moveCount && data.moveCount > 5) listItem.classList.add('tossed');
	}

	return taskId;
}

function attachAddTaskInputFieldSubmitListener(el) {
	el.addEventListener('keyup', function(e) {
		// If the last hit key is "Enter"
		if (e.keyCode == 13) {
			// If the newly creatable element has a name
			if (e.target.value) {
				// Find the "Optional" named list in the list wrapper and set as target
				var targetList = e.target.closest('.task-list-wrapper').querySelector('.' + (userName != 'creator' ? 'optional' : 'enhance'));

				// Append new list element to the target list and attach event handlers
				initListElement({name: e.target.value, targetList: targetList, isFromUserInput: true});
				// Clear input value
				e.target.value = "";
			}
		}
	});
}

function attachAddTaskCTAButtonListener(el) {
	el.addEventListener('click', function(e) {
		let addTaskWrapper = e.currentTarget.closest('.add-task-wrapper'),
			// addTaskInput = {value: prompt('Enter task name...')};
			addTaskInput = {value: 'New task'};

		// If the newly creatable element has a name
		if (addTaskInput.value) {
			// Find the "Optional" named list in the list wrapper and set as target
			var targetList = e.target.closest('.task-list-wrapper').querySelector('.' + (userName != 'creator' ? 'optional' : 'enhance'));

			// Append new list element to the target list and attach event handlers
			initListElement({name: addTaskInput.value, targetList: targetList, isFromUserInput: true});
			// Clear input value
			addTaskInput.value = "";
		}
	});
}

function toggleInbox() {
	document.body.classList.toggle('inbox-visible')
	document.getElementById('inbox').classList.toggle('hide');
	toggleListViews()
}

// If left sidebar list name is clicked show the clicked list in the list detail view pane
function attachSidebarLinkClickHandler(el) {
	el.addEventListener('click', function(e) {
		if (window.innerWidth < 769) document.body.classList.remove('lists-visible');

		if (window.innerWidth < 992) {
			swapShownList(e.target.dataset.listid);
		} else {
			// if (e.target.dataset.list == 'Inbox') {
			// 	toggleInbox()
			// } else {
				if (e.ctrlKey || e.metaKey) {
					document.getElementById(e.target.dataset.listid).classList.toggle('hide');
					document.body.classList.add('multi-list-view');
				} else {
					swapShownList(e.target.dataset.listid);
				}
			// }
		}
	});

	el.querySelector('.del').addEventListener('click', function(e) {
		e.stopPropagation();

		if (confirm(isHun ? 'Tényleg szeretnéd törölni ezt a listát?' : 'Do you really want to delete this list?')) deleteList(e.currentTarget.closest('li').querySelector('.list-name').textContent.trim());
	});

	el.querySelector('.category').addEventListener('click', function(e) {
		e.stopPropagation();
		generateWordCloud('list-categories', e.currentTarget.parentNode.dataset.listid);
	});
}

function deleteList(listName, sync) {
	let listId = toId(listName),
		list = document.getElementById(listId);

	if (!myLists[listName]) return;

	let	sidebarParentCategory = document.querySelector(`#my-lists ul[data-parentcategory="${toId(myLists[listName].parentcategory)}"]`),
		sideBarListItem = document.querySelector(`#my-lists li[data-listid="${listId}"]`)

	listCategoryModule.deleteListFromCategory(myLists[listName].parentcategory, listName);

	myLists[listName] = undefined;
	delete myLists[listName];
	if (!sync) saveDataToDB([listName], true);

	if (userName == 'creator') {
		syncLearningModules();
	}

	list.parentNode.removeChild(list);

	if (sidebarParentCategory && sidebarParentCategory.childNodes.length == 1) {
		sidebarParentCategory.closest('li').remove();
		sidebarParentCategory.remove();
	} else {
		sideBarListItem.remove();
	}
}

function updateListCategoriesView(listName, categories, sync) {
	let listNameAsId = toId(listName),
		list = document.getElementById(listNameAsId);

	list.querySelectorAll('.draggable').forEach(listEl => {
		listEl.remove();
	});
	list.querySelectorAll('.subcategory-wrapper > h3 .time').forEach(timeSum => {
		timeSum.innerHTML = '';
	});

	initListCategories(categories, listNameAsId, sync);
}

function updateListCategory(listId, newCategory) {
	let list = document.getElementById(listId),
		listName = list.dataset.list,
		oldCategory = myLists[listName].parentcategory;

	myLists[listName].parentcategory = newCategory ? newCategory : myLists[listName].parentcategory;

	listCategoryModule.addListToCategory(newCategory, listName);

	if (oldCategory != newCategory) {
		if (newCategory && !document.querySelector('#left-sidebar [data-parentcategory="' + toId(newCategory) + '"]')) {
			document.querySelector('#left-sidebar #my-lists').insertAdjacentHTML('beforeend', '<li><b class="list-parent-category-name">' + newCategory + '</b><ul data-parentcategory="' + toId(newCategory) + '"></ul></li>');
			openListsOnCategoryNameClick(document.querySelector('[data-parentcategory="' + toId(newCategory) + '"]'));
		}

		document.querySelector('#left-sidebar [data-parentcategory="' + toId(newCategory) + '"]').appendChild(document.querySelector(`#my-lists [data-listid=${listId}]`));

		if (oldCategory) {
			let oldCategoryList = document.querySelector('#left-sidebar [data-parentcategory="' + toId(oldCategory) + '"]');

			listCategoryModule.deleteListFromCategory(oldCategory, listName)

			if (oldCategoryList.childNodes.length == 0) {
				oldCategoryList.previousElementSibling.remove();
				oldCategoryList.remove();
			}
		}

		saveDataToDB([listName]);
		listCategoryModule.saveListCategoriesToDB();
	}
}

function attachDefaultHandlers(parent = document) {
	parent.querySelectorAll('.add-task').forEach(function(el) {
		attachAddTaskInputFieldSubmitListener(el);
	});

	parent.querySelectorAll('.add-task-cta-btn').forEach(function(el) {
		attachAddTaskCTAButtonListener(el);
	});

	//
	if (parent == document) {
		document.querySelectorAll('#left-sidebar [data-listid]').forEach(function(el) {
			attachSidebarLinkClickHandler(el);
		});
	} else {
		document.querySelectorAll('#left-sidebar [data-listid="' + parent.getAttribute('id') + '"]').forEach(function(el) {
			attachSidebarLinkClickHandler(el);
		});
	}


	if (parent == document) {
		document.querySelectorAll('#available-lists-to-drop [data-listid]').forEach(function(el) {
			el.addEventListener('click', function(e) {
				move(taskToBeMovedData,'#' + e.target.dataset.listid + ' .' + taskToBeMovedData.category.toLowerCase());

				document.querySelector('#available-lists-to-drop').classList.toggle('hide');
			});

		});
	} else {
		document.querySelectorAll('#available-lists-to-drop [data-listid="' + parent.getAttribute('id') + '"]').forEach(function(el) {
			el.addEventListener('click', function(e) {
				move(taskToBeMovedData,'#' + e.target.dataset.listid + ' .' + taskToBeMovedData.category.toLowerCase());

				document.querySelector('#available-lists-to-drop').classList.toggle('hide');
			});

		});
	}


	// If the prio button is clicked in any of the list panes make the priorization function visible on all lists
	parent.querySelectorAll('.prio').forEach(function(el) {
		el.addEventListener('click', function(e) {
			e.stopPropagation();
			document.body.classList.toggle('show-prio');
		});
	});

	// If the plan button is clicked open the text editor
	parent.querySelectorAll('.plan').forEach(function(el) {
		el.addEventListener('click', openEditorOnListPlanIconClick);
	});

	// If the mark button is clicked mark the list with a star
	parent.querySelectorAll('.star').forEach(function(el) {
		el.addEventListener('click', function(e) {
			e.stopPropagation();

			let listName = e.currentTarget.closest('.task-list-wrapper').dataset.list,
				currentList = myLists[listName],
				sideBarListEl = document.querySelector(`#my-lists [data-listid="${toId(listName)}"]`);

			if (!currentList.starred) {
				currentList.starred = true;
			} else {
				currentList.starred = false;
			}
			sideBarListEl.classList.toggle('starred');

			saveDataToDB([listName]);
		});
	});

	// If the share link button is clicked generate sharing link for list
	parent.querySelectorAll('.share').forEach(function(el) {
		el.addEventListener('click', function(e) {
			e.stopPropagation();
			const listName = e.currentTarget.closest('.task-list-wrapper').dataset.list;

			if (confirm(isHun? 'Szeretnél egy linket generálni a lista megosztásához?' : 'Do you want to generate a link to share this list?' )) buildListSharingURL(listName);
		});
	});

	//If the goal button is clicked ask for goal
	parent.querySelectorAll('.set-goal').forEach(function(el) {
		el.addEventListener('click', function(e) {
			let goalWrapper = e.currentTarget.closest('.task-list-wrapper').querySelector('.goal'),
				list = goalWrapper.closest('.task-list-wrapper').dataset.list;

			goalWrapper.classList.toggle('hide');

			if (goalWrapper.classList.contains('hide')) {
				myLists[list].isGoalVisible = false;
			} else {
				myLists[list].isGoalVisible = true;
			}

			saveDataToDB([list]);
		});
	});

	parent.querySelectorAll('.goal').forEach(function(el) {
		el.addEventListener('click', function(e) {
			addGoalHandler(e, e.target.parentNode.dataset.list);
		});
	});

	// If the sort button is clicked in any of the list panes make sort the child lists' elements by their priorization value
	parent.querySelectorAll('.sort').forEach(function(el) {
		el.addEventListener('click', function(e) {
			// Find the parent list wrappers's child lists and loop through them
			var parentList = event.currentTarget.parentNode.parentNode;

			parentList.querySelectorAll('ul').forEach(function(list) {
				// Get all list elements
				var listElements = list.querySelectorAll('li');
				// Sort them by their priorization value
				var listElementArray = [].slice.call(listElements).sort(function (a, b) {
					// Get both list elements' priorization value and compare them against each other
					var inputValA = Number(a.querySelector('.prio').value);
					var inputValB = Number(b.querySelector('.prio').value);
					return inputValA > inputValB ? -1 : 1;
				});

				myLists[parentList.dataset.list].categories[list.dataset.category].sort(function (a, b) {
					// Get both list elements' priorization value and compare them against each other
					return Number(a.prio) > Number(b.prio) ? -1 : 1;
				});

				saveDataToDB([parentList.dataset.list]);

				// Append the reordered list elements to the list
				listElementArray.forEach(function (listElement) {
					list.appendChild(listElement);
				});
			});
		});
	});

	// Attach task note editor handler
	parent.querySelectorAll('.note').forEach(function(el) {
		el.addEventListener('click', openEditorOnNoteIconClick);
	});
	parent.querySelectorAll('.task-note').forEach(function(el) {
		el.addEventListener('click', openEditorOnNoteIconClick);
	});

	// Forecast list task impact on a chart
	parent.querySelectorAll('.forecast').forEach(function(el) {
		el.addEventListener('click', showListImpactForecastChart);
	});

	if (parent == document) {
		document.querySelector('#editor-wrapper .close').addEventListener('click', closeNoteEditor);

		// Add list conversion handler
		document.querySelectorAll('.task-list-wrapper h2 .convert-list').forEach(function(listName) {
			listName.addEventListener('click', function(e) {
				let listWrapper = e.currentTarget.closest('.task-list-wrapper'),
					listData = myLists[listWrapper.dataset.list];

				if (!!listData && listData.parentcategory == "Scheduled") return;

				if (listData.simpleList) {
					if (confirm(isHun ? 'Szeretnéd a listád időalapúvá konvertálni?' : 'Do you want to convert this list to a time-based list?')) {
						listWrapper.classList.remove('simple-list');
						listData.simpleList = false;
						saveDataToDB([listWrapper.dataset.list]);
					}
				} else {
					if (confirm(isHun ? 'Szeretnéd a listád egyszerű listává konvertálni?' : 'Do you want to convert this list to a simple list?')) {
						listWrapper.classList.add('simple-list');
						listData.simpleList = true;
						saveDataToDB([listWrapper.dataset.list]);
					}
				}
			});
		})
	}
}

function closeAllModals() {
	if (!document.getElementById('editor-wrapper').classList.contains('hide')) document.querySelector('#editor-wrapper .close').click();

	if (!document.getElementById('word-cloud').classList.contains('hide')) hideWordCloud();

	if (!document.getElementById('help-menu').classList.contains('hide')) document.getElementById('help-icon').click();

	if (!document.getElementById('modal').classList.contains('hide')) clearModal();

	if (!document.getElementById('task-settings-modal').classList.contains('hide')) document.querySelector('#save-task-settings').click();
	document.getElementById('modal-wrapper').classList.remove('visible');

	document.querySelectorAll('.draggable').forEach(el => el.classList.remove('task-actions-shown'));
}

function closeNoteEditor(e) {
	saveNote(e);

	if (e && e.currentTarget.classList.contains('close')) {
		let editorWrapper = document.getElementById('editor-wrapper');

		editorWrapper.classList.add('hide');
		document.getElementById('editor-modal-wrapper').classList.remove('visible');
		if (editorWrapper.dataset) {
			if (editorWrapper.dataset.id) editorWrapper.dataset.id = '';

			if (editorWrapper.dataset.day) {
				// rerenderCalendar(notesCalendar, notesFromDb);
				editorWrapper.removeAttribute('data-day');
				editorWrapper.removeAttribute('data-index');
			}
		}
		quillTargetElementId = undefined;
	}

	document.querySelectorAll('.editor-supportive-tools').forEach(function(tool) {
		tool.classList.add('hidden');
	});

	if (timer !== 'undefined') {
		clearInterval(timer);
	}
}

function attachListElementEventHandlers(listItem) {
	// attachDragListeners(listItem);
	//
	// if (window.innerWidth < 992) {
	// 	listItem.addEventListener('dragend', drop);
	// }

	// Attach click handlers to all list elements "Done" and "Delete" buttons
	listItem.querySelectorAll('.done, .del').forEach(function(el) {
		el.addEventListener('click', function(e) {
			// Remove the list element from the list if any of the "Done" or "Delete" buttons are clicked
			let listItem = e.currentTarget.closest('.draggable'),
				listItemId = listItem.dataset.id,
				taskData = getTaskDataById(listItemId),
				list = listItem.closest('.list-container'),
				listName = listItem.closest('.task-list-wrapper').dataset.list,
				sound = document.getElementById('complete-sound');

			if (e.currentTarget.classList.contains('del') && !taskData.sampleTask) {
				if (!confirm(isHun ? 'Tényleg szeretnéd törölni ezt az elemet?' : 'Do you really want to delete this task?')) {
					return;
				} else if (userOptions.timeTracking && userOptions.timeTracking.trackedTaskId == listItemId) {
					stopCurrentTimeTracking(true);
				}
			} else if (listItem.querySelector('.track-time[data-tracking="on"]')) {
				stopCurrentTimeTracking(true);
			} else if (!taskData.trackedTime && taskData.time && !myLists[taskData.list].simpleList && !e.currentTarget.classList.contains('del')) {
				if (userOptions.impact_popup == 'on') initAnalytics(e, true);
				// if tracked time is not available we need to log the original task time
				logTaskTime(listItemId, taskData.time);
			}

			if (userOptions.sound_effect != 'off') {
				sound.play();
			}

			if (!document.getElementById('task-settings-modal').classList.contains('hidden')) {
				document.getElementById('task-settings-modal').classList.add('hidden');
			}

			list.removeChild(listItem);

			if (taskData.note && confirm(isHun ? 'Szeretnéd a jegyzetek közé felvenni a feladat jegyzetét?' : 'Do you want add the task note to your journal?')) {
				noteApp.addNewNote(taskData.name, taskData.note);
			}

			// Update working memory object data
			myLists[listName].categories[list.dataset.category].forEach(function(el, i) {

				if (listItem.dataset.id == el.id)	{
					// Remove element from array
					myLists[listName].categories[list.dataset.category].splice(i, 1);
					removeCalendarEvent(el.id);

					// Update database
					saveDataToDB([listName]);
				}
			});

			recalcListTimeSum(list);
		});
	});

	// Recalculate priorization score if any of the "Effect" or "Ease" values of the list item changes
	listItem.querySelectorAll('.effect, .ease').forEach(function(el) {
		el.addEventListener('change', function(e) {
			var effectValue = Number(e.target.parentNode.querySelector('.effect').value);
			var easeValue = Number(e.target.parentNode.querySelector('.ease').value);
			var prioScore = e.target.parentNode.querySelector('.prio');
			prioScore.value = effectValue + easeValue;

			var parentCategory = e.target.parentNode.parentNode;
			var parentList = parentCategory.parentNode;

			// Update working memory object data
			myLists[parentList.dataset.list].categories[parentCategory.dataset.category].forEach(function(el) {
				if (e.currentTarget.parentNode.dataset.id == el.id)	{
					el.effect = effectValue;
					el.ease = easeValue;
					el.prio = prioScore.value;

					// Update database
					saveDataToDB([parentList.dataset.list]);
				}
			});
		});
	});

	// Sum-up total time need in child list when any list element's time need changes
	listItem.querySelector('.time').addEventListener('click', showTaskSettingsModal);
	listItem.querySelector('.task-name').addEventListener('click', showTaskSettingsModal)

	listItem.querySelector('.note').addEventListener('click', openEditorOnNoteIconClick);
	if (listItem.querySelector('.task-note')) listItem.querySelector('.task-note').addEventListener('click', openEditorOnNoteIconClick);

	flatpickr(listItem.querySelector('.day'), {
		onOpen: function(selectedDates, dateStr, instance) {
			instance.clear();
		},
		onClose: assignDayToTask,
		disableMobile: true
	});

	// listItem.querySelector('.set-label').addEventListener('click', function (e) {
	// 	initLabelMenus();
	//
	// 	setTimeout(function() {
	// 		document.getElementById('label-menu-overlay').classList.remove('hidden');
	// 		document.getElementById('label-menu-overlay').classList.remove('hide');
	// 		document.getElementById('menu-toggler').classList.add('active');
	// 		document.getElementById('menu-toggler').dataset.targetId =  listItem.dataset.id;
	// 	},100);
	//
	// 	//document.body.classList.add('show-prio');
	// });

	listItem.querySelector('.identify-principle').addEventListener('click', function (e) {
		e.stopPropagation();
		let taskData = getTaskDataById(listItem.dataset.id),
			principlesString = createPrinciplesString(getRelevantPrinciples(taskData));


		let principle = taskData.principle ? taskData.principle : '',
			name = prompt(isHun ?
				'Mi a vezérelv a tevékenység kapcsán?\n\n' +
				'Vedd figyelembe a következőket:\n' +
				'- Mi a feladat lényege/jelentése?\n' +
				'- Milyen általános igazságok jellemzőek a tevékenységre?\n' +
				'- Milyen ismétlődő minták fedezhetőek fel a cselekvés kapcsán?\n' +
				'- Mi a tevékenységben résztvevő felek szándéka?\n' +
				'- Hogy lenne minden fél számára ideális az eredmény?\n' +
				'- Milyen kritériumoknak kell megfelelni?\n' +
				'- Hogyan cselekednél kiterített lapokkal, a nyilvánosság előtt az adott helyzetben?\n' +
				'- Milyen erőforrások állnak rendelkezésre?\n' +
				(principlesString ? '\nKorábban ezeket az elveket használtad a hasonló feladatoknál:\n' + principlesString : '') :
				'What is the guiding principle regards this task?\n\n' +
				'Please consider the following:\n' +
				'- What is the true meaning of this task?\n' +
				'- What universal truths are applicable to this task?\n' +
				'- What repeating patterns can be identified?\n' +
				'- What are the intentions of the involved parties?\n' +
				'- How would this be a win-win situation?\n' +
				'- What criteria should we evaluate the success of this task against?\n' +
				'- What would you do if your intentions would be an open card?\n' +
				'- What resources are available?\n' +
				(principlesString ? '\nYou have identified these principles for these kind of tasks:\n' + principlesString : ''),
				principle);

		if (!name) {
			if (taskData.principle) taskData.principle = '';
			return;
		} else if (name == taskData.principle) {
			return
		}

		taskData.principle = name;

		// Update database
		saveDataToDB([listItem.closest('.task-list-wrapper').dataset.list]);
		listItem.classList.add('has-principle');

		addNewPrinciple(name, taskData.name, listItem.dataset.id);
	});

	listItem.querySelector('.pro-con').addEventListener('click', function (e) {
		showModal()
		document.getElementById('modal-content').innerHTML = `
			<div id="procon-overlay">
				<h3>${isHun ? 'Pro/Kontra' : 'Pro/Con'}</h3>
				<p>${isHun ? 'Milyen érvek és ellenérvek állnak a feladat végrehajtása mellett?' : 'What pros and cons can you list in relation to this task?'}<br><b>${getTaskDataById(listItem.dataset.id).name}</b></p>
				<form id="procon-form">
					<div id="pros" data-factor-group="pros">
						<fieldset data>
							<legend>${isHun ? 'Érvek' : 'Pro'}</legend>
							<input class="cta-button"type="button" value="${isHun ? 'Hozzáadás' : 'Add'}">
						</fieldset>
					</div><div id="cons" data-factor-group="cons">
						<fieldset>
							<legend>${isHun ? 'Ellenérvek' : 'Con'}</legend>
							<input class="cta-button" type="button" value="${isHun ? 'Hozzáadás' : 'Add'}">
						</fieldset>
					</div>
				</form>
			</div>
		`;
		initProconHandler(e.currentTarget.closest('.draggable').dataset.id);
	});

	// listItem.addEventListener('drag', function(e) {
	// 	//log('x: ' + e.pageX + ', y: ' + e.pageY);
	// 	//console.log('x: ' + e.pageX + ', y: ' + e.pageY);
	// 	if (e.pageX + 100 > window.innerWidth && userOptions.beta_features === 'on') {
	// 		if (document.getElementById('dayview-calendar').classList.contains('hidden')) showDayViewCalendar();
	// 	}
	// });

	listItem.querySelector('.edit-task-settings').addEventListener('click', showTaskSettingsModal);
	listItem.querySelector('.task-interactions').addEventListener('click', function(e) {
		e.stopPropagation();
		document.querySelectorAll('.draggable').forEach(el => el.classList.remove('task-actions-shown'));
	});
}

function move(data, targetListSelector, categoryChangeOnly = false, avoidSync) {
	var sourceList = document.getElementById(data.id).parentNode;
	data.sourceList = sourceList;

	var movedEl = '';

	// Update working memory object data
	myLists[data.list].categories[data.category].forEach(function (el, i) {

		if (data.listDBId == el.id) {
			// Remove element from array
			movedEl = myLists[data.list].categories[data.category].splice(i, 1);
		}
	});

	// Append dragged element to the target element
	document.querySelector(targetListSelector).appendChild(document.getElementById(data.id));
	// document.getElementById(data.id).classList.remove('grabbing');

	var targetList = document.querySelector(targetListSelector);
	var targetCategoryName = targetList.dataset.category;
	var targetListName = targetList.closest('.task-list-wrapper').dataset.list;

	movedEl[0].list = targetListName;
	movedEl[0].listId = toId(targetListName);
	movedEl[0].category = targetCategoryName;
	movedEl[0].categoryClass = targetCategoryName.toLowerCase();
	// Update list element data

	// If the task has a due date and it dropped to tommorrow we set the due date to tomorrow
	if (targetListName == 'Tomorrow' && movedEl[0].day) {
		let tomorrow = moment().add(1, 'd').format('YYYY-MM-DD');
		movedEl[0].day = tomorrow;
		document.getElementById(data.id).querySelector('.task-date span').innerText = tomorrow;
	}

	// If the task has a due date and it dropped to today we set the due date to today
	if (targetListName == 'Today' && movedEl[0].day) {
		movedEl[0].day = dateToday;
		document.getElementById(data.id).querySelector('.task-date span').innerText = dateToday;
	}

	myLists[targetListName].categories[targetCategoryName].push(movedEl[0]);

	updateDayviewEventDateTime(movedEl[0].id, movedEl[0].day, avoidSync ? false: true);


	// if (window.innerWidth < 992) {
	document.getElementById('drop-to-another-list-drawer').classList.add('hide');
	// }

	// Update database
	saveDataToDB([data.list, targetListName]);

	recalcListTimeSum(sourceList);
	recalcListTimeSum(targetList);
	updateListItemCounter([data.list, targetListName]);
	showTaskAddMoveNotification(targetListName);
	if (!categoryChangeOnly) {
		removeScheduledTaskDate(movedEl[0].id);
	} else {
		updateCalendarEventProperty(movedEl[0].id, 'classNames', getTaskDataById(movedEl[0].id).categoryClass)
	}

	updateCalendarEventProperty(movedEl[0].id, 'resourceId', (movedEl[0].categoryClass == 'enhance' ? 'a' : (movedEl[0].categoryClass == 'must-do' ? 'b' : 'c')) + (6 - movedEl[0].effect));
	data = false;
	elDataTransfer = false;
}


function recalcListTimeSum(list) {
	var sum = 0;

	// Sum time
	list.querySelectorAll('li').forEach(function(el) {
		if (!el.classList.contains('hide')) {
			let taskData = getTaskDataById(el.dataset.id),
				time = taskData ? taskData.time : 15;

			sum += Number(time);
		}
	});

	let workdayMin = Number(userOptions.workday_length) * 60;

	// Refresh total time need
	if (userOptions.beta_features == 'on') {
		list.closest('.subcategory-wrapper').querySelector('h3 .time').textContent = sum > 0 ? '' + 'Σ ' + convertMinutesToTimeString(sum) + (sum > workdayMin ? ' > ' + parseInt(sum/workdayMin) + (isHun ? ' mn' : ' wd') : ''): '';
	} else {
		list.closest('.subcategory-wrapper').querySelector('h3 .time').textContent = sum > 0 ? 'Σ ' + convertMinutesToTimeString(sum) : '';
	}
}


function saveDataToDB(lists, isRemoval, isHiddenSave) {
	if (lists && !isRemoval && !isHiddenSave) updateListItemCounter(lists);
	syncListsToFireStore(lists, isRemoval);
}


function reloadOnSyncNeeded() {
	return getlastbrowserid()
		.then(function(lastUsedBrowserId) {
			if (lastUsedBrowserId != randomBrowserId) {
				// If the current and last saved browser id is not equal that means another device has used the database since our last save so we have to reload the page to refresh the full data and we should store a new  browser id to the database to set it as active device
				console.log('reloadOnSyncNeeded: Last used browser id: ' + lastUsedBrowserId + ', current browswer id: ' + randomBrowserId);
				clearInterval(syncWatcher);
				// showSyncNeededMsg();
				// alert(isHun ? 'Szinkronizálás szükséges: más böngészőből volt használva az alkalmazás utoljára' : 'Sync needed: the app was accessed from other device');
				// location.reload(true);
			} else {
				if (!document.getElementById('editor-wrapper').classList.contains('hide')) {
					saveNote();
				} else {
					saveDataToDB();
				}
			}
		});
}

document.querySelector('#load-overlay .cta-button').addEventListener('click', function(e) {
	e.preventDefault();
	location.reload(true);
});

async function loadCompassData() {
	let firestoreCompassData = await getDocumentData('', 'compassData'),
		data = isJson(firestoreCompassData) ? JSON.parse(firestoreCompassData) : firestoreCompassData;

	if (!userOptions.compassMigratedToFirestore) {
		let sqlData = await fetch(appVariables.templateUrl + '/inc/db/compass/loadcompassfromdb.php').then(response => response.json()),
			data = isJson(sqlData[0].compassdata) ? JSON.parse(sqlData[0].compassdata) : false;

		if (data) setDocumentData('', {compassData: data});

		userOptions.compassMigratedToFirestore = true;
		updateUserOptions();
	}

	processCompassData(data);
	//attachDocumentDataChangeListener();
}

function processCompassData(data, sync) {
	document.querySelectorAll('#compass .editable-table-row, #compass .compass__group__list_element').forEach(el => el.remove());
	compassTemplateHTML = document.getElementById('compass').innerHTML;

	let defaultCompassData;

	if (isHun) {
		defaultCompassData = {
			values: [{name: 'Család'}, {name: 'Egyensúly'}, {name: 'Nyugalom'}],
			roles: {
				'Társ': 'Minőségi együtt töltött időt betervezni',
				'Barát': 'Heti közös programot szervezni',
				'Munka': 'Vállalkozási lehetőségeket mérlegelni',
				'Közösség': 'Programok szervezésébe bekapcsolódni'
			},
			enhance: {
				'Szellemi': 'Olvass minden nap 1 órát',
				'Spiritualitás': 'Napi fél óra meditáció',
				'Kapcsolatok': 'Hívj fel egy ismerőst',
				'Fizikum': 'Heti 2x testmozgás'
			},
			principles : [
				{name: 'Ha a lehetőség, ami előttem áll, nem kapcsolódik egyik értékemhez vagy szerepkörömhöz sem, akkor nemet mondok rá'},
				{name: 'Sose dönts azonnal, kérj időt, hogy átfontoltan dönthess'},
				{name: 'Csak olyan dolgokra igent mondani, amelyekkel valóban foglalkozni akarok'},
				{name: 'Az új elköteleződések nem mehetnek a család rovására'}
			]
		};
	} else {
		defaultCompassData = {
			values: [{name: 'Family'}, {name: 'Balance'}, {name: 'Calmness'}],
			roles: {
				'Spouse': 'Plan weekly quality time',
				'Friend': 'Schdeule meet-up with a friend',
				'Work': 'Evaluate business opportunities',
				'Community': 'Do charity work'
			},
			enhance: {
				'Mind': 'Read 1 hour a day',
				'Spirituality': 'Meditate daily',
				'Relationships': 'Call a friend',
				'Physical': 'Workout twice a week'
			},
			principles : [
				{name: 'If an opportuntiy does not relate to any of my roles, I will turn it down'},
				{name: 'Never decide when in rush, ask for more time, to make better decisions'},
				{name: 'Only say yes to things, which I really want to do'},
				{name: 'New opportunities can not be accapted if they hurt my relationship with my family'}
			]
		};
	}

	if (isHun) {
		let translation = {
			'Roles': 'Szerepkör',
			'Goals': 'Heti cél',
			'Sharpen the saw': 'Dolgozz magadon',
			'Principles': 'Elvek',
			'Values': 'Értékek',
			'Add': 'Hozzáadás',
			'Labels': 'Címkék'
		};

		for (key in translation) {
			const regex = new RegExp(key, 'g');
			compassTemplateHTML = compassTemplateHTML.replace(regex, translation[key]);
		}
	}

	if (typeof data == 'object') {
		let compassDataFromDb = data;

		// This conditional is needed as in the past the whole compass data was stored as HTML
		if (typeof compassDataFromDb != 'object') {
			compassHTML = decodeURI(compassDataFromDb);
			document.getElementById('compass').innerHTML = compassHTML;
			extractCompassData();
		} else {
			compassData = JSON.parse(JSON.stringify(compassDataFromDb).replace(/new_line/g, "\\n"));
		}

		for (area in defaultCompassData) {
			if (!compassData[area] || Object.keys(compassData[area]).length == 0) {
				compassData[area] = defaultCompassData[area];
			}
		}
	} else {
		compassData = defaultCompassData;
	}

	['roles', 'enhance'].forEach(area => {
		for (entry in compassData[area]) {
			if (typeof compassData[area][entry] != 'object') {
				compassData[area][entry] = {
					score: 3,
					goal: compassData[area][entry],
					why: 'Enter your why'
				};
			}
		}
	});

	if (compassData.values && compassData.values.length > 0) {
		compassData.values.forEach((value, i) => {
			if (typeof value != 'object') {
				compassData.values[i] = {
					name: value
				};
			}
		});
	}

	document.getElementById('compass').innerHTML = compassTemplateHTML;

	if (window.location.href.indexOf('action=import_principle') > -1) {
		console.log('Principle import action');
		parsePrincipleSharingURL();
	}

	injectCompassData();

	if (!compassData.dailyLog) compassData.dailyLog = {};
	attachCompassEventListeners(true);
}


function saveCompassToDB() {
	updateDocumentData('', {compassData: JSON.stringify(compassData)});
}

function showSyncNeededMsg() {
	document.getElementById('load-overlay').classList.remove('hide');
	document.getElementById('sync-needed-info').classList.remove('hide');
}

function saveNotesToDB() {
	updateDocumentData('', {notes: JSON.stringify(notes)});
}


document.querySelector('#learning-modules').addEventListener('click', function(e) {
	document.getElementById('learning-modules-wrapper').classList.toggle('open');
});

document.querySelector('#nav-wrapper .logo').addEventListener('click', function(e) {
	let leftSidebar = document.getElementById('left-sidebar');

	if (window.innerWidth < 992) {
		leftSidebar.classList.toggle('hide');
	}
});

function resetDefaultView(caller) {
	if (document.body.classList.contains('lists-visible') && caller != 'lists-opener') document.querySelector('.lists-opener').click();
	if (document.body.classList.contains('compass-open') && caller != 'compass') toggleCompass();
	if (document.body.classList.contains('lifewheel-visible') && caller != 'lifewheel') toggleWheelOfLife();
	if (document.body.classList.contains('dayview-calendar-visible') && caller != 'calendar') showDayViewCalendar();
}

document.querySelectorAll('.lists-opener').forEach((opener) => {
	opener.addEventListener('click', function(e) {
		if (window.innerWidth < 769) resetDefaultView('lists-opener');

		let leftSidebar = document.getElementById('left-sidebar');
		document.getElementById('compass').classList.add('hidden');
		document.body.classList.toggle('lists-visible');

		if (leftSidebar.classList.contains('show-lists') && leftSidebar.dataset.openedby != 'compass') {
			leftSidebar.classList.remove('show-lists');
		} else {
			leftSidebar.classList.add('show-lists');
			leftSidebar.dataset.openedby = '';
		}
	});
});

document.querySelector('#available-lists-to-drop .close').addEventListener('click', function(e) {
	e.target.parentNode.classList.toggle('hide');
	document.querySelectorAll('.grabbing').forEach(el => {
		el.classList.remove('grabbing');
	});
});

function moveInArray(arr, old_index, new_index) {
	while (old_index < 0) {
		old_index += arr.length;
	}
	while (new_index < 0) {
		new_index += arr.length;
	}
	if (new_index >= arr.length) {
		var k = new_index - arr.length;
		while ((k--) + 1) {
			arr.push(undefined);
		}
	}
	arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
	return arr;
}

document.getElementById('logout-link').addEventListener('click', function(e) {
	e.preventDefault;
	document.cookie = "username=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=" + location.pathName + ";";
	location = e.currentTarget.getAttribute('href');
});


var mockedLearningModules = {
	'Knowledge Marketplace (126)' : {
		'thumbnail': 'svg-icons/knowledge-marketplace.svg',
		'modules': {
			'Good to Great in 21 Days': {
				'targetlist': '',
				'thumbnail': 'good-to-great-book.png',
				'ratings': {
					'average': '4.8',
					'reviews': '1564',
					'topReview': {
						'text': 'This step-by-step process has changed my life!!',
						'reviewer': 'Lucas Andersen',
						'position': 'CEO @ Fabric.com'
					}
				},
				'tasks' : [
					{'Day 1 - Dumbass': {'day': '1'}},
					{'Day 4 - Chicken': {'day': '4'}},
					{'Day 12 - Student': {'day': '12'}},
					{'Day 19 - Good': {'day': '19'}},
					{'Day 21 - Great': {'day': '21'}}
				]
			},
			'Built to Last - Become a Visionary Company in 14 Days': {
				'tasks' : [
					{'Day 2 - Startup': {}},
					{'Day 8 - Runner-up': {}},
					{'Day 11 - Superior': {}},
					{'Day 14 - Visionary': {}}
				]
			},
		},
	},
	'Daily Routines (49)' : {
		'thumbnail': 'svg-icons/daily-routines.svg',
		'modules' : {
			'Anthony Robbins': {
				'targetlist': 'Anthony Robbins',
				'category': 'Daily Routines',
				'tasks' : [
					{'Wake up @ 6AM': {}},
					{'Meditate @ 7AM': {}},
					{'Self-education  @ 9AM': {}},
					{'Charity @ 5PM': {}},
					{'Act of kindness @ 7PM': {}}
				]
			},
			'Jeff Bezos': {
				'targetlist': '',
				'tasks' : [
					{'Work out @ 5AM': {
							'day': '1',
							'recurring': 'true',
							'repeatInterval': '1',
							'time': '30',
							'note': 'hello'
						}
					},
					{'Meditate @ 7AM': {}},
					{'Priorize @ 9AM': {}},
					{'Strategy session @ 11AM': {}},
					{'Meetings @2PM': {}},
					{'Family Time @7PM': {}}
				]
			}
		}
	},
	'Company Modules (5)' : {
		'thumbnail': 'svg-icons/company.svg',
		'modules' : {
			'Onboarding Programme in 30 days': {
				'tasks' : [
					{'Day 1 - Get to know your collegues': {}},
					{'Day 4 - Write introductory letter': {}},
					{'Day 12 - Test task': {}},
					{'Day 19 - Summarize findings': {}},
					{'Day 21 - Plan first quarter': {}}
				]
			},
			'Become A Top Performer': {
				'tasks' : [
					{'Day 1 - Speed reading': {}},
					{'Day 4 - Super memory': {}},
					{'Day 12 - Public speech': {}},
					{'Day 19 - Presentation': {}}
				]
			}
		}
	},
	'Shared Lists (2)' : {
		'thumbnail': 'svg-icons/shared.svg',
		'modules' : {
			'How To Plan Your Website In 30 Days': {
				'tasks' : [
					{'Day 1 - Create buyer persona': {}},
					{'Day 4 - Research competitors': {}},
					{'Day 12 - Calculate revenue & conversion targets': {}},
					{'Day 19 - Create marketing copy': {}},
					{'Day 21 - Validate idea': {}}
				]
			}
		}
	}
};


// Helper functions
function getCookie(cookieName){
	return (document.cookie.match('(^|; )' + cookieName + '=([^;]*)') || 0)[2];
}

// Template creators
function addListToDOM(listNameAsId, list, listObject, goal = '') {
	let categories = listObject.categories,
		// We should prompt the user here to categorize the new list
		parentCat = listObject.parentcategory,
		localizedParentCat = isHun && trans_hu[parentCat] ? trans_hu[parentCat] : parentCat,
		localizedList = isHun && trans_hu[list]  ? trans_hu[list] : list,
		isSimpleList = listObject.simpleList;

	if (!listObject.simpleList) {
		listObject.simpleList = false;
	}

	// Create parentcategory if not exists
	if (parentCat && !document.querySelector(`#left-sidebar [data-parentcategory="${toId(parentCat)}"]`)) {
		let targetListWrapperSelectors = [
			'#my-lists',
			'#available-lists-to-drop ul',
		];

		listCategoryModule.addNewCategory(parentCat);

		targetListWrapperSelectors.forEach(selector => {
			let subListsHidden = listCategoryModule.getCategoryProp(parentCat, 'hidden');

			document.querySelector(selector).insertAdjacentHTML('beforeend', `
				<li ondrop="listCategoryModule.drop(event)" ondragover="allowDrop(event)" data-list-category="${parentCat}" class="${toId(parentCat)}">
					<b class="list-parent-category-name" draggable="true"  ondragstart="listCategoryModule.dragCategory(event)">${localizedParentCat}</b>
					<ul data-parentcategory="${toId(parentCat)}" class="${subListsHidden ? 'hide' : ''}"></ul>
				</li>
			`);
		});

		openListsOnCategoryNameClick(document.querySelector('#left-sidebar [data-parentcategory="' + toId(parentCat) + '"]'));
		openListsOnCategoryNameClick(document.querySelector('#available-lists-to-drop [data-parentcategory="' + toId(parentCat) + '"]'));
	}

	listCategoryModule.addListToCategory(parentCat, list);

	let listItemCounter = getListItemCount(list);

	// Add list to left sidebar and to drop list
	let isLightTheme = document.body.classList.contains('light-style');

	let leftSidebarListHTML = `
		<li draggable="true"  ondragstart="listCategoryModule.dragList(event)" data-listid="${listNameAsId}" data-list="${list}" class="${listObject.starred ? 'starred' : ''} ${listNameAsId == 'today' ? 'opened' : ''} ${listNameAsId}">
			<span class="list-name">${localizedList}</span>
			<span class="list-item-counter">${listItemCounter > 0 ? listItemCounter : ''}</span>
			<span class="star">
				<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/star-orange.svg">
			</span>
			<span class="del">
				<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon${isLightTheme ? '-black' : '-white'}.svg">
			</span>
			<span class="category">
				<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/${isLightTheme ? 'list-black' : 'category-select-white'}.svg">
			</span>
		</li>
	`;

	if (parentCat && document.querySelector('#my-lists [data-parentcategory="' + toId(parentCat) + '"]')) {
		document.querySelector('#my-lists [data-parentcategory="' + toId(parentCat) + '"]').insertAdjacentHTML('beforeend', leftSidebarListHTML);
		document.querySelector('#available-lists-to-drop [data-parentcategory="' + toId(parentCat) + '"]').insertAdjacentHTML('beforeend', '<li data-listid="' + listNameAsId + '">' + localizedList + '</li>');

	} else {
		document.querySelector('#my-lists').insertAdjacentHTML('beforeend', leftSidebarListHTML);
		document.querySelector('#available-lists-to-drop ul').insertAdjacentHTML('beforeend', '<li data-listid="' + listNameAsId + '">' + localizedList + '</li>');
	}

	// Append list wrapper to list pane
	var listWrapperHTMLTemplate = `
<div id="${listNameAsId}" data-list="${list}" class="task-list-wrapper ${listNameAsId != 'today' ? 'hide' : ''} ${listObject.simpleList ? ' simple-list' : ''}${listObject.parentcategory == 'Scheduled' ? ' time-based-list' : ''}">
	<h2>
		${localizedList}
		<div class="task-list-wrapper__actions">
			<button class="add-task-cta-btn"><svg class="svg-icon" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 140 140" width="140" height="140"><g transform="matrix(14,0,0,14,0,0)"><path d="M9.5,4.5A.5.5,0,0,0,9,4H6V1A.5.5,0,0,0,5.5.5h-1A.5.5,0,0,0,4,1V4H1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,1,6H4V9a.5.5,0,0,0,.5.5h1A.5.5,0,0,0,6,9V6H9a.5.5,0,0,0,.5-.5Z" fill="#000000" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path></g></svg></button>
			<a class="sub-action-switch" onclick="showSubActions(event)" href="#"><img class="svg-icon " src="${appVariables.templateUrl}/assets/images/svg-icons/more.svg"></a>
			<div class="task-list-wrapper__sub-actions hide">
				<span class="prio">Prio<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/prioritize-icon.svg"></span>
				<span class="filter-list" onclick="generateWordCloud('labels')">${isHun ? 'Szűrés' : 'Filter'}<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/filter-list.svg"></span>
				<span class="forecast ${parentCat != 'Scheduled' ? 'hide' : ''}">${isHun ? 'Hatás' : 'Impact'}<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/chart-dark.svg"></span>
				<span class="set-goal">${isHun ? 'Cél' : 'Goal'}<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/goal-white.svg"></span>
				<span class="plan">${isHun ? 'Terv' : 'Plan'}<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/plan.svg"></span>
				<span class="star">${isHun ? 'Jelöl' : 'Mark'}<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/star.svg"></span>
				<span class="share">${isHun ? 'Link' : 'Share'}<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/share-link.svg"></span>
				<span class="convert-list">${isHun ? 'Vált' : 'Convert'}<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/convert-list.svg"></span>
			</div>	
		</div>
	</h2>
	<h3 class="goal ${!listObject.goal || (myLists[list] && !myLists[list].isGoalVisible) ? 'hide' : ''}">${listObject.goal || goal ? (isHun ? 'CÉL: ' : 'GOAL: ')  + listObject.goal || goal : (isHun ? '+CÉL MEGADÁSA' : '+ADD GOAL')}</h3>
	<div class="sublists"></div>
</div>`;

	// if (list == 'Inbox') {
	// 	document.getElementById('inbox-wrapper').insertAdjacentHTML('beforeend', listWrapperHTMLTemplate);
	// } else {
		document.getElementById('task-lists').insertAdjacentHTML('beforeend', listWrapperHTMLTemplate);
	// }


	let sortedCategoryKeys = Object.keys(categories).sort();

	// Loop through list categories "Enhance, Must-Do, Optimal"
	sortedCategoryKeys.forEach(function(category) {
		// Default list template


		var categoryHTMLTemplate = '<div class="subcategory-wrapper"><h3 class="' + category.toLowerCase() + '-subcategory-header">' + (isHun ? trans_hu[category] : (category == 'Enhance' ? 'Wildly Important' : (category == 'Must-Do' ? 'Must-Do' : category))) + ' <b class="category-hint">' + (category == 'Enhance' ? '?' : (category == 'Must-Do' ? '?' : '?')) + '</b><span class="time"></span><span>' + (isHun ? 'Hatás' : 'Effect') + '</span><span>' + (isHun ? 'Könny.' : 'Ease') + '</span><span>Prio</span><span>' + (isHun ? 'Idő' : 'Time') + '</span></h3><ul data-category="' + category + '" class="' + category.toLowerCase() + ' list-container" class="list-container"></ul></div>';

		// Append list categories to task list
		document.getElementById(listNameAsId).querySelector('.sublists').insertAdjacentHTML('beforeend', categoryHTMLTemplate);
	});
}


function addGoalHandler(e, list) {
	if (e.currentTarget.innerText != '+ADD GOAL') {
		let update = confirm(isHun ? 'Szeretnéd módosítani a listádhoz tartozó célt?' : 'Do you really want to update this goal?');

		if (!update) return;
	}
	let goal = prompt(isHun ? 'Kérlek add meg a listához kapcsolódó fő célodat, számszerűsítve, hogy hová akarsz eljutni és mikorra' : 'Please enter your main goal for this list, quantifying the result you want to reach and setting a deadline', myLists[list].goal ? myLists[list].goal : '');
	// let kpi = prompt(isHun ? 'Add meg milyen mérőszámmal méred a haladást' : 'Enter your main KPI for to measure progress for this goal');
	let kpi = false;
	let goalText = goal  ? (isHun ? 'CÉL: ' : 'GOAL: ') + goal + (kpi ? ' - KPI: ' + kpi : '') : (isHun ? '+CÉL MEGADÁSA' : '+ADD GOAL');

	document.querySelector('#' + toId(list) + ' .goal').innerText = goalText;
	myLists[list].goal = goal;
	saveDataToDB([list]);

	return goalText;
}


function assignDayToTask(selectedDates, dateStr, instance, taskId) {
	targetElementId = taskId || instance.element.closest('[data-id]').dataset.id;
	targetElementSelector = 'draggable-list-item-' + targetElementId;

	let targetListEl = document.getElementById(targetElementSelector);
	let targetListElParentCat = targetListEl.closest('[data-category]').dataset.category;
	let targetListElParentList = targetListEl.closest('[data-list]').dataset.list;

	myLists[targetListElParentList].categories[targetListElParentCat].forEach(function(el) {
		if (el.id == targetElementId) {
			// el.day = prompt(isHun ? 'Milyen határidőt szeretnél beállítani a feladathoz? (formátum: 2020-12-31)' : 'When this task is due? (date format: 2020-12-31)', (el.day ? el.day : moment().format('YYYY-MM-DD')));

			let flatPickrInstance;
			if (!document.querySelector('#task-settings-modal').classList.contains('hide')) {
				flatPickrInstance = document.querySelector('#task-settings-modal .day')._flatpickr
			} else {
				flatPickrInstance = document.querySelector(`#draggable-list-item-${targetElementId} .flatpickr-input`)._flatpickr;
			}

			el.day = flatPickrInstance.selectedDates.length > 0 ? flatpickr.formatDate(flatPickrInstance.selectedDates[0], 'Y-m-d') : false;

			if (el.day) {
				let isScheduled = updateDayviewEventDateTime(el.id, el.day, true);

				targetListEl.classList.add('scheduled');
				targetListEl.querySelector('.task-date span').innerHTML = el.day ? el.day : 'Set due date';

				if (!isScheduled) {
					addCalendarEvent({
						title: el.name,
						start: el.day,
						id: el.id,
						classNames: el.categoryClass
					});
				}
			} else {
				removeCalendarEvent(el.id);
			}

			if (userName != 'creator') {
				assignScheduledTasksToLists(el.id);
			}

			if (!document.querySelector('#task-settings-modal').classList.contains('hide')) {
				document.querySelector('#task-settings-modal .due-date').innerText = el.day ? el.day : 'Set due date';
			}


			// if (el.day && confirm(isHun ? 'Szeretnél egy Google Calendar linket generálni a feladathoz?' : 'Do you want to generate a Google Calendar link for this task?')) {
			// 	let dateArray = el.day.split('-');
			// 	const event = {
			// 		start: new Date(Number(dateArray[0]), Number(dateArray[1] - 1), Number(dateArray[2]), 10),
			// 		end: new Date(Number(dateArray[0]), Number(dateArray[1] - 1), Number(dateArray[2]), 11),
			// 		title: el.name
			// 	};
			// 	var win = window.open(generateUrl(event), '_blank');
			// 	win.focus();
			// }
		}
	});

	saveDataToDB([targetListElParentList]);
}

function openEditorOnListPlanIconClick(e) {
	let editorWrapper = document.getElementById('editor-wrapper'),
		list = e.currentTarget.closest('.task-list-wrapper').dataset.list;

	editorWrapper.classList.remove('hide');
	document.getElementById('editor-modal-wrapper').classList.add('visible');
	editorWrapper.setAttribute('data-list-note', list);

	if (!quill) initQuill();

	if (myLists[list].note) {
		quill.focus();
		document.querySelector('.ql-editor').innerHTML = decodeURI(myLists[list].note);
	}

	document.querySelector('.ql-editor').addEventListener('keydown', resetTimer);
}

function openEditorOnNoteIconClick(e) {
	e.stopPropagation();

	let taskSettingsModal = document.getElementById('task-settings-modal'),
		quillTargetElementId;

	if (taskSettingsModal.classList.contains('hide')) {
		quillTargetElementId = e.currentTarget.closest('[data-id]').dataset.id;
	} else {
		quillTargetElementId = taskSettingsModal.dataset.taskId;
	}

	let editorWrapper = document.getElementById('editor-wrapper');

	editorWrapper.classList.remove('hide');
	document.getElementById('editor-modal-wrapper').classList.add('visible');
	editorWrapper.setAttribute('data-id', quillTargetElementId);
	quillTargetElementSelector = 'draggable-list-item-' + quillTargetElementId;

	if (!quill) initQuill();

	quill.focus();
	document.querySelector('.ql-editor').addEventListener('keydown', resetTimer);

	let targetListEl = document.getElementById(quillTargetElementSelector);
	let targetListElParentCat = targetListEl.closest('[data-category]').dataset.category;
	let targetListElParentList = targetListEl.closest('[data-list]').dataset.list;

	myLists[targetListElParentList].categories[targetListElParentCat].forEach(function(el) {
		if (el.id == quillTargetElementId && el.note) {
			// el.note.ops.forEach(function(element) {
			// 	Object.keys(element).forEach(function(key) {
			// 		if (key == 'insert') {
			// 			//element[key] = JSON.parse(element[key]);
			// 		}
			// 	});
			// });

			// console.log ('Data read from working memory: ');
			// console.log(el.note);
			// quill.setContents(el.note);

			document.querySelector('.ql-editor').innerHTML = decodeURI(el.note);
		}
	});

}

function saveNote(e) {
	let editorWrapper = document.getElementById('editor-wrapper');
	let editorContainer = document.querySelector('.ql-editor');

	let saveNotification = document.querySelector('.task-saved-notification');

	saveNotification.classList.add('visible');
	setTimeout(function() {
		saveNotification.classList.remove('visible');
	}, 2000);

	clearInterval(timer);

	if (quill) {
		let content = encodeURI(editorContainer.innerHTML);

		if (e && e.currentTarget.classList.contains('close')) {
			editorContainer.innerHTML = '';
			if (editorWrapper.dataset && editorWrapper.dataset.listNote) {
				saveListNote();
				editorWrapper.removeAttribute('data-list-note');
				return;
			}
		}

		if (editorWrapper.dataset && editorWrapper.dataset.listNote) {
			saveListNote();
			return;
		}

		function saveListNote() {
			let list = editorWrapper.dataset.listNote;
			myLists[list].note = content;
			saveDataToDB([list], false, true);
		}

		if (editorWrapper.dataset && editorWrapper.dataset.id) {
			// let content = quill.getContents();
			// quill.deleteText(0, quill.getLength());
			let targetListEl = document.getElementById(quillTargetElementSelector),
				quillTargetElementId = targetListEl.dataset.id,
				targetListElParentCat = targetListEl.closest('[data-category]').dataset.category,
				targetListElParentList = targetListEl.closest('[data-list]').dataset.list;

			myLists[targetListElParentList].categories[targetListElParentCat].forEach(function(el) {
				if (el.id == quillTargetElementId) {
					// content.ops.forEach(function(element) {
					// 	Object.keys(element).forEach(function(key) {
					// 		if (key == 'insert') {
					// 			element[key] = JSON.stringify(element[key]).slice(1, -1).escapeSpecialChars();
					// 		}
					// 	});
					// })
					if (decodeURI(content).replace(/(<([^>]+)>)/gi, "").length === 0) {
						el.note = '';
						targetListEl.classList.remove('has-note');
					} else {
						el.note = content;
						targetListEl.classList.add('has-note');
					}
				}
			});

			saveDataToDB([targetListElParentList], false, true);
		} else if (editorWrapper.dataset && editorWrapper.dataset.day) {
			let date = editorWrapper.dataset.day,
				index = editorWrapper.dataset.index;

			notes[date][index].content = content;


			// if (!notesFromDb.some(function(note, i) {
			// 	if (note.start == day) {
			// 		if (!stripTags(decodeURI(content))) {
			// 			noteApp.notes[date][index] = undefined;
			// 			delete noteApp.notes[date][index];
			// 			notesFromDb.splice(i, 1);
			// 		}
			// 	}
			//
			// 	return note.start == day;
			// })) {
			// 	if (stripTags(decodeURI(content))) {
			// 		notesFromDb.push({
			// 			title: day,
			// 			start: day,
			// 			description: content
			// 		});
			// 	}
			// }
			saveNotesToDB();
		}
	}
}

String.prototype.escapeSpecialChars = function() {
	return this.replace(/\\n/g, "\\n")
		.replace(/\\'/g, "\\'")
		.replace(/\\"/g, '\\"')
		.replace(/\\&/g, "\\&")
		.replace(/\\r/g, "\\r")
		.replace(/\\t/g, "\\t")
		.replace(/\\b/g, "\\b")
		.replace(/\\f/g, "\\f");
};

function isJson(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

function toId(str) {
	let prefix = '';

	// Fix Id selector when string starts with number
	if (Number.isInteger(Number(str[0]))) prefix = '_';

	return prefix + str.toLowerCase().replace(/ /g, '-');
}

function openListsOnCategoryNameClick(category) {
	category.previousElementSibling.addEventListener('click', function(e) {
		if (e.ctrlKey || e.metaKey) {
			document.querySelectorAll('.task-list-wrapper').forEach(function(list) {
				list.classList.add('hide');
			});
			category.querySelectorAll('li').forEach(function(el) {
				document.getElementById(el.dataset.listid).classList.remove('hide');
			});
			document.body.classList.add('multi-list-view');

			if (document.body.classList.contains('inbox-visible')) document.getElementById('inbox').classList.remove('hide')
		} else {
			let categoryName = category.closest('[data-list-category]').dataset.listCategory;

			e.currentTarget.classList.toggle('hide-lists')
			listCategoryModule.toggleHiddenStatus(categoryName);
			category.classList.toggle('hide');
		}
	});

	// if (window.innerWidth > 991) {
	// 	category.previousElementSibling.addEventListener('click', function(e) {
	// 		let isAllChildListsVisible = [].every.call(category.querySelectorAll('[data-listid]'),function(list) {
	// 			return !document.getElementById(list.dataset.listid).classList.contains('hide');
	// 		});

	// 		if (isAllChildListsVisible) {
	// 			document.querySelectorAll('.task-list-wrapper').forEach(function(el) {
	// 				el.classList.add('hide');
	// 			});
	// 		} else {
	// 			document.querySelectorAll('.task-list-wrapper').forEach(function(el) {
	// 				el.classList.add('hide');
	// 			});

	// 			category.querySelectorAll('[data-listid]').forEach(function(el) {
	// 				document.getElementById(el.dataset.listid).classList.remove('hide');
	// 			});
	// 		}
	// 	}); 
	// }
}

syncLearningModules();

function syncLearningModules() {
	//We try to load the creator data
	// loadlistdatafromdb('all', 'creator')
	// 	.then(function(data) {
	// 		if (data.length != 0) {
	// 			// If there is a match we load the stored lists
	// 			dbLearningModules = JSON.parse(data);
	// 		}
	//
	// 		mergeStaticAndDBLearningData();
	// 	});

	//We try to load the creator data
	firebaseDatabase.collection(`/users/6/myLists`)
		.get()
		.then((docs) => {
			let data = {};

			docs.forEach(doc => {
				data[doc.id] = doc.data();
			});

			if (typeof data == 'object') dbLearningModules = data;
			mergeStaticAndDBLearningData();
		});

}

function mergeStaticAndDBLearningData() {
	learningModules = {};

	// Restructure data to fit import script
	for (listName in dbLearningModules) {
		if (dbLearningModules[listName].categories['Enhance'].length > 0) {
			let parentCategory = learningModules[dbLearningModules[listName].parentcategory];

			if (dbLearningModules[listName].parentcategory != 'Scheduled' && dbLearningModules[listName].parentcategory != 'Capture') {
				// If category doesn't exist add to list of categories
				if (!parentCategory) {
					learningModules[dbLearningModules[listName].parentcategory] = {
						'modules': {}
					};

					parentCategory = learningModules[dbLearningModules[listName].parentcategory];
				}

				learningModules[dbLearningModules[listName].parentcategory].modules[listName] = {
					'goal': dbLearningModules[listName].goal,
					'targetlist': listName,
					'parentcategory': dbLearningModules[listName].parentcategory,
					'tasks': []
				};

				dbLearningModules[listName].categories['Enhance'].forEach(function (task, i) {
					let taskObject = {};
					taskObject[task.name] = {
						'day': task.day,
						'time': task.time,
						'note': task.note
					};

					learningModules[dbLearningModules[listName].parentcategory].modules[listName].tasks.push(taskObject);
				});
			}

		}
	}

	learningModules = {...mockedLearningModules, ...learningModules};

	var categoryCounter = 1;
	var learningModuleWrapper = document.getElementById('learning-modules-wrapper');
	learningModuleWrapper.innerHTML = '';

	for (categoryName in learningModules) {
		var categoryThumbnail = learningModules[categoryName].thumbnail;


		learningModuleWrapper.insertAdjacentHTML('beforeend', (categoryThumbnail ? '<img class="category-thumbnail" src="' + appVariables.templateUrl + '/assets/images/' + categoryThumbnail + '">' : '') + '<h3 class="category-title">' + categoryName + '</h3><div class="category-content-wrapper hide"></div>');

		let categoryWrapper = learningModuleWrapper.querySelector('.category-content-wrapper:nth-of-type(' + categoryCounter + ')');

		let modulCounter = 1;

		categoryWrapper.insertAdjacentHTML('beforeend', '<h4 class="back-to-cats">< BACK</h4>');

		for (modulName in learningModules[categoryName].modules) {
			var modulData = learningModules[categoryName].modules[modulName];

			if (modulData.thumbnail) {
				categoryWrapper.insertAdjacentHTML('beforeend', '<img class="module-thumbnail" src="' + appVariables.templateUrl + '/assets/images/' + modulData.thumbnail + '">');
			}

			if (modulData.ratings) {
				let ratings = modulData.ratings;
				let topReview = ratings.topReview;

				categoryWrapper.insertAdjacentHTML('beforeend', '<p>' + ratings.average + '★★★★★ (' + ratings.reviews + ' reviews)<br><i>"' + topReview.text + '"</i><br><strong>' + topReview.reviewer + '</strong>, ' + topReview.position + '</p>');
			}

			categoryWrapper.insertAdjacentHTML('beforeend', '<div class="module" data-parentcategory="' + categoryName + '" data-title="' + modulName + '"><h4>' + modulName + '  <button class="inject">INJECT <img class="svg-icon" src="' + appVariables.templateUrl + '/assets/images/svg-icons/plus-icon.svg"></button></h4><ul></ul></div>');

			modulData.tasks.forEach(function (el) {
				let taskData = el[Object.keys(el)[0]];
				let day = taskData.day ? taskData.day : '';
				let time = taskData.time ? taskData.time : 15;
				let note = taskData.note ? taskData.note : '';

				categoryWrapper.querySelector('.module:nth-of-type(' + modulCounter + ') ul').insertAdjacentHTML('beforeend', '<li data-targetlist="' + (modulData.targetlist ? modulData.targetlist : '') + '" data-day="' + day + '" data-time="' + time + '" data-note="' + note + '">' + Object.keys(el)[0] + '</li>');
			});


			modulCounter++;
		}
		;

		categoryCounter++;
	}

	// Inject learning module into lists
	document.querySelectorAll('.module h4 .inject').forEach(function (el) {
		// When the "+INJECT" button is clicked
		el.addEventListener('click', function (e) {
			// Loop through the list wrappers list elements
			e.target.closest('.module').querySelectorAll('li').forEach(function (el) {
				// If the list element is scheduled within a week from now set the weekly list as the target
				let taskTime = el.dataset.time;
				let taskNote = el.dataset.note;
				let targetDay = el.dataset.day.length != 0 ? moment().add(Number(el.dataset.day), 'd').format('YYYY-MM-DD') : '';

				// If the targetlist is not explicitly set we are creating a new listname from the module title
				let targetListName = el.dataset.targetlist.length != 0 ? el.dataset.targetlist : (targetDay ? setScheduledTaskTargetList(targetDay) : el.closest('.module').dataset.title);

				if (!myLists[targetListName]) {

					createNewList(targetListName, {
						'goal': '',
						'parentcategory': el.closest('.module').dataset.parentcategory,
						'categories': {
							'Enhance': []
						}
					});
					listCategoryModule.saveListCategoriesToDB();
				}

				var targetListId = toId(targetListName);
				var targetListElement = document.querySelector('#' + targetListId + ' .enhance');

				// Append new list element to the target list and attach event handlers
				initListElement({
					moduleInjection: true,
					name: el.textContent,
					targetList: targetListElement,
					listId: targetListId,
					categoryClass: 'enhance',
					time: taskTime,
					note: taskNote,
					day: targetDay
				});
			});
		});
	});

	document.querySelectorAll('#learning-modules-wrapper .category-title').forEach(function (el) {
		el.addEventListener('click', function (e) {
			document.querySelectorAll('#learningModules .category-content-wrapper').forEach(function (wrapper) {
				wrapper.classList.add('hide');
			});
			e.target.nextSibling.classList.toggle('hide');
		});
	});

	document.querySelectorAll('#learning-modules-wrapper .back-to-cats').forEach(function (el) {
		el.addEventListener('click', function (e) {
			document.querySelectorAll('#learning-modules-wrapper .category-content-wrapper').forEach(function (wrapper) {
				wrapper.classList.add('hide');
			});
		});
	});
}

function showGoalsOverview() {
	let modalContent = document.getElementById('modal-content'),
		goalsHTMLTemplate = `
			<div class="goals-summary-wrapper">
				<h2>${isHun ? 'Küldetésnyilatkozat' : 'Mission statement'}</h2>
				<p id="mission-statement">${userOptions.mission_statement ? '"' + userOptions.mission_statement + '"' : (isHun ? '+ Küldetésnyilatkozat hozzáadása' : '+ Add mission statement')}</p>
				<hr>
				<h3 id="goals-summary-title">${isHun ? 'Célösszesítő' : 'Goals Summary'}
					<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/goal.svg">
				</h3>
				<p class="description">${isHun ? '*A listáidhoz tartozó célok áttekintése/módosítása' : '*You can review/edit your list goals from here'}</p>
				<div id="goals-summary-wrapper__lists-with-goals">
					<h3>${isHun ? 'Kategóriák' : 'Areas'}</h3>
					<button id="add-new-category" class="default-cta-button default-cta-button--add" style="display: none">
						<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/plus-icon.svg">
					</button>
					<ul id="areas-of-focus"></ul>
					<hr>
					<h4>${isHun ? 'Stratégiai fókuszterületek' : 'Strategic focuses'}</h4>
					<button id="add-new-list-to-category" class="default-cta-button default-cta-button--add" style="display: none">
						<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/plus-icon.svg">
					</button>
					<div id="lists-of-areas"></div>
				</div>
			</div>
		`;
	modalContent.insertAdjacentHTML('beforeend', goalsHTMLTemplate);

	let listsByCategory = {},
		goalCounter = 0;

	for (list in myLists) {
		let goal = myLists[list].goal,
			category = myLists[list].parentcategory;

		if (goal) {
			goalCounter++;

			if (!listsByCategory[category]) {
				listsByCategory[category] = [];
				document.getElementById('areas-of-focus').insertAdjacentHTML('beforeend', `<li data-area="${category}">${isHun && trans_hu[category] ? trans_hu[category] : category}</li>`);
				document.getElementById('lists-of-areas').insertAdjacentHTML('beforeend', `
					<div class="area-list-group" data-area="${category}"></div>
				`)
			}

			document.querySelector(`.area-list-group[data-area="${category}"]`).insertAdjacentHTML('beforeend', `
				<div class="area-list" data-goallist="${list}">
					<h3 class="list-name">
						${isHun && trans_hu[list] ? trans_hu[list] : list}
						${function() {
							if (myLists[list].starred) {
								return `
									<img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/star-orange.svg">
								`;
							} else {
								return '';
							}
						}()}
					</h3>
					<p class="goal-header">${isHun ? 'Cél' : 'Goal'}</p>
					<p class="goals-summary__goal">${goal ? goal : (isHun ? '+CÉL MEGADÁSA' : '+ADD GOAL')}</p>
				</div>
			`);
		}
	}

	if (goalCounter == 0) document.getElementById('goals-summary-wrapper__lists-with-goals').classList.add('hide');

	document.querySelectorAll(`.area-list-group`).forEach(function(el) {el.classList.add('hide')});
	document.querySelector(`#areas-of-focus li`).classList.add('selected');
	document.querySelector(`.area-list-group`).classList.remove('hide');


	document.querySelectorAll('#areas-of-focus li').forEach(function(el) {
		el.addEventListener('click', function(e) {
			let area = e.currentTarget.dataset.area;

			document.querySelectorAll(`#areas-of-focus li`).forEach(function(el) {el.classList.remove('selected')});
			document.querySelectorAll(`#areas-of-focus li[data-area="${area}"]`).forEach(function(el) {
				if (el.dataset.area == area) el.classList.add('selected');
			});

			document.querySelectorAll(`.area-list-group`).forEach(function(el) {el.classList.add('hide')});
			document.querySelector(`.area-list-group[data-area="${area}"]`).classList.remove('hide');
		})
	});

	document.querySelectorAll('#lists-of-areas [data-goallist]').forEach(function(el) {
		el.addEventListener('click', function(e) {
			let setGoal = addGoalHandler(e, e.currentTarget.dataset.goallist);
			e.currentTarget.querySelector('.goals-summary__goal').innerText = setGoal != '+ADD GOAL' ? setGoal.split(': ')[1] : setGoal;
		});
	});

	document.getElementById('mission-statement').addEventListener('click', function(e) {
		let missionStatement = prompt(isHun ? 'Mi a te küldetésnyilatkozatod?' : 'What is your mission statement?');

		if (missionStatement) {
			e.currentTarget.innerHTML = '"' + missionStatement + '"';
			userOptions.mission_statement = missionStatement;
			updateUserOptions();
		}
	});
}

document.getElementById('show-goals').addEventListener('click', function() {
	if (document.getElementById('modal').classList.contains('hide')) {
		showModal()
		showGoalsOverview();
	} else {
		clearModal();
	}
});

// document.querySelectorAll('#show-schedule-calendar, #schedule-calendar .close').forEach(function (el) {
// 	el.addEventListener('click', function() {
// 		document.getElementById('schedule-calendar').classList.toggle('hide');
// 		if (events.length != 0) {
// 			rerenderCalendar(scheduleCalendar, events);
// 		}
// 	});
// });

// document.getElementById('show-ideas-list').addEventListener('click', function() {
// 	document.getElementById('left-sidebar').classList.remove('show-lists');
// 	hideAllLists();
// 	document.getElementById('ideas').classList.toggle('hide');
// });

document.querySelectorAll('#show-notes-calendar').forEach(function (el) {
	el.addEventListener('click', function() {
		if (window.innerWidth < 769) resetDefaultView();
		let notesCal = document.getElementById('notes-calendar')
		notesCal.classList.toggle('hide');
		document.getElementById('notes-calendar-wrapper').classList.toggle('visible');
		notesCal.removeAttribute('data-task-term');
		// if (notesFromDb.length != 0) {
		// 	rerenderCalendar(notesCalendar, notesFromDb);
		// }
	});
});

document.querySelector('#nav-wrapper .settings').addEventListener('click', showOptions);

function clearModal() {
	let modal = document.getElementById('modal');

	document.getElementById('modal-wrapper').classList.remove('full-screen');
	document.getElementById('modal-wrapper').classList.remove('visible');

	if (modal.querySelector('#first-run-greeting')) {
		// Move first run greeting back to document root
		document.body.appendChild(document.getElementById('first-run-greeting'));
	}
	modal.classList.add('hide');
	document.getElementById('modal-content').innerHTML = '';
}

function showModal() {
	document.getElementById('modal-wrapper').classList.add('visible');
	document.getElementById('modal').classList.remove('hide');
}

document.querySelector('#modal .close').addEventListener('click', clearModal);


async function loadNotes() {
	let firestoreNotesData = await getDocumentData('', 'notes');
		notes = isJson(firestoreNotesData) ? JSON.parse(firestoreNotesData) : firestoreNotesData;

	if (!userOptions.notesMigratedToFirestore) {
		let sqlData = await fetch(appVariables.templateUrl + '/inc/db/notes/loadnotesfromdb.php').then(response => response.json()).then(data => data);
			notes = isJson(sqlData[0].notes) ? JSON.parse(sqlData[0].notes) : false;

		if (notes) setDocumentData('', {notes: notes});

		userOptions.notesMigratedToFirestore = true;
		updateUserOptions();
	}

	loadNotesToCalendar();
}

// Sort object by key, latest date should be the first in the list
function sortObjReverse(obj) {
	let sortedNoteKeys = Object.keys(obj).sort().reverse(),
		tempObject = {};

	sortedNoteKeys.forEach(function(key) {
		tempObject[key] = obj[key];
	});

	return tempObject;
}

function sortObjByKey(obj) {
	let sortedNoteKeys = Object.keys(obj).sort(),
		tempObject = {};

	sortedNoteKeys.forEach(function(key) {
		tempObject[key] = obj[key];
	});

	return tempObject;
}

function loadNotesToCalendar() {
	var calendarEl = document.getElementById('notes-calendar');

	// Destroy previous instance
	if (vueNoteApp && vueNoteApp.unmount instanceof Function) {
		vueNoteApp.unmount();
		vueNoteApp = undefined;
		noteApp = undefined;
	}

	calendarEl.innerHTML = document.getElementById('notes-calendar-template').innerHTML;

	for (note in notes) {
		// Migrating to new note datastructure
		if (typeof notes[note] != 'object') {
			let content = notes[note];

			notes[note] = {
				title: isHun ? 'Név nélkül' : 'Untitled',
				content: content
			}
		}

		if (!Array.isArray(notes[note])) {
			notes[note] = [notes[note]];
		}
	}

	if (typeof notes == 'object') notes = sortObjReverse(notes);

	const notesList = {
		data() {
			return {
				notes: notes,
			}
		},
		methods: {
			hideNotesCalendar() {
				document.getElementById('notes-calendar').classList.toggle('hide');
				document.getElementById('notes-calendar-wrapper').classList.remove('visible');
			},
			addNewNote(name, taskNote) {
				let title = (name.length && name.length > 0 ? name : '') || prompt(isHun ? 'Add meg a jegyzet nevét' : 'Enter note title'),
					today = moment().format('YYYY-MM-DD');

				if (!title) return;

				if (!this.notes) this.notes = {};
				if (!this.notes[today]) this.notes[today] = [];

				this.notes[today].unshift({
					title: title,
					content: taskNote || ''
				});
				this.notes = sortObjReverse(this.notes);
				saveNotesToDB();
			},
			editNoteTitle(e) {
				e.stopPropagation();
				let title = e.currentTarget.closest('.title'),
					date = title.dataset.date,
					index = title.dataset.index,
					noteTitle = this.notes[date][index].title,
					newTitle = prompt(isHun ? 'Add meg a jegyzet nevét' : 'Enter note title', noteTitle ? noteTitle : (isHun ? 'Név nélkül' : 'Untitled'));

				if (newTitle && noteTitle != newTitle) {
					this.notes[date][index].title = newTitle
					saveNotesToDB();
				}
			},
			searchNotes(e) {
				let taskTerm = calendarEl.dataset.taskTerm,
					term = taskTerm || prompt('Enter search term'),
					tempObj = {};

				if (!term) return;
				if (taskTerm) calendarEl.removeAttribute('data-task-term');
				let words = term.split(' ');

				for (day in notes) {
					notes[day].forEach(note => {
						if (words.some(word => note.title.includes(word))) {
							if (!tempObj[day]) tempObj[day] = []
							tempObj[day].push(note);
						}
					})
				}

				calendarEl.classList.add('filtered');
				this.notes = tempObj;
			},
			clearSearch() {
				this.notes = notes
				calendarEl.classList.remove('filtered');
				calendarEl.removeAttribute('data-task-term');
			},
			deleteNote(e) {
				e.stopPropagation();
				let title = e.currentTarget.closest('.title'),
					date = title.dataset.date,
					index = title.dataset.index;

				if (confirm(isHun ? 'Tényleg szeretnéd törölni ezt a jegyzetet?' : 'Do you really want to delete this note?')) {
					this.notes[date].splice(index, 1);
					saveNotesToDB();
				}
			},
			openNote(e) {
				e.stopPropagation();
				let title = e.currentTarget.closest('.note-element-wrapper').querySelector('.title'),
				date = title.dataset.date,
				index = title.dataset.index;

				editDateNote({
								 dateStr: date,
								 index: index
							 });
			}
		}
	}

	vueNoteApp = Vue.createApp(notesList);
	noteApp = vueNoteApp.mount('#notes-calendar');

	// notesCalendar = new FullCalendar.Calendar(calendarEl, {
	// 	firstDay: 1,
	// 	locale: isHun ? 'hu' : 'en',
	// 	plugins: [ 'dayGrid', 'interaction' ],
	// 	events: notesFromDb,
	// 	eventClick: editDateNote,
	// 	dateClick: editDateNote
	// });
	//
	// notesCalendar.render();
}

function triggerNoteSearch(e) {
	let notesCal = document.getElementById('notes-calendar');
	notesCal.setAttribute('data-task-term', e.currentTarget.dataset.taskName);
	if (notesCal.classList.contains('hide')) {
		notesCal.classList.remove('hide')
		document.getElementById('notes-calendar-wrapper').classList.remove('visible');
		document.getElementById('notes-calendar-wrapper').classList.add('visible');
	}
	noteApp.searchNotes()
}


function setScheduledTaskTargetList(date) {
	let today = moment(moment().format('YYYY-MM-DD'));
	let taskDate = moment(date);
	let endOfWeek = moment().endOf('week');
	let endOfNextWeek = moment().endOf('week').add(1, 'w');
	let endOfMonth = moment().endOf('month');
	let endOfNextMonth = moment().endOf('month').add(1, 'M');
	let endOfYear = moment().endOf('year');
	let targetListName;

	// console.log('The given date is ' + taskDate.diff(today, 'days') + ' days from now');
	// console.log('The given date is ' + endOfWeek.diff(date, 'days') + ' days from the end of this week');
	// console.log('The given date is ' + endOfNextWeek.diff(date, 'days') + ' days from the end of next week');
	// console.log('The given date is ' + endOfMonth.diff(date, 'days') + ' days from the end of this month');
	// console.log('The given date is ' + endOfNextMonth.diff(date, 'days') + ' days from the end of next month');
	// console.log('The given date is ' + endOfYear.diff(date, 'days') + ' days from the end of this year');

	if (taskDate.diff(today, 'days') < 0) {
		targetListName = 'Today';
	} else if (taskDate.diff(today, 'days') == 0) {
		targetListName = 'Today';
	} else if (taskDate.diff(today, 'days') == 1) {
		targetListName = 'Tomorrow';
	} else if (endOfWeek.diff(date, 'days') >= 0) {
		targetListName = 'Later This Week';
	} else if (endOfNextWeek.diff(date, 'days') >= 0) {
		targetListName = 'Next Week';
	} else if (endOfNextWeek.diff(date, 'days') < 0) {
	// } else if (endOfMonth.diff(date, 'days') >= 0) {
	// 	targetListName = 'Monthly Tasks';
	// } else if (endOfNextMonth.diff(date, 'days') >= 0) {
	// 	targetListName = 'Next Month';
	// } else if (endOfYear.diff(date, 'days') >= 0) {
	// 	targetListName = 'This Year';
	// } else if (endOfYear.diff(date, 'days') < 0) {
		targetListName = 'Someday';
	}

	// console.log('Task moved to: ' + targetListName);
	return targetListName;
}

function removeScheduledTaskDate(taskId) {
	let taskData = getTaskDataById(taskId),
		parentList = taskData.list,
		targetListEl = document.getElementById('draggable-list-item-' + taskId);

	if ((myLists[parentList].parentcategory == 'Scheduled' && (parentList != 'Today' && parentList != 'Tomorrow')) || myLists[parentList].parentcategory != 'Scheduled') {
		removeCalendarEvent(taskId);
		taskData.day = '';

		targetListEl.classList.remove('scheduled');
		targetListEl.querySelector('.task-date span').innerHTML = '';
	}

	saveDataToDB([parentList]);
}

function stripTags(str) {
	return str.replace(/(<([^>]+)>)/ig,"");
}

function insertMissingDefaultListsAndIndex() {
	for (list in defaultLists) {
		if (!myLists[list]) {
			myLists[list] = defaultLists[list];
		}
	}

	let orderIndex = 100;

	for (list in myLists) {
		if (myLists[list].order) {
			orderIndex = myLists[list].order;
		} else {
			orderIndex += 100;
			myLists[list].order = orderIndex;
		}
	}
}

function sortObjectByOrderIndex(obj) {
	let objectKeys = Object.keys(obj);

	objectKeys.sort(function(key1, key2) {
		if (obj[key1].order < obj[key2].order) {
			return -1;
		} else if (obj[key1].order > obj[key2].order) {
			return +1;
		} else {
			return 0;
		}
	});

	let tempObject = {};

	objectKeys.forEach(function(key, i) {
		tempObject[objectKeys[i]] = obj[objectKeys[i]];
		delete obj[objectKeys[i]];
	});


	objectKeys.forEach(function(key, i) {
		obj[objectKeys[i]] = tempObject[objectKeys[i]];
	});


	saveDataToDB();

	return obj;
}

function sortObjectByProperty(obj, property) {
	let objectKeys = Object.keys(obj);

	objectKeys.sort(function(key1, key2) {
		if (obj[key1][property] < obj[key2][property]) {
			return -1;
		} else if (obj[key1][property] > obj[key2][property]) {
			return +1;
		} else {
			return 0;
		}
	});

	let tempObject = {};

	objectKeys.forEach(function(key, i) {
		tempObject[objectKeys[i]] = obj[objectKeys[i]];
		delete obj[objectKeys[i]];
	});


	objectKeys.forEach(function(key, i) {
		obj[objectKeys[i]] = tempObject[objectKeys[i]];
	});

	return obj;
}


function editDateNote(info) {
	let date = info.dateStr,
		index = info.index,
		editorWrapper = document.getElementById('editor-wrapper');

	editorWrapper.classList.remove('hide');
	document.getElementById('editor-modal-wrapper').classList.add('visible');
	// document.querySelectorAll('.fc-day').forEach(function(el) {
	// 	if (el.style.backgroundColor == 'lightgrey') {
	// 		el.style.backgroundColor = 'unset';
	// 	}
	// });
	// info.dayEl.style.backgroundColor = 'lightgrey';

	if (!quill) {
		initQuill();

		// quill.keyboard.addBinding({ key: 'S', ctrlKey: true }, function(e) {
		//
		// 	let editorContainer = document.querySelector('.ql-editor');
		// 	let content = encodeURI(editorContainer.innerHTML);
		//
		// 	notes[editorWrapper.dataset.day] = content;
		// 	let day = editorWrapper.dataset.day;
		//
		// 	if (!notesFromDb.some(function(note, i) {
		// 		if (note.start == day) {
		// 			if (stripTags(decodeURI(content))) {
		// 				note.description = content;
		// 			} else {
		// 				notes[day] = undefined;
		// 				delete notes[day];
		// 				notesFromDb.splice(i, 1);
		// 			}
		// 		}
		//
		// 		return note.start == day;
		// 	})) {
		// 		if (stripTags(decodeURI(content))) {
		// 			notesFromDb.push({
		// 				title: day,
		// 				start: day,
		// 				description: content
		// 			});
		// 		}
		// 	}
		//
		// 	rerenderCalendar(notesCalendar, notesFromDb);
		// 	saveNotesToDB();
		// });
	}

	quill.focus();

	if (!notes[date]) notes[date] = []

	if (notes[date][index].content) {
		document.querySelector('.ql-editor').innerHTML = decodeURI(notes[date][index].content);
	} else {
// 		document.querySelector('.ql-editor').innerHTML = `<p><strong>${isHun ? 'Milyen pozitív dolgok történtek velem ma?' : 'What good happened to me today?'}</strong></p><p></p>
// <p><strong>${isHun ? 'Mit tanultam ma?' : 'What have I learned today?'}</strong></p><p></p>`;
	}

	editorWrapper.setAttribute('data-type', 'journal');
	editorWrapper.setAttribute('data-day', date);
	editorWrapper.setAttribute('data-index', index);
}

function assignScheduledTasksToLists(taskId) {
	if (taskId) {
		let task = getTaskDataById(taskId),
			list = task.list,
			category = task.category,
			i = myLists[list].categories[category].indexOf(task);

		moveScheduledTask(task, list, category, i);
	} else {
		// It's safer to loop trough object properties as per their keys as if the object gets updated (ex.: moveScheduledTask method) its property order might change during runtime
		Object.keys(myLists).forEach(list => {
			for (category in myLists[list].categories) {
				let categoryArr = myLists[list].categories[category],
					i = categoryArr.length - 1;

				// Because of the splice operator used we have to traverse the array in reverse
				while (i >= 0) {
					// console.log(list, category, i)

					let task = categoryArr[i];
					if (task.day && dayviewCalData.length > 0) moveScheduledTask(task, task.list, task.category, i);
					i--;
				}
			}
		})
	}


	function moveScheduledTask(task, list, category, i) {
		dayviewCalData.forEach(function(event) {
			if (event.id == task.id) {
				let oldList = task.list;

				let newList = setScheduledTaskTargetList(task.day);

				if (oldList != newList) {
					//console.log('This task should be in list: ' + newList);
					let taskToBeMoved = myLists[list].categories[category].splice(i, 1)[0];
					taskToBeMoved.list = newList;
					taskToBeMoved.listId = toId(newList);
					recalcListTimeSum(document.querySelector('#' + toId(oldList) + ' .' + task.categoryClass));
					recalcListTimeSum(document.querySelector('#' + task.listId + ' .' + task.categoryClass));
					myLists[newList].categories[category].push(taskToBeMoved);
					document.querySelector('#' + toId(newList) + ' .' + task.categoryClass).appendChild(document.getElementById('draggable-list-item-' + task.id));

					updateListItemCounter([oldList,newList]);
					showTaskAddMoveNotification(newList);
				}
			}
		});
	}

	saveDataToDB();
}

//document.getElementById('move-scheduled-tasks').addEventListener('click', );

function swapShownList(listId, taskId) {
	document.getElementById('left-sidebar').classList.remove('show-lists');
	hideAllLists();

	if (document.getElementById(listId)) {
		document.querySelector('#left-sidebar [data-listid="' + listId + '"]').classList.add('opened');
		document.getElementById(listId).classList.remove('hide');
		document.getElementById(listId).scrollTo({ top: 0, behavior: 'smooth' });

		if (taskId) {
			clearLabelFilter();
			let listEl = document.getElementById(`draggable-list-item-${taskId}`);
			listEl.scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
			listEl.classList.add('scrolled-into-view');
			setTimeout(() => {
				listEl.classList.remove('scrolled-into-view')
			}, 2000)
		}
	}

	if (window.innerWidth > 991 && document.body.classList.contains('inbox-visible')) document.getElementById('inbox').classList.remove('hide');
	toggleListViews()
}

function toggleListViews() {
	let isCalendarOpen = document.body.classList.contains('dayview-calendar-visible'),
		isSingleListVisible = document.querySelectorAll('.task-list-wrapper:not(.hide)').length == 1

	if (isCalendarOpen) {
		if (isSingleListVisible) {
			document.body.classList.remove('multi-list-view');
		} else {
			document.body.classList.add('multi-list-view');
		}

		document.body.classList.remove('board-view')
	} else {
		if (isSingleListVisible) {
			document.body.classList.remove('multi-list-view');
			document.body.classList.add('board-view')
		} else {
			document.body.classList.add('multi-list-view');
			document.body.classList.remove('board-view')
		}
	}
}

function hideAllLists() {
	document.querySelectorAll('.task-list-wrapper:not(.hide)').forEach(function(el) {
		el.classList.add('hide');
	});
	document.querySelectorAll('#left-sidebar [data-listid].opened').forEach(function(el) {
		el.classList.remove('opened');
	});
}


function getTaskDataById(id) {
	let taskData;

	for (list in myLists) {
		for (category in myLists[list].categories) {
			myLists[list].categories[category].forEach(function(task) {
				if (task.id == id) {
					taskData = task;
				}
			});
		}
	}

	return taskData;
}


function setTaskDataById(id, data) {
	for (list in myLists) {
		for (category in myLists[list].categories) {
			myLists[list].categories[category].forEach(function(task) {
				if (task.id == id) {
					task =  data;
				}
			});
		}
	}
}

function setTaskPropertyById(id, key, value) {
	for (list in myLists) {
		for (category in myLists[list].categories) {
			myLists[list].categories[category].forEach(function(task) {
				if (task.id == id) {
					task[key] =  value;
				}
			});
		}
	}
}

document.getElementById('show-dayview-calendar').addEventListener('click', showDayViewCalendar);

function showDayViewCalendar() {
	if (window.innerWidth < 769) resetDefaultView('calendar');

	if (!dayviewCalendar) {
		loadDayviewData().then(toggleCalendar);
	} else {
		toggleCalendar();
	}

	function toggleCalendar() {
		document.getElementById('show-dayview-calendar').classList.toggle('dayviewcal-open');
		document.getElementById('dayview-calendar').classList.toggle('hide');

		document.body.classList.toggle('dayview-calendar-visible');
		toggleBoardViewWhenCalendarIsOpen()

		// Temporary hack :(
		dayviewCalendar.prev();
		dayviewCalendar.next();

		if (window.innerWidth < 992) {
			document.querySelectorAll('.task-list-wrapper').forEach(function(list) {
				if (!list.classList.contains('hide')) {
					list.classList.toggle('hide-when-dayviewcal-open');
				}
			});
		}
	}
}

function toggleBoardViewWhenCalendarIsOpen() {
	if (document.body.classList.contains('dayview-calendar-visible')) {
		document.body.classList.remove('board-view');
	} else {
		if (document.querySelectorAll('.task-list-wrapper:not(.hide)').length != 1) {
			document.body.classList.remove('board-view');
		} else {
			document.body.classList.add('board-view');
		}
	}
}

function extractTaskDataForCalendar(eventEl) {
	let taskData = getTaskDataById(eventEl.dataset.id);
	let start = moment();
	let end = moment().add(taskData.time, 'm');
	let diff = end.diff(start);
	let duration = moment.utc(diff).format('HH:mm:ss');

	let event = {
		title: taskData.name,
		duration: duration,
		id: taskData.id
	};

	return event;
}

function log(text) {
	if (debugMode && userName === 'gabor231') {
		let debugBox = document.getElementById('debug-box');
		if (debugBox.classList.contains('hide')) debugBox.classList.remove('hide');
		debugBox.insertAdjacentHTML('afterbegin',text + '<br>');
	}
}

function extractTasksFromListCategories() {
	for (list in myLists) {
		for (category in myLists[list].categories) {
			myLists[list].categories[category].forEach(function(task, i, arr) {
				tasks[task.id] = task;
			});
		}
	}
}

function extractListStructure() {
	for (list in myLists) {
		if (!clonedMyLists[list]) {
			clonedMyLists[list] = {};
		}

		for (property in myLists[list]) {
			if (property != 'categories') {
				clonedMyLists[list][property] = myLists[list][property];
			} else {
				for (category in myLists[list].categories) {
					if (!clonedMyLists[list].categories) {
						clonedMyLists[list].categories = {};
					}

					for (category in myLists[list].categories) {
						if (!clonedMyLists[list].categories[category]) {
							clonedMyLists[list].categories[category] = {};
						}
					}
				}
			}
		}
	}
}

function addTasksIdsToLists() {
	for (task in tasks) {
		let taskObj = tasks[task]
		clonedMyLists[taskObj.list].categories[taskObj.category][taskObj.id] = true;
	}
}


let timer, currSeconds = 0;

function resetTimer() {
	if (timer !== 'undefined') {
		clearInterval(timer);
	}

	currSeconds = 0;
	timer = setInterval(startIdleTimer, 1000);
}

function startIdleTimer() {
	currSeconds++;

	if (currSeconds == 3) {
		saveNote();
		clearInterval(timer);
	}
}



document.getElementById("logout-link").addEventListener('click', logoutFromFirebase);

function loadlistdatafromdb(lists = 'all', user = userName) {
	var data = {
		action: 'loadlistdatafromdb',
		user: user,
		lists: lists
	};

	return jQuery.post(appVariables.ajax_url, data, function(response) {
		if (response.length == 0) return false;
		return JSON.parse(response);
	});
}

function savelistdatatodb(lists = Object.keys(myLists), isRemoval = false) {
	let listsData = {},
		data = new FormData();

	lists.forEach(function(list) {
		listsData[list] = myLists[list];

		// This conditional is present only for db structure change user data migration purposes
		if (Object.keys(listsData).length == Object.keys(listsData).length && !userOptions.migrated) {
			userOptions.migrated = 'true';
			updateUserOptions();
		}

		if (isRemoval) {
			listsData[lists] = {'dump': true};
		}
	});

	data.append('action', 'savelistdatatodb');
	data.append('user', userName);
	data.append('lists', JSON.stringify(listsData));
	data.append('remove', isRemoval);

	// var data = {
	// 	action: 'savelistdatatodb',
	// 	user: userName,
	// 	lists: listsData,
	// 	remove: isRemoval
	// };

	// return jQuery.post(appVariables.ajax_url, data, function(response) {
	// 	return response;
	// });

	return fetch(appVariables.ajax_url,{
		method: 'POST', // *GET, POST, PUT, DELETE, etc.
		// mode: 'cors', // no-cors, *cors, same-origin
		// cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
		credentials: 'same-origin', // include, *same-origin, omit
		// headers: {
		// 	// 'Content-Type': 'application/json'
		// 	'Content-Type': 'application/x-www-form-urlencoded',
		// },
		// redirect: 'follow', // manual, *follow, error
		// referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
		body: data
	}).then(function(response) {
		return response.text()
	}).catch(onDataInsertionFailed)
		.then(function (text) {
		log('Lists saved: ' + text);
		return text;
	});
}


// Daily review reminder
function dailyReviewReminder() {
	if ((userOptions && userOptions.first_run === 'done') && userOptions.last_review_date != moment().format('YYYY-MM-DD') && userOptions.daily_planning_reminder == 'on') {
		let date = new Date();

		userOptions.last_review_date = moment().format('YYYY-MM-DD');
		console.log("daily review");
		updateUserOptions();

		document.getElementById('modal-content').appendChild(document.getElementById('activity-review'));
		showModal()
	}

	if ((userOptions && userOptions.first_run !== 'done') && !userOptions.last_review_date && window.innerWidth > 991) {
		// showGreeting();
		userOptions.first_run = "done";
		updateUserOptions();
	}
}

// Daily list data backup
function dailyBackup() {
	if (!userOptions.last_backup_date || userOptions.last_backup_date != moment().format('YYYY-MM-DD')) {
		let date = new Date();

		updateWPUserBackup();
		//localStorage.setItem('nhance_userid_' + userId + '_backup', JSON.stringify(myLists));
		userOptions.last_backup_date = moment().format('YYYY-MM-DD');
		console.log("daily backup completed");
		updateUserOptions();
	}
}

function showGreeting() {
	document.getElementById('modal-content').appendChild(document.getElementById('first-run-greeting'));
	showModal()

	if (window.innerWidth > 1199) document.getElementById('modal-wrapper').classList.add('full-screen');
}
//
// document.getElementById('show-goals').addEventListener('click', function() {
// 	if (document.getElementById('modal').classList.contains('hide')) {
// 		showModal()
// 		showGoalsOverview();
// 	} else {
// 		clearModal();
// 	}
// });

function extractCompassData() {
	let tabularData = {
			roles: '#roles .editable-table-row',
			enhance: '#enhance-areas .editable-table-row'
		},
		listData = {
			principles: '#principles li',
			values: '#values li'
		};

	for (data in tabularData) {
		compassData[data] = {};
		document.querySelectorAll(tabularData[data]).forEach(function(el) {
			let key = el.querySelector('.table-role').innerText,
				value = el.querySelector('.table-goal').innerText;

			compassData[data][key] = value;
		});
	}

	for (data in listData) {
		compassData[data] = [];
		document.querySelectorAll(listData[data]).forEach(function (el) {
			if (data == 'principles') {
				compassData[data].push({
					name: el.innerText
				})
			} else {
				compassData[data].push(el.innerText);
			}
		});
	}
}

// document.addEventListener('visibilitychange', function(ev) {
// 	if (document.visibilityState === 'visible') reloadOnSyncNeeded();
// });
//window.addEventListener('focus', reloadOnSyncNeeded);

function showSubActions(e) {
	e.currentTarget.closest('.task-list-wrapper__actions').querySelector('.task-list-wrapper__sub-actions').classList.toggle('hide');
}

function showTaskSubActions(e) {
	e.stopPropagation();
	document.querySelectorAll('.draggable').forEach(el => el.classList.remove('task-actions-shown'));
	e.currentTarget.closest('.draggable').classList.toggle('task-actions-shown');
}


document.getElementById('add-task-btn').addEventListener('click', addNewInboxItem);

function addNewInboxItem() {
	var targetList = document.querySelector('#inbox .' + (userName != 'creator' ? 'optional' : 'enhance')),
		taskName = prompt(isHun ? 'Kérlek add meg a listaelem nevét:' : 'Please enter a name for your task:');

	if (taskName) initListElement({name: taskName, targetList: targetList, isFromUserInput: true});
}

document.querySelector('.cta-button--planning').addEventListener('click', function(e) {
	e.preventDefault();
	clearModal();
	startPlanning()
});

document.querySelectorAll('.cta-button--intro').forEach(el => {
	el.addEventListener('click', function (e) {
		e.preventDefault();
		if (document.getElementById('desktop-only-onboarding')) document.getElementById('desktop-only-onboarding').classList.add('hide');
		clearModal();
		startIntro()
	});
});

document.querySelector('#xd-walkthrough .close').addEventListener('click', function(e) {
	document.getElementById('xd-walkthrough').classList.add('hide');
});

// Example: injectSingleList('Áthozott feladatok szétosztásra', 'krisz');
function injectSingleList(listName, user) {
	if (myLists[listName]) {
		alert(isHun ? 'Ez a lista már szerepel a listáid között!' : 'This list is aleready within your lists!');
		return;
	}

	if (confirm(isHun ? 'Szeretnéd ezt a listát beimportálni?\n' + listName : 'Do you want to import the following list?\n' + listName)) {
		loadlistdatafromdb([listName], user).then(function(rawData) {
			const listData = JSON.parse(rawData),
				listName = Object.keys(listData)[0],
				data = listData[listName];

			myLists[listName] = data;
			initLists(listData);
			alert(isHun ? 'A következő lista sikeresen importálásra került:\n' + listName : 'The following list has been successfully imported:\n' + listName);
		});
	}
}

// Example: https://localhost/n-hance.io/app/?action=import&user=krisz&list=%C3%81thozott+feladatok+sz%C3%A9toszt%C3%A1sra
function parseListSharingURL() {
	const parsedURL = new URLSearchParams(window.location.search);
	console.log(parsedURL.toString());
	injectSingleList(parsedURL.get('list'), parsedURL.get('user'));
}

function buildListSharingURL(list) {
	const domain = window.location.origin,
		path = window.location.pathname;

	alert(domain + path + '?' + new URLSearchParams({
		action: 'import_list',
		user: userName,
		list: list
	}).toString());
}

document.getElementById('notification-icon').addEventListener('click', function(e) {
	document.getElementById('app-notifications').classList.toggle('hide');
	document.getElementById('notification-icon').classList.add('checked');
});

document.addEventListener('mousedown', e => {
	const modalOpenSelectors = [
			'.nav-wrapper__button-group__icon',
			'#chart-icon',
			'#help-icon',
			'#show-dayview-calendar',
			'#show-notes-calendar',
			'.task-name',
			'.add-button',
			'.forecast',
			'.filter-list',
			'.plan',
			'.add-task-cta-btn',
			'.pro-con',
			'.done',
			'.add-table-row',
			'.add-list-element',
			'.compass-element-edit-button',
			'.compass-element-remove-button',
			'.flatpickr-calendar'
		],
		modalIds = [
			'editor-wrapper',
			'word-cloud',
			'help-menu',
			'modal',
			'task-settings-modal'
		],
		isModalOpenSelector = modalOpenSelectors.some(selector => e.target.closest(selector)),
		isModalOpen = modalIds.some(id => !document.getElementById(id).classList.contains('hide')) || document.querySelector('.draggable.task-actions-shown');
		isModalClicked = modalIds.some(id => e.target.closest('#' + id));


	if (!isModalOpenSelector && isModalOpen && !isModalClicked) closeAllModals();
});

document.addEventListener('keydown', function(event){
	if(event.key === "Escape"){
		document.querySelectorAll('.app-overlay').forEach(function(overlay) {
			overlay.classList.add('hide');
		});

		closeAllModals();
	}

	if (event.keyCode === 13 && !document.getElementById('task-settings-modal').classList.contains('hide') && !event.shiftKey) {
		event.preventDefault();
		document.getElementById("save-task-settings").click();
	}

	if (event.altKey && event.key === '+') {
		addNewInboxItem();
	}

	if (dayviewCalendar && event.altKey && event.key === 'c') {
		showDayViewCalendar();
	}

	if (event.altKey && event.key === 'b') {
		document.body.classList.toggle('board-view');
	}

	if (event.altKey && event.key === 'a') {
		generateWordCloud('labels')
	}

	if (event.ctrlKey && event.key === 'o') {
		event.preventDefault();
		document.body.classList.toggle('show-prio');
	}

	if (event.ctrlKey && event.key === 'r') {
		event.preventDefault();
		document.body.classList.toggle('x-ray');
	}

	if (event.ctrlKey && event.key === 'p') {
		event.preventDefault();
		addNewPrinciple();
	}

	if (event.altKey && event.key === 't') {
		event.preventDefault();
		let taskId = listItemIdCounter;
		initListElement({name: prompt(isHun ? 'Add meg a feladat nevét' : 'Enter task name') , targetList: document.querySelector('#today .optional'), time: 0, isFromUserInput: true});

		document.querySelector(`#draggable-list-item-${taskId} .track-time`).click();
	}

	if (event.ctrlKey && event.key === 'e') addEvent();
});

function addLifeEvent() {
	let name = prompt(isHun ? 'Add meg az esemény nevét' : 'Enter event name'),
		date = prompt(isHun ? 'Add meg az esemény dátumát' : 'Enter event date', '2021-01-01'),
		targetListEl = document.querySelector('#inbox .must-do'),
		taskId = initListElement({name:  name, targetList: targetListEl, isFromUserInput: false, day: date});

	addCalendarEvent({
		title: name,
		start: moment(date).format('YYYY-MM-DD'),
		id: taskId,
		classNames: 'must-do'
	});

	assignScheduledTasksToLists(taskId);

	if (document.getElementById('awareness-map').classList.contains('show')) {
		initWheelOfLifeForPeriodSet();
	}
}

document.getElementById('save-task-settings').addEventListener('click', saveTaskSettings);
// document.querySelector('#task-settings-modal .close').addEventListener('click', saveTaskSettings);
flatpickr(document.querySelector('#task-settings-modal .day'), {
	onOpen: function(selectedDates, dateStr, instance) {
		instance.clear();
	},
	onClose: function(selectedDates, dateStr, instance) {
		assignDayToTask(selectedDates, dateStr, instance, document.getElementById('task-settings-modal').dataset.taskId);
	},
	disableMobile: true
});

function saveTaskSettings(e) {
	e.preventDefault();
	e.stopPropagation();
	let taskId = document.getElementById('task-settings-modal').dataset.taskId,
		time = 0,
		timeString = '',
		settingsModal = document.getElementById('task-settings-modal'),
		taskData = getTaskDataById(taskId),
		impactInput = settingsModal.querySelector('input[name="task_impact"]'),
		whyInput = settingsModal.querySelector('#task-settings__task-why-input'),
		taskListElement = document.querySelector(`li.draggable[data-id="${taskId}"]`);

	const formData = new FormData(document.getElementById('task-settings-form'));

	for (entry of formData.entries()) {
		let key = entry[0],
			value = Number(entry[1]);

		if (value != 0) {
			if (key == 'task_day') {
				time += 24 * 60 * value;
				if (value) timeString += value + (isHun ? 'n ' : 'd ');
			}

			if (key == 'task_hour') {
				time += 60 * value;
				if (value) timeString += value + (isHun ? 'ó ' : 'h ');
			}

			if (key == 'task_min') {
				time += value;
				timeString += convertMinutesToTimeString(value);
			}
		}
	}

	if (time) updateTaskTime(taskId, time, timeString);
	updateTaskName();
	settingsModal.querySelector('select[name="task_min"] option').selected =true;

	taskData.effect = impactInput.value;
	taskData.why = whyInput.value;

	for (let i = 0; i <= 5; i++) {
		settingsModal.classList.remove('i-' + i);
		taskListElement.classList.remove('i-' + i);
	}

	taskListElement.classList.add('i-' + taskData.effect);

	updateCalendarEventProperty(taskId, 'resourceId', (taskData.categoryClass == 'enhance' ? 'a' : (taskData.categoryClass == 'must-do' ? 'b' : 'c')) + (6 - taskData.effect));

	document.getElementById('task-settings-modal').classList.add('hide');
	document.getElementById('task-settings-modal-wrapper').classList.remove('visible');

	settingsModal.classList.remove('simple-task');
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-name-input').removeEventListener('blur', updateTaskName);
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-why-input').removeEventListener('blur', updateTaskWhy);
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-principle-input').removeEventListener('blur', updateTaskPrinciple);


	saveDataToDB([taskData.list]);
}

// document.querySelector('#task-input-group__impact-score input[type="range"]').addEventListener('input', e => {
// 	e.target.closest('.task-input-group').querySelector('output').value = e.target.value;
// });

document.querySelectorAll('#task-settings-modal button:not(#wpaicg-generate-button)').forEach(function(formBtn) {
	formBtn.addEventListener('click', function(e) {
		e.preventDefault();
	});
});

document.querySelector('#taks-settings-modal__subtask-generator svg').addEventListener('click', e => {
	let outputWrapper = document.getElementById('task-settings-modal__subtask-generation-output');

	document.getElementById('wpaicg-generate-button').click();
	outputWrapper.classList.remove('hide');
	if (!outputWrapper.querySelector('#wpaicg-prompt-content')) {
		outputWrapper.insertAdjacentElement('beforeend', document.querySelector('.wpaicg-prompt-content'));
	}
});

document.getElementById('wpaicg-prompt-clear').addEventListener('click', e => {
	document.getElementById('task-settings-modal__subtask-generation-output').classList.toggle('hide');
});

document.getElementById('wpaicg-generate-button').addEventListener('click', e => {
	document.getElementById('wpaicg-form-field-0').value = document.getElementById('task-settings__task-name-input').value;
});

// document.querySelector('#task-settings-modal .close').addEventListener('click', function() {
// 	document.getElementById('task-settings-modal').classList.add('hide');
// });

function updateTaskTime(taskId, time, timeString) {
	// Find parent list
	let listElement = document.querySelector('li[data-id="' + taskId +'"]');

	if (!listElement) return;

	let list = listElement.closest('.list-container'),
		el = getTaskDataById(taskId);

	listElement.querySelector('.time').innerText = timeString;
	// Update working memory object data
	el.time = time;

	// e.currentTarget.parentNode.dataset.time = el.time;
	// e.currentTarget.parentNode.style.height = Math.floor(e.currentTarget.parentNode.dataset.time / 15 * 58) + 'px';

	updateDayviewEventDateTime(taskId, el.day, true);

	// Update database
	saveDataToDB([list.closest('.task-list-wrapper').dataset.list]);
	recalcListTimeSum(list);
}

function showTaskSettingsModal(e) {
	let settingsModal = document.getElementById('task-settings-modal'),
		taskId = e.target.closest('li[data-id]').dataset.id,
		taskData = getTaskDataById(taskId),
		impactInput = settingsModal.querySelector('input[name="task_impact"]');
		// impactOutput = settingsModal.querySelector('output[name="task_impact_value"]');

	document.getElementById('task-settings-modal-wrapper').classList.add('visible');
	settingsModal.classList.remove('hide');

	if (myLists[taskData.list].simpleList) settingsModal.classList.add('simple-task');
	settingsModal.setAttribute('data-task-id', taskId);
	settingsModal.querySelector('#task-settings-modal__task-name input').value = taskData.name == 'New task' ? '' : taskData.name;

	if (taskData.name == 'New task') settingsModal.querySelector('#task-settings-modal__task-name input').focus();

	settingsModal.querySelector('.due-date').innerText = taskData.day || 'Set due date';

	settingsModal.querySelector(`.task-input-group--category input[value="${taskData.categoryClass}"]`).checked = true;

	// let dur = moment.duration(taskData.time, 'minutes');
	// settingsModal.querySelector('input[name="task_day"]').value = dur.days();
	// // settingsModal.querySelector('input[name="task_hour"]').value = dur.hours();
	// settingsModal.querySelector('input[name="task_min"]').value = taskData.time;
	// settingsModal.querySelector('input[name="task_min"]').focus();

	impactInput.value = taskData.effect;
	// impactOutput.value = taskData.effect;
	settingsModal.classList.add('i-' + taskData.effect);
	initTaskLabelMenu(settingsModal, taskData);
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-why-input').value = taskData.why || '';
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-principle-input').value = getTaskPrinciple(taskData);
	showRelevantPrinciples(taskData);
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-name-input').addEventListener('blur', updateTaskName);
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-why-input').addEventListener('blur', updateTaskWhy);
	document.querySelector(`#task-settings-modal[data-task-id="${taskId}"]`).querySelector('#task-settings__task-principle-input').addEventListener('blur', updateTaskPrinciple);

	settingsModal.querySelectorAll('input[name="task_category"]').forEach(input => {
		input.addEventListener('click', e => {
			let taskId = document.getElementById('task-settings-modal').dataset.taskId,
				taskData = getTaskDataById(taskId);

			move({
				listDBId: taskId,
				id: 'draggable-list-item-' + taskId,
				list: taskData.list,
				sourceList: taskData.categoryClass,
				category: taskData.category
			},
			`#${taskData.listId} .${e.currentTarget.value}`,
			true);
		});
	})
}

function updateTaskWhy(e) {
	if (e) e.stopPropagation();

	let settingsModal = document.getElementById('task-settings-modal'),
		taskId = settingsModal.dataset.taskId,
		taskData = getTaskDataById(taskId);

	taskData.why = e.currentTarget.value;
	saveDataToDB([taskData.list]);
}

function updateTaskPrinciple(e) {
	if (e) e.stopPropagation();

	let settingsModal = document.getElementById('task-settings-modal'),
		taskId = settingsModal.dataset.taskId,
		taskData = getTaskDataById(taskId),currentTaskName = taskData.name;

	// Remove old principle
	if (getTaskPrinciple(taskData)) {
		compassData.principles.forEach(function(principle, i) {
			if (principle.name == getTaskPrinciple(taskData)) {
				compassData.principles.splice(i, 1);
			}
		});
	}

	if (e.currentTarget.value) {
		addNewPrinciple(e.currentTarget.value, taskData.name, taskId);
	}
}

function updateTaskName(e) {
	if (e) e.stopPropagation();

	let settingsModal = document.getElementById('task-settings-modal'),
		taskId = settingsModal.dataset.taskId,
		taskData = getTaskDataById(taskId),currentTaskName = taskData.name,
		newTaskName = settingsModal.querySelector('#task-settings-modal__task-name input').value,
		taskListElement = document.querySelector(`li.draggable[data-id="${taskId}"]`);

	if (newTaskName && newTaskName != currentTaskName) {
		taskData.name = newTaskName;
		taskListElement.querySelector('.task-name__full__desc').innerText = newTaskName;
		saveDataToDB([taskData.list]);

		updateCalendarEventProperty(taskId, 'title', newTaskName);
	}
}

function convertMinutesToTimeString(min, timer) {
	let dur = moment.duration(min, 'minutes');
	let days = dur.days() > 0 ? dur.days() + (isHun ? 'n ' : 'd ') : '';
	let hours = dur.hours() > 0 ? dur.hours() : '';
	let minutes = dur.minutes() > 0 ? (hours ? '' : '0') + ':' + (dur.minutes() < 10 ? '0' + dur.minutes() : dur.minutes()) : (dur.hours() > 0 ? ':00' : '0:00');

	return (dur.months() > 0 ? dur.months() + 'm ' : '') + days + ((hours + minutes != '0:00' || timer) ? hours + minutes : '');
}


function showTaskAddMoveNotification(targetList) {
	let taskAddMoveNotification = document.getElementById('task-add-move-notification'),
		localizedList = isHun && trans_hu[targetList]  ? trans_hu[targetList] : list;

	taskAddMoveNotification.innerHTML = isHun ? 'Feladat hozzáadva a <b>' + localizedList + '</b> listához' : 'Task added to the <b>' + targetList + '</b> list'

	taskAddMoveNotification.classList.toggle('hidden');
	setTimeout(function() {
		taskAddMoveNotification.classList.toggle('hidden');
	},3000);
}

function updateListItemCounter(lists = Object.keys(myLists), label) {
	lists.forEach(function (list) {
		let listItemCounter = 0,
			listId = toId(list);

		for (category in myLists[list].categories) {
			if (label) {
				myLists[list].categories[category].forEach(task => {
					if (label != 'Unlabeled') {
						if (task.labels && task.labels.length > 0 && task.labels.indexOf(label) > -1) listItemCounter++
					} else {
						if (!task.labels || task.labels.length == 0) listItemCounter++
					}
				})
			} else {
				let labelFilter = document.body.dataset.filterLabel

				if (labelFilter) {
					myLists[list].categories[category].forEach(task => {
						if (task.labels && task.labels.length > 0 && task.labels.indexOf(labelFilter) > -1) listItemCounter++
					})
				} else {
					listItemCounter += myLists[list].categories[category].length;
				}
			}
		}

		let targetCounter = document.querySelector('#my-lists li[data-listid="' + listId + '"] .list-item-counter');

		targetCounter.innerText = listItemCounter > 0 ? listItemCounter : '';

		targetCounter.classList.add('updated');

		setTimeout(function () {
			targetCounter.classList.remove('updated');
		}, 1000)
	});
}

function initCompassTimeline() {
	let timeline = document.querySelector('#timeline-wrapper .timeline');

	timeline.innerHTML = '';

	if (dayviewCalData && dayviewCalData.length != 0) {
		document.getElementById('timeline-wrapper').classList.remove('hide');
		dayviewCalData.sort((a, b) => moment(a.start).diff(moment(b.start), 'days'));
		dayviewCalData.forEach(function(event, index) {
			let daysFromNow = moment(event.start).diff(moment(), 'days') + 1,
				today = moment().format('YYYY-MM-DD'),
				eventDate = moment(event.start).format('YYYY-MM-DD');

			if (daysFromNow > 0 && daysFromNow < 30 && event.start != today) {
				timeline.insertAdjacentHTML('beforeend', `
		<div class="container ${index % 2 == 0 ? 'left' : 'right'}">
		
			<div class="content">
				<h2>${eventDate}</h2>
				<p class="days-to-event-indicator">${isHun ? '' : 'in '} <b>${daysFromNow}</b> ${isHun ? ' nap múlva' : 'days'}</p>
				<p>${event.title}</p>
			</div>
		</div>`);
			}
		});

		if (timeline.querySelectorAll('.container').length > 0) document.getElementById('timeline-wrapper').classList.remove('hide');
	}
}

function getListItemCount(list) {
	let listItemCounter = 0;

	if (myLists[list]) {
		for (category in myLists[list].categories) {
			listItemCounter += myLists[list].categories[category].length;
		}
	}

	return listItemCounter;
}


function getNextAvailableId() {
	let highestId = 1;

	for (list in myLists) {
		for (category in myLists[list].categories) {
			myLists[list].categories[category].forEach(function(task) {
				if (task.id > highestId) {
					highestId = task.id;
				}
			});
		}
	}

	return ++highestId;
}

function toggleHelpMenu() {
	document.getElementById('help-menu').classList.toggle('hide')
}

document.querySelectorAll('#help-menu li').forEach(helpItem => {
	helpItem.addEventListener('click', toggleHelpMenu);
});

function showKeyBoardShortcuts() {
	document.getElementById('modal-content').innerHTML = '';
	document.getElementById('modal-content').insertAdjacentHTML('afterbegin', document.getElementById('keyboard-shortcut-list').innerHTML);
	showModal()
}

function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

function arraysEqual(a, b) {
	a = Array.isArray(a) ? a : [];
	b = Array.isArray(b) ? b : [];
	return a.length === b.length && a.every((el, ix) => el === b[ix]);
}

function allPropertiesEqual(a, b) {
	let equal = true;

	for (prop in a) {
		if (a[prop] != b[prop]) equal = false;
	}

	return equal;
}

function getRndInteger(min, max) {
	return Math.floor(Math.random() * (max - min + 1) ) + min;
}