function showOptions() {
    showModal();
    document.getElementById('modal-content').innerHTML = `
		<h3 class="user-options-header">${isHun ? 'Beállítások' : 'User Options'}</h3>
		<form id="user-options">
			<div class="user-options-navbar">
				<ul>
					<li data-settings-group="general">${isHun ? 'Általános' : 'General'}</li>
					<li data-settings-group="theme" class="beta-feature">${isHun ? 'Téma' : 'Theme'}</li>
					<li data-settings-group="labels">${isHun ? 'Címkék' : 'Labels'}</li>
				</ul>
			</div>
			<div class="user-options-settings">
				<div class="form-multi-group general">
					<h4>${isHun ? 'Általános' : 'General'}</h4>
					<div class="form-group beta-feature" style="display: none"> 
						<fieldset> 
							<legend class="setting-name">${isHun ? 'Nyelv' : 'Language'}</legend><input type="radio" id="lang_hu" name="language" value="hu" ${userOptions && userOptions.language == 'hu' ? 'checked' : ''}> 
							<label for="lang_hu">${isHun ? 'Magyar' : 'Hungarian'}</label> 
							<input type="radio" id="lang_en" name="language" value="en" ${userOptions && userOptions.language == 'en' ? 'checked' : ''}> 
							<label for="lang_en">${isHun ? 'Angol' : 'English'}</label> 
						</fieldset> 
					</div>
					<div class="form-group"> 
						<fieldset> 
							<legend class="setting-name">${isHun ? 'Hang' : 'Sound effect'}</legend> 
							<input type="radio" id="sound-on" name="sound-effect" value="on" ${userOptions && userOptions.sound_effect == 'on' ? 'checked' : ''}> 
							<label for="sound-on">${isHun ? 'Be' : 'On'}</label> 
							<input type="radio" id="sound-off" name="sound-effect" value="off" ${userOptions && userOptions.sound_effect == 'off' ? 'checked' : ''}> 
							<label for="sound-off">${isHun ? 'Ki' : 'Off'}</label> 
						</fieldset> 
					</div>
					<div class="form-group beta-feature"> 
						<label class="setting-name" for="workday-length">${isHun ? 'Munkanap hossza (óra)' : 'Working day length (hours)'}</label> 
						<input id="workday-length" type="number" min="1" value="${userOptions && userOptions.workday_length ? userOptions.workday_length : 6}"> 
					</div>
					<div class="form-group beta-feature"> 
						<label class="setting-name" for="baseline-progress-points">${isHun ? 'Napi cél haladási pont (e)' : 'Progress point goal (k)'}</label> 
						<input id="baseline-progress-points" type="number" min="100" step="50" value="${userOptions && userOptions.baseline_progress_points ? userOptions.baseline_progress_points : 1000}"> 
					</div>
					<div class="form-group beta-feature"> 
						<fieldset> 
							<legend class="setting-name">${isHun ? 'Hatás mutatása elvégzéskor' : 'Show impact on completion'}</legend><input type="radio" id="impact-popup-on" name="impact-popup" value="on" ${userOptions && userOptions.impact_popup == 'on' ? 'checked' : ''}> 
							<label for="impact-popup-on">${isHun ? 'Be' : 'On'}</label> 
							<input type="radio" id="impact-popup-off" name="impact-popup" value="off" ${userOptions && userOptions.impact_popup == 'off' ? 'checked' : ''}> 
							<label for="impact-popup-off">${isHun ? 'Ki' : 'Off'}</label> 
						</fieldset> 
					</div>
					<div class="form-group beta-feature"> 
						<fieldset> 
							<legend class="setting-name beta-feature">${isHun ? 'Napi tervezés emlékeztető' : 'Daily planning reminder'}</legend><input type="radio" id="daily-planning-reminder-on" name="daily-planning-reminder" value="on" ${userOptions && userOptions.daily_planning_reminder == 'on' ? 'checked' : ''}> 
							<label for="daily-planning-reminder-on">${isHun ? 'Be' : 'On'}</label> 
							<input type="radio" id="daily-planning-reminder-off" name="daily-planning-reminder" value="off" ${userOptions && userOptions.daily_planning_reminder == 'off' ? 'checked' : ''}> 
							<label for="daily-planning-reminder-off">${isHun ? 'Ki' : 'Off'}</label> 
						</fieldset> 
					</div>
					<div class="form-group beta-feature"> 
						<fieldset> 
							<legend class="setting-name">${isHun ? 'Prio nézet induláskor' : 'Prio view at startup'}</legend><input type="radio" id="prio-on" name="prio-default" value="on" ${userOptions && userOptions.prio == 'on' ? 'checked' : ''}> 
							<label for="prio-on">${isHun ? 'Be' : 'On'}</label> 
							<input type="radio" id="prio-off" name="prio-default" value="off" ${userOptions && userOptions.prio == 'off' ? 'checked' : ''}> 
							<label for="prio-off">${isHun ? 'Ki' : 'Off'}</label> 
						</fieldset> 
					</div>
					<div class="form-group"> 
						<fieldset> 
							<legend class="setting-name">${isHun ? 'Régi taskok jelölése' : 'Highlight old tasks'}</legend><input type="radio" id="x-ray-on" name="x-ray-mode" value="on" ${userOptions && userOptions.x_ray_mode == 'on' ? 'checked' : ''}> 
							<label for="x-ray-on">${isHun ? 'Be' : 'On'}</label> 
							<input type="radio" id="x-ray-off" name="x-ray-mode" value="off" ${userOptions && userOptions.x_ray_mode == 'off' ? 'checked' : ''}> 
							<label for="x-ray-off">${isHun ? 'Ki' : 'Off'}</label> 
						</fieldset> 
					</div>
					${function() {
        let output = '';

        if (userName == 'gabor231' || userName == 'creator' || userName == 'soma') {
            output = `
								<div class="form-group"> 
									<fieldset> 
										<legend class="setting-name">${isHun ? 'Tesztfunkciók' : 'Beta features'}</legend> 
										<input type="radio" id="beta-on" name="beta-features" value="on" ${userOptions && userOptions.beta_features == 'on' ? 'checked' : ''}> 
										<label for="beta-on">${isHun ? 'Be' : 'On'}</label> 
										<input type="radio" id="beta-off" name="beta-features" value="off" ${userOptions && userOptions.beta_features == 'off' ? 'checked' : ''}> 
										<label for="beta-off">${isHun ? 'Ki' : 'Off'}</label> 
									</fieldset> 
								</div>`;
        }

        return output;
    }()
    }
				</div>
				<div class="form-multi-group theme hide">
					<h4>${isHun ? 'Téma' : 'Theme'}</h4>
					<div class="form-group"> 
						<label class="setting-name" for="style">${isHun ? 'Stílus' : 'Select style'}</label>
						<select name="style" id="style-selector"> 
							<option value="dx-style" ${userOptions && userOptions.color_scheme == 'dx-style' ? 'selected' : ''}>N-hance</option> 
							<option value="light-style" ${userOptions && userOptions.color_scheme == 'light-style' ? 'selected' : ''}>Light</option> 
							<!-- <option value="wunderlist" ${userOptions && userOptions.color_scheme == 'wunderlist' ? 'selected' : ''}>Wunderlist</option> --> 
							<option value="mastermind" ${userOptions && userOptions.color_scheme == 'mastermind' ? 'selected' : ''}>BPR</option>
						</select>
					</div>
				</div>
				<div class="form-multi-group labels hide">
					<h4>${isHun ? 'Saját címkék' : 'Custom labels'}</h4>
                    <ul>${listCustomLabels()}</ul>
                    <button class="default-cta-button">${isHun ? 'Új címke' : 'Add new'}</button>
				</div>
			</div>
		</form>`;

    document.querySelector('#style-selector').addEventListener('change', changeStyle);
    document.querySelector('#baseline-progress-points').addEventListener('change', updateProgressPointGoal)
    document.querySelector('#workday-length').addEventListener('change', updateWorkdayLength);document.querySelectorAll('[name="prio-default"]').forEach(function(radio) {radio.addEventListener('change', togglePrioViewOnStartup)});
    document.querySelectorAll('[name="sound-effect"]').forEach(function(radio) {radio.addEventListener('change', toggleSoundEffect)});
    document.querySelectorAll('[name="daily-planning-reminder"]').forEach(function(radio) {radio.addEventListener('change', toggleDailyPlanningReminder)});
    document.querySelectorAll('[name="impact-popup"]').forEach(function(radio) {radio.addEventListener('change', toggleImpactPopup)});
    document.querySelectorAll('[name="language"]').forEach(function(radio) {radio.addEventListener('change', toggleLanguage)});
    document.querySelectorAll('[name="x-ray-mode"]').forEach(function(radio) {radio.addEventListener('change', toggleXRayMode)});
    document.querySelectorAll('[name="beta-features"]').forEach(function(radio) {radio.addEventListener('change', toggleBetaFeatures)});
    document.querySelectorAll('.user-options-navbar li').forEach(function(tab) {
        tab.addEventListener('click', function(e) {
            let form = document.getElementById('user-options');

            form.querySelectorAll('.form-multi-group').forEach(function(group) {
                group.classList.add('hide');
            });

            form.querySelector('.form-multi-group.' + tab.dataset.settingsGroup).classList.remove('hide');
        });
    });

    addSettingsMenuLabelHandlers();
}

function listCustomLabels() {
    // Load principle labels from compass
    let customLabels = [],
        customLabelsHTML = '';

    // Load custom labels from compass
    if (compassData.labels) {
        compassData.labels.forEach(function(label) {
            if (label && customLabels.indexOf(label) == -1) {
                customLabels.push(label);
            }
        });
    }

    if (customLabels) {
        customLabels.forEach(function(label) {
            customLabelsHTML += '<li class="label">' + label + '</li>';
        });
    }

    return customLabelsHTML;
}

function addNewCustomLabelFromSettings(e) {
    e.preventDefault();
    let labelName = prompt(isHun ? 'Kérlek add meg a címke nevét:' : 'Add new label name');

    if (labelName) {
        if (!compassData.labels) compassData.labels = [];
        if (compassData.labels.indexOf(labelName) == -1) {
            compassData.labels.push(labelName);
            saveCompassToDB();

            document.querySelector('#user-options .form-multi-group.labels ul').insertAdjacentHTML('beforeend', `<li class="label">${labelName}</li>`);

            document.querySelector('#user-options .form-multi-group.labels .label:last-child').addEventListener('click', removeCustomLabel);

        } else {
            alert(isHun ? 'Ez a címke már létezik' : 'This label already exists')
        }
    }
}

function addSettingsMenuLabelHandlers() {
    document.querySelectorAll('#user-options .form-multi-group.labels .label').forEach(function(label) {
        label.addEventListener('click', removeCustomLabel);
    });

    document.querySelector('#user-options .form-multi-group.labels .default-cta-button').addEventListener('click', addNewCustomLabelFromSettings);
}

function removeCustomLabel(e) {
    let customLabel = e.currentTarget.innerText;

    if (confirm(isHun ? 'Tényleg szeretnéd törölni ezt a címkét?' : 'Do you really want to delete this label?')) {
        e.currentTarget.remove();
        if (compassData.labels) {
            compassData.labels.forEach(function(label, i) {
                if (label ==  customLabel) {
                    compassData.labels.splice(i, 1);
                    saveCompassToDB();
                }
            });
        }
    }
}