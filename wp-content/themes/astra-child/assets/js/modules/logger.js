function logTaskTime(taskId, trackedTime) {
    let taskData = getTaskDataById(taskId),
        today = moment().format('YYYY-MM-DD');

    if (!compassData.dailyLog) compassData.dailyLog = {};

    if (!compassData.dailyLog[today] || typeof compassData.dailyLog[today] != 'object') {
        compassData.dailyLog[today] = [];
    }

    // let lastLogEntry = compassData.dailyLog[today][compassData.dailyLog[today].length - 1];
    taskData.completionTime = moment().format('HH:mm');

    // If this is an update, the id of the last entry matches the current task id and the previous save happened a minute ago we know that we can safely update the time
    // if (lastLogEntry && isUpdate && lastLogEntry.id == taskId && lastLogEntry.trackedTime + 1 == trackedTime) {
    //     lastLogEntry.trackedTime = trackedTime;
    //     saveCompassToDB();
    //     return;
    // }

    compassData.dailyLog[today].push(JSON.parse(JSON.stringify(taskData)));
    let lastLogEntry = compassData.dailyLog[today][compassData.dailyLog[today].length - 1];

    // Upon task completion we offer to save the note so it is not needed in the log
    delete lastLogEntry.note;
    lastLogEntry.trackedTime = trackedTime;

    saveCompassToDB();
}

function logTaskMoves(taskData) {
    if (!taskData.moveCount) taskData.moveCount = 0;
    taskData.moveCount++;
}

function logDayData(days, filter) {
    let entriesWrapper = document.querySelector('#chart-overlay__charts .chart-group:last-of-type .chart-group__entries-list'),
        logKeys = Object.keys(compassData.dailyLog).reverse(),
        lastDayIncluded = moment().subtract(days, 'days').format('YYYY-MM-DD');

    entriesWrapper.innerHTML = '';

    logKeys.forEach(day => {
        let isAfterLastIncludedDay = moment(day).diff(lastDayIncluded, 'days');

        if (isAfterLastIncludedDay > 0) {
            listDayEntries(entriesWrapper, day, filter);
        }
    });
}

function modifyTimeEntry(e) {
    e.stopPropagation();
    let target = e.currentTarget,
        parent = target.closest('.time-log-entry'),
        name = parent.querySelector('.entry-name').innerText;

    if (parent.dataset.completionTime) {
        let completionTime = parent.dataset.completionTime,
            date = target.closest('.chart-group__entries-list__day-group').dataset.day;

        let updatedTrackedTime = prompt(isHun ? 'Szeretnéd módosítani a rögzített időt? (formátum: óra:perc)' : 'Do you want to modify the tracked time? (format: hour:min)', '0:15');

        if (updatedTrackedTime) {
            compassData.dailyLog[date].forEach((entry, i) => {
                if (entry.completionTime == completionTime && entry.name == name) {
                    if (updatedTrackedTime != 0) {
                        let splitTime = updatedTrackedTime.split(':'),
                            hours = splitTime[0],
                            mins = splitTime[1];

                        entry.trackedTime = hours * 60 + Number(mins);
                    }

                    saveCompassToDB();
                    // Sorry, I am fckin tired
                    document.getElementById('chart-icon').click();
                    document.getElementById('chart-icon').click();
                }
            });
        }
    }
}

function deleteTimeEntry(e) {
    e.stopPropagation();
    let target = e.currentTarget,
        parent = target.closest('.time-log-entry'),
        name = parent.querySelector('.entry-name').innerText;

    if (parent.dataset.completionTime) {
        let completionTime = parent.dataset.completionTime,
            date = target.closest('.chart-group__entries-list__day-group').dataset.day;

        if (confirm(isHun ? 'Tényleg szeretnéd törölni ezt a sort?' : 'Do you really want to delete this entry?')) {
            compassData.dailyLog[date].forEach((entry, i) => {
                if (entry.completionTime == completionTime && entry.name == name) {
                    compassData.dailyLog[date].splice(i, 1);

                    saveCompassToDB();
                    // Sorry, I am fckin tired
                    document.getElementById('chart-icon').click();
                    document.getElementById('chart-icon').click();
                }
            });
        }
    }
}

function listDayEntries(entriesWrapper, day, filter) {
    let totalTime = 0,
        entries,
        filteredEntries = [];

    if (filter != 'all') {
        filteredEntries = compassData.dailyLog[day].filter(entry => entry.labels && entry.labels.indexOf(filter) > -1);
    }

    entries = filter != 'all' ? filteredEntries : compassData.dailyLog[day];
    if (entries.length == 0) return;

    function getMins(timeStr) {
        let timeArr = timeStr.split(':');

        return Number(timeArr[0]) * 60 + Number(timeArr[1]);
    }

    entries.sort(function(a, b) {
        return getMins(a.completionTime) - getMins(b.completionTime);
    });
    entriesWrapper.insertAdjacentHTML('beforeend', `
        <div class="chart-group__entries-list__day-group" data-day="${day}">
            <h3 class="day-header">${day}</h3>
            <div class="day-group__entries">
                ${getEntryRow((isHun ? 'NÉV' : 'TITLE'), (isHun ? 'BEFEJEZVE' : 'COMPLETED AT'), (isHun ? 'IDŐTARTAM' : 'DURATION'))}
            </div>
        </div>
    `);

    let dayGroup = entriesWrapper.querySelector('.chart-group__entries-list__day-group:last-of-type .day-group__entries');

    entries.forEach(function(entry) {
        totalTime += Number(entry.trackedTime || entry.time);
        dayGroup.insertAdjacentHTML('beforeend', getEntryRow(entry.name, entry.completionTime, convertMinutesToTimeString(entry.trackedTime || entry.time), entry.category));
    });

    dayGroup.insertAdjacentHTML('beforeend', getEntryRow((isHun ? 'ÖSSZESEN' : 'TOTAL'), '', convertMinutesToTimeString(totalTime)));
    // console.log('Total time tracked on ' + day + ' is ' + convertMinutesToTimeString(totalTime));
}

function getEntryRow(name, completionTime, trackedTime, category = '') {
    return `<div class="time-log-entry" data-completion-time="${completionTime}">
                <div class="entry-name">${name}<span class="entry-category" data-category="${category}"></span></div>
                <div class="completion-time">${completionTime}</div>
                <div class="tracked-time">
                    ${trackedTime}
                </div>
                <div class="time-log-entry__button-group">
                    <button class="edit-button default-cta-button" onclick="modifyTimeEntry(event)">
                        <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/edit-task-settings.svg">
                    </button>
                    <button class="remove-button default-cta-button" onclick="deleteTimeEntry(event)">
                        <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon.svg">
                    </button>
                </div>
            </div>`;
}