/**
 * Set the html tags text or html attributes to the language in the parameter based on map.json file.
 * - text: append the convenient language text to the html tag content.
 * - *: set the * attribute to the convenient language text.
 * - before: if true, the convenient language text will be added before the html tag content.
 * @param language the shortened version of the language
 */
function setLanguage(language){
    const lan = language
    setBodyClassLanguage(lan);
$.getJSON("../wp-content/themes/astra-child/assets/js/modules/language/map.json",function (data){
    data.forEach((entity)=>{
        let tag = document.getElementById(entity.id);
        if (tag!=null) {
            if (entity.text) {
                if (!entity.before) {
                    tag.innerHTML += (entity.text[lan]);
                } else {
                    console.log(entity.id);
                    let after = tag.innerHTML;
                    tag.innerHTML = (entity.text[lan]) + after;
                }
            } else {
                let attributeName = (Object.getOwnPropertyNames(entity)[1]);
                let attributeValue = entity[attributeName][lan];
                tag.setAttribute(attributeName, attributeValue);
            }
        } else console.warn("failed to set language for "+entity.id);

    })
})
}

/**
 * If the language is hungarian, add the "hu" class to the body
 * @param language
 */
function setBodyClassLanguage(language){
    if (language=="hu"){
        document.body.classList.add("hu");
    }
}