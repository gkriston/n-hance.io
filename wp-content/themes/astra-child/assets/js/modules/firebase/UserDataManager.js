let firebaseConnection;
let firebaseDatabase;

/**
 * Initialize the firebase connection.
 *
 * @returns {Object} The firebase object with initialized connection.
 */
async function initializeFirebase(){
    let firebaseConfig;

    if (window.location.href.indexOf('localhost') > -1) {
        firebaseConfig = {
            apiKey: "AIzaSyATZB-dmzwFIsYkZPUPMGh8wxZLBpuUM30",
            authDomain: "n-hance-staging.firebaseapp.com",
            databaseURL: "https:/n-hance-staging-default-rtdb.europe-west1.firebasedatabase.app",
            projectId: "n-hance-staging",
            storageBucket: "n-hance-staging.appspot.com",
            messagingSenderId: "476624282887",
            appId: "1:476624282887:web:1b9f5020d296d08ba091a2"
        };
    } else {
        firebaseConfig = {
            apiKey: "AIzaSyAjjxXVt5E7kUwI75jIIX8D_UPfGeqFGQA",
            authDomain: "n-hance-test.firebaseapp.com",
            databaseURL: "https://n-hance-test-default-rtdb.europe-west1.firebasedatabase.app",
            projectId: "n-hance-test",
            storageBucket: "n-hance-test.appspot.com",
            messagingSenderId: "935377140927",
            appId: "1:935377140927:web:7387b11e6b415e3c5b1a0a",
            measurementId: "G-FD37L7NNK6"
        };
    }



    // Initialize Firebase
    firebase.initializeApp(firebaseConfig)
    firebase.analytics();
    firebaseConnection = firebase;
    firebaseDatabase = firebase.firestore();
    // if (location.hostname === "localhost") {
    //     firebaseDatabase.useEmulator("localhost", 8080);
    // }
    console.log("logged in to firebase");

    // Initialize offline support
    // firebase.firestore().enablePersistence()
    //     .catch((err) => {
    //         if (err.code == 'failed-precondition') {
    //             // Multiple tabs open, persistence can only be enabled
    //             // in one tab at a a time.
    //             // ...
    //         } else if (err.code == 'unimplemented') {
    //             // The current browser does not support all of the
    //             // features required to enable persistence
    //             // ...
    //         }
    //     });


    return firebase;
}

/**
 * Login or create and login to firebase authentication
 * Login if exists to the firebase authentication with the user_email and sha256 value of user_pass.
 * Otherwise call autoCreateFirebaseUser() to create user.
 *
 * @param first_run only for correct display of first run guide (optional parameter)
 * @returns {Promise<T>} returns the logged in user as a promise object
 */
async function autoLoginToFirebase() {
    if (firebaseConnection.auth().currentUser == null) {
        let userData = appVariables.userLoginData,
            promise = await firebaseConnection.auth().signInWithEmailAndPassword(userData.user_email, userData.user_pass)
            .then((user)=>{
                console.log(user);
                return user;
            })
            .catch((error) => {
                console.log(error);
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode === "auth/user-not-found") {
                    console.log("AutoCreatingFirebaseUser...");
                    return autoCreateFirebaseUser();
                }
            });

        return promise;
    } else {
        return Promise.resolve(firebaseConnection.auth().currentUser);
    }
}

/**
 * Create a user in firebase authentication and insert the default datas and browserid into it.
 *
 * @returns {Promise<T>}  the promise object of user creation.
 */
function autoCreateFirebaseUser(){
    let userData = appVariables.userLoginData;
    return firebaseConnection.auth().createUserWithEmailAndPassword(userData.user_email, userData.user_pass)
        .then(() => {
            return insertFirebaseUserData();
        })
        .catch((error) => {
            var errorCode = error.code;
            var errorMessage = error.message;
        });
}

/**
 * Insert user data into firebase
 * @Important this function depends on the firebase configuration, maybe required to call this function immediately after first login
 * @returns {Promise<T>} The return of the firebase set function
 */
function insertFirebaseUserData(){
    let users = firebaseDatabase.collection("users");
    var doc = {
        userName: appVariables.userName,
        firebaseUID: firebaseConnection.auth().currentUser.uid,
        registerDate: moment().format('YYYY MMMM Do, h:mm:ss a')
    }
    return users.doc(userId).set(doc,{merge: true}).then(() => {
        console.log('User successfully created');
        return true;
    });
}

//TODO: refactor the document identification to an other random or pseudo random string or create and manage it by firebase.auth
/**
 * Save and create a random browser id
 * @returns {Promise<T>} the saving methods result
 */
function saveCurrentBrowserId() {
    if (!randomBrowserId) {
        let newRandomBrowserId = Math.random().toString(36).substring(7);
        let date = new Date();
        document.cookie = 'randbrowserid=' + newRandomBrowserId + '; expires=' + new Date(new Date().setMonth(date.getMonth() + 1)) + '; path=' + appPath;
        randomBrowserId = newRandomBrowserId;
    }
    return autoLoginToFirebase().then((res) => {
        console.log("Current browser id: " + randomBrowserId);
        let users = firebaseDatabase.collection("users");
        var doc = {
            randbrowserid: randomBrowserId,
        }
        let result = users.doc(userId).set(doc, {merge: true})
        return result;
    });
}

/**
 * Get the user's last browser id stored in firebase firestore
 *
 * @returns {String} Last browser id from firebase
 */
function getlastbrowserid() {
    return autoLoginToFirebase().then(() => {
        let users = firebaseDatabase.collection("users");
        return users.doc(userId).get("randbrowserid").then(doc => {
            var browserid = doc.get("randbrowserid");
            return browserid;
        })
    });
}

/**
 * Save the useroptions from the parameter
 * @param userOpt
 * @returns {Promise<*>}
 */
function saveUserOptions(userOptions){
    return updateDocumentData('', {userOptions: userOptions});
}

/**
 * Get the useroptions from firebase.
 * @param first_run only for correct display of first run guide (optional parameter)
 * @returns {Promise<*>}
 */
async function getUserOptions(){
   return getDocumentData('', 'userOptions')
       .then((data) => {
           if (data) {
               // Listen to root level user data changes (compassData, dayviewCalData, notes)
               return data;
           } else {
               console.log("No user options data available");
           }
       })
       .catch((error)=>{
           console.log(error);
       });
}

/**
 * Sign out from firebase authentication.
 *
 * @returns { Promise<void> } The return of firebase signOut() method.
 */
function logoutFromFirebase() {
    return firebaseConnection.auth().signOut();
}


function getCollectionData(path) {
    return firebaseDatabase.collection(`/users/${userId}/${path}`)
        .get()
        .then((docs) => {
            let data = {};

            docs.forEach(doc => {
                data[doc.id] = doc.data();
            });

            console.log(data);
            return data;
        }).catch((error) => {
            console.log("Error getting document:", error);
        });
    ;
}

function attachCollectionDataChangeListener(path) {
    return firebaseDatabase.collection(`/users/${userId}/${path}`)
        .onSnapshot({includeMetadataChanges: true}, docs => {
            if (!docs.metadata.hasPendingWrites) {
                let data = {};

                docs.forEach(doc => {
                    data[doc.id] = doc.data();
                });

                docs.docChanges().forEach(change => {
                    let data = change.doc.data();

                    if (change.type === "added") {
                        console.log("Added: ", data);
                    }
                    if (change.type === "modified") {
                        console.log("Modified: ", data);
                    }
                    if (change.type === "removed") {
                        console.log("Removed: ", data);
                    }
                });

                console.log(data);
            }
        });
}

function attachListsDataChangeListener() {
    firebaseDatabase.collection(`/users/${userId}/myLists`)
        .onSnapshot({includeMetadataChanges: true}, docs => {
            if (!docs.metadata.hasPendingWrites) {
                let rerenderCalendar;

                docs.docChanges().forEach(change => {
                    let listName = change.doc.id,
                        data = change.doc.data();

                    if (data.parentcategory) {
                        if (change.type === "added") {
                            if (!myLists[listName]) createNewList(listName, data, false, true);
                        }
                        if (change.type === "modified") {
                            myLists[listName].categories = data.categories;

                            for (category in myLists[listName].categories) {
                                myLists[listName].categories[category].forEach(listEl => {
                                    if (listEl.day) rerenderCalendar = true;
                                });
                            }

                            updateListCategoriesView(listName, data.categories, true);
                            updateListItemCounter([listName]);
                            console.log(listName + ' list updated');
                        }
                        if (change.type === "removed") {
                            deleteList(listName, true)
                        }
                    }
                });

                if (rerenderCalendar) initDayViewCalendar(true);
            }
        });
}

function getDocumentData(path, field) {
    return firebaseDatabase.doc(`/users/${userId}` + (path ? `/${path}` : ''))
        .get()
        .then(doc => {
            if (doc.data()) {
                if (field && doc.data().hasOwnProperty(field)) {
                    return doc.data()[field];
                } else {
                    return doc.data();
                }
            } else {
                return false;
            }
        });
}

function setDocumentData(path, data) {
    return firebaseDatabase.doc(`/users/${userId}` + (path ? `/${path}` : ''))
        .set(data, {merge: true});
}

function updateDocumentData(path, data) {
    return firebaseDatabase.doc(`/users/${userId}` + (path ? `/${path}` : ''))
        .update(data);
}

function addDocumentToCollection(path, data) {
    return firebaseDatabase.collection(`/users/${userId}` + (path ? `/${path}` : ''))
        .add(data);
}

function removeDocumentData(path, field) {
    let finalPath = `/users/${userId}` + (path ? `/${path}` : '');

    if (!field) {
        return firebaseDatabase.doc(finalPath)
            .delete();
    } else {
        return firebaseDatabase.doc(finalPath)
            .update({
                [`${field}`] : firebase.firestore.FieldValue.delete()
            });
    }
}

async function removePropertyFromField(path, field, key) {
    let data = await getDocumentData(path);

    for (property in data[field]) {
        if (property == key) delete data[field][property];
    }

    await firebaseDatabase.doc(`/users/${userId}` + (path ? `/${path}` : ''))
        .set(data);

    return data;
}

function syncListsToFireStore(lists = Object.keys(myLists), isRemoval) {
    lists.forEach(list => {
        if (isRemoval) {
            removeDocumentData(`myLists/${list}`);
        } else if (myLists[list]) {
            setDocumentData(`myLists/${list}`, myLists[list]);
        }
    });
}

function attachDocumentDataChangeListener(path) {
    return firebaseDatabase.doc(`/users/${userId}` + (path ? `/${path}` : ''))
        .onSnapshot({includeMetadataChanges: true}, doc => {
            var source = doc.metadata.hasPendingWrites ? "Local" : "Server";
            console.log(source, " data: ", doc.data());

            if (source == 'Server') {
                // If compassData has changed trigger view update
                if (doc.data().compassData != JSON.stringify(compassData)) {
                    console.log('compass update');
                    let data = isJson(doc.data().compassData) ? JSON.parse(doc.data().compassData) : doc.data().compassData;

                    processCompassData(data, true);
                }

                // If notes have changed trigger view update
                if (doc.data().notes != JSON.stringify(notes)) {
                    console.log('notes update');
                    notes = isJson(doc.data().notes) ? JSON.parse(doc.data().notes) : doc.data().notes;
                    loadNotesToCalendar();
                }

                // If notes have changed trigger view update
                if (dayviewCalendar && doc.data().dayviewCalData && !arraysEqual(doc.data().dayviewCalData, dayviewCalData)) {
                    let rerenderCalendar;

                    dayviewCalData.forEach(ev => {
                        let match = doc.data().dayviewCalData.find(el => ev. id == el.id);

                        if (!match || !allPropertiesEqual(ev, match)) {
                            rerenderCalendar = true;
                            console.log('matching element with unmatching start');
                        }
                    });

                    if (rerenderCalendar) {
                        console.log('calendar update');
                        dayviewCalData = doc.data().dayviewCalData;
                        initDayViewCalendar(true);
                    }
                }

                // Stop timetracking if stopped on another device
                if (doc.data().userOptions.timeTracking && userOptions.timeTracking && !allPropertiesEqual(doc.data().userOptions.timeTracking, userOptions.timeTracking)) {
                    userOptions.timeTracking = doc.data().userOptions.timeTracking;

                    if (userOptions.timeTracking.trackingNow == true) {
                        continueTimeTracking();
                    } else {
                        stopCurrentTimeTracking(false, true);
                    }
                }
            }

            return doc.data();
        });
}


