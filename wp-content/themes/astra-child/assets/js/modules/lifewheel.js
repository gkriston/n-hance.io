document.getElementById('show-wheel-of-life').addEventListener('click', toggleWheelOfLife);

function toggleWheelOfLife() {
    if (window.innerWidth < 769) resetDefaultView('lifewheel');

    let map = document.getElementById('awareness-map'),
        wheel = $('#spidergraphcontainer');

    if (map.classList.contains('show')) {
        map.classList.remove('show');
        document.body.classList.remove('lifewheel-visible');
        hideWordCloud();
        document.getElementById('awareness-map').classList.remove('help');

        if (document.body.classList.contains('interactive-planning-mode')) {
            document.querySelector('.introjs-skipbutton').click();
        }

        userOptions.wheel_data = wheel.data('spidergraph').activedata.data;
        updateUserOptions();
    } else {
        initWheelOfLifeForPeriodSet();
        map.classList.add('show');
    }
}

document.querySelector('[name="range1"]').addEventListener('input', initWheelOfLifeForPeriodSet);
function initWheelOfLifeForPeriodSet() {
    let value = Number(document.querySelector('[name="range1"]').value),
        currentYear = Number(dateToday.split('-')[0][3]);

    updateWheelRange(value);

    switch (value) {
        case 2:
            initWheelOfLife('202' + (currentYear) + '-01-01', 5);
            break;
        case 3:
            initWheelOfLife(userOptions.birth_date || '1984-05-29', 80);
            break;
        default:
            initWheelOfLife('202' + currentYear + '-01-01');
    }
}


function editRoleFromWheel(e) {
   let role = e.currentTarget.closest('.menu-item').dataset.role;
   document.querySelector(`.table-role[data-area="${role}"] .edit-table-row`).click();
}

function removeRoleFromWheel(e) {
    let role = e.currentTarget.closest('.menu-item').dataset.role;
    document.querySelector(`.table-role[data-area="${role}"] .remove-table-row`).click();
}

function removeLifeEvent(e) {
    let taskId = e.currentTarget.closest('.menu-item').dataset.taskId;
    document.querySelector(`li.draggable[data-id="${taskId}"] .del`).click();

    if (document.body.classList.contains('interactive-planning-mode') || document.getElementById('awareness-map').classList.contains('show')) {
        initWheelOfLifeForPeriodSet();
    }
}

document.querySelector('#awareness-map .close').addEventListener('click', toggleWheelOfLife);
document.querySelector('#awareness-map .help').addEventListener('click', e => {
    document.getElementById('awareness-map').classList.toggle('help');
});

function initWheelOfLife(startDate = '2021-01-01', years = 1) {
    document.querySelector('#awareness-map__wheel-wrapper').innerHTML = `
			<div id="aw-map__life-events">
			    <button class="add-button add-life-event-button default-cta-button" onclick="addLifeEvent()">
                    <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/plus-icon.svg">
                </button>	
				<ul>
				</ul>
				<div id="aw-map__roles-and-values-wrapper">
					<div id="aw-map__roles">
                        <button class="add-button add-role-button default-cta-button" onclick="generateWordCloud('roles')">
                            <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/plus-icon.svg">
                        </button>
						<ul>
						</ul>
					</div>
					<hr>
					<div id="aw-map__values">
					    <button class="add-button add-value-button default-cta-button" onclick="generateWordCloud('values')">
                            <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/plus-icon.svg">
                        </button>
						<ul>
						</ul>
					</div>
					<div id="spidergraphcontainer">
					</div>
				</div>
			</div>`;

    $('#awareness-map').addClass('show');
    document.body.classList.add('lifewheel-visible');

    let wheel = $('#spidergraphcontainer'),
        wheelData = [3,3,3,3,3,3,3];

    wheel.spidergraph({
        'fields': isHun ? ['Egészség', 'Elégedettség', 'Kapcsolatok', 'Szabadidő', 'Karrier', 'Pénzügyek', 'Mások segítése',] : ['Physical Body', 'Emotions & Meaning', 'Relationships', 'Free time', 'Career/Mission', 'Finances', 'Contribution'],
        'gridcolor': 'rgba(20,20,20,1)'
    });

    //wheel.spidergraph('resetdata');

    wheel.spidergraph('setactivedata', {
        'strokecolor': 'rgba(230,204,0,0.8)',
        'fillcolor': 'rgba(230,204,0,0.6)',
        'data': userOptions.wheel_data && userOptions.wheel_data.length == wheelData.length ? userOptions.wheel_data: wheelData,
        'linear': true
    });


    let awarenessMapStylesId = 'aw-map-styles';

    if (document.getElementById(awarenessMapStylesId)) {
        document.getElementById(awarenessMapStylesId).remove();
    }

    let lifeEventList = document.querySelector('#aw-map__life-events > ul'),
        today = moment().format('YYYY-MM-DD'),
        endDate = moment(startDate).add(years, 'years').format('YYYY-MM-DD'),
        styleTag = document.createElement('style'),
        styleRules = `#aw-map__life-events:after {content: '${startDate} | ${endDate}'!important}`;

    styleTag.setAttribute('id', awarenessMapStylesId);

    lifeEventList.insertAdjacentHTML('beforeend', `
		<li class="menu-item" data-date="${today}"><span class="special today">${isHun ? 'Most' : 'Now'}<div class="arrow-left"></div></span></li>
		<li class="menu-item" data-date="${endDate}"><span class="arrow-left special"></span></li>
	`);


    // Add events more from a month to life events list;
    if (dayviewCalData.length > 0) {
        dayviewCalData.forEach(function (event, index) {
            let daysFromToday = moment(event.start).diff(moment(), 'days') + 1,
                daysFromLastDay = moment(endDate).diff(moment(event.start), 'days');

            if (daysFromToday > 30 && event.start != today && daysFromLastDay >= 0) {
                lifeEventList.insertAdjacentHTML('beforeend', `
				<li class="menu-item" data-date="${event.start}" data-task-id="${event.id}">
					<div class="event-marker">
						<div class="event-title">    
                            <button class="remove-button default-cta-button" onclick="removeLifeEvent(event)">
                                <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon.svg">
                            </button>
                            ${event.title}
                            <span class="event-date">
                                ${event.start}
                            </span>
                        </div>
					</div>
				</li>
			`);
            }
        });
    }

    let lifeEvents = document.querySelectorAll('#aw-map__life-events > ul li');

    function calculateRotationDegree(date) {
        let spanDays = moment(endDate).diff(startDate, 'days'),
            daysToDate = moment(date).diff(startDate, 'days');

        return Math.round(360 * daysToDate / spanDays) - 90;
    }

    lifeEvents.forEach((el, i) => {
        let rotationDegree = calculateRotationDegree(el.dataset.date),
            isToday = el.dataset.date == moment().format('YYYY-MM-DD');

        styleRules += `
        #aw-map__life-events > ul .menu-item:nth-child(${i + 1}) {
          transform: rotate(${rotationDegree}deg) translateX(${isToday ? '-300px' : '-285px'});
        }

       #aw-map__life-events > ul .menu-item:nth-child(${i + 1}) .event-marker {
            transform: rotate(${rotationDegree * -1}deg);
        }`;
    });

    let labels = getAllLabels();

    if (labels.roles) {
        labels.roles.forEach(role => {
            document.querySelector('#aw-map__roles ul').insertAdjacentHTML('beforeend', `
            <li class="menu-item" data-role="${role}">
                <span class="menu-item__inner">
                    <div class="role__goal">
                        ${compassData.roles[role].goal}
                    </div>
                    <div class="role">${role}</div>
                    <div class="aw-map__roles-and-values__button-group">
                        <button class="edit-button default-cta-button" onclick="editRoleFromWheel(event)">
                            <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/edit-task-settings.svg">
                        </button>
                        <button class="remove-button default-cta-button" onclick="removeRoleFromWheel(event)">
                            <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon.svg">
                        </button>
                    </div>
                    <div class="score"><span class="score-wrapper">${compassData.roles[role].score}/5</span></div>
                </span>
            </li>
        `);
        });

        labels.roles.forEach((role, i) => {
            let isSingleValue = labels.roles.length == 1,
                rotationDegree = isSingleValue ? 90 : i * (Math.round(180 / (labels.roles.length - 1)));

            styleRules += `
			#aw-map__roles .menu-item:nth-child(${i + 1}) {
			  transform: rotate(${rotationDegree}deg) translateX(-216px);
				${isSingleValue ? 'top: 0!important;' : ''}
			}

		   #aw-map__roles .menu-item:nth-child(${i + 1}) > span {
				transform: rotate(${isSingleValue ? -90 : '-' + rotationDegree}deg);
			}`;
        });
    }

    if (labels.values) {
        labels.values.reverse().forEach(value => {
            document.querySelector('#aw-map__values ul').insertAdjacentHTML('beforeend', `
            <li class="menu-item">
                <span>
                    ${value}
                    <div class="aw-map__roles-and-values__button-group">
                        <button class="remove-button default-cta-button">
                            <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon.svg">
                        </button>
                    </div>
                </span>
            </li>
        `);
        });

        labels.values.forEach((value, i) => {
            let isSingleValue = labels.values.length == 1,
                rotationDegree = isSingleValue ? -90 : i * (Math.round(180 / (labels.values.length - 1))) + 180;

            styleRules += `
			#aw-map__values .menu-item:nth-child(${i + 1}) {
			  transform: rotate(${rotationDegree}deg) translateX(-216px);
              ${isSingleValue ? 'top: 0!important;' : ''}
			}

		   #aw-map__values .menu-item:nth-child(${i + 1}) span {
				transform: rotate(${isSingleValue ? 90 : '-' + rotationDegree}deg);
			}`;
        });

        document.querySelectorAll('#aw-map__values li').forEach(el => {
            el.addEventListener('click', e => {
                if (confirm(isHun ? 'Tényleg szeretnéd törölni ezt a sort?' : 'Do you really want to delete this row?')) {
                    let value = e.target.innerText;

                    e.currentTarget.remove();
                    document.querySelectorAll('.compass__group__list_element').forEach(el => {
                        if (el.innerText == value) el.remove();
                    });
                    removeValueFromCompassData('values', value);
                    saveCompassToDB();

                    if (document.body.classList.contains('interactive-planning-mode') || document.getElementById('awareness-map').classList.contains('show')) {
                        initWheelOfLifeForPeriodSet();
                    }
                }
            });
        });
    }

    styleTag.appendChild(document.createTextNode(styleRules));
    document.body.appendChild(styleTag);
}
