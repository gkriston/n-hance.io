function startPlanning() {
    let intro = introJs(),
        cfg = {
            overlayOpacity: 0,
            exitOnOverlayClick: false,
            steps: [
                {
                    intro: isHun ? "Tervezzük meg a napodat!" : "Let's plan your day!"
                },
                {
                    element: document.querySelector('#show-wheel-of-life'),
                    intro: isHun ? "Vess egy pillantást az életkerékre" : "Have a look at your wheel of life",
                    position: 'right'
                },
                {
                    element: document.querySelector('#show-compass'),
                    intro: isHun ? "Nézd át az iránytűdet" : "Read through your compass items",
                    position: 'right'
                },
                {
                    intro: isHun ? "<h3>Rendezd a listáidat</h3>Az egér bal gombját nyomvatartva kattins bármelyik listaelemre és húzd a megfelelő helyre egy másik listán belül.<br>A többlistás nézet automatikusan bezáródik, ha egy új listát nyitsz meg." : "<h3>Review your lists</h3>Click and hold the left mouse button down to drag your list element to another list.<br> The multi-list view automatically closes when you open a new list.",
                    position: 'right'
                },
            ],
        };

    cfg = Object.assign(cfg, introCfg);

    let prevElement;

    intro.setOptions(cfg).onchange(function (el) {
        console.log(el);
        if (el.getAttribute('id') == 'show-compass') {
            document.getElementById('show-wheel-of-life').click();
        }

        if (prevElement && prevElement.getAttribute('id') == 'show-compass' && el.classList.contains('introjsFloatingElement') && window.innerWidth > 991) {
            let listsToOpen = ['today', 'tomorrow', 'this-week'];

            document.querySelectorAll('.task-list-wrapper').forEach(function (list) {
                list.classList.add('hide');
            });

            listsToOpen.forEach(function (list) {
                document.getElementById(list).classList.remove('hide');
                document.body.classList.add('multi-list-view');
            });
        }
        el.click();

        prevElement = el;
    }).onexit(function (el) {
        document.body.classList.remove('daily-planning-mode');
    }).start();
    document.body.classList.add('daily-planning-mode');
}