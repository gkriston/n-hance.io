function startIntro() {
    document.getElementById('xd-walkthrough').classList.remove('hide')
    return

    // if (window.innerWidth < 1200) {
    //     alert(isHun ? 'Az alkalmazásbemutató megtekintéséhez kérlek jelentkezz be asztali számítógépről' : 'For the best onboarding experience please go through this process on a desktop computer');
    //     return;
    // }

    document.getElementById('show-wheel-of-life').click();
    setUpOnboardingTasks();
    addTasksToOnboardingList();

    let intro = introJs(),
        cfg = {
            overlayOpacity: 0,
            exitOnOverlayClick: false,
            showStepNumbers: false,
            // keyboardNavigation: false,
            disableInteraction: true,
            steps: [
                {
                    intro: isHun ? "<h3>Kezdhetünk?</h3><p><b>Nézzük meg, hogyan találhatod meg a belső iránytűdet és indulhatsz el álmaid élete felé!</b></p>" : "<h3>Can we begin?</h3><p><b>Let's have a look how can we make your more efficient!</b></p>"
                },{
                    element: document.querySelector('.onboarding-steps:first-of-type'),
                    intro: isHun ? "<h3>Hol tartasz jelenleg az életedben?</h3><p>Kérlek add meg a születési dátumodat, ha még nem tetted meg.</p>" : "<h3>Where are you at in your life?</h3><p>Please enter your birth date, if you haven't done it yet.</p>",
                    position: 'bottom'
                },{
                    element: document.querySelector('.onboarding-steps:nth-of-type(2)'),
                    intro: isHun ? "<h3>Az Életkerék</h3><p>Az életkerékre ránézve mindig szemmel tarthatod, hogy mennyire van egyensúlyban az életed, így jobb döntéseket hozhatsz.</p><p><b>Az egérrel kattintva az életkerék egyes területeit jelölő cikkeken, beállíthatod jelenlegi helyzetedet.</b></p><p>(A kör közepénél: katasztrofális, a kör szélénél: tökéletes)</p>" : "<h3>The Wheel of Life</h3><p>The wheel let's you review how balanced your life is to make wise decisions.</p><p><b>You can set how satisified you are with each life area by clicking on a vector.</p><p></b>(The closer you get to the origo means you've got more work to do with that area)</p>",
                    position: 'bottom'
                },{
                    intro: isHun ? "<h3>Személyes Iránytű</h3><p>Az Életkerék személyes iránytűként funkcionál, ide viheted fel és itt tudod átnézni a számodra fontos értékeket és szerepköröket. </p><p><b>Hogy az alkalmazásból a legtöbbet kihozhasd, kérlek gondold át a következő pontokat:</b></p>" : "<h3>Personal Compass</h3><p>The Life Wheel functions as your personal compass where you can review your values and roles . </p><p><b>To make the most out of this app, please take your time to consider the following steps:</b></p>"
                },{
                    element: document.querySelector('.onboarding-steps:nth-of-type(3)'),
                    intro: isHun ? "<p>Azonosítsd be a számodra fontos értékeket, hogy ezekhez igazodó célokat tűzhess ki.</p><p><b>Neked mi számít leginkább az életben?</b></p><p>Új értéket hozzáadni a blokk alatt megjelenő Hozzáadás gombbal tudsz, vagy választhatsz a szófelhőben megjelenített értékek közül is.<p>Törölni a már hozzáadott érték nevére kattintva tudsz.</p></p>" : "<p>Identify your important roles and goals so you'll be able to filter which tasks are leading to those and which not</p><p><b>What is the most important for you in your life?</b></p><p>You can add an item by choosing a value from the word cloud or adding your own one.</p><p>You can remove previously added items by clicking on their names.</p>",
                    position: 'bottom'
                },{
                    element: document.querySelector('.onboarding-steps:nth-of-type(4)'),
                    intro: isHun ? "<p>A szerepköreid és a hozzájuk tartozó célok beazonosításával, képes leszel szűrni a feladatokat, amelyek közelebb juttathatnak a céljaidhoz.</p><p><b>Milyen fontos szerepköreid vannak és mi lehet a következő cél ezekben?</b></p>" : "<p>Identify your important roles and goals so you'll be able to filter which tasks are leading to those and which not. </p><p><b>What are your main roles and what is what you want to achieve in them?</b></p>",
                    position: 'top'
                    // },{
                    //     element: document.querySelector('#enhance-areas'),
                    //     intro: isHun ? "Tűzz ki heti célokat a mentális, spirituális, fizikai jóléted és a kapcsolataid fejlesztésére" : "Set goals to sharpen the saw mentally, spiritually, physically and to imporve your relationships",
                    //     position: 'right'
                    // },{
                    //     element: document.querySelector('#principles'),
                    //     intro: isHun ? "Fogalmazz meg döntéshozatali elveket, hogy ne a pillanat hevében kelljen esetlegesen téves döntéseket hoznod" : "Define guiding principles, so you will be able to make better decisions on the spot without going down the wrong road",
                    //     position: 'right'
                },{
                    element: document.querySelector('#show-compass'),
                    intro: isHun ? "Az Életkerék nézetben meghatározott elemek az Iránytű menüpontban is láthatóak, hogy a mindennapos tervezés során is szemmel tudd tartani őket." : "The elements of the Lifewheel are also visible in the Compass menu, so you won't miss them when you are planning your day.",
                    position: 'right'
                },{
                    element: document.querySelector('#today .add-task'),
                    intro: isHun ? "<h3>Új feladatok hozzáadása</h3><p>Vidd fel a feladataidat, amelyekhez jegyzeteket is hozzáadhatsz és határidőt is rendelhetsz. </p><p><b>Feladatot felvinni a beviteli mezőbe begépelve a feladat nevét, majd az Enter gombot lenyomva tudsz.</b></p><p>Minden feladat az <b>Opcionális</b> kategóriába kerül, ameddig nem érdemes arra, hogy átsorold máshová.</p>" : "<h3>Adding tasks to the list by typing the task name into the input field and pressing Enter.</h3><p>Add your tasks here. You can add notes and set deadlines to each of your tasks.</p><p>By default every task belongs to the <b>Optional</b> sublist until you move it somewhere else from there.</p>",
                    position: 'bottom'
                },{
                    element: document.querySelector('#today'),
                    intro: "<p>" + (isHun ? "<p><b>Tegyél különbséget a feladataid között</b> az alapján, hogy mennyire segítenek eljutni a céljaidhoz. </p><p>Egyszerű <b>húzd és dobd</b> módon tudod az egyes feladatokat a megfelelő kategóriába sorolni.</p>" : "<p>Two tasks can be very different depending on if they help you to reach your goals or not. You can just <b>drag and drop</b> tasks to any category.</p>") + '</p><img style="max-width: 100%;" src="' + appVariables.templateUrl + '/assets/images/n-hance-dnd-howto.gif">',
                    position: 'bottom'
                },{
                    element: document.querySelector('#today .enhance'),
                    intro: isHun ? "Vannak feladatok, amelyek elvégzésével szélsebesen roboghatsz az álmaid felé." : "Some will help you achieve your goals at a rapid pace.",
                    position: 'bottom'
                },{
                    element: document.querySelector('#today .must-do'),
                    intro: isHun ? "Vannak, amelyek a jelenlegi helyzetedben fognak megtartani." : "Some will keep you in the rat race.",
                    position: 'bottom'
                },{
                    element: document.querySelector('#today .optional'),
                    intro: isHun ? "Lesznek feladatok, amelyeket jobb ha megfontolsz, hogy elvégzel -e egyáltalán, mert hátráltathatnak az utadon." : "Some will even set you back on the road ahead.",
                    position: 'bottom'
                },{
                //     element: document.querySelector('#today .task-list-wrapper__actions .forecast'),
                //     intro: isHun ? "Már a nap elején előre tudod jelezni a haladásod a feladatokhoz rendelt idő és hatáspontszám alapján." : "You can forecast your progress at the beginning of the day which is calculated based on the impact score and time estimate assigned to each task.",
                //     position: 'left'
                // },{
                    element: document.querySelector('#show-dayview-calendar'),
                    intro: isHun ? "A heti tervezéshez ajánlott a naptárad használata, ahová elsőként a legnagyobb hatású feladataid elvégzésére érdemes időt rögzítened." : "Viewing your lists and the calendar side-by-side is also the recommended way to plan your day/week,  by time-boxing your most impactful activities first.",
                    position: 'bottom'
                },{
                    element: document.querySelector('#today .track-time'),
                    intro: isHun ? "Az időrögzítéssel percre pontosan követheted, hogy mire mennyi időt fordítottál." : "With the time tracking feature you will always know how much time you have invested into each task.",
                    position: 'bottom'
                },{
                    element: document.querySelector('#today .draggable .done'),
                    intro: isHun ? "Ahogy a nap folyamán kipipálod a feladatokat azonnal visszajelzést kapsz azok jelentőségéről." : "As you finish off tasks during the day, you will get immediate feedback about their impact.",
                    position: 'bottom'
                },{
                    element: document.querySelector('#chart-icon'),
                    intro: isHun ? "A statisztikákban visszamenőleg is követheted az eredményeid alakulását, hogy megbizonyosodj arról, a helyes úton jársz." : "You can also track your results retrospectively to make sure that you are still on the right path.",
                    position: 'bottom'
                },{
                    element: document.querySelector('.task-management-practice'),
                    intro: isHun ? "Hogy megismerkedj az alkalmazás haszánlatával, dobd a mintafeladatokat a megfelelő oszlopokba, majd jelöld elvégzettnek őket." : "To get familiar with the use of the app now drop the sample tasks to the appropriate columns then mark them complete.",
                    position: 'top'
                },{
                //     element:  document.querySelector('#today .must-do .draggable'),
                //     intro: isHun ? "<h3>Címkézés</h3><p>Ha nem egyértelmű, hogy egy feladat melyik kategóriába tartozik, segítségül hívhatod a Címkézés menüt, amivel a feladatot értékeidhez és szerepköreidhez kötheted.</p><p><b>Ha egy feladat nem köthető egyik számodra fontos értékhez vagy szerepkörhöz sem, nagy valószínűséggel jobb ha törekszel a hasonló jellegű feladatok elkerülésére a jövőben.</b></p>" : "<h3>Labeling</h3><p>If your are unsure where you should put a task, you can use the Label menu to connect the task to your values and your goals.</p><p><b>If you can't connect a task to one of your values or roles, most probably a task like this should not be allowed on you task list in the future</b></p>",
                //     position: 'right'
                // },{
                //     element:  document.getElementById('label-menu-instructions'),
                //     intro: isHun ? "A Címkézés menüben automatikusan megjelennek az Iránytűdbe felvitt értékek és szerepkörök, így könnyen tudod a feladatot társítani hozzájuk. Egy feladat több címkét is kaphat." : "The Label menu automatically loads the Values and Roles from the Compass, so you can easily connect the task to them. Multiple labelas are allowed.",
                //     position: 'left'
                // },{
                //     element:  document.getElementById('label-menu-instructions'),
                //     intro: isHun ? "A menü következő szintjébe lépve tudod kiválasztani az adott értéket vagy szerepkört." : "Choosing either values or roles, you will see all the items you can select from.",
                //     position: 'left'
                // },{
                //     element:  document.getElementById('label-menu-instructions'),
                //     intro: isHun ? "Végül, értékmérőként egy pontszámot is rendelhetsz a kiválaszott értékhez, az alapján, hogy a feladat mennyire járul hozzá az előrelépéshez az adott területen." : "Lastly, you can add a score to the selected item, based on how much this task contributes to the specified area.",
                //     position: 'left'
                // },{
                //     element:  document.getElementById('show-features'),
                //     intro: isHun ? "Ha kiváncsi vagy, hogy mi mindent tud még az alkalmazás, bármikor megnézheted a részeletes funkcióbemutatót, a listák feletti kérdőjel gombra kattintva." : "If you want to know what else you can do with the app, just click on the question mark icon above your lists to see the detailed function overview.",
                //     position: 'top'
                // },{
                    intro: isHun ? "<h3>Vágj bele!</h3>Lépj a tudatosság következő szintjére és éld álmaid életét! </p><p><b>Hajrá!<b>" : "<h3>Rock on!</h3>Raise your consciousness to the next level and live the life of your dreams! </p><p><b>You can do it!<b>"
                }
            ]
        };

    cfg = Object.assign(cfg, introCfg);

    let prevElement;

    intro.setOptions(cfg).onchange(function(el) {
        if (el.getAttribute('data-step') == 1) {
            setTimeout(() => {
                if (!userOptions.birth_date) {
                    userOptions.birth_date = prompt(isHun ? 'Kérlek írd be a születésed napját' : 'Please enter your birth date', '1984-05-29');
                    updateUserOptions();
                }

                let rangeInput = document.querySelector('[name="range1"]'),
                    event = new Event('change');

                rangeInput.value = 3;
                rangeInput.dispatchEvent(event);
                document.body.classList.add('onboarding-step-1');
            },1000);
        }

        if (el.getAttribute('data-step') == 2) document.body.classList.add('onboarding-step-2');

        if (el.getAttribute('data-step') == 3) {
            document.body.classList.add('onboarding-step-3');
            generateWordCloud('values');
        }

        if (el.getAttribute('data-step') == 4) {
            document.body.classList.add('onboarding-step-4');
            generateWordCloud('roles');
        }

        if (el.getAttribute('id') == 'show-compass') {
            document.getElementById('awareness-map').classList.remove('show');
            document.getElementById('onboarding-steps').classList.add('hide');
            hideWordCloud();
            document.querySelectorAll('.task-list-wrapper').forEach(function(listWrapper) {
                listWrapper.classList.add('hide');
            });
            document.getElementById('today').classList.remove('hide');
            el.click();
        }

        if (el.classList.contains('add-task')) {
            document.querySelector('#today .enhance .draggable').classList.add('hover');
            // document.querySelector('#today .must-do .draggable.hover .set-label').setAttribute('id', 'label-intro');
        }

        if (el.classList.contains('forecast')) {
            el.click();
        }

        if (el.getAttribute('id') == 'show-dayview-calendar') {
            document.querySelector('#modal .close').click();
            el.click();
        }

        if (el.classList.contains('track-time')) {
            document.getElementById('show-dayview-calendar').click();

            if (!document.getElementById('compass').classList.contains('hidden')) {
                document.getElementById('show-compass').click();
            }
        }

        if (el.classList.contains('done')) {
            document.querySelector('#modal .close').click();
            document.querySelector('#today .draggable').classList.remove('hover');
            document.querySelectorAll('#today .draggable').forEach((task, i) => {
                let taskId = task.dataset.id,
                    taskData = getTaskDataById(taskId);

                if (taskData.sampleTask) {
                    setTimeout(() => {
                        task.querySelector('.done').click();
                    }, (i + 1) * 500);
                }
            });
        }

        if (el.getAttribute('id') == 'chart-icon') {
            document.querySelector('#modal .close').click();
            setUpPracticeTasks();
            showLoggedTaskVisualization();
        }

        if (el.classList.contains('task-management-practice')) {
            clearModal();
            el.click();
        }

        // if (prevElement && prevElement.classList.contains('optional')) {
        //     document.getElementById('label-intro').classList.add('hover');
        // }
        //
        // if (el.getAttribute('id') == 'label-menu-instructions' && el.closest('#label-menu-overlay').classList.contains('hidden')) {
        //     initLabelMenus();
        //
        //     setTimeout(function() {
        //         document.getElementById('label-menu-overlay').classList.remove('hidden');
        //         document.getElementById('menu-toggler').classList.add('active');
        //         document.getElementById('menu-toggler').dataset.targetId =  document.querySelector('#today .must-do .draggable').dataset.id;
        //     },100);
        // }
        //
        // if (prevElement && prevElement.getAttribute('id') == 'label-menu-instructions') {
        //     document.getElementById('label-menu-overlay').querySelector('.menu-item').click();
        // }

        prevElement = el;
    }).onexit(function(el) {
        // Removing onboarding class step classes
        document.body.classList.remove('interactive-planning-mode');
        for (let i = 1; i < 6; i++) {
            document.body.classList.remove('onboarding-step-' + i);
        }

        // Resetting awareness map period
        let rangeInput = document.querySelector('[name="range1"]'),
            event = new Event('change');

        rangeInput.value = 1;
        rangeInput.dispatchEvent(event);
        document.getElementById('awareness-map').classList.remove('show');

        // Hiding word cloud and clearing modal
        document.getElementById('word-cloud__list').innerHTML = '';
        wordCloud.classList.add('hide');
        clearModal();
        // document.getElementById('label-menu-overlay').classList.add('hidden');

        document.querySelectorAll('#today .draggable').forEach((task, i) => {
            let taskId = task.dataset.id,
                taskData = getTaskDataById(taskId);

            if (taskData.sampleTask) {
                task.querySelector('.del').click();
            }
        });

        if (!document.querySelector('#task-management-practice .draggable')) {
            setUpPracticeTasks();
            document.querySelector('.task-management-practice').click();
        }
    }).start();
    document.body.classList.add('show-prio');
    document.getElementById('modal-wrapper').classList.remove('full-screen');
    document.body.classList.add('interactive-planning-mode');
    document.getElementById('onboarding-steps').classList.remove('hide');
}

function generateWordCloud(type, id) {
    hideWordCloud();

    let roles = isHun ?
        [
            'Szülő', 'Tanár', 'Tanuló', 'Gyermek', 'Barát', 'Munkatárs', 'Alkalmazott', 'Partner', 'Vállalkozó', 'Vezető', 'Társ', 'Családtag', 'Közösség tagja', 'Állampolgár'
        ] : [
            'Parent', 'Teacher', 'Student', 'Child', 'Friend', 'Co-worker', 'Employee', 'Partner', 'Entrepreneur', 'Leader', 'Spouse', 'Family Member', 'Member of Community', 'Citizen'
        ],
        values = isHun ?
            [
                'Család', 'Kapcsolatok', 'Anyagi biztonság', 'Összetartozás', 'Közösség', 'Személyes fejlődés', 'Hűség', 'Spiritualitás', 'Alkalmazotti biztonság', 'Személyes felelősségvállalás', 'Alapvető igények kielégítése', 'Harmónia', 'Egészség', 'Élmények', 'Tisztelet', 'Könyörületesség', 'Társadalmi pozíció', 'Kreativitás', 'Megbízhatóság', 'Biztonság'
            ] : [
                'Family', 'Relationships', 'Financial Security', 'Belonging', 'Community', 'Personal Growth', 'Loyalty', 'Spirituality', 'Employment Security', 'Personal Responsibility', 'Basic Needs', 'Harmony', 'Health', 'Experiences', 'Respect', 'Compassion', 'Social Standing', 'Creativity', 'Trustworthiness', 'Security'
            ],
        valuesList;


    let title = document.getElementById('word-cloud__title'),
        instructions = document.getElementById('word-cloud__instructions'),
        list = document.getElementById('word-cloud__list');

    list.innerHTML = '';
    wordCloud.dataset.type = type;

    switch (type) {
        case 'roles':
            valuesList = roles;
            title.innerHTML = isHun ? 'Szerepkörök' : 'Roles';
            break;
        case 'labels':
            valuesList = getUsedLabels();
            title.innerHTML = isHun ? 'Címkék' : 'Labels';
            break;
        case 'values':
            valuesList = values;
            title.innerHTML = isHun ? 'Értékek' : 'Values';
            break;
        case 'list-categories':
            wordCloud.dataset.listId = id;
            title.innerText = isHun ? 'Lista kategória' : 'List category';
            instructions.innerText = isHun ? 'Melyik kategóriába szeretnéd átsorolni ezt a listát?' : 'What category do you want to move this list to?';
            valuesList = listCategoryModule.getListCategories();
            break;
        case 'principle-label':
            wordCloud.dataset.principleName = id;
            title.innerText = isHun ? 'Elv kategória' : 'Principle category';
            let allLabels = getAllLabels();
            let mostUsedLabels = getLabelsByMostUsedOrder();
            valuesList = [];
            for (category in allLabels) {allLabels[category].forEach(label => valuesList.push(label))}
            valuesList = valuesList.concat(mostUsedLabels.filter((item) => valuesList.indexOf(item) < 0));
            break;
    }

    valuesList.forEach(function(value) {
        let activeLabel = document.body.dataset.filterLabel;

        list.insertAdjacentHTML('beforeend', `<li data-active="${value == activeLabel ? true : false}" data-value="${value}">${isHun && trans_hu[value] ? trans_hu[value]: value}</li>`)
    });
    wordCloud.classList.remove('hide');

    list.querySelectorAll('li').forEach(function(word) {
        word.addEventListener('click', function(e) {
            e.stopPropagation();

            if (type == 'labels') {
                filterListsByLabel(word.dataset.value);
                document.body.setAttribute('data-filter-label', word.dataset.value)
                // hideWordCloud();
            } else {
                addValueFromWordCloud(type, word.dataset.value);
            }


            // Refresh wheel in onboarding mode
            if (document.body.classList.contains('interactive-planning-mode') || document.getElementById('awareness-map').classList.contains('show')) {
                initWheelOfLifeForPeriodSet();
            } else {
                // Hide wheel when not in onboarding mode
                document.getElementById('awareness-map').classList.remove('show');
            }
        })
    });

    wordCloud.querySelector('input').value = '';
    wordCloud.querySelector('.close').addEventListener('click', hideWordCloud);
    wordCloud.querySelector('form').addEventListener('submit', e => {
       e.preventDefault();
       e.stopPropagation();
        if (wordCloud.classList.contains('hide')) return;

        addCustomValueFromWordCloud();

        if (document.body.classList.contains('interactive-planning-mode') || document.getElementById('awareness-map').classList.contains('show')) {
            initWheelOfLifeForPeriodSet();
        } else {
            // Hide wheel when not in onboarding mode
            document.getElementById('awareness-map').classList.remove('show');
        }
    });
}

function hideWordCloud() {
    document.getElementById('word-cloud__instructions').innerHTML = '';
    document.getElementById('word-cloud__list').innerHTML = '';
    wordCloud.classList.add('hide');
}

function addCustomValueFromWordCloud(e) {
    let type = wordCloud.dataset.type,
        value = wordCloud.querySelector('input').value;

    wordCloud.querySelector('input').value = '';

    addValueFromWordCloud(type, value);
}

function addValueFromWordCloud(type, value) {
   switch (type) {
        case 'values':
            addCompassListElement(value);
            break;
        case 'roles':
            addCompassTableRow(false, value);
            break;
        case 'list-categories':
            updateListCategory(wordCloud.dataset.listId, value);
            hideWordCloud();
            break;
       case 'principle-label':
            if (compassData.principles) {
                compassData.principles.forEach(principle => {
                    if (principle.name == wordCloud.dataset.principleName) {
                        principle.label = value;
                    }
                })
            }
            hideWordCloud();
            break;
    }

    return value;
}

function setUpOnboardingTasks() {
    // initListElement({name: isHun ? 'Egy szimpla feladat, amivel nem azonnal kell foglalkoznod' : 'A simple task, which does not need your attention immediately' , targetList: document.querySelector('#inbox .optional'), isFromUserInput: false, time: 30, sampleTask: true});
    initListElement({name: isHun ? 'Ez egy különlegesen fontos feladat, ami nagy előrelépést jelenthet az életedben' : 'This is a really important task, which can enhance your life greatly', targetList: document.querySelector('#today .enhance'), isFromUserInput: false, time: 240, sampleTask: true, effect: 5});
    initListElement({name: isHun ? 'Nagy hatású feladat, amit érdemes várható eredmény és időbefektetés alapján priorizálni' : 'A task with a great impact, which is worth to be priorized based on effect and ease', targetList: document.querySelector('#today .enhance'), isFromUserInput: false, time: 90, sampleTask: true, effect: 3});
    initListElement({name: isHun ? 'Mókuskerék feladat, ha ezen dolgozol folyamatosan, nem várható nagy változás' : 'A rat-race task, will convserve you in the state where you are', targetList: document.querySelector('#today .must-do'), isFromUserInput: false, time: 120, sampleTask: true});
    initListElement({name: isHun ? 'Opcionális feladat, amit delegálhatsz, elnapolhatsz, vagy simán törölhetsz' : 'An optional task, which is best to be delagate, postponed or eleminated', targetList: document.querySelector('#today .optional'), isFromUserInput: false, time: 30, sampleTask: true});
    // initListElement({name: isHun ? 'Szuper ötlet, amit később érdemes mérlegeld, mert akár valami nagyszerű is kisülhet belőle' : 'A great idea, which you need to chew on at a later date and may have a big positive impact', targetList: document.querySelector('#ideas .optional'), isFromUserInput: false, time: 30, sampleTask: true});
    // initListElement({name: isHun ? 'Egy napközbeni felismerés, ami a nap végén átgondolva, fontos tanulsággal szolgálhat' : 'A realization during the day which worth to be reflected on, because you can learn something from it', targetList: document.querySelector('#reflect .optional'), isFromUserInput: false, sampleTask: true});
    recalcListTimeSum(document.querySelector('#today .must-do'));
    recalcListTimeSum(document.querySelector('#today .optional'))
}

function showLoggedTaskVisualization() {
    let newEntryCompletion = moment();

    // Update logged tasks completion time
    compassData.dailyLog[dateToday].forEach((entry, i) => {
        if (entry.sampleTask) {
            newEntryCompletion = newEntryCompletion.add(Math.ceil(entry.time / 60), 'hours');
            entry.completionTime = newEntryCompletion.format('HH:ss');
        }
    });
    // Show logged events;
    initAnalytics();
    let event = new Event('change'),
        analyticsRangeSelector = document.getElementById('analytics-range')
    analyticsRangeSelector.value = 1;
    analyticsRangeSelector.dispatchEvent(event);
    // Remove dummy data from log
    compassData.dailyLog[dateToday] = compassData.dailyLog[dateToday].filter(entry => !entry.sampleTask);
}

function addTasksToOnboardingList() {
    if (!myLists['N-hance Onboarding']) {
        createNewList('N-hance Onboarding', {
            'goal': '',
            'parentcategory': 'Default Lists',
            'simpleList' : false,
            'order': 4,
            'categories': {
                'Enhance': [],
                'Must-Do': [],
                'Optional': []
            }
        });
    }

    document.querySelectorAll('#n-hance-onboarding .draggable').forEach((task, i) => {
        let taskId = task.dataset.id,
            taskData = getTaskDataById(taskId);

        if (taskData.sampleTask) {
            task.querySelector('.del').click();
        }
    });

    let onboardingTasks = [
        {
            name: isHun ? 'Olvasd el a segítség menüpontban található útmutató' : 'Read the get started guide from the help menu',
            category: 'enhance',
            time: 5,
            effect: 5
        },{
            name: isHun ? 'Add hozzá a Napi listához feladataidat és rendelj hozzájuk időbecslést' : 'Add your tasks to your Daily list and estimate time for them',
            category: 'enhance',
            time: 5,
            effect: 4
        },{
            name: isHun ? 'Húzd és dobd módszerrel helyezd a megfelelő oszlopba a feladatokat (Szintlépés, Kötelező, Opcionális)' : 'Drag & Drop the tasks into one of the columns (Enhance, Must-Do, Optional)',
            category: 'enhance',
            time: 5,
            effect: 4
        },{
            name: isHun ? 'Tervezd meg a következő hetedet a Naptár nézetben a megfelelő naphoz dobva a feladatokat' : 'Plan the week ahead by opening the calendar view and dropping the tasks to specific days',
            category: 'enhance',
            time: 15,
            effect: 3
        },{
            name: isHun ? 'Módosítsd a feladat beállításait a ceruza ikonra kattintva' : 'Adjust task settings by clicking the edit icon when hovering over them',
            category: 'must-do',
            time: 5,
            effect: 2
        },{
        //     name: isHun ? 'Becsüld meg előre az adott nap haladását a lista neve melleti hatás ikonra kattintva' : 'Forecast task impact with the impact button next to the name of the list',
        //     category: 'optional',
        //     time: 5,
        //     effect: 1
        // },{
            name: isHun ? 'Próbáld ki az időrögzítési funkciót az feladatnál megjelenő óra ikonra kattintva' : 'Try out the time tracking feature by clicking on the clock icon while hovering over a task',
            category: 'optional',
            time: 5,
            effect: 1
        }
    ]

   onboardingTasks.forEach(task => {
      initListElement({
          name: task.name,
          targetList: document.querySelector(`#n-hance-onboarding .${task.category}`),
          isFromUserInput: false,
          time: task.time,
          sampleTask: true,
          effect: task.effect
      })
   });

    let categories = ['enhance', 'must-do', 'optional'];
    categories.forEach(category => recalcListTimeSum(document.querySelector(`#n-hance-onboarding .${category}`)));
    updateListItemCounter(['N-hance Onboarding']);
}


function setUpPracticeTasks() {
    if (!myLists['Task Management Practice']) {
        createNewList('Task Management Practice', {
            'goal': isHun ? "Dobd a mintafeladatokat a megfelelő oszlopokba, majd jelöld elvégzettnek őket" : "Drop the sample tasks to the appropriate columns then mark them complete",
            'parentcategory': 'Default Lists',
            'simpleList' : false,
            isGoalVisible: true,
            'order': 5,
            'categories': {
                'Enhance': [],
                'Must-Do': [],
                'Optional': []
            }
        });
    }

    document.querySelectorAll('#task-management-practice .draggable').forEach((task, i) => {
        let taskId = task.dataset.id,
            taskData = getTaskDataById(taskId);

        if (taskData.sampleTask) {
            task.querySelector('.del').click();
        }
    });

    let practiceTasks = [
        {
            name: isHun ? 'Üzleti ötlet pitchelése befeketőknek' : 'Pitch business idea to potential investors',
            time: 90
        },
        {
            name: isHun ? 'Online marketing kurzus elvégzése' : 'Take a course on online marketing',
            time: 240
        },
        {
            name: isHun ? 'Hétvégi családi túra a hegyekbe' : 'Weekend family trip to the mountains',
            time: 300
        },
        {
            name: isHun ? 'Napi olvasási idő' : 'Daily reading time',
            time: 30
        },
        {
            name: isHun ? 'Fizetetlen túlóra a munkahelyen' : 'Unpaid overtime at work',
            time: 120
        },
        {
            name: isHun ? 'Negyedéves értékesítési jelentés elkészítése' : 'Prepare Q2 sales performance report',
            time: 180
        }
    ];

    practiceTasks.forEach(task => {
        initListElement({name: task.name, targetList: document.querySelector('#task-management-practice .optional'), isFromUserInput: false, time: task.time, sampleTask: true});
    });

    recalcListTimeSum(document.querySelector('#task-management-practice .optional'))
    updateListItemCounter(['Task Management Practice']);
}

