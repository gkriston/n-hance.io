function startFeatureShowcase() {
    if (window.innerWidth < 1200) {
        alert(isHun ? 'Az alkalmazásbemutató megtekintéséhez kérlek jelentkezz be asztali számítógépről' : 'For the best onboarding experience please go through this process on a desktop computer');
        return;
    }

    let intro = introJs(),
        cfg = {
            overlayOpacity: 0,
            exitOnOverlayClick: false,
            steps: [
                {
                    intro: isHun ? "<h3>Kezdhetünk?</h3><p><b>Nézzük meg, hogyan lehetsz még hatékonyabb!</b></p><p>Az ismertetőben a billentyűzet jobbra gombjával is tudsz navigálni.</p>" : "<h3>Can we begin?</h3><p><b>Let's have a look how can we make you more efficient!</b></p><p> You can also step forward with the right key.</p>"
                },{
                    element: document.querySelector('#show-schedule-calendar'),
                    intro: isHun ? "<h3>Naptár nézet</h3>A naptár előhívásával gyorsan rögzíthetsz feladatokat a megfelelő napokhoz." : "<h3>Calendar view</h3>Access your calendar view to quickly schedule tasks or jump to a scheduled task.",
                    position: 'right'
                },{
                    element: document.querySelector('#show-notes-calendar'),
                    intro: isHun ? "<h3>Napló</h3><p>A napló menüpont alatti naptárral rögzíthetsz naplóbejegyzéseket bármelyik naphoz." : "<h3>Journal</h3><p>Reflect on your daily happenings and record your thoughts in the journal calendar.",
                    position: 'right'
                },{
                    element: document.querySelector('#add-task-btn'),
                    intro: isHun ? "<h3>Gyors hozzáadás</h3><p>A Gyors hozzáadás gombra kattintva a <b>Bejövők</b> listához adhatsz feladatokat. Ugyanezt megteheted az <b>Alt</b> és <b>+</b> gombok egyidejű lenyomásával is.</p>" : "<h3>Quick add</h3><p>You can quickly add tasks to your <b>Inbox</b> list by clicking on the quick add button, or by pressing the <b>Alt</b> and <b>+</b> buttons simultaneously.</p>",
                    position: 'right'
                },{
                    element: document.querySelector('#today h3 span:last-of-type'),
                    intro: isHun ? "A feladataidhoz időt rendelve többé nem lesz kétség mi fér bele a napodba." : "Assign time to your tasks so you will be able to plan you day knowing what fits into it.",
                    position: 'right'
                },{
                    element: document.querySelector('#today .set-goal'),
                    intro: isHun ? "A listáidhoz a listanézetből is tudsz célt rendelni, így sosem téveszted a céljaidat szem elől." : "You can add a goal to your lists right from the list view so you will never lose them out of sight.",
                    position: 'right'
                // },{
                //     element: document.querySelector('#today h2'),
                //     intro: isHun ? "A lista nevére kattintva oda-vissza konvertálhatod listáidat időalapú és szimpla listák között" : "By clicking on the list name you can convert your lists from time-based to simple lists and vice-versa.",
                //     position: 'right'
                // },{
                //     element: document.querySelector('#today .prio'),
                //     intro: isHun ? "A Prio gombra kattintva további priorizálási funkciókat hívhatsz segítségül, amelyekkel hatás és megvalósítás nehézsége pontszámokat rendelve a feladatokhoz, még jobban képes leszel különbséget tenni köztük." : "The prio button will let you reveal additional priorization options where you can assign effect/ease scores to your tasks, so you will be able to differentiate them.",
                //     position: 'right'
                },{
                    element: document.querySelector('#my-lists'),
                    intro: isHun ? "<p><b>A listáidat a nevükre kattintva tudod megnyitni.</b></p><p> Ha a <b>CTRL gomb (CMD Mac-en)</b> egyidejű lenyomásával kattintasz, több listát is egymás mellé tudsz nyitni. Ha egy kategórianévre kattinasz ugyanezen gombot lenyomva, a kategória összes listája megnyílik egyszerre.</p>" : "<p><b>Click on the list names to open them.</b></p><p>Hold the <b>CTRL button (CMD on Mac)</b> while clicking to open lists side-by-side. If you click on a category name while holding the same button, all lists within hte category will open at once.</p>",
                    position: 'right'
                },{
                    element: document.querySelector('#left-sidebar li.scheduled'),
                    intro: isHun ? "Az időhöz kötött feladataidat rögzítsd az előre definiált időalapú listákba." : "Your tasks with deadlines go to one of the predefined time-based lists.",
                    position: 'right'
                },{
                    element: document.querySelector('#left-sidebar .default-lists .ideas'),
                    intro: isHun ? "<h3>Ötletek</h3><b>Van egy jó ötleted</b>? Rögzítsd azonnal a dedikált <b>Ötletek</b> listára." : "<h3>Ideas</h3>A great idea has popped in to your mind? Jot it down right away into the dedicated Ideas list.",
                    position: 'right'
                // },{
                //     element: document.querySelector('[data-listid="reflect"]'),
                //     intro: isHun ? "<h3>Reflektálás</h3><b>Tanultál valamit, vagy úgy érzed valamin változtatnod kell?</b> Rögzítsd a <b>Reflektálás</b> listára, hogy a nap végén végigfuthasd a felismeréseidet." : "<h3>Reflect</h3><b>Have you learned something</b>, or feel the need of changing things? Quickly record it to the <b>Reflect</b> list so you will be able to digest your thoughts when you have time at the end of the day.",
                //     position: 'right'
                // },{
                //     element: document.querySelector('#planning'),
                //     intro: isHun ? "<h3>Napi tervezés</h3>A napi tervezéshez a Tervezés gombra kattintva tudsz hozzákezdeni." : "<h3>Daily Planning</h3>Every day you can do a guided planning by clicking on the planning button in the menu.",
                //     position: 'right'
                // },{
                //     element: document.querySelector('#show-goals'),
                //     intro: isHun ? "<h3>Célok</h3>A <b>Célok</b> menü összesíti az egyes listáidhoz rendelt célokat." : "<h3>Goals</h3>Set a goal for your lists to keep yourself on track."
                },{
                    element: document.querySelector('#nav-wrapper .settings'),
                    intro: isHun ? "<h3>Beállítások</h3>A beállítások menüpontot a fogaskerék ikonra kattintva éred el." : "<h3>Settings</h3>Click the wheel icon to access the app settings menu.",
                    position: 'right'
                // },{
                //     element: document.querySelector('.crisp-client'),
                //     intro: isHun ? "<h3>Segítségnyújtás</h3>Kérdéses van, vagy egy jó ötlettel hozzájárulnál az alkalmazás jobbátételéhez? Írj nekünk bátran a chaten keresztül!" : "<h3>Chat with us</h3>Have a question or a great idea to imporove our app? Contact us right away!",
                //     position: 'right'
                },{
                    intro: isHun ? "<h3>Vágj bele!</h3>Lépj a tudatosság következő szintjére és éld álmaid életét! </p><p><b>Hajrá!<b>" : "<h3>Rock on!</h3>Raise your consciousness to the next level and live the life of your dreams! </p><p><b>You can do it!<b>"
                }
            ]
        };

    cfg = Object.assign(cfg, introCfg);

    let prevElement;

    intro.setOptions(cfg).onchange(function(el) {
        if (el.getAttribute('id') == 'show-schedule-calendar') {
            document.getElementById('show-schedule-calendar').click();
        }

        if (el.getAttribute('id') == 'add-task-btn') {
            document.getElementById('show-schedule-calendar').click();
        }

        if (el.classList.contains('task-list-wrapper')) {
            document.querySelectorAll('.task-list-wrapper').forEach(function(listWrapper) {
                listWrapper.classList.add('hide');
            });
            el.classList.remove('hide');
        }

        if (el.classList.contains('prio')) {
            el.click();
        }
        if (!!prevElement && prevElement.classList.contains('prio')) {
            prevElement.click();
        }

        // if (el.getAttribute('id') == 'show-goals') {
        //     document.getElementById('show-goals').click();
        // }

        if (el.parentNode.getAttribute('id') == 'nav-wrapper') {
            document.querySelector('#nav-wrapper .settings').click();
        }

        if (!!prevElement && prevElement.parentNode.getAttribute('id') == 'nav-wrapper') {
            document.querySelector('#modal .close').click();
        }

        prevElement = el;
    }).start();

    introJs().setOption('hintButtonLabel', 'OK').addHints();
}