function initLabelMenus() {
    // injectInceptionMenuItems({'main-menu': ['values', 'ratings', 'roles']}, 'main-menu');
    injectInceptionMenuItems({'main-menu': ['values', 'roles', 'principles']}, 'main-menu');
}

function injectInceptionMenuItems(data, prop) {
    let instructions = document.getElementById('label-menu-instructions'),
        menuWrapper = document.querySelector('.menu ul'),
        menuToggler = document.getElementById('menu-toggler'),
        menuItemHTML = `<li class='menu-item ${prop == 'score' ? 'impact-indicator' : ''}'><span><span></span></span></li>`,
        styleRules = '',
        i = 0;

    if (prop == 'main-menu') {
        instructions.innerText = isHun ? 'Mi alapján szeretnéd címkézni a feladatot?' : 'On what basis do you want to label this task?';
    } else if (prop == 'score') {
        instructions.innerText = isHun ? 'Milyen pontszámot tudsz hozzárendelni ehhez az címkéhez?' : 'What score can you assign to this label?';
    } else {
        instructions.innerText = isHun ? 'Melyik területhez kapcsolódik a feladat?' : 'Which label do you want to apply?';
    }

    menuWrapper.innerHTML = '';
    menuWrapper.dataset.menuType = prop;
    menuStyleId = 'inception-menu-styles' + (prop != 'main-menu' ? '-' + prop : '');

    if (!document.getElementById(menuStyleId)) {
        let styleTag = document.createElement('style');
        styleTag.setAttribute('id', menuStyleId);

        for (key in data[prop]) {
            let rotationDegree = i * (Math.round(360 / Object.keys(data[prop]).length));

            styleRules += `
                .menu-toggler.active ~ ul[data-menu-type="${prop}"] .menu-item:nth-child(${Number(i) + 1}) {
                  transform: rotate(${rotationDegree}deg) translateX(-110px);
                }

                ul[data-menu-type="${prop}"] .menu-item:nth-child(${Number(i) + 1}) > span > span {
                    transform: rotate(-${rotationDegree}deg);
                }`;

            i++;
        }

        styleTag.appendChild(document.createTextNode(styleRules));
        document.body.appendChild(styleTag);
    }

    let translation = {
        'roles': 'Szerepkör',
        'values': 'Értékek',
        'motivation': 'Motiváció',
        'principles': 'Elvkategória'
    };

    for (key in data[prop]) {
        menuWrapper.insertAdjacentHTML('beforeend', menuItemHTML);
        let menuItemText = Array.isArray(data[prop]) ? data[prop][key] : key,
            menuItemWrapper = document.querySelector('.menu .menu-item:last-of-type > span > span');

        if (prop == 'score') {menuItemWrapper.parentElement.classList.add('i-' + menuItemText)}

        menuItemWrapper.dataset.key = menuItemText;

        if (isHun && prop == 'main-menu') {
            for (string in translation) {
                const regex = new RegExp(string, 'g');
                menuItemText = menuItemText.replace(regex, translation[string]);
            }
        }

        menuItemWrapper.innerText = menuItemText;
    }

    document.querySelectorAll('.menu-item').forEach(function(el) {
        let span = el.querySelector('span > span');

        el.addEventListener('click', function(e) {
            e.stopPropagation();
            e.preventDefault();

            let listWrapper = e.currentTarget.parentElement;

            if (listWrapper.dataset.menuType != 'score') {
                listWrapper.dataset.selectedMenuItem = e.currentTarget.textContent;
            }

            if (prop == 'main-menu') {
                let menuKey = span.dataset.key;

                if (menuKey == 'motivation') {
                    injectInceptionMenuItems({
                        motivation: [
                            isHun ? 'Bizonyosság' : 'Certainity',
                            isHun ? 'Változatosság' : 'Uncertainity',
                            isHun ? 'Fontosságérzet' : 'Significance',
                            isHun ? 'Kapcsolatok' : 'Connection',
                            isHun ? 'Fejlődés' : 'Growth',
                            isHun ? 'Hozzájárulás' : 'Contribution'
                        ]
                    }, menuKey);
                } else if (menuKey == 'principles') {
                    let availableLabels = [];

                    compassData.principles.forEach(function(principle, i) {
                        if (principle.label && availableLabels.indexOf(principle.label) == -1) {
                            availableLabels.push(principle.label);
                        }
                    });

                    injectInceptionMenuItems({
                        principles: availableLabels
                    }, menuKey);
                } else {
                    injectInceptionMenuItems(compassData, menuKey);
                }
            } else if (listWrapper.dataset.menuType != 'score') {
                injectInceptionMenuItems({ score: [1,2,3,4,5]}, 'score');
            } else if (listWrapper.dataset.menuType == 'score') {
                let targetLabelContainer = getLabelContainer(),
                    value = span.dataset.key,
                    label = listWrapper.dataset.selectedMenuItem,
                    labelText = value ? label + ' / ' + value : label,
                    isUpdate = updateTaskLabel(label , value),
                    injectedLabelWrapper;

                if (isUpdate) {
                    targetLabelContainer.querySelector('.label[data-label="' + label + '"]').remove();
                }

                targetLabelContainer.insertAdjacentHTML('beforeend', `<span class="label" data-label="${label}">${labelText}</span>`);
                injectedLabelWrapper = targetLabelContainer.querySelector('.label:last-child');

                injectedLabelWrapper.addEventListener('click', removeLabelOnClick);
                injectedLabelWrapper.classList.add('i-' + value);

                menuToggler.click();
            }
        });
    });

    menuToggler.addEventListener('click', function(e) {
        e.stopPropagation();
        closeLabelOverlay();

        if (menuToggler.nextElementSibling.dataset.menuType != 'main-menu') {
            document.querySelector('#draggable-list-item-' + menuToggler.dataset.targetId + ' .set-label').click();
        }
    });

    function updateTaskLabel(label, value) {
        let taskId = menuToggler.dataset.targetId,
            taskData = getTaskDataById(taskId),
            menuType = document.querySelector('.menu ul').dataset.menuType,
            isUpdate = false;

        if (!taskData.label || typeof taskData.label != 'object') taskData.label = {};
        if (!taskData.label[menuType]) taskData.label[menuType] = {};

        if (taskData.label[menuType].hasOwnProperty(label))  isUpdate = true;
        taskData.label[menuType][label] = value;

        parentListName = document.querySelector('#draggable-list-item-' + taskId).closest('.task-list-wrapper').dataset.list;
        saveDataToDB([parentListName]);

        return isUpdate;
    }

    function getLabelContainer() {
        let labelMenu = menuToggler,
            targetId = labelMenu.dataset.targetId,
            targetEl = document.querySelector('#draggable-list-item-' + targetId),
            targetElLabelContainer = targetEl.querySelector('.applied-label');

        return targetElLabelContainer;
    }

    function closeLabelOverlay() {
        if (menuToggler.classList.contains('active')) {
            let menuType = menuToggler.nextElementSibling.dataset.menuType;

            if (menuType != 'main-menu') document.getElementById('inception-menu-styles-' + menuType).remove();

            if (menuToggler.nextElementSibling.dataset.menuType == 'main-menu') {
                document.getElementById('label-menu-overlay').classList.add('hidden');
            }

            menuToggler.classList.remove('active');
        }
    }
}

document.addEventListener('DOMContentLoaded', function() {
    let labelSelect = document.getElementById('task-label-input__values');

    labelSelect.addEventListener('change', function(e) {
       let label = labelSelect.value;

       labelSelect.querySelectorAll('option').forEach(function(option) {
          if (option.innerText == label && !option.getAttribute.disabled) {
              applyLabel(label);
          }
       });
    });

    document.getElementById('add-new-custom-label').addEventListener('click', addNewCustomLabel);
});

function addNewCustomLabel() {
    labelName = prompt(isHun ? 'Kérlek add meg a címke nevét:' : 'Add new label name');

    if (labelName) {
        if (!compassData.labels) compassData.labels = [];
        if (compassData.labels.indexOf(labelName) == -1) compassData.labels.push(labelName);

        document.getElementById('custom-labels').insertAdjacentHTML('beforeend', `<option>${labelName}</option>`);

        applyLabel(labelName);
        saveCompassToDB();
    }
}

function applyLabel(labelName, id, labelListElementOnly = false) {
    let taskId = id || document.getElementById('task-settings-modal').dataset.taskId,
        taskData = getTaskDataById(taskId),
        taskListElement = document.querySelector(`li.draggable[data-id="${taskId}"]`);

    // Apply label if not exists
    if (!taskData.labels) taskData.labels = [];

    if (taskData.labels.indexOf(labelName) == -1) {
        taskData.labels.push(labelName);
        saveDataToDB([taskData.list]);

        if (!taskListElement.querySelector('.applied-label')) taskListElement.querySelector('.task-name').insertAdjacentHTML('beforeend', '<div class="applied-label"></div>');

        taskListElement.querySelector('.applied-label').insertAdjacentHTML('beforeend', `<span class="label">${labelName}</span>`);

        taskListElement.classList.add(toId(labelName));

        if (!labelListElementOnly) {
            if (!document.querySelector('.task-input-group--labels .applied-label')) {
                document.querySelector('.applied-labels-wrapper').insertAdjacentHTML('afterend', '<div class="applied-label"></div>');
            }

            document.querySelector('.task-input-group--labels .applied-label').insertAdjacentHTML('beforeend', `<span class="label">${labelName}</span>`);

            settingsModalInjectedLabelWrapper = document.querySelector('.task-input-group--labels .applied-label .label:last-child');
            settingsModalInjectedLabelWrapper.addEventListener('click', removeLabelOnClick);
            showRelevantPrinciples(taskData);
        }
    }
}

function listAppliedLabels(data) {
    let output = '';

    // Convert to new label format
    if (data.label && typeof data.label == 'object') {
        if (!data.labels) {
            data.labels = [];

            for (category in data.label) {
                for (label in data.label[category]) {
                    data.labels.push(label);
                }
            }
        }
    }

    if (data.labels && data.labels.length > 0) {
        output += '<div class="applied-label">';

        data.labels.forEach(function(label) {
            output += `<span class="label">${label}</span>`;
        });

        output += '</div>';
    }

    return output;
}

function initTaskLabelMenu(settingsModal, taskData) {
    // Load value labels from compass to label select
    let labels = getAllLabels(),
        labelsWrapper = document.querySelector('.task-input-group--labels');

    // List top 5 most commonly used labels
    if (!commonLabels) {
        commonLabels = getLabelsByMostUsedOrder();

        // labelsWrapper.querySelector('h4').insertAdjacentHTML('afterend', `<div class="common-labels"><span class="fieldset-group-name">${isHun ? 'Gyors hozzáadás' : 'Quick add'}</span></div>`)
        // labelsWrapper.querySelector('h4').insertAdjacentHTML('afterend', `<div class="common-labels"></div>`)
        // commonLabels.forEach(label => {
        //     labelsWrapper.querySelector('.common-labels').insertAdjacentHTML('beforeend', `<span class="label">${label}</span>`);
        // });
        //
        // settingsModal.querySelectorAll('.common-labels .label').forEach(label => {
        //    label.addEventListener('click', (e) => {
        //        applyLabel(e.currentTarget.innerText);
        //    })
        // });
    }

    document.getElementById('custom-labels').innerHTML = '';

    if (labels.customLabels != 0) {
        labels.customLabels.forEach(function(label) {
            document.getElementById('custom-labels').insertAdjacentHTML('beforeend', `<option>${label}</option>`);
        });
        document.getElementById('custom-labels').classList.remove('hide');
    } else {
        document.getElementById('custom-labels').classList.add('hide');
    }


    document.getElementById('role-labels').innerHTML = '';
    labels.roles.forEach(role => {
        document.getElementById('role-labels').insertAdjacentHTML('beforeend', `<option>${role}</option>`);
    });

    // document.getElementById('value-labels').innerHTML = '';
    // labels.values.forEach(value => {
    //     document.getElementById('value-labels').insertAdjacentHTML('beforeend', `<option>${value}</option>`);
    // });

    document.getElementById('most-used').innerHTML = '';
    commonLabels.forEach(value => {
        document.getElementById('most-used').insertAdjacentHTML('beforeend', `<option>${value}</option>`);
    });

    // List already applied labels
    settingsModal.querySelector('.applied-labels-wrapper').innerHTML = '';
    if (settingsModal.querySelector('.applied-label')) {
        settingsModal.querySelector('.applied-label').remove();
    }
    settingsModal.querySelector('.applied-labels-wrapper').innerHTML = listAppliedLabels(taskData);

    if (settingsModal.querySelector('.applied-label')) {
        // settingsModal.querySelector('.applied-label').insertAdjacentHTML('afterbegin', `
		// 	<span class="fieldset-group-name">${isHun ? 'Aktív címkék' : 'Active labels'}</span>
		// `);

        document.querySelectorAll('.applied-labels-wrapper .label').forEach(function(label) {
            label.addEventListener('click', removeLabelOnClick);
        });
    }
}


function removeLabelOnClick(e) {
    if (confirm(isHun ? 'Szeretnéd eltávolítani ezt a címkét?' : 'Do you want to remove this label?')) {
        let taskData = getTaskDataById(document.getElementById('task-settings-modal').dataset.taskId),
            taskListElement = document.querySelector(`li.draggable[data-id="${taskData.id}"]`),
            labelName = e.currentTarget.innerText,
            index;

        taskData.labels.forEach(function(label, i) {
            if (label == labelName) index = i;
        });

        taskData.labels.splice(index, 1);
        e.currentTarget.remove();

        taskListElement.classList.remove(toId(labelName));
        taskListElement.querySelectorAll('.label').forEach(function(el) {
            if (el.innerText == labelName) el.remove();
        });
        saveDataToDB([taskData.list]);
        showRelevantPrinciples(taskData);
    }
}

function getAllLabels() {
    let labels = {
        customLabels: [],
        roles: [],
        values: []
    };

    if (compassData.principles) {
        compassData.principles.forEach(function(principle) {
            if (principle.label && labels.customLabels.indexOf(principle.label) == -1) {
                labels.customLabels.push(principle.label);
            }
        });
    }

    // Load custom labels from compass and merge with principle labels
    if (compassData.labels) {
        compassData.labels.forEach(function(label) {
            if (labels.customLabels) {
                if (label && labels.customLabels.indexOf(label) == -1) {
                    labels.customLabels.push(label);
                }
            }
        });
    }

    if (compassData.roles) {
        for (role in compassData.roles) {
            labels.roles.push(role);
        }
    }

    if (compassData.values) {
        compassData.values.forEach(function(value) {
            labels.values.push(value.name);
        });
    }

    return labels;
}

function getLabelsByMostUsedOrder() {
    let labels = [];

    for (list in myLists) {
        for (category in myLists[list].categories) {
            myLists[list].categories[category].forEach(task => {
                if (task.labels) {
                    task.labels.forEach(label => {
                        let labelIndex = -1;

                        labels.forEach((l, i) => {
                            if (l.name == label) labelIndex = i;
                        });

                        if (labelIndex == -1) {
                            labels.push({
                                name: label,
                                occurence: 1
                            });
                        } else {
                            labels[labelIndex].occurence++;
                        }
                    })
                }
            });
        }
    }

    labels.sort((a, b) => b.occurence - a.occurence);
    labels = labels.splice(0, 5);
    labels = labels.map(label => label.name);

    return labels;
}

function getUsedLabels() {
    let labelLists = [];

    for (listName in myLists) {
        for (category in myLists[listName].categories) {
            myLists[listName].categories[category].forEach(task => {
                if (task.labels && task.labels.length > 0){
                    task.labels.forEach(label => {
                        labelLists.push(label);
                    })
                }
            })
        }
    }
    const counts = {};
    labelLists.forEach((x) => {
        counts[x] = (counts[x] || 0) + 1;
    });
    labelLists = Object.keys(counts).sort((a,b) => counts[b]-counts[a])

    labelLists.push('Unlabeled')
    console.log(labelLists)
    return labelLists;
}

function filterListsByLabel(label) {
    const normalizedLabel = toId(label);

    clearLabelFilter();
    document.querySelectorAll('#word-cloud li').forEach(el => {
        if (el.dataset.value == label) el.dataset.active = true
    });
    document.querySelectorAll('.task-list-wrapper .draggable').forEach(el => {
        if (label != 'Unlabeled') {
            if (!el.classList.contains(normalizedLabel)) {
                let taskId = el.dataset.id;

                el.classList.add('hide');
            }
        } else {
            getUsedLabels().forEach(l => {
                if (el.classList.contains(toId(l))) el.classList.add('hide')
            });
        }
    })
    document.querySelectorAll('.list-container').forEach(list => {
        recalcListTimeSum(list);
    });
    updateListItemCounter(Object.keys(myLists), label);
}

function clearLabelFilter() {
    document.querySelectorAll('.task-list-wrapper .draggable').forEach(el => {
        let taskData = getTaskDataById(el.dataset.id);
        el.classList.remove('hide')
    });
    document.querySelectorAll('.list-container').forEach(list => {
        recalcListTimeSum(list);
    });
    document.body.removeAttribute('data-filter-label')
    updateListItemCounter();
    document.querySelectorAll('#word-cloud li').forEach(el => el.dataset.active = false);
}