function initPrincipleMindMap() {
    // if (!window.jm) {
        let principles = [],
            labels = {},
            jsMindContainer = document.getElementById('jsmind_container');

        jsMindContainer.innerHTML = '';

        compassData.principles.forEach(function(principle, i) {
            let randomNo = getRandomIntInclusive(1, 2),
                direction;

            switch(randomNo) {
                case 1:
                    direction = 'left';
                    break;
                case 2:
                    direction = 'right';
                    break;
            }

            if (principle.label) {
                if (!labels[principle.label]) {
                    labels[principle.label] = [];

                    principles.push({
                        id : principle.label,
                        topic : principle.label,
                        direction: direction,
                        "background-color": "#4d2780"
                    });
                }

                labels[principle.label].push({
                    id : principle.label + "-principle-" + i,
                    topic : principle.name,
                    "background-color": "#474850"
                });
            } else {
                principles.push({
                    id : "principle-" + i,
                    topic : principle.name,
                    direction: direction,
                    "background-color": "#474850"
                });
            }
        });

        function getRandomIntInclusive(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
        }

        function load_jsmind(){
            var mind = {
                "meta":{
                    "name":"principles",
                    "author":"gkriston84@gmail.com",
                    "version":"0.1",
                },
                "format":"node_tree",
                "data": {
                    "id":"root",
                    "background-color": "#f67d2c",
                    "topic": isHun ? "Elvek" : "Principles","children": principles
                }
            };

            for (label in labels) {
                mind.data.children.forEach(function(child, i) {
                   if (child.id == label) {
                       mind.data.children[i].children = labels[label];
                   }
                });
            }

            var options = {
                container:'jsmind_container',
                editable:true,
                theme:'primary'
            }
            window.jm = jsMind.show(options,mind);
            // jm.set_readonly(true);
            // var mind_data = jm.get_data();
            // alert(mind_data);
            // jm.add_node("biases","biases1", "Bias1", {"background-color":"red"});
            // jm.set_node_color('sub21', 'green', '#ccc');
        }

        load_jsmind();
    // }

    jsMindContainer.classList.add('show');
    jsMindContainer.classList.remove('hide');
}

