function initDailyCardsSlider() {
    let cardSliderWrapper = document.getElementById('card-slider');

    document.getElementById('cards-overlay').classList.toggle('hide');
    document.getElementById('card-icon').classList.add('checked');

    if (cardSliderWrapper && !cardSliderWrapper.classList.contains('slick-initialized')) {
        jQuery('#card-slider').slick({
            infinite: true,
            prevArrow: '<button type="button" class="slick-prev"><</button>',
            nextArrow: '<button type="button" class="slick-next">></button>',
            responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    respondTo: 'window',
                    adaptiveHeight: true
                }
            }]
        });

        document.querySelectorAll('.add-card-principle').forEach(function(btn) {
            btn.addEventListener('click', function(e) {
                let principle = e.currentTarget.closest('.card-principle').querySelector('.card-principle-text').textContent;

                addNewPrinciple(principle);
            });
        });

        document.getElementById('cards-overlay').addEventListener('click', function(e) {
            if (e.target.getAttribute('id') == 'card-slider' || e.target.classList.contains('slick-track')) {
                document.getElementById('cards-overlay').classList.add('hide');
            }
        });
    }
}

function initPrincipleSlider() {
    let principlesSliderWrapper = document.getElementById('principles-slider-overlay'),
        sliderItems = '';

    // if (!principlesSliderWrapper) {

        if (principlesSliderWrapper) {
            jQuery('#principles-slider-overlay').slick('unslick');
            principlesSliderWrapper.remove();
        }

        compassData.principles.forEach(function(principle) {
            sliderItems += `
			<div class="simple-card">
				<div class="simple-card-inner">
					<div class="simple-card-content">
					    ${principle.label ? '<div class="principle-label">' + principle.label + '</div>' : ''}
					    <span class="principle-tool edit-principle"><img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/edit.svg" height="45"></span>
                        <span class="principle-tool edit-principle-label"><img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/label-2.svg" height="45"></span>
					    ${principle.task ? `<span class="principle-tool show-related-task">
					        <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/task-identification.svg" height="45">
					        <span class="related-task-info">${
                                    isHun ? 
                                    'Ez az elv a következő feladatnál lett létrehozva: ' :
                                    'This principle was created regards this task: '
                                    } <span class="related-task-name">${principle.task}</span>
					        </span>
                        </span>` : ''}
						<h2>${principle.name}</h2>
					</div>
				</div>
			</div>`;
        });

        let sliderHTML = `<div id="principles-slider-overlay" class="hide app-overlay">${sliderItems}</div>`;
        document.body.insertAdjacentHTML('beforeend', sliderHTML);

        let principlesSliderOverlay = document.getElementById('principles-slider-overlay');
        principlesSliderOverlay.classList.remove('hide');

        jQuery('#principles-slider-overlay').slick({
            infinite: true,
            prevArrow: '<button type="button" class="slick-prev"><</button>',
            nextArrow: '<button type="button" class="slick-next">></button>',
            responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    respondTo: 'window',
                    adaptiveHeight: true
                }
            }]
        });
    // }

    principlesSliderOverlay.classList.remove('hide');

    principlesSliderOverlay.addEventListener('click', function(e) {
        if (e.target.getAttribute('id') == 'principles-slider-overlay' || e.target.classList.contains('slick-track')) {
            principlesSliderOverlay.classList.add('hide');
        }
    });

    document.querySelectorAll('.edit-principle').forEach(function(el) {
        el.addEventListener('click', function(e) {
            let oldPrinciple = e.currentTarget.closest('.simple-card-content').querySelector('h2').textContent;
            let newPrinciple = prompt(isHun ?
                'Hogyan szeretnéd átfogalmazni az elvet?\n\n' +
                'Kérlek vedd figyelembe a következőket:\n' +
                '- Milyen feladat kapcsán fogalmaztad meg eredetileg az elvet?\n' +
                '- Milyen más szituációkban alkalmazható az elv?\n\n' +
                'Hiedelmek felülírása:\n' +
                '- Az elv hozta az elvárt eredményeket?\n' +
                '- Milyen múltbeli esemény miatt gondoltad, hogy ez egy jó elv?\n' +
                '- Hogyan tudnád átfogalmazni az elvet, hogy igazán szolgáljon téged?\n' +
                '- Hogyan kellene megfogalmazni az elvet, hogy 10x-es eredményt hozzon?' :

                'Enter updated principle\n\n' +
                'Please consider the following:\n' +
                '- In relation to what task have you created this principle?\n' +
                '- What situations was this principle also used in?\n\n' +
                'Challenging beliefs:\n' +
                '- Was it delivering the results you have expected?\n' +
                '- What experience made you think that this is a good principle?\n' +
                '- How can you rephrase this principle to truly serve you?\n' +
                '- What would be the principle to deliver 10x results?',
                oldPrinciple);

            if (newPrinciple && oldPrinciple != newPrinciple) {
                e.currentTarget.closest('.simple-card-content').querySelector('h2').textContent = newPrinciple;

                compassData.principles.forEach(function(principle) {
                    if (principle.name == oldPrinciple) principle.name = newPrinciple;
                });

                document.querySelectorAll('#principles .compass__group__list_element').forEach(function(el) {
                    let elText = el.childNodes[0].nodeValue;
                    if (elText == oldPrinciple) el.childNodes[0].nodeValue = newPrinciple;
                });

                saveCompassToDB();
            }
        });
    });

    document.querySelectorAll('.edit-principle-label').forEach(function(el) {
        el.addEventListener('click', function(e) {
            let editedPrinciple = e.currentTarget.closest('.simple-card-content').querySelector('h2').textContent,
                principleIndex,
                oldPrincipleLabel,
                availableLabels = [];

            compassData.principles.forEach(function(principle, i) {
                if (principle.name == editedPrinciple) {
                    principleIndex = i;
                    oldPrincipleLabel = principle.label ? principle.label : '';
                }

                if (principle.label && availableLabels.indexOf(principle.label) == -1) {
                    availableLabels.push(principle.label);
                }
            });

            availableLabels = availableLabels.join(', ');

            let newPrincipleLabel = prompt(
                isHun ?
                    'Hogyan szeretnéd címkézni az elvet?\n' +
                    (availableLabels.length > 0 ? '\nLétező kategóriák:\n' + availableLabels : '') :
                    'Enter principle label\n' +
                    (availableLabels.length > 0 ? '\nExisting categories:\n ' + availableLabels : '')
                    , oldPrincipleLabel);

            if (newPrincipleLabel && oldPrincipleLabel != newPrincipleLabel) {
                let labelWrapper = e.currentTarget.closest('.simple-card').querySelector('.principle-label');

                if (labelWrapper) {
                    labelWrapper.textContent = newPrincipleLabel;
                } else {
                    e.currentTarget.closest('.simple-card-content').insertAdjacentHTML('afterbegin', `
                     <div class="principle-label">${newPrincipleLabel}</div>
                    `);
                }

                compassData.principles[principleIndex].label = newPrincipleLabel;
                saveCompassToDB();
            } else if (!newPrincipleLabel && compassData.principles[principleIndex].label) {
                delete compassData.principles[principleIndex].label;
                e.currentTarget.closest('.simple-card-content').querySelector('.principle-label').remove();
                saveCompassToDB();
            }
        });
    });
}