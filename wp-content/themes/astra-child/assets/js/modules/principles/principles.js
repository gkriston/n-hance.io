function addNewPrinciple(principleName = '', taskName, taskId) {
    let availableLabels = [],
        isExistingPrinciple = false;

    compassData.principles.forEach(function(principle) {
        if (principle.name == principleName) {
            //alert(isHun ? 'Ez az elv már létezik' : 'This principle already exists');
            isExistingPrinciple = true;
        }

        if (principle.label && availableLabels.indexOf(principle.label) == -1) {
            availableLabels.push(principle.label);
        }
    });

    if (isExistingPrinciple) return;

    if (compassData.labels) {
        // Load custom labels from compass and merge with principle labels
        compassData.labels.forEach(function(label) {
            if (label && availableLabels.indexOf(label) == -1) {
                availableLabels.push(label);
            }
        });
    }

    availableLabels = availableLabels.join(', ');

    if (!principleName) principleName = prompt(isHun ? 'Fogalmazd meg az új elvet' : 'Summarize the new princple');

    if (!principleName) return;

    // let setLabelNow = confirm(
    //     isHun ?
    //         'Szeretnéd kategorizálni az elvet a feladat jellege alapján?' :
    //         'Do you want to categorize this principle based on the task type?');

    compassData.principles.push({
        name: principleName,
        task: taskName ? taskName : ''
    });

    if (setLabelNow) {
        generateWordCloud('principle-label', principleName)
    }

    // if (confirm(isHun ? 'Szeretnél egy megosztási linket generálni az elvhez?' : 'Do you want to generate a sharing link to this principle?')) buildPrincipleSharingURL(principleName, label);

    // if (label && taskName && taskId) {
    //     applyLabel(label, taskId, true);
    // }

    initPrincipleBoard();
    saveCompassToDB();
}

function getPrincipleLabels() {
    let labels = [];

    if (compassData.principles) {
        compassData.principles.forEach(function (principle, i) {
            if (principle.label && labels.indexOf(principle.label) > -1) labels.push(principle.name);
        });
    }

    return labels;
}

function removePrinciple(e) {
    let parentListElement = e.currentTarget.closest('li');
    principleName = e.currentTarget.closest('li').textContent;

    if (confirm(isHun ? 'Szeretnéd törölni ezt az elvet?' : 'Do you really want to delete this principle?'));
    parentListElement.remove();

    compassData.principles.forEach(function(principle, i) {
        if (principle.name == principleName) {
            compassData.principles.splice(i, 1);
        }
    });

    saveCompassToDB();
}

function attachPrincipleEventHandlers() {
    document.querySelector('#principle-visualization .close').addEventListener('click', function(){
        document.getElementById('principle-visualization').classList.toggle('show');
    });
    document.getElementById('principle-card-view-button').addEventListener('click', initPrincipleSlider);
    document.getElementById('principle-board-view-button').addEventListener('click', function() {
        initPrincipleBoard();
        document.getElementById('jsmind_container').classList.remove('show');
        document.getElementById('jsmind_container').innerHTML = '';
    });
    document.getElementById('principle-mindmap-view-button').addEventListener('click', initPrincipleMindMap);
    document.getElementById('add-new-principle').addEventListener('click', function(e) {addNewPrinciple()});
}

function initPrincipleBoard() {
    let principlesHorizontal = document.getElementById('principles-horizontal-view');

    principlesHorizontal.innerHTML = '';
    myPrinciples = {
        'Uncategorized': []
    };

    compassData.principles.forEach(function(principle) {
        if (principle.label) {
            if (!myPrinciples[principle.label]) myPrinciples[principle.label] = [];
            myPrinciples[principle.label].push(principle.name);
        } else {
            myPrinciples['Uncategorized'].push(principle.name);
        }
    });

    for (category in myPrinciples) {
        let categoryWrapper = principlesHorizontal.querySelector('.' + toId(category));

        if (!categoryWrapper) {
            principlesHorizontal.insertAdjacentHTML('beforeend', '<div class="principles-list-category-wrapper"><h2>' + category + '</h2><ul class="principles-list ' + toId(category) + ' "></ul></div>');
        }

        categoryWrapper = principlesHorizontal.querySelector('.' + toId(category));

        myPrinciples[category].forEach(function(principle) {
            categoryWrapper.insertAdjacentHTML('beforeend', '<li>' + principle + '<button class="remove-principle"><img class="svg-icon" src="' + appVariables.templateUrl + '/assets/images/svg-icons/delete-icon.svg"></button></li>');
        });
    }

    principlesHorizontal.querySelectorAll('.remove-principle').forEach(function(removeBtn) {
        removeBtn.addEventListener('click', removePrinciple);
    });
}

document.getElementById('principle-icon').addEventListener('click', function(e) {
    initPrincipleBoard();
    document.getElementById('principle-visualization').classList.toggle('show');
});

document.getElementById('card-icon').addEventListener('click', initDailyCardsSlider);

function getRelevantPrinciples(taskData) {
    let relevantPrinciples = {},
        principlesString = '';

    // If there is a task label set and there are principles categorized under that label show them here
    if (compassData.principles) {
        compassData.principles.forEach(function (principle, i) {
            let taskLabels = taskData.labels ? taskData.labels : [];

            if (taskLabels.length > 0 && principle.label && taskLabels.indexOf(principle.label) > -1) {
                if (!relevantPrinciples[principle.label]) {
                    relevantPrinciples[principle.label] = [];
                }

                relevantPrinciples[principle.label].push(principle.name);
            }
        });
    }

    return relevantPrinciples;

}

function createPrinciplesString(relevantPrinciples) {
    let principlesString = '';

    if (Object.keys(relevantPrinciples).length > 0) {
        for (category in relevantPrinciples) {
            let numOfCategories = Object.keys(relevantPrinciples).length,
                isLastCategory = Object.keys(relevantPrinciples)[numOfCategories - 1] == category;

            principlesString += category + '\n';
            relevantPrinciples[category].forEach(function(principle, i, arr) {
                principlesString += '- ' + principle + '\n';
            });

            if (!isLastCategory) principlesString += '\n\n';
        }
    }

    return principlesString;
}

function getTaskPrinciple(taskData) {
    let taskPrinciple = '';

    if (compassData.principles && compassData.principles.length > 0) {
        compassData.principles.forEach(principle => {
            if (principle.task == taskData.name) taskPrinciple = principle.name;
        })
    }

    return taskPrinciple;
}

function showRelevantPrinciples(taskData) {
    let principlesWrapper = document.querySelector('#task-settings-modal .relevant-principles-wrapper'),
        relevantPrinciples = getRelevantPrinciples(taskData);

    if (Object.keys(relevantPrinciples).length > 0) {
        principlesWrapper.innerHTML = createRelevantPrinciplesHTML(relevantPrinciples);

        document.querySelector('.relevant-principles').classList.remove('hide');
    } else {
        document.querySelector('.relevant-principles').classList.add('hide');
    }
}

function createRelevantPrinciplesHTML(relevantPrinciples) {
    let principlesHTML = '';

    for (category in relevantPrinciples) {
        principlesHTML += `
            <div class="relevant-principles__category">
                <h5>${category}</h5>
                <div class="relevant-principles__category__elements" data-principle-category="${category}">
                    <ul>
                        ${
                            function() {
                                let listElementsHTML = '';
                
                                relevantPrinciples[category].forEach(function(principle) {
                                    listElementsHTML += `<li>${principle}</li>`;
                                })
                
                                return listElementsHTML;
                            }()
                        }
                    </ul>
                </div>
            </div>
        `;
    }

    return principlesHTML;
}



// Example: https://localhost/n-hance.io/app/?action=import_principle&user=gabor231&principle=First+things+first&label=General
function parsePrincipleSharingURL() {
    const parsedURL = new URLSearchParams(window.location.search),
        principleName = parsedURL.get('principle'),
        label = parsedURL.get('label');

    let existingPrinciple = false;

    compassData.principles.forEach(function(principle) {
        if (principle.name == principleName) existingPrinciple = true;
    });

    if (existingPrinciple) {
        alert(isHun ? 'Ez az elv már létezik' : 'This principle already exists');
        return;
    }

    compassData.principles.push({
        name: principleName,
        label: label,
        task: ''
    });

    alert((isHun? 'Elv beimportálva:': 'Principle imported:') + `\n${principleName} - ${label}`);

    saveCompassToDB();
}

function buildPrincipleSharingURL(principle, label) {
    const domain = window.location.origin,
        path = window.location.pathname;

    let sharingUrl = domain + path + '?' + new URLSearchParams({
        action: 'import_principle',
        user: userName,
        principle: principle,
        label: label
    }).toString();

    console.log(sharingUrl);
    alert(sharingUrl);
}

function injectPrinciplesToNoteTool() {
    let principleTool = document.querySelector('.editor-supportive-tools.principles'),
        principlesWrapper = principleTool.querySelector('.relevant-principles__categories'),
        relevantPrinciples = {},
        notRelatedPrinciples = {}

    if (!principleTool.classList.contains('hidden')) {
        principleTool.classList.add('hidden');
        return;
    }

    principlesWrapper.innerHTML = '';

    if (document.getElementById('editor-wrapper').dataset.id) {
        let taskId = document.getElementById('editor-wrapper').dataset.id,
            taskData = getTaskDataById(taskId);
            relevantPrinciples = getRelevantPrinciples(getTaskDataById(taskId)),
            taskPrinciple = {};

        if (taskData.principle) {
            let key = isHun ? 'Fő elv' : 'Main principle';
            taskPrinciple[key] = [];
            taskPrinciple[key].push(taskData.principle);

            principlesWrapper.innerHTML = createRelevantPrinciplesHTML(taskPrinciple) + '<hr>';

            principleTool.classList.remove('hidden');
        }

        if (Object.keys(relevantPrinciples).length > 0) {
            principleTool.classList.remove('hidden');
            principlesWrapper.insertAdjacentHTML('beforeend', createRelevantPrinciplesHTML(relevantPrinciples));
        }

        principleTool.querySelector('.editor-supportive-tools__inner h4').innerHTML = isHun ? 'Releváns elvek' : 'Relevant principles';
    } else {
        principleTool.querySelector('.editor-supportive-tools__inner h4').innerHTML = isHun ? 'Elvek' : 'Principles';
    }


    compassData.principles.forEach(function (principle, i) {
        let name = principle.name,
            label = principle.label || 'Uncategorized',

        notRelatedPrinciple = !relevantPrinciples[label] || relevantPrinciples[label].indexOf(name) == -1;

        if (notRelatedPrinciple) {
            if (!notRelatedPrinciples[label]) notRelatedPrinciples[label] = [];
            notRelatedPrinciples[label].push(name);
        }
    });

    let prefix = Object.keys(relevantPrinciples).length != 0 ? '<hr>' + (isHun ? '<h4>Egyéb elvek</h4>' : '<h4>Egyéb elvek</h4>') : '';

    principleTool.classList.remove('hidden');
    principlesWrapper.insertAdjacentHTML('beforeend', prefix  + createRelevantPrinciplesHTML(notRelatedPrinciples));
}
