function continueTimeTracking() {
    let taskId = userOptions.timeTracking.trackedTaskId;

    // If task has been deleted stop tracking
    if (!getTaskDataById(taskId)) {
        document.getElementById('time-tracker-wrapper').classList.add('hidden');
        userOptions.timeTracking = {
            trackingNow: false
        };
        saveUserOptions(userOptions);

        return;
    }

    trackedTime = getTrackedTime();
    document.querySelector(`#draggable-list-item-${taskId} .track-time`).setAttribute('data-tracking', 'on');
    document.getElementById('time-tracker-wrapper').classList.remove('hidden');
    document.querySelector('#time-tracker-wrapper span').innerHTML = convertMinutesToTimeString(trackedTime, true);
    document.getElementById('tracked-task-name').innerHTML = getTaskDataById(taskId).name;
    currentlyTrackedTaskId = taskId;

    timeTracker = setInterval(timeTrackerInterval, 1000);
}

function getTrackedTime() {
    let taskId = userOptions.timeTracking.trackedTaskId,
        timeFormat = 'YYYY-MM-DDT:HH:mm:ss',
        trackingStart = userOptions.timeTracking.trackingStart,
        timeNow = moment().format(timeFormat),
        duration = moment.duration(moment.utc(moment(timeNow, timeFormat).diff(moment(trackingStart, timeFormat)))),
        trackedTime = duration.hours() * 3600 + duration.minutes() * 60 + duration.seconds();

    return trackedTime;
}


function trackTime(e) {
    let btn = e.currentTarget,
        listElement = btn.closest('.draggable'),
        taskId = listElement.dataset.id,
        taskData = getTaskDataById(taskId);

    currentlyTrackedTaskId = stopCurrentTimeTracking();
    // Refresh task data as this is not a pale task anymore if the user started to track time
    taskData.creationDate = dateToday;
    taskData.moveCount = 0;
    saveDataToDB([taskData.list]);
    listElement.classList.remove('tossed');
    listElement.classList.remove('old');

    if (!currentlyTrackedTaskId || currentlyTrackedTaskId != taskId) {
        btn.setAttribute('data-tracking', 'on');
        document.getElementById('time-tracker-wrapper').classList.remove('hidden');
        document.querySelector('#time-tracker-wrapper span').innerHTML = '0:00:00';
        currentlyTrackedTaskId = taskId;

        timeTracker = setInterval(timeTrackerInterval, 1000);
    }
}

function timeTrackerInterval() {
    if (!currentlyTrackedTaskId) {
        clearInterval(timeTracker);
        return;
    }

    let taskData = getTaskDataById(currentlyTrackedTaskId);

    if (!taskData.trackedTime) taskData.trackedTime = 0;

    // This block is required when the function is called upon page load when time tracking is already in progress
    if (!userOptions.timeTracking || !userOptions.timeTracking.trackingNow) {
        userOptions.timeTracking = {
            trackingNow: true,
            trackedTaskId: currentlyTrackedTaskId,
            trackingStart: moment().format("YYYY-MM-DDT:HH:mm:ss")
        }
        updateUserOptions();
    }

    trackedTime = getTrackedTime();
    document.getElementById('tracked-task-name').innerHTML = taskData.name;
    document.querySelector('#time-tracker-wrapper span').innerHTML = convertMinutesToTimeString(Math.floor(trackedTime / 60), true) + ':' + (trackedTime % 60 < 10 ? '0' + trackedTime % 60 : trackedTime % 60);

    // if (trackedTime % 60 == 0) {
    //     taskData.trackedTime++;
    //
    //     saveDataToDB([taskData.list], false, true);
    //     userOptions.timeTracking.lastTracked = moment().format("YYYY-MM-DDT:HH:mm:ss");
    //     updateUserOptions();
    //     // Track time on DB save
    //     logTaskDataOnCompletion(currentlyTrackedTaskId, Math.floor(trackedTime / 60), true);
    //     //console.log('Tracked time saved to DB');
    // }

    //console.log(trackedTime);
}

function stopCurrentTimeTracking(isCompletion, sync) {
    let btn = document.querySelector('.track-time[data-tracking="on"]'),
        trackedTaskId = false;

    if (btn) {
        let listItem = btn.closest('.draggable');
        trackedTaskId = listItem.dataset.id;

        console.log(userOptions.timeTracking);
        userOptions.timeTracking = {
            trackingNow: false
        };
        if (!sync) updateUserOptions();

        let taskData = getTaskDataById(trackedTaskId),
            taskTimeMins = Math.ceil(trackedTime / 60);

        taskData.trackedTime += taskTimeMins;

        document.getElementById('time-tracker-wrapper').classList.add('hidden');
        btn.setAttribute('data-tracking', 'off');


        if (taskTimeMins) {
            if (userOptions.impact_popup == 'on' && !sync) initAnalytics(false, true);
            if (!sync) logTaskTime(trackedTaskId, taskTimeMins);
            listItem.querySelector('.task-tracked-time span').innerHTML = convertMinutesToTimeString(taskData.trackedTime);
            document.getElementById('tracked-task-name').innerHTML = '';
            listItem.classList.add('time-recorded');
            console.log(taskData.name + ' - current tracked time: ' + taskTimeMins + 'm');
            console.log(taskData.name + ' - total tracked time: ' + taskData.trackedTime + 'm');
        } else if (!taskData.trackedTime && taskData.time && isCompletion) {
            // If tracked time is not available we need to log the original task time but only if this is a task completion otherwise we keep saving the original time estimate more than once
            logTaskTime(trackedTaskId, taskData.time);
        }
    }

    clearInterval(timeTracker);
    currentlyTrackedTaskId = undefined;
    trackedTime = 0;

    return trackedTaskId;
}