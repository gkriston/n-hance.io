function initDayViewCalendar(rerender) {
    let ThirdPartyDraggable = FullCalendar.ThirdPartyDraggable;
    let isMobile = window.innerWidth < 992;

    if (dayviewCalendar && !rerender) return;

    dayviewCalendarEl = document.getElementById('dayview-calendar');
    dayviewCalendarEl.innerHTML = '';

    if (document.body.classList.contains('dayview-calendar-visible')) {
        document.body.classList.remove('board-view');
    }

    if (!rerender) {
        if (dayviewCalData && dayviewCalData.length > 0) {
            dayviewCalDataBackup = JSON.stringify(dayviewCalData);
            // Clear items older than a month by date
            dayviewCalData = dayviewCalData.filter(entry => {
                return moment(entry.start.split('T')[0]).diff(moment(), 'days') > -30;
            });

            // Clear redundant items without task
            dayviewCalData = dayviewCalData.filter(entry => {
                return getTaskDataById(entry.id);
            });

            // Make sure the right classes and resources are set
            dayviewCalData.forEach(entry => {
                let taskData = getTaskDataById(entry.id);

                if (!entry.classNames) entry.classNames = taskData.categoryClass;

                if (!entry.resourceId) entry.resourceId = (taskData.categoryClass == 'enhance' ? 'a' : (taskData.categoryClass == 'must-do' ? 'b' : 'c')) + (6 - taskData.effect);
            });

            // dayviewCalData.forEach((entry,i) => {
            //     if (entry.start.indexOf('T') != entry.start.lastIndexOf('T')) {
            //         let entryParts = dayviewCalData[i].start.split('T');
            //         dayviewCalData[i].start = entryParts[0] + 'T' +  entryParts[2];
            //     }
            // });
            //
            // dayviewCalData.forEach((entry,i) => {
            //     if (entry.end && entry.end.indexOf('T') != entry.end.lastIndexOf('T')) {
            //         let entryParts = dayviewCalData[i].end.split('T');
            //         dayviewCalData[i].end = entryParts[0] + 'T' +  entryParts[2];
            //     }
            // });
        } else {
            dayviewCalData = [];
        }

        // Add scheduled items if missing
        for (list in myLists) {
            for (category in myLists[list].categories) {
                myLists[list].categories[category].forEach(function (task) {
                    if (task.day) {
                        let isInCalendar = dayviewCalData.some(entry => entry.id == task.id);

                        if (!isInCalendar) {
                            dayviewCalData.push({
                                title: task.name,
                                start: moment(task.day).format('YYYY-MM-DD'),
                                id: task.id,
                                classNames: task.categoryClass
                            });
                        }
                    }
                });
            }
        }
    }


    // If we are only rerendering the calendar we don't need to move the tasks within lists
    if (!rerender) assignScheduledTasksToLists();
    initCompassTimeline();

    dayviewCalendar = new FullCalendar.Calendar(dayviewCalendarEl, {
        // plugins: isMobile ? ['dayGrid', 'interaction'] : [ 'dayGrid', 'timeGrid', 'resourceTimeline', 'interaction' ],
        resourceGroupField: 'importance',
        resources: [
            { id: 'a5', importance: 'Wildly Imp.', title: '200' },
            { id: 'a4', importance: 'Wildly Imp.', title: '400' },
            { id: 'a3', importance: 'Wildly Imp.', title: '600' },
            { id: 'a2', importance: 'Wildly Imp.', title: '800' },
            { id: 'a1', importance: 'Wildly Imp.', title: '1000' },
            { id: 'b5', importance: 'Must-Do', title: '20' },
            { id: 'b4', importance: 'Must-Do', title: '40' },
            { id: 'b3', importance: 'Must-Do', title: '60' },
            { id: 'b2', importance: 'Must-Do', title: '80' },
            { id: 'b1', importance: 'Must-Do', title: '100' },
            { id: 'c5', importance: 'Optional', title: '2' },
            { id: 'c4', importance: 'Optional', title: '4' },
            { id: 'c3', importance: 'Optional', title: '6' },
            { id: 'c2', importance: 'Optional', title: '8' },
            { id: 'c1', importance: 'Optional', title: '10' },
        ],
        aspectRatio: 1.6,
        customButtons: {
            planButton: {
                text: 'Plan ⯆',
                click: function() {
                    if (!document.getElementById('plan-menu')) {
                        let planMenuHTML = `
                        <ul id="plan-menu" class="hide">
                            <li onclick="initCalView('resourceTimelineDay')">Day</li>
                            <li onclick="initCalView('resourceTimelineWeek')">Week</li>
                            <li onclick="initCalView('resourceTimelineYear')">Year</li>
                        </ul>
                    `;

                        document.querySelector('.fc-toolbar-chunk:last-of-type .fc-button-group').insertAdjacentHTML('beforeend', planMenuHTML);
                    }

                    document.getElementById('plan-menu').classList.toggle('hide');
                    document.querySelector('.fc-planButton-button').classList.toggle('menu-open');
                }
            }
        },
        headerToolbar: {
            right: isMobile ? 'timeGridDay, listWeek' : `timeGridWeek,dayGridMonth${userOptions.beta_features == 'on' ? ',planButton' : ''}`,
            left:   'today prev,next',
            center: 'title',
        },

        editable: true,
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        locale: isHun ? 'hu' : 'en',
        buttonText: isHun ? {week: 'Hét', month: 'Hónap', today: 'Ma', day: 'Nap', year: 'Év'} : {week: 'Week', month: 'Month', today: 'Today', day: 'Day', year: 'Year'},
        initialView: isMobile ? 'timeGridDay' : 'timeGridWeek',
        allDayText: isHun ? 'Határidős' : 'Priorities',
        slotDuration: '00:15:00',
        droppable: true,
        slotLabelInterval: "01:00",
        scrollTime: moment().format('HH:mm:ss'),
        slotMinTime: '00:00:00',
        displayEventTime: false,
        // draggedElId: -1,
        titleFormat: {
            month: 'long',
            year: 'numeric'
        },
        navLinks: true,
        navLinkDayClick: function(date, jsEvent) {
            initCalView('resourceTimelineDay', jsEvent);
            dayviewCalendar.gotoDate(date);
        },
        nowIndicator: true,
        eventSources: [
            dayviewCalData,
            // 'gkriston84@gmail.com'
        ],
        // googleCalendarApiKey: 'AIzaSyAICYOv7H4Cr3mEMjw-vaOB9CIx9Wt5oQY',
        views: {
            timeGridWeek: {
                titleFormat: {
                    month: 'short',
                    year: 'numeric'
                },
                dayHeaderContent: function(date) {
                    const isToday = moment(date.date).format('YYYY-MM-DD') == dateToday

                    return { html: `${moment(date.date).format('ddd')} <b class="${isToday ? 'today' : ''}">${moment(date.date).format('D')}</b>`}
                },
            },
            resourceTimelineDay: {
                titleFormat: {
                    month: 'long',
                    day: 'numeric',
                    year: 'numeric'
                },
                slotMinWidth: 15,
                eventMinWidth: 15,
                resourceAreaWidth: '15%',
                slotDuration: '00:15:00',
                viewDidMount: function() {
                    dayviewCalendar.gotoDate(dateToday)
                },
                visibleRange: {
                    start: dateToday,
                    end: dateToday
                },
                resourceAreaHeaderContent: 'Importance',
                resourceGroupLabelClassNames: function(arg) {
                    if (arg.groupValue == 'Wildly Imp.') {
                        return [ 'enhance' ]
                    } else if (arg.groupValue == 'Must-Do') {
                        return [ 'must-do' ]
                    } else {
                        return [ 'optional' ]
                    }
                }
            },
            resourceTimelineWeek: {
                titleFormat: {
                    month: 'short',
                    year: 'numeric'
                },
                slotMinWidth: 15,
                eventMinWidth: 15,
                resourceAreaWidth: '15%',
                slotDuration: '24:00:00',
                slotLabelInterval: {
                    days: 1
                },
                viewDidMount: function() {
                    dayviewCalendar.gotoDate(dateToday)
                },
                visibleRange: {
                    start: dateToday,
                    end: dateToday
                },
                resourceAreaHeaderContent: 'Importance',
                resourceGroupLabelClassNames: function(arg) {
                    if (arg.groupValue == 'Wildly Imp.') {
                        return [ 'enhance' ]
                    } else if (arg.groupValue == 'Must-Do') {
                        return [ 'must-do' ]
                    } else {
                        return [ 'optional' ]
                    }
                },
                slotLabelFormat: function(date) {
                    const isToday = moment(date.date).format('YYYY-MM-DD') == dateToday

                    return { html: `${moment(date.date).format('ddd')} <b class="${isToday ? 'today' : ''}">${moment(date.date).format('D')}</b>`}
                },
            },
            resourceTimelineYear: {
                titleFormat: {
                    month: 'short',
                    year: 'numeric'
                },
                slotMinWidth: 40,
                eventMinWidth: 40,
                resourceAreaWidth: '15%',
                slotDuration: {
                    days: 7
                },
                viewDidMount: function() {
                    dayviewCalendar.gotoDate(dateToday)
                },
                navLinkDayClick: function(date, jsEvent) {
                    initCalView('resourceTimelineWeek', jsEvent);
                    dayviewCalendar.gotoDate(date);
                },
                slotLabelFormat: [
                    { month: 'numeric',  day: 'numeric' }, // top level of text
                    // { hour: 'numeric', meridiem: 'short' } // lower level of text
                ],
                slotLabelInterval: {
                    days: 7
                },
                resourceAreaHeaderContent: 'Importance'
            }
        },
        // views: {
        // 	timeGridSingleDay: {
        // 		type: 'timeGrid',
        // 		duration: { days: 1 }
        // 	}
        // },
        drop: function (info) {
            //console.log('drop');
            //log('Dayview drop handler fired');

            this.draggedElId = info.draggedEl.dataset.id;
        },
        eventReceive: function (info) {
            console.log('eventReceive');
            //log('Dayview eventReceive handler fired');
            let taskData = getTaskDataById(this.draggedElId);

            if (!taskData) return;

            // Clear item if exists
            // removeCalendarEvent(this.draggedElId);

            // let hasEventInDb = dayviewCalData.some(function(el, i) {
            //     return info.event.id == el.id;
            // })

            let eventData = {
                title: info.event.title,
                start: moment(info.event.start).format(),
                end: moment(info.event.start).add(taskData.time, 'm').format(),
                id: info.event.id,
                classNames: taskData.categoryClass,
                resourceId: (taskData.categoryClass == 'enhance' ? 'a' : (taskData.categoryClass == 'must-do' ? 'b' : 'c')) + (6 - taskData.effect)
            };


            let eventTestData = {
                    title: 'Test',
                    start: '2022-02-05T00:00:00+01:00',
                    end: '2022-02-05T00:15:00+01:00',
                    id: 2000,
                    classNames: 'enhance',
                    resourceId: 'a5'
                };

            // If event target is not the calendar remove event is already in the calendar we remove it
            // if (event.target.classList.contains('draggable')) {
            //     log('eventReceive: target is not calendar, event removed');
            //     dayviewCalendar.getEvents().forEach(function(event, i) {
            //         if (eventIndex == i) {
            //             event.remove();
            //         }
            //     });
            // }

            if (event &&!event.target.classList.contains('draggable')) {
                log('eventReceive: event target is calendar, event added');
                addCalendarEvent(eventData);
                // saveDayviewDataToDb();

                let date = moment(eventData.start).format('YYYY-MM-DD');

                if (taskData.day != date) {
                    let listElement = document.querySelector(`li.draggable[data-id="${taskData.id}"]`);
                    taskData.day = date;
                    listElement.classList.add('scheduled');
                    listElement.querySelector(`.task-date span`).innerText = date;
                    let oldListId = taskData.listId;
                    assignScheduledTasksToLists();
                    if (info.event.getResources().length > 0) assignResourceToEvent(eventData.id, info.event.getResources());
                    recalcListTimeSum(document.querySelector('#' + oldListId + ' .' + taskData.categoryClass));
                    recalcListTimeSum(document.querySelector('#' + taskData.listId + ' .' + taskData.categoryClass));
                    saveDataToDB([taskData.list]);
                }
            }
            //rerenderCalendar(dayviewCalendar, dayviewCalData);
        },
        eventDrop: function(info) {
            //console.log('eventDrop');
            let eventExists = false;

            dayviewCalData.forEach(function(ev, i) {
                if (ev.id == info.event.id) {
                    eventExists = true;
                    let taskData = getTaskDataById(ev.id),
                        start = moment(info.event.start).format(),
                        end = moment(info.event.start).add(taskData.time, 'm').format(),
                        resource = info.event.getResources();

                    if (taskData) {
                        if (start.indexOf('T') > -1 && end) {
                            dayviewCalData[i].start = start;
                            dayviewCalData[i].end = end;
                            updateDayviewEventDateTime(ev.id, start, true);
                        } else {
                            dayviewCalData[i].start = moment(info.event.start).format('YYYY-MM-DD');
                        }

                        if (!end) delete dayviewCalData[i].end;
                        if (info.event.allDay) {
                            dayviewCalData[i].allDay = true;
                        } else {
                            dayviewCalData[i].allDay = false;
                        }

                        let date = dayviewCalData[i].start.split('T')[0];

                        if (taskData.day != date) {
                            taskData.day = date;
                            document.querySelector(`li.draggable[data-id="${taskData.id}"] .task-date span`).innerText = date;
                            let oldListId = taskData.listId;
                            assignScheduledTasksToLists();
                            recalcListTimeSum(document.querySelector('#' + oldListId + ' .' + taskData.categoryClass));
                            recalcListTimeSum(document.querySelector('#' + taskData.listId + ' .' + taskData.categoryClass));
                            saveDataToDB([taskData.list]);
                        }

                        if (resource.length > 0) assignResourceToEvent(ev.id, resource);
                    } else {
                        removeCalendarEvent(ev.id);
                    }

                    saveDayviewDataToDb();
                }
            });

            if (!eventExists) {
                let taskData = getTaskDataById(info.event._def.defId),
                    date = moment(info.event.start).format('YYYY-MM-DD');

                if (!taskData) return;

                addCalendarEvent({
                    title: taskData.name,
                    start: moment(info.event.start).format(),
                    end: moment(info.event.end).add(taskData.time, 'm').format(),
                    id: info.event.id,
                    classNames: taskData.categoryClass
                });

                if (taskData.day != date) {
                    taskData.day = date;
                    document.querySelector(`li.draggable[data-id="${taskData.id}"] .task-date span`).innerText = date;
                    assignScheduledTasksToLists();
                    saveDataToDB([taskData.list]);
                    recalcListTimeSum(document.querySelector('#' + taskData.listId + ' .' + taskData.categoryClass));
                }
            }
        },
        eventResize: function(info) {
            //console.log('eventResize');
            dayviewCalData.forEach(function(ev, i) {
                if (ev.id == info.event.id) {
                    dayviewCalData[i].start = moment(info.event.start).format();
                    dayviewCalData[i].end = moment(info.event.end).format();
                    saveDayviewDataToDb();

                    let taskTime = moment(info.event.end).diff(moment(info.event.start), 'minutes');

                    setTaskPropertyById(ev.id, 'time', taskTime);
                    saveDataToDB();
                    document.querySelector('#draggable-list-item-' + ev.id + ' .time').innerText = convertMinutesToTimeString(taskTime);

                    let taskData = getTaskDataById(ev.id);
                    recalcListTimeSum(document.querySelector(`#${taskData.listId} .${taskData.categoryClass}`));
                }
            });
        },
        eventClick: function(info) {
            //console.log('eventClick');
            let parentListId,
                taskId;

            for (list in myLists) {
                for (category in myLists[list].categories) {
                    myLists[list].categories[category].forEach(function(task) {
                        if (task.id == info.event._def.publicId) {
                            taskId = task.id;
                            parentListId = task.listId;
                        }
                    });
                }
            }

            if (window.innerWidth < 992) showDayViewCalendar();

            if (parentListId) {
                swapShownList(parentListId, taskId);
            }

            //document.querySelector('#draggable-list-item-' + taskId + ' .task-name').click();
        },
        dateClick: function(info) {
            //console.log('dateClick');
            if (dayviewCalData.some(entry => entry.id == this.draggedElId)) {
                this.draggedElId = -1;

                return;
            }

            let date = info.dateStr;
            var targetList = document.querySelector('#' + toId(setScheduledTaskTargetList(date)) + ' .optional');

            let taskName = 'New task';

            if (taskName) {
                let nextTaskId = listItemIdCounter,
                    formattedDate = moment(date).format('YYYY-MM-DD');

                // document.querySelectorAll('.fc-day').forEach(function(el) {
                //     if (el.style.backgroundColor == 'lightgrey') {
                //         el.style.backgroundColor = 'unset';
                //     }
                // });
                // info.dayEl.style.backgroundColor = 'lightgrey';

                let taskObject = {
                    name: taskName, targetList: targetList, isFromUserInput: true, day: formattedDate
                }

                if (info.resource) {
                    let resourceId = info.resource._resource.id;

                    if (resourceId[0] == 'a') {
                        taskObject.category = 'Enhance';
                        taskObject.categoryClass = 'enhance';
                    } else if (resourceId[0] == 'b') {
                        taskObject.category = 'Must-Do';
                        taskObject.categoryClass = 'must-do';
                    } else {
                        taskObject.category = 'Optional';
                        taskObject.categoryClass = 'optional';
                    }

                    taskObject.effect = 6 - resourceId[1];
                    taskObject.targetList = document.querySelector('#' + toId(setScheduledTaskTargetList(date)) + ' .' + taskObject.categoryClass);
                }

                let resourceViewType = dayviewCalendar.currentData.currentViewType,
                    isDailyResource = resourceViewType == 'resourceTimelineWeek' || resourceViewType == 'resourceTimelineYear',
                    taskTime = isDailyResource ? (resourceViewType == 'resourceTimelineWeek' ? 1440 : 10080) : 15;

                taskObject.time = taskTime;

                initListElement(taskObject);

                let eventData = {
                    title: taskName,
                    start: date,
                    id: nextTaskId,
                    classNames: getTaskDataById(nextTaskId).categoryClass
                };

                if (date.indexOf('T') > -1) eventData.end = moment(info.date).add(15,'m').format();
                if (info.resource) {
                    eventData.start = moment(info.date).format();
                    eventData.end = moment(info.date).add(taskTime,'m').format();
                    eventData.resourceId = info.resource._resource.id;

                    if (isDailyResource) eventData.allDay = true;
                }
                addCalendarEvent(eventData);

                // if (date && confirm(isHun ? 'Szeretnél egy Google Calendar linket generálni a feladathoz?' : 'Do you want to generate a Google Calendar link for this task?')) {
                //     let dateArray = date.split('-');
                //     const event = {
                //         start: new Date(Number(dateArray[0]), Number(dateArray[1] - 1), Number(dateArray[2]), 10),
                //         end: new Date(Number(dateArray[0]), Number(dateArray[1] - 1), Number(dateArray[2]), 11),
                //         title: taskName
                //     };
                //
                //     var win = window.open(generateUrl(event), '_blank');
                //     win.focus();
                // }
            }
        }
        // datesRender: attachDayCalDropListener
    });

    dayviewCalendar.render();

    dayviewCalendarEl.insertAdjacentHTML('afterbegin', `
        <div id="close-calendar" onclick="showDayViewCalendar()">
            <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/collapse-right.svg">
            Close
        </div>
    `)

    // These lines are needed to render the calendar properly at first run
    if ( dayviewCalendarEl.classList.contains('hidden')) {
        dayviewCalendarEl.classList.remove('hidden');
        dayviewCalendarEl.classList.add('hide');
    }

    new ThirdPartyDraggable(document.getElementById('app'), {
        itemSelector: '.draggable',
        eventData: extractTaskDataForCalendar
    });
}


function assignResourceToEvent(taskId, resource) {
    if (resource.length > 0) {
        let resourceId = Array.isArray(resource) ? resource[0]._resource.id : resource,
            category = resourceId[0] == 'a' ? 'enhance' : (resourceId[0] == 'b' ? 'must-do' : 'optional'),
            impact = 6 - resourceId[1],
            taskData = getTaskDataById(taskId),
            taskListElement = document.getElementById('draggable-list-item-' + taskId);

        updateCalendarEventProperty(taskId, 'resourceId', resourceId);

        taskData.effect = impact;
        for (let i = 0; i <= 5; i++) {
            taskListElement.classList.remove('i-' + i);
        }
        taskListElement.classList.add('i-' + taskData.effect);

        move({
                listDBId: taskId,
                id: 'draggable-list-item-' + taskId,
                list: taskData.list,
                sourceList: taskData.categoryClass,
                category: taskData.category
            },
            `#${taskData.listId} .${category}`,
            true,
            true);
    }
}


async function loadDayviewData() {
    let firestoreDayviewData = await getDocumentData('', 'dayviewCalData');
        dayviewCalData = firestoreDayviewData;

    if (!userOptions.dayviewMigratedToFirestore) {
        let sqlData = await fetch(appVariables.templateUrl + '/inc/db/dayview/loaddayviewcalfromdb.php').then(response => response.json()),
            data = isJson(sqlData[0].dayviewdata) ? JSON.parse(sqlData[0].dayviewdata) : false;

        if (data) {
            setDocumentData('', {dayviewCalData: data});
            dayviewCalData = data;
        }

        userOptions.dayviewMigratedToFirestore = true;
        updateUserOptions();
    }

    initDayViewCalendar();
}


function saveDayviewDataToDb() {
    updateDocumentData('', {dayviewCalData: dayviewCalData});
}


function rerenderCalendar(calendar, data) {
    calendar.getEvents().forEach(function(event) {
        event.remove();
    });

    data.forEach(function(event) {
        calendar.addEvent(event);
    });

    calendar.render();

    saveDayviewDataToDb();
}

function addCalendarEvent(eventData) {
    //console.log('addition');
    removeCalendarEvent(eventData.id, false);

    let targetListEl = document.getElementById('draggable-list-item-' + eventData.id);
    //console.log('removal');

    if (targetListEl) {
        targetListEl.classList.add('scheduled');
        targetListEl.querySelector('.task-date span').innerHTML = eventData.start.split('T')[0];
    } else {
        return;
    }

    dayviewCalData.push(eventData);
    dayviewCalendar.addEvent(eventData);
    saveDayviewDataToDb();
}

function removeCalendarEvent(eventId, save = true) {
    let idMatched;

    if (!dayviewCalData) return;

    dayviewCalData.forEach((entry, i) => {
        if (entry.id == eventId) {
            idMatched = entry.id;
            let targetListEl = document.getElementById('draggable-list-item-' + entry.id);
            //console.log('removal');

            if (targetListEl) {
                targetListEl.classList.remove('scheduled');
                targetListEl.querySelector('.task-date span').innerHTML = '';
            }

            dayviewCalData.splice(i, 1);
        }
    });

    while (dayviewCalendar.getEventById(eventId)) dayviewCalendar.getEventById(eventId).remove();

    if (idMatched && save) saveDayviewDataToDb();

    return idMatched;
}

function updateCalendarEventProperty(eventId, property, value) {
    let idMatched;

    if (!dayviewCalData) return;

    dayviewCalData.forEach(entry => {
        if (entry.id == eventId) {
            idMatched = entry.id;
            //console.log('update');
            entry[property] = value;

            if (property == 'resourceId') {
                dayviewCalendar.getEventById(eventId).setResources([value]);
            } else {
                dayviewCalendar.getEventById(eventId).setProp(property, value);
            }
        }
    });

    if (idMatched) saveDayviewDataToDb();

    return idMatched;
}


function updateDayviewEventDateTime(taskId, newDate, sync) {
    let idMatched;

    if (!dayviewCalData) return;

    dayviewCalData.forEach(function(event, i) {
        if (event.id == taskId) {
            idMatched = event.id;
            // console.log('property update');

            let isDateTime = event.start.indexOf('T') > -1,
                taskData = getTaskDataById(taskId),
                calendarEvent = dayviewCalendar.getEventById(taskId);

            if (isDateTime) {
                let startDateTimeParts = event.start.split('T');
                    // endDateTimeParts = event.end.split('T');

                startDateTimeParts[0] = newDate.split('T')[0];
                event.start = startDateTimeParts.join('T');
                if (sync) calendarEvent.setStart(event.start);

                // endDateTimeParts[0] = newDate.split('T')[0];

                let duration = moment(event.end).diff(event.start, 'm');

                if (duration !== taskData.time) {
                    let diff = taskData.time - duration;

                    event.end = moment(event.end).add(diff, 'm').format();
                }

                // event.end = endDateTimeParts.join('T');
                if ((sync && event.end.split('T')[0] != event.start.split('T')[0]) || !calendarEvent.allDay) calendarEvent.setEnd(event.end);
                // dayviewCalendar.getEventById(idMatched).setEnd(moment(dayviewCalendar.getEventById(idMatched).start).add(getTaskDataById(taskId).time, 'm').format())
            } else {
                event.start = newDate.split('T')[0];

                if (sync) {
                    calendarEvent.setStart(event.start);
                    if (calendarEvent.end) {
                        calendarEvent.setEnd(null);
                    }
                }
            }
        }
    });

    if (idMatched) saveDayviewDataToDb();

    return idMatched;
}

function initCalView(view, event) {
    dayviewCalendar.changeView(view)
    if (event && event.target.classList.contains('fc-scrollgrid-sync-inner')) return;
    if (document.getElementById('plan-menu')) document.getElementById('plan-menu').classList.toggle('hide');
    document.querySelector('.fc-planButton-button').classList.toggle('menu-open');
}

function restoreTasksFromDayviewCalDataBackup() {
    JSON.parse(dayviewCalDataBackup).forEach(el => {
        let category = el.classNames.split('-')
        formattedCategory = [];

        category.forEach(cat => formattedCategory.push(cat.classNames.charAt(0).toUpperCase() + cat.classNames.slice(1)));
        formattedCategory = formattedCatgory.join('-');

        initListElement({
            name: el.title,
            list: 'Today',
            listId: 'today',
            categoryClass: el.classNames,
            category: formattedCategory,
            day: el.start.split('T')[0]
        });
    })
}