function initProconHandler(taskId) {
    loadFormData(taskId);

    document.getElementById('procon-form').addEventListener('submit', function(e) {
        e.preventDefault();
    })

    document.querySelectorAll('#procon-form input[type="button"]').forEach(function(btn) {
        btn.addEventListener('click', addFactor);
    });

    document.querySelectorAll('#procon-form input[type="text"]').forEach(attachInputListeners);

    function addFactor(triggerEl, isFromUserInput = true) {
        let target = triggerEl.currentTarget || triggerEl,
            parent = target.closest('fieldset'),
            numOfFactors = parent.querySelectorAll('input[type="text"]').length,
            isPro = parent.closest('[data-factor-group').dataset.factorGroup == 'pros';

        target.insertAdjacentHTML('beforebegin',
            `<input type="text" name="${isPro ? 'pro' + ++numOfFactors : 'con' + ++numOfFactors}" placeholder="${isPro ? (isHun ? 'Írd be az érved' : 'Add pro') : (isHun ? 'Írd be az ellenérved' : 'Add con')}...">`
        );

        let newFactor = parent.querySelector('input[name="' + (isPro ? 'pro' + numOfFactors : 'con' + numOfFactors) + '"]');
        newFactor.focus();

        if (isFromUserInput) attachInputListeners(newFactor);
    }

    function attachInputListeners(input) {
        input.addEventListener('keyup', function(e) {
            if (e.keyCode != 13) removeFactorOnEmpty(e.currentTarget);
        });

        input.addEventListener('blur', function(e) {
            saveFormData(taskId);
        });

        input.addEventListener('keyup', function(e) {
            if (e.keyCode == 13 && e.currentTarget.value) {
                let addButton = e.currentTarget.closest('fieldset').querySelector('input[type="button"]');
                addFactor(addButton);
                saveFormData(taskId);
            }
        });
    }

    function saveFormData(taskId) {
        let object = {},
            form = document.getElementById('procon-form'),
            formData = new FormData(form),
            taskData = getTaskDataById(taskId);

        formData.forEach(function(value, key){
            if (value) object[key] = value;
        });

        let hasProcon = Object.keys(object).length

        if (hasProcon) {
            document.getElementById('draggable-list-item-' + taskId).classList.add('has-procon');
        } else {
            document.getElementById('draggable-list-item-' + taskId).classList.remove('has-procon');
        }

        taskData.procon =  hasProcon ? object : false;
        saveDataToDB([taskData.list]);
    }

    function loadFormData(taskId) {
        let data = getTaskDataById(taskId),
            form = document.getElementById('procon-form');

        let counter = 0;

        if (data.procon) {
            for (key in data.procon) {
                let isPro = key.indexOf('pro') > -1;

                if (data.procon[key]) {
                    form.querySelector('[data-factor-group="' + (isPro ? 'pros' : 'cons') + '"] input[type="button"]').insertAdjacentHTML('beforebegin',
                        `<input type="text" name="${(isPro ? 'pro' : 'con') + ++counter}" placeholder="Add ${isPro ? 'pro' : 'con'}..." value="${data.procon[key]}">`
                    );
                }
            }
        } else {
            addFactor(form.querySelector('[data-factor-group="pros"] input[type="button"]'), false);
            addFactor(form.querySelector('[data-factor-group="cons"] input[type="button"]'), false);
        }
    }

    function removeFactorOnEmpty(el) {
        if (!el.value) el.remove();
    }
}
