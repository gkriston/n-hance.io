let listCategoryModule = {
    init: function() {
        if (!this.isInitialized) {
            this.defaultCategory = 'Uncategorized';
            this.listObj = myLists;

            if (!userOptions.list_categories) {
                userOptions.list_categories = [];
            }

            this.listCategories = userOptions.list_categories;
            this.categories = this.getListCategories();
            this.isInitialized = true;
        }
    },
    getListCategories: () => {
        let categoriesList = []

        if (Object.keys(userOptions.list_categories).length > 0) userOptions.list_categories.forEach(category => categoriesList.push(category.name));
        return categoriesList;
    },
    addNewCategory: function(category = this.defaultCategory) {
        if (!this.isExistingCategory(category)) {
            this.listCategories.push({
                name: category,
                lists: []
            });
        }
    },
    addListToCategory: function(category = this.defaultCategory, list) {
        if (!category) return;
        this.addNewCategory(category);

        this.listCategories.forEach(cat => {
            if (cat.name == category && cat.lists.indexOf(list) == -1) {
                cat.lists.push(list)
            }
        });
    },
    moveListToCategory: (catLists, listName, targetList) => {
        let listMoved = false;

        catLists.forEach((list, i, arr) => {
            if (list == targetList && !listMoved) {
                arr.splice(i, 0, listName);
                listMoved = true;
            }
        });
    },
    deleteListFromCategory: function(category = this.defaultCategory, list) {
        this.listCategories.forEach(cat => {
            if (cat.name == category) {
                let i = cat.lists.indexOf(list);
                cat.lists.splice(i, 1);
            }
        });
        listCategoryModule.saveListCategoriesToDB();
    },
    isExistingCategory: function(category) {
        if (Object.keys(this.listCategories).length == 0) return false;
        return this.listCategories.some(cat => cat.name == category);
    },
    saveListCategoriesToDB: () => {
        userOptions.list_categories = listCategoryModule.listCategories;
        updateUserOptions();
    },
    dragList: e => {
        let list = e.currentTarget.dataset.list,
            category = e.currentTarget.closest('[data-list-category]').dataset.listCategory;
        elDataTransfer = JSON.stringify({
            type: 'list',
            name: list,
            category: category
        });
    },
    dragCategory: e => {
        let category = e.currentTarget.closest('[data-list-category]').dataset.listCategory;
        elDataTransfer = JSON.stringify({
            type: 'category',
            name: category
        });
    },
    drop: e => {
        e.stopPropagation();
        let transferredData = elDataTransfer && JSON.parse(elDataTransfer);

        if (transferredData) {
            if (transferredData.type == 'listElement' && e.target.getAttribute('draggable') == "true") {
                move(transferredData,'#' + e.target.dataset.listid + ' .' + transferredData.category.toLowerCase());
            } else if (transferredData.type == 'list') {
                let draggedList = transferredData.name,
                    targetList = e.target.dataset && e.target.dataset.list;

                if (targetList) {
                    let sourceCategory = transferredData.category,
                        targetCategory = e.target.closest('[data-list-category]').dataset.listCategory;

                    console.log(`Move %c${sourceCategory}/${draggedList} %cbefore %c${targetCategory}/${targetList}`, 'color: orange', '','color: green');
                    listCategoryModule.moveList(sourceCategory, draggedList, targetCategory, targetList);
                }
            } else {
                let draggedCategory = transferredData.name,
                    targetCategory = e.currentTarget.dataset.listCategory;

                listCategoryModule.moveCategory(draggedCategory, targetCategory);
            }
        }
    },
    moveCategory: (category, targetCategory) => {
        // Move category within category array
        let listCats = listCategoryModule.listCategories,
            catIndex = listCategoryModule.getCategoryIndex(category);

        // Remove and store category object
        let movedCat = listCats.splice(catIndex, 1)[0],
            // Get target category index
            targetCatIndex = listCategoryModule.getCategoryIndex(targetCategory);

        // Add category before target in list
        listCats.splice(targetCatIndex, 0, movedCat);
        listCategoryModule.saveListCategoriesToDB();
        listCategoryModule.updateListOrderIndex();

        // Move list within DOM
        let listCategoryEl = document.getElementById(`my-lists`),
            sourceListEl = document.querySelector(`#my-lists [data-list-category="${category}"]`),
            targetListEl = document.querySelector(`#my-lists [data-list-category="${targetCategory}"]`);

        listCategoryEl.insertBefore(sourceListEl, targetListEl);
    },
    moveList: (sourceCategory, listName, targetCategory, targetList) => {
        let sourceCatLists = listCategoryModule.getCategoryLists(sourceCategory);

        // Removing list from original place
        sourceCatLists.forEach((list, i, arr) => {
            if (list == listName) {
                arr.splice(i, 1);
            }
        });

        // Moving within the same category
        if (sourceCategory == targetCategory) {
            listCategoryModule.moveListToCategory(sourceCatLists, listName, targetList);
        } else {
            // Moving to other category
            let targetCatLists = listCategoryModule.getCategoryLists(targetCategory);
            listCategoryModule.moveListToCategory(targetCatLists, listName, targetList);
        }

        // Set new parent category for list
        myLists[listName].parentcategory = targetCategory;

        // Save list orderand update order index for lists
        listCategoryModule.updateListOrderIndex();
        listCategoryModule.saveListCategoriesToDB();

        // Move list within DOM
        let listCategoryEl = document.querySelector(`#my-lists ul[data-parentcategory="${toId(targetCategory)}"]`),
            sourceListEl = document.querySelector(`#my-lists [data-list="${listName}"]`),
            targetListEl = document.querySelector(`#my-lists [data-list="${targetList}"]`);

        listCategoryEl.insertBefore(sourceListEl, targetListEl);
    },
    getCategoryLists: (categoryName) => {
        let categoryLists = [];

        document.querySelectorAll(`#my-lists [data-parentcategory="${toId(categoryName)}"] li`).forEach(el => categoryLists.push(el.dataset.list));

        listCategoryModule.listCategories.forEach((category, i, arr) => {
            if (category.name == categoryName) {
                arr[i].lists = categoryLists;
            }
        });

        return categoryLists;
    },
    getCategoryIndex: (categoryName) => {
        let categoryIndex;

        listCategoryModule.listCategories.forEach((category, i) => {
            if (category.name == categoryName) {
                categoryIndex = i;
            }
        });

        return categoryIndex;
    },
    updateListOrderIndex: () => {
        let orderIndex = 1;

        listCategoryModule.listCategories.forEach(category => {
            category.lists.forEach((list, i, arr) => {
                if (myLists[list]) {
                    myLists[list].order = orderIndex++;
                } else {
                    arr[i] = null;
                }
            });
        });

        listCategoryModule.listCategories.forEach(category => {
            category.lists = category.lists.filter(list => !!list);
        });

        saveDataToDB();
    },
    toggleHiddenStatus: (category) => {
        listCategoryModule.listCategories.forEach((cat) => {
            if (cat.name == category) {
                if (cat.hidden) {
                    cat.hidden = false;
                } else {
                    cat.hidden = true;
                }

                listCategoryModule.saveListCategoriesToDB();
            }
        });
    },
    getCategoryProp: (category, property) => {
        let prop;

        listCategoryModule.listCategories.forEach((cat) => {
            if (cat.name == category) {
                prop = cat[property];
            }
        });

        return prop;
    }
}
