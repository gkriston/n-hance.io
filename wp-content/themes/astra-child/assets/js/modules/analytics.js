document.getElementById('chart-icon').addEventListener('click', initAnalytics);

function showListImpactForecastChart(e) {
    let parentList = e.currentTarget.closest('.task-list-wrapper'),
        forecastList = parentList.dataset.list,
        listId = parentList.getAttribute('id');

    if (document.querySelectorAll(`#${listId} li.draggable`).length < 1) {
        alert(isHun ? 'Tervezd meg a napodat az előrejelzéshez!' : 'Plan your day first, to make the forecast available!');
        return;
    }

    showModal();
    document.getElementById('modal-content').innerHTML = `
		<div id="chart-overlay">
			<h3>${isHun ? 'Optimális feladatvégzés VS várható haladás előrejelzése' : 'Optimal progress VS progress forecast'} <b class="n-tooltip impact-tooltip">?</b></h3>
			<div id="chart-overlay__charts"></div>
		</div>
	`;
    let targetSelector = '#chart-overlay__charts .chart-group:last-of-type .chart-group__charts';

    addNewChartGroup();

    if (Object.keys(compassData.dailyLog).length == 0) {
        document.querySelector(".chart-group__charts").insertAdjacentHTML('afterend', `
            <img class="svg-icon placeholder" src="${appVariables.templateUrl}/assets/images/svg-icons/chart-dark.svg">
            <p>Your need to complete tasks to see your own analytics data</p>`);

        return;
    } else {
        document.querySelector(".chart-group__charts").innerHTML = '';
    }

    let chartClosingValues = drawLineChart(1, 'all', targetSelector, false, forecastList, true);
    // let maxValueSecondChart = drawLineChart(1, 'all', targetSelector, false, forecastList, false, maxValueFirstChart);

    if (chartClosingValues.length == 2) {
        let percentage = Math.round(chartClosingValues[0]/chartClosingValues[1] * 100);

        document.querySelector(".chart-group__charts").insertAdjacentHTML('afterend', `
            <p style="margin-top: 0">${isHun ? 'A haladásod az optimálishoz képest a következő lesz: ' : 'Your progress compared to the optimal will be the following: '}<b>${percentage}%</b></p>
        `);
    }
}

function initAnalytics(e, dailyChartOnly) {
    let modal = document.getElementById('modal');

    modal.classList.remove('daily-chart-popup');
    modal.classList.remove('hide');
    document.getElementById('progress-point-indicator').classList.remove('show');

    document.getElementById('modal-content').innerHTML = `
		<div id="chart-overlay">
			<h3>${isHun ? 'Elvégzett feladatok' : 'Completed tasks'} <b class="n-tooltip impact-tooltip">?</b></h3>
			<select id="analytics-range" class="${dailyChartOnly ? 'hide' : ''}" onchange="showTasksAnalytics()">
			    <option value="1">${isHun ? 'Ma' : 'Today'}</option>
			    <option value="7" selected>${isHun ? 'Utolsó 7 nap' : 'Last 7 days'}</option>
			    <option value="30">${isHun ? 'Utolsó 30 nap' : 'Last 30 days'}</option>
            </select>
            <select id="analytics-labels" class="${dailyChartOnly ? 'hide' : ''}" onchange="showTasksAnalytics()">
                <option value="all" selected>${isHun ? 'Összes címke' : 'All labels'}</option>
			    ${function() {
        let optionsHTML;

        getLoggedTaskLabels().forEach(label => {
            optionsHTML += `<option value="${label}">${label}</option>`;
        });

        return optionsHTML;
    }()}
            </select>
			<div id="chart-overlay__charts"></div>
		</div>
	`;

    if (dailyChartOnly) {
        modal.classList.add('daily-chart-popup');

        setTimeout(() => {
            if (modal.classList.contains('daily-chart-popup')) {
                let progressIndicator = document.getElementById('progress-point-indicator');
                showTasksAnalytics(dailyChartOnly);

                let log = compassData.dailyLog[dateToday],
                    lastTask = log[log.length - 1],
                    multiplier = lastTask.category == 'Enhance' ? 200 : (lastTask.category == 'Optional' ? 20 : 2);

                let scoreText = multiplier + ' x ' + lastTask.trackedTime + (isHun ? ' perc x hatás ' : ' mins x impact of ') + lastTask.effect + ' = ' + Math.round(multiplier * lastTask.trackedTime * lastTask.effect) + (isHun ? ' haladási pont' : ' progress points');


                progressIndicator.querySelector('p').innerHTML = scoreText;
                progressIndicator.classList.add('show');
            }
        }, 1000);

        setTimeout(() => {
            if (modal.classList.contains('daily-chart-popup')) {
                modal.classList.remove('daily-chart-popup');
                modal.classList.add('hide');
                document.getElementById('progress-point-indicator').classList.remove('show');
            }
        }, 4000);
    } else {
        showModal();
    }

    showTasksAnalytics(dailyChartOnly);
}

function showTasksAnalytics(dailyChartOnly) {
    let targetSelector = '#chart-overlay__charts .chart-group:last-of-type .chart-group__charts',
        days = dailyChartOnly ? 1 : document.getElementById('analytics-range').value,
        filter = document.getElementById('analytics-labels') ? document.getElementById('analytics-labels').value : 'all';

    if (document.getElementById('chart-overlay__charts')) document.getElementById('chart-overlay__charts').innerHTML = '';

    compassData.dailyLog = sortObjByKey(compassData.dailyLog);

    // Clear entries older than 30 days
    for (day in compassData.dailyLog) {
        let diff = moment().diff(day, 'days')

        if (diff > 30) delete compassData.dailyLog[day];
    }
    saveCompassToDB();

    addNewChartGroup();

    let dates = [];

    for (let i = days; i > 0; i--) {
        dates.push(moment().subtract(i - 1, 'days').format('YYYY-MM-DD'))
    }

    let dataAvailable = Object.keys(compassData.dailyLog).some(date => dates.indexOf(date) > -1 && compassData.dailyLog[date].length > 0);

    if (Object.keys(compassData.dailyLog).length == 0 || !dataAvailable) {
        document.querySelector(".chart-group__charts").insertAdjacentHTML('afterend', `
            <p><b>SAMPLE DATA:</b> Your need to complete tasks to see your own analytics data</p>
            <img class="svg-icon placeholder ${!dailyChartOnly ? 'sample-charts' : ''}" src="${appVariables.templateUrl}/assets/images/${dailyChartOnly ? 'svg-icons/chart-dark.svg' : 'sample-analytics.jpg'}">`);

        return;
    } else {
        if (!document.querySelector(".chart-group__charts")) return;

        document.querySelector(".chart-group__charts").innerHTML = '';
    }

    //showTaskTimeTotals(7, true, targetSelector);
    if (!dailyChartOnly) {
        showTaskTimeTotals(days, filter, true, targetSelector, 'pie');
        if (filter == 'all') drawPieChart(getLabelTimeSums(), targetSelector, 'allLabels');
    } else {
        // drawLineChart(days, filter, targetSelector, false);
        let chartClosingValues = drawLineChart(days, filter, targetSelector, false, false, true);
        if (chartClosingValues.length == 2) {
            let percentage = Math.round(chartClosingValues[0]/chartClosingValues[1] * 100);

            document.querySelector(".chart-group__charts").insertAdjacentHTML('afterend', `
            <p style="margin: -10px 0 30px">${isHun ? 'A haladásod az optimálishoz képest a következő volt: ' : 'Your progress compared to the optimal was the following: '}<b>${percentage}%</b></p>
        `);
        }
    }

    if (userOptions.beta_features == 'on' && !dailyChartOnly) {
        addNewChartGroup();

        // drawLineChart(days, filter, targetSelector, false);
        let chartClosingValues = drawLineChart(days, filter, targetSelector, false, false, true);
        if (chartClosingValues.length == 2) {
            let percentage = Math.round(chartClosingValues[0]/chartClosingValues[1] * 100);

            document.querySelector(".chart-group__charts").insertAdjacentHTML('afterend', `
            <p style="margin: -10px 0 30px">${isHun ? 'A haladásod az optimálishoz képest a következő volt: ' : 'Your progress compared to the optimal was the following: '}<b>${percentage}%</b></p>
        `);
        }
        drawLineChart(days, filter, targetSelector, false, false, false, false, 'dots');
    }


    if (!dailyChartOnly) logDayData(days, filter);
    // showTaskTimeTotals(7, false, targetSelector, 'days', (isHun ? 'Munkanap' : 'Workdays'));

    // addNewChartGroup();
    // showTaskTimeTotals(30, true, targetSelector, 'pie');
    // drawLineChart(30, targetSelector);
    // showTaskTimeTotals(30, false, targetSelector, 'days', (isHun ? 'Munkanap' : 'Workdays'));
}

function addNewChartGroup() {
    if (!document.getElementById('chart-overlay__charts')) return;

    document.getElementById('chart-overlay__charts').insertAdjacentHTML('beforeend',`
		<div class="chart-group">
		    <div class="chart-group__charts"></div>
		    <div class="chart-group__entries-list"></div>
		</div>
	`);
}

function showTaskTimeTotals(days = 30, filter, addHeader, targetSelector, format = '%', yAxisDesc = '%') {
    let categories = [
            {
                'Category': 'Enhance',
                'CategoryHun': 'Szintlépés',
                'Totals': 0
            },
            {
                'Category': 'Must-Do',
                'CategoryHun': 'Kötelező',
                'Totals': 0
            },
            {
                'Category': 'Optional',
                'CategoryHun': 'Opcionális',
                'Totals': 0
            }
        ],
        logKeys = Object.keys(compassData.dailyLog).reverse(),
        lastDayIncluded = moment().subtract(days, 'days').format('YYYY-MM-DD');

    for (day in compassData.dailyLog) {
        let isAfterLastIncludedDay = moment(day).diff(lastDayIncluded, 'days');

        if (isAfterLastIncludedDay > 0) {
            compassData.dailyLog[day].forEach(function(task) {
                categories.forEach(function(cat) {
                    if (task.category == cat['Category'] && (filter == 'all' || (task.labels && task.labels.indexOf(filter) > -1))) {
                        cat['Totals'] += Number(task.trackedTime || task.time);
                    }
                });
            });
        }
    }

    // console.log(categories);
    // if (addHeader) {
    //     document.querySelector(targetSelector).insertAdjacentHTML('beforeend',`
	// 		<h4>${isHun ? `Utolsó ${days} nap` : `Last ${days} days`}</h4>
	// 	`);
    // }

    if (categories.some(category => category["Totals"] > 0)) {
        if (format == 'pie') {
            drawPieChart(convertCatTotals(categories,format), targetSelector);
        } else {
            showFinishedTasksByCategory(convertCatTotals(categories,format), targetSelector, yAxisDesc);
        }
    }
}

function createPulse(days, filter, previousPeriod = false, forecastList, baseline) {
    let pulse = [],
        labels = [],
        score = 0,
        firstDay = previousPeriod ? moment().subtract(days, 'days').format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'),
        lastDayIncluded = moment().subtract((previousPeriod ? days * 2 : days), 'days').format('YYYY-MM-DD');

    if (forecastList) {
        let forecastDays = {},
            completionTime = moment();
            forecastDays[dateToday] = [];

        forecastDays[dateToday].unshift({
            name: 'The day has begun',
            category: 'Must-Do',
            effect: 1,
            time: 1,
            completionTime: moment().format('HH:mm')
        });

        for (category in myLists[forecastList].categories) {
            let currentCategory = myLists[forecastList].categories[category];

            currentCategory.forEach(task => {
                completionTime.add(task.time, 'm');
                let date = moment(completionTime._d).format('YYYY-MM-DD');

                if (!forecastDays[date]) forecastDays[date] = [];

                forecastDays[date].push({
                    name: task.name,
                    category: task.category,
                    effect: task.effect,
                    time: task.time,
                    completionTime: completionTime.format('HH:mm')
                });
            });
        }




        for (day in forecastDays) {
            let entries = JSON.parse(JSON.stringify(forecastDays[day]));
            addEntriesToPulseData(entries, day);
        }
    } else if (baseline) {
        let sampleTasks = [{
                name: 'Bumper task',
                category: 'Optional',
                effect: 1,
                time: 1
            },{
                name: 'Urgent task',
                category: 'Optional',
                effect: 1,
                time: userOptions.baseline_progress_points * 1000
            }];

        let entries = [],
            completionTime = moment('09:00', 'HH:mm');

        sampleTasks.forEach(task => {
            let taskCompletionTime = completionTime.add(task.trackedTime || task.time, 'm');

            // Keep track of last completed task time
            completionTime = taskCompletionTime;

            entries.push({
                name: task.name,
                category: task.category,
                effect: task.effect,
                time: task.trackedTime || task.time,
                completionTime: taskCompletionTime.format('HH:mm')
            });
        });

        if (entries) addEntriesToPulseData(entries, moment().format('YYYY-MM-DD'));
    } else {
        if (days == 1 && !compassData.dailyLog[dateToday]) compassData.dailyLog[dateToday] = [];

        for (day in compassData.dailyLog) {
            let daysFromFirstDay = moment(day).diff(firstDay, 'days'),
                isAfterLastIncludedDay = moment(day).diff(lastDayIncluded, 'days');

            if (daysFromFirstDay <= 0 &&isAfterLastIncludedDay > 0) {
                let entries = JSON.parse(JSON.stringify(compassData.dailyLog[day]));

                entries.unshift({
                    name: 'The day has begun',
                    category: 'Must-Do',
                    effect: 1,
                    time: 1,
                    completionTime: moment('00:01', 'HH:mm').format('HH:mm')
                });

                if (days == 1 && entries.length == 1) {
                    entries.push({
                        name: 'Still haven\' done much',
                        category: 'Must-Do',
                        effect: 1,
                        time: 1,
                        completionTime: moment().subtract(1, 'h').format('HH:mm')
                    });
                }
                if (entries) addEntriesToPulseData(entries, day);
            }
        }
    }


    function addEntriesToPulseData(entries, day) {
            entries.forEach(entry => {
            if (filter == 'all' || (entry.labels && entry.labels.indexOf(filter) > -1)) {
                let entryData = {};

                labels.push(entry.name);
                entryData.date = day + 'T' + entry.completionTime;
                entryData.close = function () {
                    let value,
                        time = entry.trackedTime || entry.time,
                        effect = Number(entry.effect);

                    if (entry.category == 'Enhance') {
                        value = Math.round(200 * effect * time);
                    } else if (entry.category == 'Must-Do') {
                        value = Math.round(20 * effect * time);
                    } else {
                        value = effect * time * 2;
                    }

                    score += value;

                    return score;
                }();

                pulse.push(entryData);
            }
        });
    }

    //console.log(pulse);

    return {
        data: JSON.stringify(pulse),
        labels: labels
    };
}

function convertCatTotals(categories, format) {
    let totalTime = 0;

    categories.forEach(function(category) {
        totalTime += category['Totals'];
    });

    if (format == 'days') {
        categories.forEach(function(category) {
            // Convert task time from minutes to working days
            category['Totals'] = Math.round(category['Totals'] / 60 / userOptions.workday_length);
        });
    } else {
        categories.forEach(function(category) {
            // Convert task time from minutes to 8 hour working days
            category['Totals'] = Math.round(category['Totals'] / totalTime * 100);
        });
    }

    return categories;
}

function showFinishedTasksByCategory(data, targetSelector, yAxisDesc) {
    // set the dimensions of the canvas
    var margin = {top: 30, right: 20, bottom: 40, left: 40},
        width = 300 - margin.left - margin.right,
        height = 200 - margin.top - margin.bottom;


// set the ranges
    var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);

    var y = d3.scale.linear().range([height, 0]);

// define the axis
    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")


    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left")
        .ticks(10);


// add the SVG element
    var svg = d3.select(targetSelector).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");


// load the data
// 	d3.json(data, function(error, data) {

    data.forEach(function(d) {
        d.xAxis = d[isHun ? 'CategoryHun' : 'Category'];
        d.yAxis = +d['Totals'];
    });

    // scale the range of the data
    x.domain(data.map(function(d) { return d.xAxis; }));
    y.domain([0, d3.max(data, function(d) { return d.yAxis; })]);

    // add axis
    svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis)
        .selectAll("text")
        .style("text-anchor", "center")
        .attr("dx", "0em")
        .attr("dy", "1em")
        .attr("transform", "rotate(0)" );

    svg.append("g")
        .attr("class", "y axis")
        .call(yAxis)
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 5)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(yAxisDesc);


    // Add bar chart
    svg.selectAll("bar")
        .data(data)
        .enter().append("rect")
        .attr("class", "bar")
        .attr("x", function(d) { return x(d.xAxis); })
        .attr("width", x.rangeBand())
        .attr("y", function(d) { return y(d.yAxis); })
        .attr("height", function(d) { return height - y(d.yAxis); });

    // });
}

function drawPieChart(data, targetSelector, type) {
    // Feel free to change or delete any of the code you see in this editor!
    var margin = {top: 30, right: 20, bottom: 40, left: 40},
        width = 300 - margin.left - margin.right,
        height = 200 - margin.top - margin.bottom;

    // Remove 0 values
    data = data.filter(category => category.Totals != 0);

    var svg = d3.select(targetSelector)
        .append("svg")
        .attr("class", "pie-chart")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    svg.append("g")
        .attr("class", "slices");
    svg.append("g")
        .attr("class", "lines");
    svg.append("g")
        .attr("class", "labels");

    var width = 300,
        height = 200,
        radius = Math.min(width, height) / 2;

    var pie = d3.layout.pie()
        .sort(null)
        .value(function(d) {
            return d.value;
        });

    var arc = d3.svg.arc()
        .outerRadius(radius * 0.8)
        .innerRadius(radius * 0.4);

    var outerArc = d3.svg.arc()
        .innerRadius(radius * 0.9)
        .outerRadius(radius * 0.9);

    svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    var key = function(d){ return d.data.label; };
    var label = function() {
        let list = [];
        data.forEach(function(el) {
            list.push(el[isHun ? 'CategoryHun' : 'Category']);
        });

        return list;
    }();

    var color = d3.scale.ordinal()
        .domain(label)
        .range(type == 'allLabels' ? getColors(false, false, data.length) : getColors(label));

    var value = function() {
        let list = [];
        data.forEach(function(el) {
            list.push(el['Totals']);
        });

        return list;
    }();

    var data = function() {
        var labels = color.domain();
        return labels.map(function(label){
            let value;

            data.forEach(function(el) {
                if (el['Category'] == label || el['CategoryHun'] == label) value = el.Totals;
            });

            return { label: label == 'Enhance' ? 'Wildly Imp.' : label, value: value }
        });
    }();


        /* ------- PIE SLICES -------*/
        var slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(data), key);

        slice.enter()
            .insert("path")
            .style("fill", function(d) { return color(d.data.label); })
            .attr("class", "slice");

        slice
            .transition().duration(1000)
            .attrTween("d", function(d) {
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function(t) {
                    return arc(interpolate(t));
                };
            })

        slice.exit()
            .remove();

        /* ------- SLICE TO TEXT POLYLINES -------*/

        var polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(data), key);

        polyline.enter()
            .append("polyline");

        polyline.transition().duration(1000)
            .attrTween("points", function(d){
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function(t) {
                    var d2 = interpolate(t);
                    var pos = outerArc.centroid(d2);
                    pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                    return [arc.centroid(d2), outerArc.centroid(d2), pos];
                };
            });

        polyline.exit()
            .remove();

        /* ------- TEXT LABELS -------*/

        var text = svg.select(".labels").selectAll("text")
            .data(pie(data), key);

        text.enter()
            .append("text")
            .attr("dy", ".35em")
            .text(function(d) {
                return d.data.label;
            });

        function midAngle(d){
            return d.startAngle + (d.endAngle - d.startAngle)/2;
        }

        text.transition().duration(1000)
            .attrTween("transform", function(d) {
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function(t) {
                    var d2 = interpolate(t);
                    var pos = outerArc.centroid(d2);
                    pos[0] = (radius - 20) * (midAngle(d2) < Math.PI ? 1 : -1);
                    return "translate("+ pos +")";
                };
            })
            .styleTween("text-anchor", function(d){
                this._current = this._current || d;
                var interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function(t) {
                    var d2 = interpolate(t);
                    return midAngle(d2) < Math.PI ? "start":"end";
                };
            });

        text.exit()
            .remove();
}

function drawLineChart(days, filter, targetSelector, previousPeriod, forecastList, baseline = false, maxValue, type) {
    let pulse = createPulse(days, filter, previousPeriod, forecastList);
    //const root = JSON.parse('[{"date":"2002-12-31T08:25","close":54},{"date":"2003-01-31T08:25","close":34}]');
    let root = type == 'dots' ? getTaskCompletionTimesByCat() : JSON.parse(pulse.data),
        maxY,
        minX,
        maxX,
        baselineData,
        chartCloseYs = [];

    if (root.length < 2) return chartCloseYs;

    var label = d3.select(".label");
// Set the dimensions of the canvas / graph
    var	margin = {top: 30, right: 20, bottom: 40, left: 60},
        width = 300 - margin.left - margin.right,
        height = 200 - margin.top - margin.bottom;

// Parse the date / time
    var	parseDate = d3.time.format("%Y-%m-%dT%H:%M").parse;

// Set the ranges
    var	x = d3.time.scale().range([0, width]);
    var	y = d3.scale.linear().range([height, 0]);

// Define the axes
    var	xAxis = d3.svg.axis().scale(x)
        .orient("bottom").ticks(5);

    var	yAxis = d3.svg.axis().scale(y)
        .orient("left").ticks(5).tickFormat(function (d) {
            if (type != 'dots') return d;

            switch (d) {
                case 1:
                    return 'Opctional'
                case 2:
                    return 'Must-Do'
                case 3:
                    return 'Enhance'
            }
    });

// Define the line
    var	valueline = d3.svg.line()
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y(d.close); });

// Adds the svg canvas
    var	svg = d3.select(targetSelector)
        .append("svg")
        .attr("class", "line-chart")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Get the data

        root.forEach(function(d) {
            d.date = parseDate(d.date);
            d.close = +d.close;
        });

        let data = root;

        maxY = type == 'dots' ? 4 : d3.max(data, function(d) { return d.close; });
        chartCloseYs.push(data[data.length - 1].close);

        if (baseline) {
            let dates = [];
            baselineData = JSON.parse(createPulse(days, 'all', false, false, true).data);

            // Scale baseline according to the number of days logged
            Object.values(root).forEach(entry => {
                let formattedDate = moment(entry.date).format('YYYY-MM-DD');
                if (dates.indexOf(formattedDate) == -1) dates.push(formattedDate);
            });

            baselineData.forEach(entry => {
               entry.close *= dates.length;
            });

            // Set baseline timestamps to equal regular data first end last entry times
            baselineData[0].date = moment(root[0].date).format('YYYY-MM-DDTHH:mm');
            baselineData[1].date = moment(root[root.length - 1].date).format('YYYY-MM-DDTHH:mm');

            if (maxY < baselineData[baselineData.length - 1].close) maxY = baselineData[baselineData.length - 1].close;

            chartCloseYs.push(baselineData[baselineData.length - 1].close);
        }

        // Scale the range of the data
        if (type == 'dots') {
            minX = moment(data[0].date).subtract(1, 'hours')._d
            maxX = moment(data[data.length - 1].date).add(1, 'hours')._d
        }

        x.domain(d3.extent(type == 'dots' ? [{close: 0, date: minX},{close: 4, date: maxX}] : data, function(d) { return d.date; }));
        y.domain([type == 'dots' ? 0 : Math.min(...d3.extent(data, function(d) { return d.close; })), maxValue || maxY]);

        if (type == 'dots') {
            svg
                .selectAll("circle")
                .data(data)
                .enter()
                .append("circle")
                .style('fill', function(d,i) {
                    return getColors(false, true)[d.close - 1];
                })
                .attr("r", 2)
                .attr("cx", function(d) {
                    return x(d.date)
                })
                .attr("cy", function(d) {
                    return y(d.close)
                })
                .on("mouseover", function(d,i) {

                    label.style("transform", "translate("+ x(d.date) +"px," + (y(d.close)) +"px)")
                    label.text(d.close)

                });
        } else {
            svg.append("path")
                .attr("class", "line")
                .attr("d", valueline(data));
        }

        // Add the X Axis
        svg.append("g")			// Add the X Axis
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis.ticks(4));

        // Add the Y Axis
        svg.append("g")			// Add the Y Axis
            .attr("class", "y axis")
            .call(yAxis);

        if (type != 'dots') {
            svg.append('g')
                .classed('labels-group', true)
                .selectAll('text')
                .data(data)
                .enter()
                .append('text')
                .classed('label', true)
                .attr({
                    'x': function(d, i) {
                        return x(d.date);
                    },
                    'y': function(d, i) {
                        return y(d.close) + 12;
                    }
                })
                .text(function(d, i) {
                    let label = d.close;

                    if (pulse.labels.length > 0 && pulse.labels[i]) label = pulse.labels[i];

                    return label;
                });
        }
        if (baseline) {
            root = baselineData;

            root.forEach(function (d) {
                d.date = parseDate(d.date);
                d.close = +d.close;
            });

            data = root;

            // Scale the range of the data
            x.domain(d3.extent(data, function (d) {
                return d.date;
            }));
            // y.domain([0, d3.max(data, function (d) {
            //     return d.close;
            // })]);

            // Add the valueline path.
            svg.append("path")        // Add the valueline path.
                .attr("class", "line")
                .attr("d", valueline(data));
        }

        // if (days == 1 || days == 7) {
        //     root = JSON.parse(createPulse(days, filter, true).data);
        //
        //     root.forEach(function (d) {
        //         d.date = parseDate(d.date);
        //         d.close = +d.close;
        //     });
        //
        //     data = root;
        //
        //     // Scale the range of the data
        //     x.domain(d3.extent(data, function (d) {
        //         return d.date;
        //     }));
        //     y.domain([0, d3.max(data, function (d) {
        //         return d.close;
        //     })]);
        //
        //     // Add the valueline path.
        //     svg.append("path")        // Add the valueline path.
        //         .attr("class", "line")
        //         .attr("d", valueline(data));
        // }

    return chartCloseYs;
}

function getLoggedTaskLabels() {
    let labels = [];

    for (day in compassData.dailyLog) {
        if (compassData.dailyLog[day]) {
            compassData.dailyLog[day].forEach(task => {
                if (task.labels && task.labels.length > 0) {
                    task.labels.forEach(label => {
                        if (labels.indexOf(label) == -1) labels.push(label);
                    });
                }
            });
        }
    }

    return labels;
}

function getColors(labels, defaultColors = true, length = 3) {
    let palette = ["#a3a3a3", "#f67d2c", "#ef2453"],
        colors = []
    ;

    if (labels && defaultColors) {
        labels.forEach(label => {
            let color;

            if (label == 'Enhance' || label == "Szintlépés") {
                color = palette[2];
            } else if (label == 'Must-Do' || label == "Kötelező") {
                color = palette[1];
            } else {
                color = palette[0];
            }

            colors.push(color);
        });
    } else if (defaultColors) {
        return palette;
    } else {
        for (let i = 0; i < length; i++) {
            for (let j = 0; j < 6; j++) {
                const defaultColorHues = [346, 24, 0]

                colors[i] = new flatColor(defaultColorHues[getRndInteger(0,2)]).hex
            }
        }
    }

    return colors;
}


function getLabelTimeSums() {
    let tasksByLabel = {},
        formattedData = [];

    for (day in compassData.dailyLog) {
        compassData.dailyLog[day].forEach(entry => {
            if (entry.labels && entry.labels.length > 0) {
                entry.labels.forEach(label => {
                    if (!tasksByLabel[label]) tasksByLabel[label] = 0

                    tasksByLabel[label] += entry.trackedTime || entry.time;
                })
            } else {
                if (!tasksByLabel['Unlabeled']) tasksByLabel['Unlabeled'] = 0

                tasksByLabel['Unlabeled'] += entry.trackedTime || entry.time;
            }
        })
    }

    for (label in tasksByLabel) {
        formattedData.push({Category: label, Totals: tasksByLabel[label]})
    }

    return formattedData;
}

function getTaskCompletionTimesByCat(days, filter, previousPeriod) {
    let completedTasksByCategory = [],
        firstDay = previousPeriod ? moment().subtract(days, 'days').format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'),
        lastDayIncluded = moment().subtract((previousPeriod ? days * 2 : days), 'days').format('YYYY-MM-DD');

    for (day in compassData.dailyLog) {
        compassData.dailyLog[day].forEach(entry => {
            let close;

            switch (entry.category) {
                case ('Enhance'):
                    close = 3
                    break
                case ('Must-Do'):
                    close = 2
                    break
                case ('Optional'):
                    close = 1
                    break;
            }

            completedTasksByCategory.push({close: close, date: entry.completionTime});
        })
    }

    completedTasksByCategory.sort((a,b) => {
        let aMins = a.date.split(':')[0] * 60 + a.date.split(':')[1],
            bMins = b.date.split(':')[0] * 60 + b.date.split(':')[1];

        return aMins - bMins
    })

    completedTasksByCategory.map(entry => entry.date = dateToday + 'T' + entry.date)

    return completedTasksByCategory
}

var flatColor = function(h){
    var PHI = 0.618033988749895;
    var s, v, hue;
    if(h===undefined){
        hue = (Math.floor(Math.random()*(360 - 0 + 1)+0))/360;
        h = ( hue + ( hue / PHI )) % 360;
    }
    else h/=360;
    v = Math.floor(Math.random() * (100 - 20 + 1) + 20);
    s = (v-10)/100;
    v = v/100;

    var r, g, b, i, f, p, q, t;
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    r = Math.round(r * 255);
    g = Math.round(g * 255);
    b = Math.round(b * 255);

    var finalColor = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);

    this.h = h;
    this.s = s;
    this.v = v;
    this.r = r;
    this.g = g;
    this.b = b;
    this.hex = finalColor;

}