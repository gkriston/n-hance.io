function allowDrop(ev) {
    // Prevent default browser action to try to open element as a link on drop
    ev.preventDefault();
}

function drag(event) {
    let dragEl = event.item;

    elDataTransfer = false;
    // Check if it is a list or compass element
    if (dragEl.classList.contains('compass__group__list_element') || dragEl.localName == 'tr') {
        let elementType = dragEl.localName == 'tr' ? 'tableRow' : 'listElement',
            category =  dragEl.closest('[data-compass-group]').dataset.compassGroup;

        if (elementType == 'listElement') {
            elDataTransfer = JSON.stringify({type: 'compassElement', key: dragEl.innerText, category: category, elementType: elementType});
        } else if (elementType == 'tableRow') {
            elDataTransfer = JSON.stringify({type: 'compassElement', key: dragEl.querySelector('td').innerText, category: category, elementType: elementType});
        }
    } else {
        // Identify dragged element by it's unique ID
        let taskData = getTaskDataById(dragEl.dataset.id);
        var parentList = taskData.list;
        var parentCategory = taskData.category;
        var listElementId = dragEl.id;
        var taskTime = taskData.time;
        // document.getElementById(listElementId).classList.toggle('grabbing');
        var listDBId = taskData.id;
        // dataTransfer.setData("text", JSON.stringify({type: 'listElement', list: parentList, category: parentCategory, id: listElementId, listDBId: listDBId, time: taskTime}));
        elDataTransfer = JSON.stringify({type: 'listElement', list: parentList, category: parentCategory, id: listElementId, listDBId: listDBId, time: taskTime});

        if (window.innerWidth < 992) {
            document.getElementById('drop-to-another-list-drawer').classList.remove('hide');
        }
    }
}

function drop(ev) {
    ev.stopPropagation();
    let dropTarget;

    // nextElement.classList.remove('droptarget');
    // if (document.querySelector('.droptarget')) document.querySelector('.droptarget').classList.remove('droptarget');
    //
    // if (nextElement.x) {
    if (window.innerWidth > 991) {
        dropTarget = ev.originalEvent ? ev.originalEvent.target : ev.currentTarget;
    } else {
        let touchRelease = ev.originalEvent && ev.originalEvent.changedTouches && ev.originalEvent.changedTouches.length > 0 ? ev.originalEvent.changedTouches[0] : false;

        dropTarget = touchRelease ? document.elementFromPoint(touchRelease.clientX, touchRelease.clientY) : false;
    }
    // }
    let targetIsNotListContainer = !dropTarget.closest('.list-container') && (ev.to && !ev.to.classList.contains('list-container')),
        targetIsNotDrawer = dropTarget.id != 'drop-to-another-list-drawer',
        targetIsNotCompass = elDataTransfer && JSON.parse(elDataTransfer).type !== 'compassElement';


    if (targetIsNotListContainer && targetIsNotDrawer && targetIsNotCompass) {
        return;
    }

    // If element is dropped over calendar
    if (dayviewCalendar && dropTarget && dropTarget.closest('#dayview-calendar')) {
        log('Event handler fired from universal drop handler');
        // We find the timeslot for the drop if exists
        // let timeSlot = dropTarget.closest('[data-time]');

        document.getElementById('drop-to-another-list-drawer').classList.add('hide');

        // if (timeSlot) {
        //     let date = document.querySelector('#dayview-calendar [data-date]').dataset.date;
        //     let start = moment(date + 'T' + timeSlot.dataset.time).format();
        //     let end = moment(start).add(Number(ev.item.querySelector('.time').value),'m').format();
        //     let taskData = extractTaskDataForCalendar(ev.item);
        //     let eventData = {
        //         title: taskData.title,
        //         start: start,
        //         end: end,
        //         id: taskData.id,
        //         classNames: taskData.categoryClass
        //     };
        //
        //     let isEventInCalendar = dayviewCalendar.getEvents().some(function(el) {
        //         return el.id == eventData.id;
        //     })
        //
        //     if (!isEventInCalendar) {
        //         log('Event added from universal drop handler');
        //         addCalendarEvent(eventData);
        //
        //         console.log(timeSlot.dataset.time);
        //     }
        // }
        return;
    }

    // if (!ev.item.dataTransfer) return;
    if (!elDataTransfer) return;


    // var transferredData = ev.item.dataTransfer.getData("text");
    var transferredData = elDataTransfer;

    // If no data is transfered (element hasn't been moved) then just hide drawer and return
    if (!transferredData) {
        document.getElementById('drop-to-another-list-drawer').classList.add('hide');
        return;
    }

    var data = JSON.parse(transferredData);

    // This is a compass element so handle it as such
    if (data.type == 'compassElement') {
        compassDragDropHandler(data, ev);
        return;
    }


    if (dropTarget.id == 'drop-to-another-list-drawer' || !document.getElementById('available-lists-to-drop').classList.contains('hide')) {
        document.getElementById('drop-to-another-list-drawer').classList.add('hide');
        document.getElementById('available-lists-to-drop').classList.remove('hide');
        taskToBeMovedData = data;

        return;
    }

    // Get the dragged element's data (ID)
    // var sourceList = document.getElementById(data.id).parentNode;
    var sourceList = ev.from;
    data.sourceList = sourceList;

    var movedEl = '';

    // Update working memory object data
    myLists[data.list].categories[data.category].forEach(function(el, i) {

        if (data.listDBId == el.id)	{
            // Remove element from array
            movedEl = myLists[data.list].categories[data.category].splice(i, 1);
        }
    });

    // let isSimple = myLists[ev.item.closest('.task-list-wrapper').dataset.list].simpleList;

    // Append dragged element to the target element
    // if (ev.item != ev.target && ev.target.classList.contains('draggable')) {
    //     ev.item.insertBefore(document.getElementById(data.id), ev.target);
    // } else {
    //     if (isSimple) {
    //         ev.item.closest('.task-list-wrapper').querySelector('.optional.list-container').appendChild(document.getElementById(data.id));
    //     } else {
    //         ev.item.appendChild(document.getElementById(data.id));
    //     }
    //
    // }


    // document.getElementById(data.id).classList.toggle('grabbing');

    var targetList = ev.to;

    if (!ev.to) return;

    var targetCategoryName = targetList.dataset.category;
    var targetListName = targetList.closest('.task-list-wrapper').dataset.list;

    movedEl[0].list = targetListName;
    movedEl[0].listId = toId(targetListName);
    movedEl[0].category = targetCategoryName;
    movedEl[0].categoryClass = targetCategoryName.toLowerCase();
    // Update list element data

    // If the task has a due date and it dropped to tommorrow we set the due date to tomorrow
    if (targetListName == 'Tomorrow' && movedEl[0].day) {
        let tomorrow = moment().add(1, 'd').format('YYYY-MM-DD');
        movedEl[0].day = tomorrow;
        document.getElementById(data.id).querySelector('.task-date span').innerText = tomorrow;
    }

    // If the task has a due date and it dropped to today we set the due date to today
    if (targetListName == 'Today' && movedEl[0].day) {
        movedEl[0].day = dateToday;
        document.getElementById(data.id).querySelector('.task-date span').innerText = dateToday;
    }

    myLists[targetListName].categories[targetCategoryName].splice(ev.newIndex, 0, movedEl[0]);

    updateDayviewEventDateTime(movedEl[0].id, movedEl[0].day, true);

    // if (nextElement != ev.target && nextElement.classList.contains('draggable')) {
    //     nextElement.querySelectorAll('li').forEach(function(el, i) {
    //         if (ev.target.id == el.id) {
    //             moveInArray(myLists[targetListName].categories[targetCategoryName], myLists[targetListName].categories[targetCategoryName].length - 1, i - 1)
    //         }
    //     });
    // }

    if (window.innerWidth < 992) {
        document.getElementById('drop-to-another-list-drawer').classList.add('hide');
    }

    let taskData = getTaskDataById(data.listDBId);
    logTaskMoves(taskData);

    // Update database
    saveDataToDB([data.list, targetListName], false);

    recalcListTimeSum(sourceList);
    recalcListTimeSum(targetList);


    updateCalendarEventProperty(movedEl[0].id, 'classNames', getTaskDataById(movedEl[0].id).categoryClass);
    updateCalendarEventProperty(movedEl[0].id, 'resourceId', (movedEl[0].categoryClass == 'enhance' ? 'a' : (movedEl[0].categoryClass == 'must-do' ? 'b' : 'c')) + (6 - movedEl[0].effect));
    updateDayviewEventDateTime(movedEl[0].id, movedEl[0].day, true);
    data = false;
    elDataTransfer = false;
}

function compassDragDropHandler(data, ev) {
    if (data.elementType == 'listElement') {
        let draggedElIndex = compassData[data.category].findIndex(el => el.name == data.key),
            targetElIndex = compassData[data.category].findIndex(el => el.name == ev.target.innerText),
            listWrapper = ev.target.closest('.compass__group__list'),
            draggedEl = listWrapper.querySelector('li:nth-of-type(' + (draggedElIndex + 1) + ')'),
            targetEl = listWrapper.querySelector('li:nth-of-type(' + (targetElIndex + 1) + ')');

        listWrapper.insertBefore(draggedEl, targetEl);

        // Inject copy of dragged element at index
        let draggedElData = compassData[data.category].splice(draggedElIndex, 1)[0];
        // Remove original
        compassData[data.category].splice(targetElIndex, 0, draggedElData);
    } else {
        let draggedElIndex = Object.keys(compassData[data.category]).indexOf(data.key.split('\n')[0]),
            targetElIndex;

        ev.target.closest('tbody').querySelectorAll('.editable-table-row').forEach((el, i) => {
            if (el.classList.contains('sortable-chosen')) {
                targetElIndex = i;
            }
        });

        // let table = ev.target.closest('tbody'),
        //     draggedEl = table.querySelectorAll('.editable-table-row')[draggedElIndex],
        //     targetEl = table.querySelectorAll('.editable-table-row')[targetElIndex];
        //
        // table.insertBefore(draggedEl, targetEl);

        let tempObject = {},
            tempArray = [];

        // Store object data in an array to have numerical indexes
        for (key in compassData[data.category]) {
            let obj = {};
            obj[key] = compassData[data.category][key];

            tempArray.push(obj);
        }
        // Move element within array
        tempArray.splice(targetElIndex, 0, tempArray.splice(draggedElIndex, 1)[0]);
        // Construct temporary object
        tempArray.forEach(function(el) {
            let key = Object.keys(el)[0],
                value = el[key];

            tempObject[key] = el[key];
        });

        compassData[data.category] = tempObject;
    }

    saveCompassToDB();
}

function dragEnter(e) {
    let parentNode = e.target.parentNode;

    document.querySelectorAll('.list-container.droptarget').forEach(list => {
        list.classList.remove('droptarget');
    });

    if (e.target && (e.target.getAttribute('draggable') || e.target.classList.contains('list-container'))) e.target.classList.add('droptarget');
    if (parentNode.getAttribute('draggable') && parentNode.classList.contains('editable-table-row')) parentNode.classList.add('droptarget');
}

function dragOver(e) {
    let mouseY = e.pageY,
        elDimensions = e.target.getBoundingClientRect(),
        mouseYInEl = mouseY - elDimensions.y,
        mouseAtElHeightPercentage = Math.round(mouseYInEl / elDimensions.height * 100)

        if (e.target && (e.target.getAttribute('draggable')))  {
            let isLastEl = e.target.closest('ul').querySelector('.draggable:last-of-type') == e.target
            if (isLastEl && mouseAtElHeightPercentage > 50) {
                console.log('droptarget-after')
            }
        }
}

function dragLeave(e) {
    let parentNode = e.target.parentNode;
    if (e.target && (e.target.getAttribute('draggable') || e.target.classList.contains('list-container'))) {
        e.target.classList.remove('droptarget');
        document.querySelectorAll('.list-container.droptarget').forEach(list => {
           list.classList.remove('droptarget');
        });
    }
    if (parentNode.getAttribute('draggable') && parentNode.classList.contains('editable-table-row')) parentNode.classList.remove('droptarget');
}

function dragEnd(e) {
    if (e.target.classList.contains('editable-table-row')) {
        document.querySelectorAll('.editable-table-row').forEach(function(row) {
            row.classList.remove('droptarget');
        });
    }

    if (document.querySelector('.grabbing') && !document.getElementById('drop-to-another-list-drawer').classList.contains('hide')) {
        document.querySelectorAll('.grabbing').forEach(el => {
            el.classList.remove('grabbing');
        })

    }
    document.getElementById('drop-to-another-list-drawer').classList.add('hide');

    if (document.querySelector('.droptarget')) {
        document.querySelectorAll('.droptarget').forEach(el => {
            el.classList.remove('droptarget');
        });
    }

    document.querySelectorAll('.list-container.droptarget').forEach(list => {
        list.classList.remove('droptarget');
    });
}

function attachDragListeners(el) {
    // document.querySelectorAll('.list-container').forEach(list => {
    //     Sortable.create(list, {
    //         group: 'shared',
    //         animation: 150,
    //         setData: drag,
    //         onEnd: drop
    //     });
    // });
    el.addEventListener('mouseup', () => console.log('mouseup'));
    el.addEventListener('dragend', () => console.log('dragend'));
}