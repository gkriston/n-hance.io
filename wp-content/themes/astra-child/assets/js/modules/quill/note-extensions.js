function initQuill() {
    let toolbar = [
        [{ header: [1, 2, false] }],
        ['bold', 'italic', 'underline', 'strike','link'],
        ['video'],
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [ 'task-list' ],
        ['save-note-data'],
        ['insert-table'],
    ], betaToolbar = [
        ['note-templates'],
        ['principles'],
        ['mental-models']
    ]

    if (userName != 'szenadam@gmail.com') {
        toolbar = toolbar.concat(betaToolbar);
    }

    quill = new Quill('#editor-container', {
        modules: {
            toolbar: toolbar,
            'task-list': true
        },
        placeholder: isHun? 'Adj hozzá jegyzetet itt...' : 'Add note...',
        theme: 'snow'  // or 'bubble'
    });

    let saveNoteDataButton = document.querySelector('.ql-save-note-data');
    saveNoteDataButton.addEventListener('click', saveNote);

    let openNewGSheetButton = document.querySelector('.ql-insert-table');
    openNewGSheetButton.addEventListener('click', function () {
        let win = window.open('https://sheet.new', '_blank');
        win.focus();
    });

    if (userName != 'szenadam@gmail.com') {
        let saveNoteTemplateButton = document.querySelector('.ql-note-templates'),
            editor = document.querySelector('.ql-editor');

        saveNoteTemplateButton.addEventListener('click', function () {
            if (!compassData.taskNoteTemplate || typeof compassData.taskNoteTemplate != 'object') compassData.taskNoteTemplate = {}

            let savedTemplates = showAvailableTemplateNames();
            document.querySelector('.note-templates input').value = '';

            document.getElementById('saved-templates').innerHTML = savedTemplates;
            document.querySelector('.note-templates-wrapper').classList.remove('hide');
            document.querySelector('input[list="saved-templates"]').focus()
        });

        function showAvailableTemplateNames() {
            let templates = compassData.taskNoteTemplate,
                templatesHTML = '';

            if (templates && Object.keys(templates).length > 0) {
                for (template in templates) {
                    templatesHTML += '<option value="' + template + '">';
                }
            }

            return templatesHTML;
        }

        let mentalModelsButton = document.querySelector('.ql-mental-models');
        mentalModelsButton.addEventListener('click', function () {
            document.querySelector('.editor-supportive-tools.mental-models').classList.toggle('hidden');
            initMentalModelsSlider();
        });

        let principlesButton = document.querySelector('.ql-principles');
        principlesButton.addEventListener('click', injectPrinciplesToNoteTool);

        document.querySelectorAll('.editor-supportive-tools .close').forEach(function (toolCloseBtn) {
            toolCloseBtn.addEventListener('click', function (e) {
                e.stopPropagation();
                e.currentTarget.parentNode.classList.add('hidden');
            });
        });

        document.querySelector('.note-templates .close').addEventListener('click', function() {
            document.querySelector('.note-templates-wrapper').classList.add('hide');
        });

        document.getElementById('save-note-template-button').addEventListener('click', function() {
            let templateName = document.querySelector('.note-templates input').value;

            if (templateName) {
                quill.focus();

                let range = quill.getSelection(),
                    template = quill.getContents(range.index, range.length);

                saveTaskTemplate(templateName, template);
            }

            document.querySelector('.note-templates-wrapper').classList.add('hide');
        });

        document.getElementById('load-note-template-button').addEventListener('click', function() {
            let templateNameInput = document.querySelector('.note-templates input'),
                templateName = templateNameInput.value;

            templateNameInput.value = '';

            let templates = compassData.taskNoteTemplate;

            if (templates && Object.keys(templates).length > 0) {
                let templateNames = '';

                for (template in templates) {
                    templateNames += template + ' ,'
                }

                templateNames = templateNames.slice(0, -2);

                let selectedTemplate = templateName;

                if (compassData.taskNoteTemplate[selectedTemplate]) {
                    let Delta = Quill.import('delta'),
                        caretPosition = quill.getSelection(true),
                        passedDelta = new Delta(compassData.taskNoteTemplate[selectedTemplate]),
                        existingDeltaBeforeCaret =  quill.getContents(0, caretPosition.index),
                        existingDeltaAfterCaret = quill.getContents(caretPosition.index),
                        combinedDelta = existingDeltaBeforeCaret.concat(passedDelta).concat(existingDeltaAfterCaret);

                    quill.setContents(combinedDelta);
                    saveNote();
                    //document.querySelector('.ql-editor').innerHTML = template);
                } else {
                    alert(isHun ? 'Nem található sablon a megadott névvel' : 'No template is available with the given name')
                }
            } else {
                alert(isHun ? 'Nincsenek elérhető jegyzetsablonok' : 'No note templates are available');
            }

            document.querySelector('.note-templates-wrapper').classList.add('hide');
        });
    }
}

function initMentalModelsSlider() {
    let cardSliderWrapper = document.getElementById('mental-model-slider');

    if (cardSliderWrapper && !cardSliderWrapper.classList.contains('slick-initialized')) {
        jQuery('#mental-model-slider').slick({
            infinite: true,
            prevArrow: '<button type="button" class="slick-prev"><</button>',
            nextArrow: '<button type="button" class="slick-next">></button>',
            arrows: false,
            responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    respondTo: 'window',
                    adaptiveHeight: true
                }
            }]
        });

        let filtered = false;

        document.querySelectorAll('#editor-mental-models-box li[data-category]').forEach(function(category) {
            category.addEventListener('click', function(e) {
                let cat = e.currentTarget.dataset.category;

                if (filtered) {
                    jQuery('#mental-model-slider').slick('slickUnfilter');
                    filtered = false;
                }

                if (cat != 'all') {
                    jQuery('#mental-model-slider').slick('slickFilter', '.' + cat);
                    filtered = true;
                }

                document.getElementById('mental-models-menu').classList.add('hidden');
            });
        });

        document.getElementById('mental-models-menu__return-btn').addEventListener('click', function() {
            document.getElementById('mental-models-menu').classList.remove('hidden');
        });
    }
}

function saveTaskTemplate(templateName, template) {
    if (!compassData.taskNoteTemplate[templateName]) {
        compassData.taskNoteTemplate[templateName] = template;
    } else {
        let isOverride = confirm(isHun ? 'Szeretnéd felülírni a sablont?' : 'Do you want to override this template?');

        if (isOverride) {
            compassData.taskNoteTemplate[templateName] = template;
        } else {
            let isDeletion = confirm(isHun ? 'Szeretnéd törölni a sablont?' : 'Do you want to delete this template?');

            if (isDeletion) {
                delete compassData.taskNoteTemplate[templateName];
            }
        }
    }

    saveCompassToDB();
}