function injectCompassData() {
    let tabularData = {
            roles: '#roles tbody',
            enhance: '#enhance-areas tbody'
        },
        listData = {
            values: '#values ul',
            labels: '#labels ul'
        };

    let tableRowHTMLTemplate = getCompassTableRowHTML(),
        listElementInnerHTML = getCompassListElementHTML();

    for (data in tabularData) {
        document.querySelectorAll(tabularData[data]).forEach(function(el) {
            for (category in compassData[data]) {
                el.insertAdjacentHTML('beforeend', tableRowHTMLTemplate);
                el.querySelector('.editable-table-row:last-of-type .table-role').dataset.area = category;
                el.querySelector('.editable-table-row:last-of-type .area-name').insertAdjacentHTML('afterbegin', category);
                el.querySelector('.editable-table-row:last-of-type .score').innerHTML = compassData[data][category].score;
                el.querySelector('.editable-table-row:last-of-type .table-goal').insertAdjacentHTML('afterbegin', compassData[data][category].goal);
                el.querySelector('.editable-table-row:last-of-type .area-why').insertAdjacentHTML('afterbegin', compassData[data][category].why || '');
            }
        });
    }

    for (data in listData) {
        document.querySelectorAll(listData[data]).forEach(function (el) {
            if (compassData[data]) {
                compassData[data].forEach(function(listElement) {
                    el.insertAdjacentHTML('beforeend', listElementInnerHTML);
                    el.querySelector('li:last-of-type').insertAdjacentHTML('afterbegin', listElement.name);
                });
            }
        });
    }
}

function addCompassListElement(value, taskName = '') {
    let listElementHTMLTemplate = getCompassListElementHTML(value);

    let category = document.getElementById('word-cloud').dataset.type,
        targetList = document.querySelector(`[data-compass-group="${category}"]`).querySelector('ul');

    if (compassData[category].length == 7) {
        alert(isHun ? 'Nem ajánlott hétnél több értéket megadni, hogy megmaradjon a fókusz' : 'It is not recommended to add more than seven values to keep you focused.');
        return;
    }

    if (compassData[category].find(el => el.name == value)) {
        alert('This element is already included');
        return;
    }
    targetList.insertAdjacentHTML('beforeend', listElementHTMLTemplate);
    compassData[category].push({name: value});
    saveCompassToDB();

    targetList.querySelector('li:last-child .remove-list-element').addEventListener('click',  removeCompassListElement);

    if (targetList.querySelector('li:last-child .compass-element-edit-button')) {
        targetList.querySelector('li:last-child .compass-element-edit-button').addEventListener('click', function(e) {
            console.log(e.currentTarget.closest('.compass__group__list_element').innerText);
        });
    }

    // attachDragListeners(targetList.querySelector('li:last-child'));
}


function addCompassTableRow(e, value) {
    let area = value || prompt(isHun ? 'Add meg a területet' : 'Enter area'),
        score,
        goal,
        why;

    if (!area) return;

    let category = document.getElementById('word-cloud').dataset.type || e.currentTarget.closest('.compass__group').dataset.compassGroup,
        targetTable = document.querySelector(`[data-compass-group="${category}"]`).querySelector('tbody');

    if (compassData[category][area]) {
        alert('This element is already included');
        return;
    }

    if (!score) {
        score = userOptions.beta_features == 'on' ? prompt(isHun ? 'Hogyan értékeled ezt a területet jelenleg 1 és 5 között (később is értékelheted)?' : 'How do you rate this area currently between 1 and 5? (you can do this later too)', 3) : 3;
    }

    if (!goal) {
        goal = prompt(isHun ? 'Add meg a célt (később is megteheted)' : 'Enter goal (you can do this later too)');
    }
    if (!goal) goal = ' ';

    let tableRowHTMLTemplate = getCompassTableRowHTML(area, score, goal, why);

    targetTable.insertAdjacentHTML('beforeend', tableRowHTMLTemplate);

    compassData[category][area] = {
        score: score,
        goal: goal,
        why: ''
    };
    saveCompassToDB();

    targetTable.querySelector('tr:last-child .remove-table-row').addEventListener('click', removeCompassTableRow);
    targetTable.querySelector('tr:last-child .edit-table-row').addEventListener('click', editTableRowHandler);


    // attachDragListeners(targetTable.querySelector('tr:last-child'));
}


function toggleCompass() {
    if (window.innerWidth < 769) resetDefaultView('compass');

    let compassContainer = document.getElementById('compass');
    let leftSidebar = document.getElementById('left-sidebar');

    compassContainer.classList.toggle('hidden');
    document.body.classList.toggle('compass-open');

    if (window.innerWidth < 992) {
        if (leftSidebar.dataset.openedby != 'compass' ) {
            leftSidebar.dataset.openedby = 'compass';
        } else {
            leftSidebar.dataset.openedby = '';
        }
    }
}

function attachCompassEventListeners(sync) {
    document.querySelectorAll('.remove-list-element').forEach(function(el) {
        el.addEventListener('click', removeCompassListElement);
    });

    // document.querySelectorAll('.edit-list-element').forEach(function(el) {
    //     el.addEventListener('click', function(e) {
    //         let name = e.currentTarget.closest('.compass__group__list_element').innerText;
    //     });
    // });

    // document.querySelectorAll('.compass__group__list_element').forEach(function(el) {
    //     attachDragListeners(el);
    // });

    document.querySelectorAll('.compass__group__wrapper tbody').forEach(list => {
        Sortable.create(list, {
            animation: 150,
            onStart: drag,
            onEnd: drop,
            filter: 'tr:not(.editable-table-row)'
        });
    });

    document.getElementById('add-enhance-area').addEventListener('click', addCompassTableRow);

    document.querySelectorAll('.remove-table-row').forEach(function(el) {
        el.addEventListener('click', removeCompassTableRow);
    });

    document.querySelectorAll('.edit-table-row').forEach(function(el) {
        el.addEventListener('click', editTableRowHandler);
    });

    // document.querySelectorAll('.editable-table-row').forEach(function(el) {
    //     attachDragListeners(el);
    // });


    if (sync) attachPrincipleEventHandlers();
}

function editTableRowHandler(e) {
    e.preventDefault();
    let category = e.target.closest('.compass__group').dataset.compassGroup,
        parentRow = e.currentTarget.closest('.editable-table-row'),
        area = parentRow.querySelector('.area-name').innerText.trim(),
        score = compassData[category][area].score,
        goal = compassData[category][area].goal,
        newScore = userOptions.beta_features == 'on' ? prompt(isHun ? 'Hogyan értékeled ezt a területet jelenleg 1 és 5 között?' : 'How do you rate this area currently between 1 and 5?', score) : 3,
        newGoal = prompt(isHun ? 'Mi az új célod?' : 'What is your new goal?', goal);

    if ((newGoal && goal != newGoal) || (newScore && score != newScore)) {
        compassData[category][area].score = newScore;
        parentRow.querySelector('.score').innerText = newScore;
        compassData[category][area].goal = newGoal;
        parentRow.querySelector('.table-goal').innerText = newGoal;
        saveCompassToDB();

        if (document.body.classList.contains('interactive-planning-mode') || document.getElementById('awareness-map').classList.contains('show')) {
            initWheelOfLifeForPeriodSet();
        }
    }
}


function getCompassListElementHTML(name = '') {
    return `<li class="compass__group__list_element" draggable="true">
        ${name}
        <button class="remove-list-element compass-element-remove-button">
            <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon.svg">
        </button>
    </li>`;
}


function getCompassTableRowHTML(area = '', score = '3', goal = '', why = '') {
    return `<tr class="editable-table-row" draggable="true" ">
        <td colspan="2" class="table-role" id="area-1" data-area="${area}">        
            <button class="edit-table-row compass-element-edit-button">
                <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/edit-task-settings.svg">
            </button>
            <button class="remove-table-row compass-element-remove-button">
                <img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/delete-icon.svg">
            </button>
            <div class="area-wrapper" onclick="expandWhy(event)">
                <div class="area-goal-wrapper">
                    <div class="area">
                        <span class="expander"><img class="svg-icon" src="${appVariables.templateUrl}/assets/images/svg-icons/expand.svg"></span>
                        <span class="area-name">${area}</span>
                        <br>
                        <span class="score-wrapper"><span class="score">${score}</span>/5</span>
                    </div><div class="table-goal">${goal}</div>
                </div>
                <div class="why-wrapper hide">
                    <hr>
                    <h4 class="why-title">Purpose</h4>
                    <textarea placeholder="Why is this extremely important to you?" onclick="editWhy(event)" onmouseleave="saveWhy(event)" class="area-why">${why}</textarea>
                </div>
            </div>        
        </td>
    </tr>`;
}

function editWhy(e) {
    e.stopPropagation();
}

function saveWhy(e) {
    e.stopPropagation();

    compassData['roles'][e.currentTarget.closest('.table-role').dataset.area].why = e.currentTarget.value;
    saveCompassToDB();
}

function expandWhy(e) {
    if (e.currentTarget.classList.contains('area-wrapper')) {
        e.currentTarget.closest('.editable-table-row').classList.toggle('open');
        e.currentTarget.closest('.editable-table-row').querySelector('.why-wrapper').classList.toggle('hide');
    }
}

function removeCompassListElement(e) {
    if (confirm(isHun ? 'Tényleg szeretnéd törölni ezt az elemet?' : 'Do you really want to delete this element?')) {
        let listElement = e.target.closest('.compass__group__list_element'),
            category = listElement.closest('.compass__group').dataset.compassGroup,
            index;

        removeValueFromCompassData(category, listElement.innerText);
        listElement.remove();
        saveCompassToDB();
    }
}

function removeCompassTableRow(e) {
    e.preventDefault();
    if (confirm(isHun ? 'Tényleg szeretnéd törölni ezt a sort?' : 'Do you really want to delete this row?')) {
        let area = e.target.closest('.editable-table-row').querySelector('.area-name').innerText.trim(),
            category = e.target.closest('.compass__group').dataset.compassGroup;

        delete compassData[category][area];

        e.target.closest('.editable-table-row').remove();
        //extractCompassData();
        saveCompassToDB();

        if (document.body.classList.contains('interactive-planning-mode') || document.getElementById('awareness-map').classList.contains('show')) {
            initWheelOfLifeForPeriodSet();
        }
    }
}

function removeValueFromCompassData(category, value) {
    compassData[category].forEach(function(el, i) {
        if (el.name == value) {
            index = i;
        }
    });

    compassData[category].splice(index, 1);
}
