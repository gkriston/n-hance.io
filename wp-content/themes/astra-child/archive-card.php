<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header(); ?>


    <div <?php astra_primary_class(); ?>>

        <?php astra_primary_content_top(); ?>

        <main id="main" class="site-main">

            <?php
                $args_cards = array(
                'post_type' => 'card'
                );

                $loop_cards = new WP_Query( $args_cards );
            ?>

            <?php if ( $loop_cards->have_posts() ) : ?>

                <?php do_action( 'astra_template_parts_content_top' ); ?>

                <div id="card-slider">
                    <?php
                    while ( $loop_cards->have_posts() ) :
                        $loop_cards->the_post(); ?>

                        <?php if (get_field('card_back')) : ?>
                        <div class="flip-card">
                            <div class="flip-card-inner">
                                <div class="flip-card-front">
                                    <?php the_content(); ?>
                                    <p class="answer-info">Kattints a kártyára a válaszért...</p>
                                </div>
                                <div class="flip-card-back">
                                    <?php the_field('card_back'); ?>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="simple-card">
                            <div class="simple-card-inner">
                                <div class="simple-card-content">

                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php endwhile; ?>
                </div>

                <?php do_action( 'astra_template_parts_content_bottom' ); ?>

            <?php else : ?>

                <?php do_action( 'astra_template_parts_content_none' ); ?>

            <?php endif; ?>

        </main><!-- #main -->

        <?php astra_pagination(); ?>

        <?php astra_primary_content_bottom(); ?>

    </div><!-- #primary -->

<?php get_footer(); ?>