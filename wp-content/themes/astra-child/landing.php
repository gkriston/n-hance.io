<?php
/**
 * Template Name: Landing
**/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header(); ?>
</div>
<section id="hero" class="landing">
	<div class="ast-container">
		<div class="row">
			<div class="col-lg-6 text-box">
				<h1 class="hero-title">Framework for your mind, unavoidable higher results</h1>
				<p class="hero-description">Forget unprioritized, stressful to-do lists and try the app which launches your productivity to the stratosphere by giving you guidance on what to focus on and helps you get smarter through making new knowledge engraving a daily habit.</p>
				<div class="cta-button-holder">
                    <?php do_shortcode('[contact-form-7 id="122" title="Contact form 1"]'); ?>
<!--					<a href="#" class="cta-button">GET MORE PRODUCTIVE NOW</a>-->
				</div>
			</div>
			<div class="col-lg-6 img-box">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/n-hance-desktop-view.jpg">
			</div>
		</div>
		<div class="row achievable">
			<div class="col-lg-6">
				<ul>
					<li>Prioritizing goals based on their importance for your life</li>
					<li>Per list based goal setting to keep you on track</li>
					<li>Side-by-side you for easier overview and goal setting</li>
					<li>Level up with action plans from our knowledge marketplace</li>
				</ul>
			</div>
			<div class="col-lg-6">
				<ul>
					<li>Priorization view with scoring panel to see what really matters</li>
					<li>Automatic time estimate summing for better time-management</li>
					<li>Customizable looks to make your to-do list personal</li>
					<li>Smart integrations for easy task importing</li>
				</ul>
			</div>
		</div>
	</div>
	<img class="bullseye" src="<?php echo get_stylesheet_directory_uri();?>/assets/images/bullseye.svg">
</section>
<div class="ast-container">
<?php get_footer(); ?>