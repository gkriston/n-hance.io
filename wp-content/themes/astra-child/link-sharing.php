<?php
/**
 * Template Name: Link sharing
 **/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

get_header(); ?>

</div>
<?php $nhance_options = get_user_meta(get_current_user_id(), 'nhance_options')[0]; ?>
<?php $is_hun = false; ?>

<?php if ($is_hun) : ?>
<section id="hero" class="landing">
    <div class="ast-container">
        <div class="row">
            <div class="col-lg-6 col-md-6 text-box">
                <h1 class="hero-title">Szuper tartalom vár rád</h1>
                <p class="hero-description">A megosztott lista megtekintéséhez be kell jelentkezned. Ha még nincs fiókod, kérd meg a link megosztóját, hogy igényeljen számodra egyet. Az alkalmazás egyelőre csak zárt körben érhető el.</p>
                <p><b>Megértésed köszönjük!</b></p>
                <div class="cta-button-holder">
                    <a href="<?php echo site_url() . (is_user_logged_in() ? '/app' : '/wp-admin'); ?>" class="cta-button">BELÉPÉS</a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 img-box">
                <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/n-hance-desktop-view.jpg">
            </div>
        </div>
    </div>
    <img class="bullseye" src="<?php echo get_stylesheet_directory_uri();?>/assets/images/bullseye.svg">
</section>
<div class="ast-container">
    <?php else : ?>
    <section id="hero" class="landing">
        <div class="ast-container">
            <div class="row">
                <div class="col-lg-6 col-md-6 text-box">
                    <h1 class="hero-title">Amazing content is awaiting for you</h1>
                    <p class="hero-description">To view the shared list you must be logged in. If you don't have an account yet, please ask the person you've received the link from to request one for you. The app is only available for closed beta at the moment.</p>
                    <p><b>Thank you for your understanding!</b></p>
                    <div class="cta-button-holder">
                        <a href="<?php echo site_url() . (is_user_logged_in() ? '/app' : '/wp-admin'); ?>" class="cta-button">BELÉPÉS</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 img-box">
                    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/n-hance-desktop-view.jpg">
                </div>
            </div>
        </div>
        <img class="bullseye" src="<?php echo get_stylesheet_directory_uri();?>/assets/images/bullseye.svg">
    </section>
    <div class="ast-container">
        <?php endif; ?>

        <?php get_footer(); ?>
