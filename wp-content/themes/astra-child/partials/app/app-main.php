<?php $nhance_options = get_user_meta(get_current_user_id(), 'nhance_options')[0]; ?>
<?php $is_hun = false; ?>

<?php
if (!$nhance_options['first_run'] || $nhance_options['first_run'] != 'done') {
//    echo do_shortcode('[gravityform id="1" ajax="true"]');

    function isMobile() {
        return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    if (isMobile()) :?>
        <div id="desktop-only-onboarding">
            <h2><?php echo $is_hun ? 'Máris egy lépéssel közelebb kerültél a céljaidhoz!' : 'You are one step closer to the life of your dreams!'; ?></h2>
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/crossroad.svg">
            <p class="next-step"><?php echo $is_hun ? 'Hogy a legtöbbet kihozhasd a szoftervből, az első indítást kérlek végezd el asztali számítógépről a bemutató végignézéséhez.' : 'For the best experience it is recommended to use a desktop web browser.'; ?></p>
            <div class="cta-button-holder" style="margin-top: 35px;">
                <a href="" class="cta-button cta-button--intro">LET'S START</a>
            </div>
        </div>
    <?php endif; ?>
<!--    <script>-->
<!--        var surveyNotificationInterval;-->
<!---->
<!--        jQuery(document).bind('gform_post_render', () => {-->
<!--            surveyNotificationInterval = setInterval(() => {-->
<!--                if (document.querySelector('.gform_confirmation_wrapper')) {-->
<!--                    document.querySelector('.gform_confirmation_wrapper').style.display = 'none';-->
<!--                    userOptions.first_run = 'done';-->
<!--                    updateUserOptions();-->
<!--                    updateWPUserOptions();-->
<!--                    clearInterval(surveyNotificationInterval);-->
<!--                }-->
<!--            }, 3000);-->
<!--        });-->
<!--    </script>-->
    <?php
}
?>


<div id="modal-wrapper">
    <div id="modal" class="hide">
        <span class="close">✕</span>
        <div id="modal-content">
        </div>
    </div>
</div>
<div id="awareness-map">
    <?php
        $range_labels = array(
                $is_hun ? '1 év' : '1 year',
                $is_hun ? '5 év' : '5 year',
                $is_hun ? 'Teljes élet' : 'Whole life'
        );
    ?>
    <span class="help">?</span>
    <span class="close">✕</span>
    <form name="myform" oninput="range1value.value = (range1.valueAsNumber == 1 ? (isHun ? '1 év' : '1 year') : (range1.valueAsNumber == 2 ?  (isHun ? '5 év' : '5 year') : (isHun ? 'Teljes élet' : 'Whole life')))">
        <input name="range1" type="range" step="1" min="1" max="3" value="3">
        <output name="range1value" for="range1" >2</output>
    </form>
    <div id="awareness-map__wheel-wrapper"></div>
</div>
<div id="debug-box" class="hide">
</div>
<div id="onboarding-steps" class="hide">
    <ul>
        <li class="onboarding-steps" data-step="1">1</li>
        <li class="onboarding-steps" data-step="2">2</li>
        <li class="onboarding-steps" data-step="3">3</li>
        <li class="onboarding-steps" data-step="4">4</li>
    </ul>
</div>

<div id="word-cloud" class="hide">
    <span class="close">✕</span>
    <h3 id="word-cloud__title"></h3>
    <div id="word-cloud__instructions"></div>
    <p><?php echo $is_hun ? 'Gyakran használt' : 'Regularly used'; ?></p>
    <ul id="word-cloud__list"></ul>
    <button onclick="clearLabelFilter()" class="clear-filter-btn cta-button hide"><?php echo $is_hun ? 'Szűrés törlése' : 'Clear filter'; ?></button>
    <form>
        <input type="text" placeholder="<?php echo $is_hun ? 'Saját hozzáadása' : 'Add new'; ?>">
        <button type="submit" class="cta-button"><?php echo $is_hun ? 'Hozzáad' : 'Add'; ?></button>
    </form>
</div>
<div id="time-tracker-wrapper" class="hidden">
    <div id="tracked-task-name"></div>
    <img  onclick="stopCurrentTimeTracking()" class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/time.svg" height="45">
    <span><?php echo $is_hun ? '0mp' : '0s'; ?></span>
</div>
<div id="nav-wrapper">
    <a href="<?php echo get_site_url(); ?>">
        <img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/n-letter-orange.svg" height="45">
    </a>
    <div id="hot-button-group">
        <button id="show-compass" onclick="toggleCompass()">
            <img class="svg-icon svg-icon-white" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/compass.svg">
            <img class="svg-icon svg-icon-black" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/compass-black.svg">
            <span class="btn-title">
                    <?php echo $is_hun ? 'Iránytű' : 'Compass'; ?>
                </span>
        </button>
        <button id="show-wheel-of-life">
            <img class="svg-icon svg-icon-white" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/wheel.svg">
            <img class="svg-icon svg-icon-black" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/wheel-black.svg">
            <span class="btn-title">
                    <?php echo $is_hun ? 'Életkerék' : 'Lifewheel'; ?>
                </span>
        </button>
        <button id="show-dayview-calendar">
            <img class="svg-icon svg-icon-white" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/calendar-white.svg">
            <img class="svg-icon svg-icon-black" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/calendar-black.svg">
            <span class="btn-title">
                    <?php echo $is_hun ? 'Naptár' : 'Calendar'; ?>
                </span>
        </button>
        <!--            <button id="show-schedule-calendar">-->
        <!--                <img class="svg-icon" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/images/svg-icons/calendar-white.svg">-->
        <!--                <span class="btn-title">-->
        <!--                    --><?php //echo $is_hun ? 'Naptár' : 'Calendar'; ?>
        <!--                </span>-->
        <!--            </button>-->
        <button id="show-notes-calendar">
            <img class="svg-icon svg-icon-white" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/write-note-white.svg">
            <img class="svg-icon svg-icon-black" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/write-note-black.svg">
            <span class="btn-title">
                    <?php echo $is_hun ? 'Napló' : 'Notes'; ?>
                </span>
        </button>
        <button class="lists-opener">
            <img class="svg-icon svg-icon-white" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/list-white.svg">
            <img class="svg-icon svg-icon-black" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/list-black.svg">
            <span class="btn-title">
                    <?php echo $is_hun ? 'Listák' : 'Lists'; ?>
                </span>
        </button>
    </div>
    <ul id="nav-wrapper__button-group">
        <li id="learning-modules" style="display: none" class="nav-wrapper__button-group__icon">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/level-up-icon.svg">
            <?php echo $is_hun ? 'Tudástár' : 'Resources'; ?>
        </li>
        <li id="chart-icon" class="nav-wrapper__button-group__icon">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/chart.svg" height="45">
            <?php echo $is_hun ? 'Statok' : 'Stats'; ?>
        <li id="principle-icon" class="nav-wrapper__button-group__icon">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/principles.svg" height="45">
            <?php echo $is_hun ? 'Elvek' : 'Principles'; ?>
        </li>
        <li id="card-icon" class="nav-wrapper__button-group__icon">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/idea-2.svg">
            <?php echo $is_hun ? 'Tanulj' : 'Learn'; ?>
        </li>
        <li id="notification-icon" class="nav-wrapper__button-group__icon">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/bell.svg">
            <?php echo $is_hun ? 'Értesítés' : 'Alerts'; ?>
        </li>
        <li id="help-icon" class="nav-wrapper__button-group__icon" onclick="toggleHelpMenu()">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/question.svg">
            <?php echo $is_hun ? 'Segítség' : 'Help'; ?>
        </li>
        <li class="settings nav-wrapper__button-group__icon">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/settings.svg">
            <?php echo $is_hun ? 'Opciók' : 'Settings'; ?>
        </li>
    </ul>
</div>
<div id="drop-to-another-list-drawer" class="hide" ondrop="drop(event)" ondragover="allowDrop(event)"><?php echo $is_hun ? 'Dobd ide a feladatot másik listába áthelyezéshez' : 'Drop task here to move it to another list'; ?>
</div>
<div id="available-lists-to-drop" class="hide">
	<span class="close">✕</span>
	<ul>
	</ul>
</div>
<div id="content-wrapper">
    <div id="app-notifications" class="hide">
        <?php
        // The Query
        $the_query = new WP_Query( array('post_type' => 'post') );

        // The Loop
        if ( $the_query->have_posts() ) {
            echo '<ul>';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                ?>
                    <li>
                        <?php the_category(); ?>
                        <h3><?php the_title(); ?></h3>
                        <p class="post-time"><?php the_time('Y-m-d'); ?></p>
                        <p><?php the_excerpt(); ?></p>
                        <a target="_blank" href="<?php the_permalink(); ?>"><?php echo ($is_hun ? 'Tovább >>' : 'Read more >>'); ?></a>
                    </li>
                <?php
            }
            echo '</ul>';
        } else {
            // no posts found
        }
        /* Restore original Post Data */
        wp_reset_postdata();
        ?>
    </div>

    <div id="left-sidebar">
		<h2 class="lists-opener"><span id="user-initials"><?php echo substr(wp_get_current_user()->user_nicename, 0, 2); ?></span><?php echo $is_hun ? 'Listáim' : 'Lists'; ?></h2>
		<div id="compass" class="hidden" >
            <div class="compass__group" data-compass-group="roles">
                <div class="compass__group__wrapper">
                    <table id="roles">
                        <tbody ondrop="drop(event)" ondragover="allowDrop(event)">
                        <tr>
                            <th><h4 class="compass__group__header">Roles</h4></th>
                            <th><h4 class="compass__group__header">Goals</h4></th>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <button id="add-role" class="add-table-row" onclick="generateWordCloud('roles')"><img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/plus-icon.svg"></button>
            </div>
            <div class="compass__group" data-compass-group="enhance">
                <div class="compass__group__wrapper">
                    <table id="enhance-areas">
                        <tbody ondrop="drop(event)" ondragover="allowDrop(event)">
                        <tr>
                            <th><h4 class="compass__group__header">Balanced living</h4></th>
                            <th class="compass__group__header"></h4></th>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <button id="add-enhance-area" class="add-table-row"><img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/plus-icon.svg"></button>
            </div>
            <div class="compass__group" id="values" data-compass-group="values">
                <div class="compass__group__wrapper">
                    <h4 class="compass__group__header">Values</h4>
                    <ul class="compass__group__list" ondrop="drop(event)" ondragover="allowDrop(event)">
                    </ul>
                </div>
                <button class="add-list-element" onclick="generateWordCloud('values')"><img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/plus-icon.svg"></button>
            </div>
            <div class="compass__group">
                <div class="compass__group__wrapper hide" id="timeline-wrapper">
                    <h4 class="compass__group__header"><?php echo $is_hun ? 'Közelgő események (1hó)' : 'Upcoming events (1m)'; ?></h4>
                    <div class="timeline">
                    </div>
                </div>
            </div>
		</div>
		<div id="sidebar-content-wrapper">
            <div id="sidebar-secondary-button-group" class="hide">
                <button id="intro" onclick="showGreeting()"><?php echo $is_hun ? 'Bemutató' : 'Intro'; ?></button>
                <button id="planning" onclick="startPlanning()"><?php echo $is_hun ? 'Tervezés' : 'Plan!'; ?></button>
                <button id="show-goals"><?php echo $is_hun ? 'Célok' : 'Goals'; ?></button>
                <button id="show-features" onclick="startFeatureShowcase()"><?php echo $is_hun ? '?' : '?'; ?></button>
            </div>
			<ul id="my-lists" data-hint="A listáidat a nevükre kattintva tudod megnyitni.</b></p><p> Ha a <b>CTRL gomb</b> egyidejű lenyomásával kattintasz, több listát is egymás mellé tudsz nyitni.">
				<li onclick="createNewList()"><?php echo $is_hun ? 'Új lista' : 'Create list'; ?></li>
			</ul>
			<a href="<?php echo wp_logout_url(); ?>" id="logout-link"><?php echo $is_hun ? 'Kilépés' : 'Logout'; ?> <img class="svg-icon svg-icon-white" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/exit-icon.svg"><img class="svg-icon svg-icon-black" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/exit-icon-black.svg"></a><br>
		</div>
	</div>
	<div id="schedule-calendar" class="hide calendar">
		<span class="close">✕</span>
		<div id="schedule-calendar-content" class="calendar-content"></div>
	</div>
    <div id="notes-calendar-wrapper">
        <div id="notes-calendar" class="hide calendar">
        </div>
    </div>
	<div id="notes-calendar-template" class="hide calendar">
        <h2><?php echo $is_hun ? 'Jegyzeteim' : 'Notes'; ?></h2>
        <button id="search-notes" @click="searchNotes">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/search-icon-thin.svg'; ?>"></button>
        <button id="clear-notes-search" @click="clearSearch">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/clear.svg'; ?>"></button>
        <button id="add-new-note" class="cta-button" @click="addNewNote">
            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/plus-icon-white.svg'; ?>"></button>
		<span class="close" @click="hideNotesCalendar">✕</span>
		<div id="notes-calendar-content" class="calendar-content">
            <div v-if="notes">
                <div class="note-date-wrapper" v-for="(dateNotes, date) in notes">
                    <div v-for="(note, index) in dateNotes" class="note-element-wrapper">
                        <div :data-date="date" :data-index="index" class="title" @click="openNote">
                            {{note.title ? note.title : 'Unknown'}}
                            <div class="note-edit-actions">
                                <img class="svg-icon edit-note"  @click="openNote" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/note-icon.svg">
                                <img class="svg-icon edit-title"  @click="editNoteTitle" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/edit-task-settings.svg">

                                <img class="svg-icon delete-icon"  @click="deleteNote" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/delete-icon.svg">
                            </div>
                        </div>
                        <div class="date">{{date}}</div>
                    </div>
                </div>
            </div>
            <div v-else id="no-notes">
                <img @click="addNewNote" class="svg-icon" style="width: 100px; height: 100px;" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/note-icon.svg">
                <p>Please add your first note</p>
            </div>
        </div>
    </div>



	<div id="app">
<!--		<button id="show-dayview-calendar" ondragenter="showDayViewCalendar(event)" ondragover="allowDrop(event)"><img class="svg-icon" src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/images/svg-icons/calendar-white.svg"></button>-->
        <div id="inbox-wrapper"></div><div id="planner-wrapper">
            <div id="dayview-calendar" class="calendar hidden"></div>
            <div id="task-lists"></div>
        </div>
	</div>
	</div>
    <div id="learning-modules-wrapper">
    </div>
    <div id="editor-modal-wrapper">
        <div id="editor-wrapper" class="hide">
            <span class="close">✕</span>
            <span class="task-saved-notification">✓ <?php echo $is_hun ? 'Jegyzet mentve' : 'Note saved '; ?></span>
            <div class="note-templates-wrapper hide">
                <div class="note-templates">
                    <span class="close">✕</span>
                    <label for="browser"><?php echo $is_hun ? 'Írd be a jegyzetsablon nevét' : 'Enter note name'; ?></label>
                    <input list="saved-templates" name="template" id="template" placeholder="<?php echo $is_hun ? '...vagy válassz korábbi mentésekből' : '...or select and an old one'; ?>">
                    <datalist id="saved-templates">
                    </datalist>
                    <button id="save-note-template-button"><?php echo $is_hun ? 'Mentés' : 'Save'; ?></button>
                    <button id="load-note-template-button"><?php echo $is_hun ? 'Betölt' : 'Load'; ?></button>
                </div>
            </div>
            <div id="editor-principles-box" class="editor-supportive-tools principles hidden">
                <span class="close">✕</span>
                <div class="editor-supportive-tools__inner">
                    <h4><?php echo is_hun ? 'Releváns elvek' : 'Relevant principles';?></h4>
                    <div class="relevant-principles__categories">
                    </div>
                </div>
            </div>
            <div id="editor-mental-models-box" class="editor-supportive-tools mental-models hidden">
                <span class="close">✕</span>
                <div class="editor-supportive-tools__inner">
                    <?php require(locate_template('partials/app/app-mental-models.php')); ?>
                </div>

            </div>
            <div id="editor-container"></div>
        </div>
    </div>
</div>


<?php require(locate_template('partials/app/app-intro.php')); ?>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/label-menu.js'></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/drag-drop.js'></script>

<audio id="complete-sound">
    <source src="<?php echo get_stylesheet_directory_uri(); ?>/assets/complete.wav" type="audio/wav">
    Your browser does not support the audio element.
</audio>
<div id="label-menu-overlay" class="hidden app-overlay">
    <nav class='menu'>
        <div id="label-menu-instructions"></div>
    <span class='menu-toggler' id='menu-toggler'>
      <button class="btn-close"><svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 140 140" width="140" height="140"><g transform="matrix(14,0,0,14,0,0)"><path d="M9.5,4.5A.5.5,0,0,0,9,4H6V1A.5.5,0,0,0,5.5.5h-1A.5.5,0,0,0,4,1V4H1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,1,6H4V9a.5.5,0,0,0,.5.5h1A.5.5,0,0,0,6,9V6H9a.5.5,0,0,0,.5-.5Z" fill="#000000" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path></g></svg></button>
    </span>
        <ul>
        </ul>
    </nav>
</div>
<div id="load-overlay">
    <div id="load-in-progress">
        <svg id="eijlpjcoycck1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 300 300" shape-rendering="geometricPrecision" text-rendering="geometricPrecision"><style><![CDATA[#eijlpjcoycck4_to {animation: eijlpjcoycck4_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck4_to__to { 0% {transform: translate(33.750000px,260.600005px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(55.450000px,260.600005px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(33.750000px,260.600005px)} }#eijlpjcoycck6_to {animation: eijlpjcoycck6_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck6_to__to { 0% {transform: translate(116.250000px,124.800003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(85.450000px,124.900003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(116.250000px,124.800003px)} }#eijlpjcoycck8_to {animation: eijlpjcoycck8_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck8_to__to { 0% {transform: translate(57.900002px,166.550003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(73.600002px,166.550003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(57.900002px,166.550003px)} }#eijlpjcoycck10_to {animation: eijlpjcoycck10_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck10_to__to { 0% {transform: translate(108.250000px,78.750000px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(127.750000px,78.850000px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(108.250000px,78.750000px)} }#eijlpjcoycck12_to {animation: eijlpjcoycck12_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck12_to__to { 0% {transform: translate(212.199997px,166.550003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(239.699997px,166.550003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(212.199997px,166.550003px)} }#eijlpjcoycck14_to {animation: eijlpjcoycck14_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck14_to__to { 0% {transform: translate(125.700001px,43.800001px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(99.050001px,43.800001px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(125.700001px,43.800001px)} }#eijlpjcoycck16_to {animation: eijlpjcoycck16_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck16_to__to { 0% {transform: translate(267.500000px,33.350000px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(237.900000px,33.350000px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(267.500000px,33.350000px)} }#eijlpjcoycck18_to {animation: eijlpjcoycck18_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck18_to__to { 0% {transform: translate(237.900002px,124.900009px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(216.400002px,124.900009px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(237.900002px,124.900009px)} }#eijlpjcoycck20_to {animation: eijlpjcoycck20_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck20_to__to { 0% {transform: translate(190.950005px,209.150002px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(163.050005px,209.150002px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(190.950005px,209.150002px)} }#eijlpjcoycck22_to {animation: eijlpjcoycck22_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck22_to__to { 0% {transform: translate(63.299999px,209.150002px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(34.549999px,209.150002px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(63.299999px,209.150002px)} }#eijlpjcoycck24_to {animation: eijlpjcoycck24_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck24_to__to { 0% {transform: translate(239.950005px,78.849998px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(261.832537px,78.849998px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(239.950005px,78.849998px)} }#eijlpjcoycck26_to {animation: eijlpjcoycck26_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck26_to__to { 0% {transform: translate(173.299999px,253.150002px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(197.699999px,253.150002px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(173.299999px,253.150002px)} }#eijlpjcoycck28_to {animation: eijlpjcoycck28_to__to 1400ms linear infinite normal forwards}@keyframes eijlpjcoycck28_to__to { 0% {transform: translate(137.549999px,166.550003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 50% {transform: translate(165.049999px,166.550003px);animation-timing-function: cubic-bezier(0.445000,0.050000,0.550000,0.950000)} 100% {transform: translate(137.549999px,166.550003px)} }]]></style><g id="eijlpjcoycck2"><g id="eijlpjcoycck3"><g id="eijlpjcoycck4_to" transform="translate(33.750000,260.600005)"><polygon id="eijlpjcoycck4" points="0.600000,285.600000 57.600000,285.600000 68.100000,235.800000 11.100000,235.800000" transform="translate(-34.350000,-260.700005)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck5"><g id="eijlpjcoycck6_to" transform="translate(116.250000,124.800003)"><path id="eijlpjcoycck6" d="M112.500000,107.900000L113.600000,107.900000L124,144.800000L180,144.800000L168.400000,104.800000L60.900000,104.800000L52.500000,144.800000L108.300000,144.800000C109.700000,134.700000,111.100000,122.300000,112.500000,107.900000L112.500000,107.900000Z" transform="translate(-116.250000,-124.800003)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck7"><g id="eijlpjcoycck8_to" transform="translate(57.900002,166.550003)"><path id="eijlpjcoycck8" d="M85.400000,172.800000C86.700000,167.100000,87.900000,159.800000,89.200000,150.600000L33.300000,150.600000L26.600000,182.500000L83.400000,182.500000L85.400000,172.800000Z" transform="translate(-57.900002,-166.550003)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck9"><g id="eijlpjcoycck10_to" transform="translate(108.250000,78.750000)"><polygon id="eijlpjcoycck10" points="150.200000,58.500000 63.100000,58.500000 54.500000,99 162,99" transform="translate(-108.250000,-78.750000)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck11"><g id="eijlpjcoycck12_to" transform="translate(212.199997,166.550003)"><path id="eijlpjcoycck12" d="M242.900000,150.600000L186.200000,150.600000C184.600000,159.600000,183.100000,170.200000,181.500000,182.500000L236.200000,182.500000L242.900000,150.600000Z" transform="translate(-212.199997,-166.550003)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck13"><g id="eijlpjcoycck14_to" transform="translate(125.700001,43.800001)"><polygon id="eijlpjcoycck14" points="164.200000,34.900000 85.900000,34.900000 82.100000,52.700000 169.300000,52.700000" transform="translate(-125.700001,-43.800001)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck15"><g id="eijlpjcoycck16_to" transform="translate(267.500000,33.350000)"><polygon id="eijlpjcoycck16" points="243.300000,14 235,52.700000 291.800000,52.700000 300,14" transform="translate(-267.500000,-33.350000)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck17"><g id="eijlpjcoycck18_to" transform="translate(237.900002,124.900009)"><path id="eijlpjcoycck18" d="M207.600000,133.100000C206.900000,136.600000,206.100000,140.600000,205.400000,144.900000L262,144.900000L270.400000,104.900000L213.500000,104.900000L207.600000,133.100000Z" transform="translate(-237.900002,-124.900009)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck19"><g id="eijlpjcoycck20_to" transform="translate(190.950005,209.150002)"><path id="eijlpjcoycck20" d="M193.100000,197.100000L191.800000,197.100000L189.200000,188.300000L133,188.300000L144.700000,230L240.100000,230L248.900000,188.300000L194.200000,188.300000C193.900000,191.100000,193.500000,194.100000,193.100000,197.100000Z" transform="translate(-190.950005,-209.150002)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck21"><g id="eijlpjcoycck22_to" transform="translate(63.299999,209.150002)"><polygon id="eijlpjcoycck22" points="30.500000,230 87.300000,230 96.100000,188.300000 39.300000,188.300000" transform="translate(-63.299999,-209.150002)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck23"><g id="eijlpjcoycck24_to" transform="translate(239.950005,78.849998)"><polygon id="eijlpjcoycck24" points="207.200000,99.100000 264.100000,99.100000 272.700000,58.600000 215.900000,58.600000" transform="translate(-239.950005,-78.849998)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck25"><g id="eijlpjcoycck26_to" transform="translate(173.299999,253.150002)"><polygon id="eijlpjcoycck26" points="135.400000,270.600000 213.600000,270.600000 221,235.700000 125.600000,235.700000" transform="translate(-173.299999,-253.150002)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g><g id="eijlpjcoycck27"><g id="eijlpjcoycck28_to" transform="translate(137.549999,166.550003)"><polygon id="eijlpjcoycck28" points="160.900000,150.600000 104.900000,150.600000 113.900000,182.500000 170.200000,182.500000" transform="translate(-137.549999,-166.550003)" fill="rgb(255,255,255)" stroke="none" stroke-width="1"/></g></g></g></svg>
        <p>
              <?php echo $is_hun ? 'Betöltés...' : 'Loading...'; ?>
        </p>
    </div>

    <div id="sync-needed-info" class="hide">
        <svg id="ex2bdh05hub1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 300 300" shape-rendering="geometricPrecision" text-rendering="geometricPrecision"><style><![CDATA[#ex2bdh05hub10_tr {animation: ex2bdh05hub10_tr__tr 1400ms linear infinite normal forwards}@keyframes ex2bdh05hub10_tr__tr { 0% {transform: translate(207.199997px,232.105545px) rotate(0deg)} 100% {transform: translate(207.199997px,232.105545px) rotate(360deg)} }]]></style><g id="ex2bdh05hub2"><g id="ex2bdh05hub3"><path id="ex2bdh05hub4" d="M123.400000,77.800000C72.100000,77.800000,17.500000,64.300000,17.500000,39.300000C17.500000,14.300000,72.100000,0.800000,123.400000,0.800000C174.700000,0.800000,229.300000,14.300000,229.300000,39.300000C229.300000,64.300000,174.800000,77.800000,123.400000,77.800000ZM123.400000,11.400000C64.300000,11.400000,28,27.600000,28,39.300000C28,51,64.300000,67.200000,123.400000,67.200000C182.500000,67.200000,218.800000,50.900000,218.800000,39.300000C218.800000,27.700000,182.500000,11.400000,123.400000,11.400000Z" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="1"/><path id="ex2bdh05hub5" d="M123.400000,144.200000C72.100000,144.200000,17.500000,130.700000,17.500000,105.700000C17.500000,102.800000,19.900000,100.400000,22.800000,100.400000C25.700000,100.400000,28.100000,102.800000,28.100000,105.700000C28.100000,117.400000,64.400000,133.600000,123.500000,133.600000C182.600000,133.600000,218.900000,117.300000,218.900000,105.700000C218.900000,102.800000,221.300000,100.400000,224.200000,100.400000C227.100000,100.400000,229.500000,102.800000,229.500000,105.700000C229.300000,130.700000,174.800000,144.200000,123.400000,144.200000Z" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="1"/><path id="ex2bdh05hub6" d="M119.800000,210.600000C68.500000,210.600000,17.500000,197.100000,17.500000,172.100000C17.500000,169.200000,19.900000,166.800000,22.800000,166.800000C25.700000,166.800000,28.100000,169.200000,28.100000,172.100000C28.100000,183.800000,60.700000,200,119.800000,200C122.700000,200,125.100000,202.400000,125.100000,205.300000C125,208.300000,122.700000,210.600000,119.800000,210.600000Z" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="1"/><path id="ex2bdh05hub7" d="M224.100000,157.600000C221.200000,157.600000,218.800000,155.200000,218.800000,152.300000L218.800000,39.300000C218.800000,36.400000,221.200000,34,224.100000,34C227,34,229.400000,36.400000,229.400000,39.300000L229.400000,152.300000C229.300000,155.300000,227,157.600000,224.100000,157.600000Z" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="1"/><path id="ex2bdh05hub8" d="M119.800000,277C68.500000,277,17.500000,263.500000,17.500000,238.500000L17.500000,39.300000C17.500000,36.400000,19.900000,34,22.800000,34C25.700000,34,28.100000,36.400000,28.100000,39.300000L28.100000,238.500000C28.100000,250.200000,60.700000,266.400000,119.800000,266.400000C122.700000,266.400000,125.100000,268.800000,125.100000,271.700000C125,274.700000,122.700000,277,119.800000,277Z" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="1"/></g></g><g id="ex2bdh05hub9" transform="matrix(1 0 0 1 0.76374700000002 -0.21106261999998)"><g id="ex2bdh05hub10_tr" transform="translate(207.199997,232.105545) rotate(0)"><g id="ex2bdh05hub10" transform="translate(-207.199997,-232.105545)"><g id="ex2bdh05hub11"><path id="ex2bdh05hub12" d="M260.600000,267.400000C260.200000,267.400000,259.800000,267.300000,259.500000,267.100000C258.500000,266.500000,258.200000,265.200000,258.800000,264.100000C272.600000,241.900000,270.600000,213.400000,253.900000,193.300000C236.100000,171.900000,206.100000,165.300000,181,177.300000C179.900000,177.800000,178.700000,177.400000,178.100000,176.300000C177.600000,175.200000,178,174,179.100000,173.400000C206,160.500000,238,167.600000,257.100000,190.500000C275,212,277.200000,242.500000,262.400000,266.300000C262,267,261.300000,267.400000,260.600000,267.400000Z" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="5" stroke-miterlimit="10"/><g id="ex2bdh05hub13"><polygon id="ex2bdh05hub14" points="251.800000,244.100000 255.900000,243.100000 261.100000,263.600000 281.200000,257 282.500000,261.100000 258,269.100000" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="5" stroke-miterlimit="10"/></g></g><g id="ex2bdh05hub15"><path id="ex2bdh05hub16" d="M207.200000,297.200000C188.400000,297.200000,169.900000,289,157.200000,273.700000C139.300000,252.100000,137.100000,221.700000,151.900000,197.900000C152.500000,196.900000,153.800000,196.600000,154.900000,197.200000C155.900000,197.800000,156.200000,199.100000,155.600000,200.200000C141.800000,222.400000,143.800000,250.900000,160.500000,271C178.300000,292.400000,208.300000,299,233.400000,287C234.500000,286.500000,235.700000,286.900000,236.300000,288C236.800000,289.100000,236.400000,290.400000,235.300000,290.900000C226.200000,295.100000,216.700000,297.200000,207.200000,297.200000Z" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="5" stroke-miterlimit="10"/><g id="ex2bdh05hub17"><polygon id="ex2bdh05hub18" points="162.600000,220.100000 158.500000,221.200000 153.300000,200.700000 133.200000,207.300000 131.900000,203.200000 156.400000,195.200000" fill="rgb(255,255,255)" stroke="rgb(255,255,255)" stroke-width="5" stroke-miterlimit="10"/></g></g></g></g></g></svg>
        <p class="sync-message">
            <?php echo $is_hun ? 'Szinkronizáció szükséges...' : 'Sync needed...'; ?>
        </p>
        <div class="cta-button-holder">
            <a href="" class="cta-button cta-button--intro"><?php echo $is_hun ? 'SZINKRONIZÁCIÓ' : 'SYNC NOW'; ?></a>
        </div>
    </div>
    <div id="db-save-error" class="hide">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 300 300" style="enable-background:new 0 0 300 300;" xml:space="preserve">
<style type="text/css">
    .st0{fill:#FFFFFF;}
</style>
            <g>
                <g>
                    <path class="st0" d="M126.8,77.1c-51.4,0-106-13.5-106-38.5c0-25,54.6-38.5,106-38.5c51.4,0,106,13.5,106,38.5
			C232.8,63.6,178.2,77.1,126.8,77.1z M126.8,10.6c-59.2,0-95.5,16.3-95.5,28s36.3,28,95.5,28c59.2,0,95.5-16.3,95.5-28
			S186,10.6,126.8,10.6z"/>
                    <path class="st0" d="M126.8,143.6c-51.4,0-106-13.5-106-38.5c0-2.9,2.4-5.3,5.3-5.3c2.9,0,5.3,2.4,5.3,5.3c0,11.7,36.3,28,95.5,28
			c59.2,0,95.5-16.3,95.5-28c0-2.9,2.4-5.3,5.3-5.3c2.9,0,5.3,2.4,5.3,5.3C232.8,130.1,178.2,143.6,126.8,143.6z"/>
                    <path class="st0" d="M126.8,210.1c-51.4,0-106-13.5-106-38.5c0-2.9,2.4-5.3,5.3-5.3c2.9,0,5.3,2.4,5.3,5.3c0,11.7,36.3,28,95.5,28
			c2.9,0,5.3,2.4,5.3,5.3C132.1,207.7,129.7,210.1,126.8,210.1z"/>
                    <path class="st0" d="M227.6,157c-2.9,0-5.3-2.4-5.3-5.3V38.5c0-2.9,2.4-5.3,5.3-5.3c2.9,0,5.3,2.4,5.3,5.3v113.2
			C232.8,154.6,230.5,157,227.6,157z"/>
                    <path class="st0" d="M126.8,276.6c-51.4,0-106-13.5-106-38.5V38.5c0-2.9,2.4-5.3,5.3-5.3c2.9,0,5.3,2.4,5.3,5.3V238
			c0,11.7,36.3,28,95.5,28c2.9,0,5.3,2.4,5.3,5.3C132.1,274.2,129.7,276.6,126.8,276.6z"/>
                </g>
                <path class="st0" d="M210.6,300c-37.9,0-68.6-30.8-68.6-68.6s30.8-68.6,68.6-68.6c37.8,0,68.6,30.8,68.6,68.6S248.5,300,210.6,300z
		 M210.6,173.3c-32,0-58.1,26.1-58.1,58.1c0,32,26.1,58.1,58.1,58.1c32,0,58.1-26.1,58.1-58.1C268.7,199.3,242.6,173.3,210.6,173.3z
		"/>
                <path class="st0" d="M233.8,262L180,208.2c-2.1-2.1-2.1-5.4,0-7.5c2.1-2.1,5.4-2.1,7.5,0l53.8,53.8c2.1,2.1,2.1,5.4,0,7.5
		C239.2,264,235.8,264,233.8,262z"/>
                <path class="st0" d="M180,262c-2.1-2.1-2.1-5.4,0-7.5l53.8-53.8c2.1-2.1,5.4-2.1,7.5,0c2.1,2.1,2.1,5.4,0,7.5L187.5,262
		C185.4,264,182.1,264,180,262z"/>
            </g>
</svg>

        <p class="sync-message">
            <?php echo $is_hun ? 'Hálózati hiba: nem sikerült elmenteni az adatokat...' : 'Network error: data could not be saved...'; ?>
        </p>
        <div class="cta-button-holder">
            <a href="" class="cta-button cta-button--intro"><?php echo $is_hun ? 'ÚJRATÖLTÉS' : 'RELOAD'; ?></a>
        </div>
    </div>
</div>


<div id="add-task-btn">
    <button><svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 140 140" width="140" height="140"><g transform="matrix(14,0,0,14,0,0)"><path d="M9.5,4.5A.5.5,0,0,0,9,4H6V1A.5.5,0,0,0,5.5.5h-1A.5.5,0,0,0,4,1V4H1a.5.5,0,0,0-.5.5v1A.5.5,0,0,0,1,6H4V9a.5.5,0,0,0,.5.5h1A.5.5,0,0,0,6,9V6H9a.5.5,0,0,0,.5-.5Z" fill="#000000" stroke="#000000" stroke-linecap="round" stroke-linejoin="round"></path></g></svg></button>
</div>

<div id="cards-overlay" class="hide app-overlay">
    <?php require(locate_template('partials/app/app-cards.php')); ?>
</div>
<div id="principle-visualization">
    <h1><?php echo $is_hun ? 'Elvek' : 'Principles'; ?></h1>
    <div class="principle-options">
        <button id="add-new-principle"><?php echo $is_hun ? 'Új +' : 'Add +'; ?></button>
        <button id="principle-card-view-button"><?php echo $is_hun ? 'Kártyák' : 'Cards'; ?></button>
        <button id="principle-board-view-button"><?php echo $is_hun ? 'Tábla' : 'Board'; ?></button>
        <button id="principle-mindmap-view-button"><?php echo $is_hun ? 'Elmetérkép' : 'MindMap'; ?></button>
    </div>
    <span class="close">✕</span>
    <div id="jsmind_container"></div>
    <div id="principles-horizontal-view">
    </div>
</div>
<div id="task-add-move-notification" class="hidden">Task move notification</div>
<div id="task-settings-modal-wrapper">
    <div id="task-settings-modal" class="hide">
        <!--    <span class="close">✕</span>-->
        <div id="taks-settings-modal__subtask-generator">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M320 0c17.7 0 32 14.3 32 32V96H480c35.3 0 64 28.7 64 64V448c0 35.3-28.7 64-64 64H160c-35.3 0-64-28.7-64-64V160c0-35.3 28.7-64 64-64H288V32c0-17.7 14.3-32 32-32zM208 384c-8.8 0-16 7.2-16 16s7.2 16 16 16h32c8.8 0 16-7.2 16-16s-7.2-16-16-16H208zm96 0c-8.8 0-16 7.2-16 16s7.2 16 16 16h32c8.8 0 16-7.2 16-16s-7.2-16-16-16H304zm96 0c-8.8 0-16 7.2-16 16s7.2 16 16 16h32c8.8 0 16-7.2 16-16s-7.2-16-16-16H400zM264 256c0-22.1-17.9-40-40-40s-40 17.9-40 40s17.9 40 40 40s40-17.9 40-40zm152 40c22.1 0 40-17.9 40-40s-17.9-40-40-40s-40 17.9-40 40s17.9 40 40 40zM48 224H64V416H48c-26.5 0-48-21.5-48-48V272c0-26.5 21.5-48 48-48zm544 0c26.5 0 48 21.5 48 48v96c0 26.5-21.5 48-48 48H576V224h16z"></path></svg>
            <?php echo do_shortcode("[wpaicg_form id=911 custom=yes]"); ?>
        </div>
        <form action="" id="task-settings-form">
            <div class="note">
                <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/write-note-white.svg'; ?>">
            </div>
            <div id="task-settings-modal__task-name" class="task-settings-modal__input">
                <input id="task-settings__task-name-input" type="text" placeholder="What do you need to accomplish?">
                <label for="task-settings__task-name-input">What?</label>
            </div>
            <div id="task-settings-modal__task-why-principle-wrapper">
                <div id="task-settings-modal__task-why" class="task-settings-modal__input">
                    <textarea id="task-settings__task-why-input" type="text" placeholder="Why is this really important?"></textarea>
                    <label for="task-settings__task-why-input">Why?</label>
                    <div class="critical-questions">
                        <h4>Critical questions</h4>
                        <p>What exactly does this mean?</p>
                        <p>What am I assuming here?</p>
                        <p>Are these reasons good enough?</p>
                        <p>Is there enough evidence?</p>
                    </div>
                </div>
                <div id="task-settings-modal__task-principle" class="task-settings-modal__input">
                    <textarea id="task-settings__task-principle-input" type="text" placeholder="What are the success principles to do this effectively?"></textarea>
                    <label for="task-settings__task-principle-input">How?</label>
                    <div class="critical-questions">
                        <h4>Critical questions</h4>
                        <p>What critical steps should be completed?</p>
                        <p>What processes should be followed?</p>
                        <p>What relevant information should be looked up?</p>
                        <p>What alternative ways of looking at this are there?</p>
                        <p>How can I set up a mutually beneficial joint venture to do this on a higher level?</p>
                        <p>What could I offer someone to do this without any upfront compensation?</p>
                    </div>
                </div>
            </div>
            <div id="task-settings-modal__subtask-generation-output" class="hide">
                <h4>Potential Steps</h4>
            </div>
            <fieldset class="relevant-principles hide">
                <legend><?php echo $is_hun ? 'Releváns elvek' : 'Relevant principles'; ?></legend>
                <div class="relevant-principles-wrapper"></div>
            </fieldset>
            <div class="task-input-group task-input-group--category">
                <fieldset>
                    <legend><?php echo $is_hun ? 'Kategória' : 'Category'; ?></legend>
                    <div class="task-input-group--category__wrapper">
                        <div class="task-input-group__element">
                            <input id="task-category-wi" type="radio" value="enhance" name="task_category">
                            <label for="task-category-wi"><?php echo $is_hun ? 'Szintlépés' : 'Wildly Important'; ?></label>
                        </div>
                        <div class="task-input-group__element">
                            <input id="task-category-must-do" type="radio" value="must-do" name="task_category">
                            <label for="task-category-must-do"><?php echo $is_hun ? 'Kötelező' : 'Must-Do'; ?></label>
                        </div>
                        <div class="task-input-group__element">
                            <input id="task-category-optional" type="radio" value="optional" name="task_category">
                            <label for="task-category-optional"><?php echo $is_hun ? 'Opcionális' : 'Optional'; ?></label>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div id="task-input-group__impact-score" class="task-input-group">
                <fieldset>
                    <legend><?php echo $is_hun ? 'Hatás pontszám a kategórián belül' : 'Impact in Category'; ?><b class="n-tooltip impact-tooltip">?</b></legend>
                    <!--            <label for="task-label-input__impact-score"><b></b> </label>-->
                    <!--            <output name="task_impact_value" for="task_impact" ></output>-->
                    <div class="impact-score__description">
                        <div><?php echo $is_hun ? 'Minimális' : 'Minimal'; ?><br><span>1</span></div>
                        <div style="text-align: right;"><?php echo $is_hun ?  'Óriási' : 'Enormous'; ?><br><span>5</span></div>
                    </div>
                    <input type="range" list="impact-score-values" value="1" min="1" max="5" name="task_impact">
                    <datalist id="impact-score-values">
                        <option value="1"></option>
                        <option value="2"></option>
                        <option value="3"></option>
                        <option value="4"></option>
                        <option value="5"></option>
                    </datalist>
                </fieldset>
            </div>
            <div class="task-input-group task-input-group--time">
                <fieldset>
                    <legend>Time Estimate</legend>
                    <div class="time-and-date">
                        <div class="task-input-group--time__wrapper">
                            <div class="task-input-group__element">
                                <!--                    <label for="task_day">--><?php //echo $is_hun ? 'Nap' : 'Day'; ?><!--</label>-->
                                <!--                    <input type="number" min="0" max="364" step="1" value="0" name="task_day">-->
                                <!--                </div>-->
                                <!--                <div class="task-input-group__element">-->
                                <!--                    <label for="task_hour">--><?php //echo $is_hun ? 'Óra' : 'Hour'; ?><!--</label>-->
                                <!--                    <input type="number" min="0" max="23" step="1" value="0" name="task_hour">-->
                                <!--                </div>-->
                                <!--                <div class="task-input-group__element">-->
                                <!--                    <label for="task_min">--><?php //echo $is_hun ? 'Perc' : 'Min'; ?><!--</label>-->
                                <!--                    <input type="number" min="0" max="55" step="5" value="15" name="task_min">-->
                                <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/time.svg'; ?>">
                                <select name="task_min">
                                    <option disabled selected>hh:mm</option>
                                    <option value="5">0:05</option>
                                    <option value="15">0:15</option>
                                    <option value="30">0:30</option>
                                    <option value="45">0:45</option>
                                    <option value="60">1:00</option>
                                    <option value="90">1:30</option>
                                    <option value="120">2:00</option>
                                    <option value="180">3:00</option>
                                    <option value="240">4:00</option>
                                    <option value="300">5:00</option>
                                    <option value="360">6:00</option>
                                    <option value="420">7:00</option>
                                    <option value="480">8:00</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>Due Date</legend>
                    <div class="time-and-date">
                        <div class="day">
                            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/calendar.svg'; ?>">
                            <span class="due-date"></span>
                        </div>
                    </div>
                </fieldset>
            </div>
            <div class="task-input-group task-input-group--labels">
                <fieldset>
                    <legend><?php echo $is_hun ? 'Címkék' : 'Labels'; ?></legend>
                    <div class="task-input-group__element">
                        <select id="task-label-input__values" name="task_label">
                            <option selected disabled><?php echo $is_hun ? 'Válassz címkét' : 'Select label'; ?></option>
                            <optgroup data-label-type="most-used" id="most-used" label="<?php echo $is_hun ? 'Gyakori' : 'Most used'; ?>">
                            </optgroup>
                            <optgroup data-label-type="custom" id="custom-labels" label="<?php echo $is_hun ? 'Egyedi' : 'Custom labels'; ?>">
                            </optgroup>
                            <optgroup data-label-type="roles" id="role-labels" label="<?php echo $is_hun ? 'Szerepkörök' : 'Roles'; ?>">
                            </optgroup>
                            <!--                    <optgroup data-label-type="values" id="value-labels" label="--><?php //echo $is_hun ? 'Értékek' : 'Values'; ?><!--">-->
                            <!--                    </optgroup>-->
                        </select>
                        <button id="add-new-custom-label">
                            <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/label.svg'; ?>">
                        </button>
                    </div>
                    <!--                <button id="apply-label">--><?php //echo $is_hun ? 'Kiválasztás' : 'Apply'; ?><!--</button>-->
                    <div class="applied-labels-wrapper"></div>
                    <!--                <div class="applied-label">-->
                    <!--                    <p>--><?php //echo $is_hun ? 'Aktív címkék' : 'Active labels'; ?><!--</p>-->
                    <!--                    <span class="label">Family</span>-->
                    <!--                </div>-->
            </div>
            </fieldset>
            <div id="task-settings-cta-buttons">
                <!--            <button class="cta-button close">--><?php //echo $is_hun ? 'MÉGSEM' : 'CANCEL'; ?><!--</button>-->
                <button class="cta-button" type="submit" id="save-task-settings"><?php echo $is_hun ? 'KÉSZ' : 'DONE'; ?></button>
            </div>
        </form>
    </div>
</div>

<div id="help-menu" class="hide">
    <ul>
<!--        <li id="show-tips">--><?php //echo $is_hun ? 'Használati tippek' : 'Show tooltips'; ?><!--</li>-->
<!--        <li id="daily-planning" onclick="startPlanning()">-->
<!--            <img src="--><?php //echo get_stylesheet_directory_uri() . '/assets/images/list-bullets.png'; ?><!--">-->
<!--            --><?php //echo $is_hun ? 'Napi tervezési útmutató' : 'Guided daily planning'; ?>
<!--        </li>-->
<!--        <li id="blog"><a href="https://n-hance.io/blog" target="_blank">Blog</a></li>-->
        <li id="rewatch-onboarding" onclick="startIntro()">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/magic-wand.png'; ?>">
            <?php echo $is_hun ? 'Az alkalmazás funkciói' : 'Feature showcase'; ?>
        </li>
        <li id="watch-walkthrough">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/svg-icons/walkthrough.svg'; ?>">
            <a href="https://share.vidyard.com/watch/j7mC5nTp9xzpZ15Ps3VjWF?" target="_blank"><?php echo $is_hun ? 'Részletes bemutató' : 'In-depth walkthrough'; ?></a>
        </li>
        <li id="show-get-started-guide">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/travel-map.png'; ?>">
            <a href="https://n-hance.io/get-started-guide/" target="_blank">
                <?php echo $is_hun ? 'Indulási segédlet' : 'Getting started guide'; ?>
            </a>
        </li>
<!--        <li id="new-features"><a href="https://n-hance.io/blog/new-featueres" target="_blank">--><?php //echo $is_hun ? 'Újdonságok' : 'What\'s new'; ?><!--</a></li>-->
<!--        <li id="feature-showcase" onclick="startFeatureShowcase()">-->
<!--            <img src="--><?php //echo get_stylesheet_directory_uri() . '/assets/images/edit-view.png'; ?><!--">-->
<!--            --><?php //echo $is_hun ? 'Funkció bemutató' : 'Feature showcase'; ?>
<!--        </li>-->
        <li id="keyboard-shortcuts" onclick="showKeyBoardShortcuts()">
            <img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/keyboard.png'; ?>">
            <?php echo $is_hun ? 'Gyorsbillentyűk' : 'Keyboard shortcuts'; ?>
        </li>
    </ul>
</div>
<div id="keyboard-shortcut-list">
    <ul class="keyboard-shortcuts">
        <li>
            <div class="feature-triggered">Opening multiple lists side by side</div>
            <div class="feature-shortcut">Hold down the <b>CTRL key</b> and <b>click</b> on the name of the lists</div>
        </li>
        <li>
            <div class="feature-triggered">Opening all lists within a category side by side</div>
            <div class="feature-shortcut">Hold down the <b>CTRL key</b> and <b>click</b> on the category name</div>
        </li>
        <li>
            <div class="feature-triggered">Quick add task</div>
            <div class="feature-shortcut">Press the <b>ALT</b> + <b>“+”</b> keys simultaneously</div>
        </li>
        <li>
            <div class="feature-triggered">Quick add task with deadline</div>
            <div class="feature-shortcut">Press the <b>CTRL</b> + <b>e</b> keys simultaneously</div>
        </li>
        <li>
            <div class="feature-triggered">Quick add task and start timetracking</div>
            <div class="feature-shortcut">Press the <b>ALT</b> + <b>t</b> keys simultaneously</div>
        </li>
        <li>
            <div class="feature-triggered">Toggle calendar</div>
            <div class="feature-shortcut">Press the <b>ALT</b> + <b>c</b> keys simultaneously</div>
        </li>
        <li>
            <div class="feature-triggered">Show task filter menu</div>
            <div class="feature-shortcut">Press the <b>ALT</b> + <b>a</b> keys simultaneously</div>
        </li>
        <li>
            <div class="feature-triggered">Toggle horizontal/vertical view for a single list</div>
            <div class="feature-shortcut">Press the <b>ALT</b> + <b>b</b> keys simultaneously</div>
        </li>
    </ul>
</div>
<div id="progress-point-indicator">
    <p>0.5 x 15 mins x impact of 1 = 8 progress points</p>
</div>
<div class="add-task-wrapper hide">
    <input class="add-task" type="text" placeholder="Enter new task name...">
</div>
