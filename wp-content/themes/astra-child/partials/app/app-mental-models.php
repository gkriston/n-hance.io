<?php
$nhance_options = get_user_meta(get_current_user_id(), 'nhance_options')[0];
$is_hun = true;
$args_models = array(
    'post_type' => 'mental_models',
    'lang' => $is_hun ? 'hu' : 'en'
);

$loop_models = new WP_Query( $args_models );
?>

<?php if ( $loop_models->have_posts() ) : ?>

    <?php do_action( 'astra_template_parts_content_top' ); ?>

    <?php
        $category_array = array();

        while ( $loop_models->have_posts() ) {
            $loop_models->the_post();
            $the_category = wp_get_post_terms( get_the_ID(), 'mental_model_category')[0]->name;

            if ($the_category && !in_array($the_category, $category_array)) {
                array_push($category_array, $the_category);
            }
        }

        sort($category_array);

        echo '<div id="mental-models-menu">
                    <div id="mental-models-menu__content-wrapper">
                    <h3>' . ($is_hun ? 'Projekt fázisok' : 'Project stages') . '</h3>
                    <ul class="category-selector">';
        //echo '<li data-category="all"><div class="card-category">' . ($is_hun ? 'Összes' : 'All') . '</div></li>';

        foreach ($category_array as $category) {
            echo '<li class="menu-item" data-category="' . str_replace(' ', '_', strtolower($category)) . '"><span>' . $category . '</span></li>';
        }

        echo '</ul></div></div>';

        rewind_posts();
    ?>

    <button id="mental-models-menu__return-btn"><< <?php echo $is_hun ? 'Vissza' : 'Back'; ?></button>
    <div id="mental-model-slider">
        <?php while ( $loop_models->have_posts() ) : ?>
            <?php $loop_models->the_post(); ?>
            <?php $the_category = wp_get_post_terms( get_the_ID(), 'mental_model_category')[0]->name; ?>

            <div class="simple-card <?php echo str_replace(' ', '_', strtolower($the_category)); ?>">
                <div class="simple-card-inner">
                    <div class="simple-card-content">
                        <?php if ($the_category) : ?>
                        <div class="card-category">
                            <?php echo $the_category; ?>
                        </div>
                        <?php endif; ?>
                        <h4><?php the_title(); ?></h4>
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>

        <?php endwhile; ?>
    </div>

<?php endif; ?>

<?php wp_reset_query(); ?>
