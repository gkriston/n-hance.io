<?php $nhance_options = get_user_meta(get_current_user_id(), 'nhance_options')[0]; ?>
<?php $is_hun = false; ?>
<?php if ($is_hun) : ?>
    <div id="activity-review">
        <h3>Megtervezted már a napodat/hetedet és reflektáltál az elmúlt nap történéseire?</h3>
        <div class="row">
            <div class="col-lg-3">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/planning.svg">
            </div><div class="col-lg-9">
                <p><b>Mindig tervezz időt a következőkre:</b></p>
                <ul>
                    <li>Nézd át az életed fontos szerepeit és a hozzájuk tartozó célokat</li>
                    <li>Gondold át, hogyan tudnál dolgozni a saját fejlődéseden</li>
                    <li>Reflektálj az előző nap tanulságaira és a helyzetekre, amelyek negatív érzelmeket váltottak ki belőled</li>
                    <li>Mérlegeld, hogy mindig a legfontosabb feladatokkal foglalkozol -e</li>
                </ul>
            </div>
        </div>
        <p><b>Ne felejtsd el:</b><br>Minden nap egy új esély a fejlődéshez!</p>
        <div class="cta-button-holder">
            <a href="" class="cta-button cta-button--planning">TERVEZÉS</a>
        </div>
    </div>
<?php else : ?>
    <div id="activity-review">
        <h3>Have you planned your day/week yet and reflected on the past day's happenings?</h3>
        <div class="row">
            <div class="col-lg-3">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/planning.svg">
            </div><div class="col-lg-9">
                <p><b>Always make sure to:</b></p>
                <ul>
                    <li>Review you life roles and goals</li>
                    <li>Think about if you have taken the time to sharpen the saw</li>
                    <li>Reflect on lessons learned or pain felt</li>
                    <li>Review if you are working on the most impactful tasks</li>
                </ul>
            </div>
        </div>
        <p><b>Remember:</b><br>Every day is a new chance to grow!</p>
        <div class="cta-button-holder">
            <a href="" class="cta-button cta-button--planning">LET'S PLAN</a>
        </div>
    </div>
<?php endif; ?>


<?php if ($is_hun) : ?>
    <div id="first-run-greeting">
        <h1>Gratulálok! Ráléptél az útra egy tudatosabb jövő felé!</h1>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/crossroad.svg">
            </div><div class="col-lg-6 col-md-6">
                <p class="usp-list-header"><b>Az alkalmazás aktív használatával:</b></p>
                <ul>
                    <li>Képes leszel beazonosítani, hogy jelenleg az életed mely területe igényli a legnagyobb figyelmet</li>
                    <li>Meghatározhatod azokat a következő lépcsőfokokat, amelyek elérése által teljesebbé válhat az életed</li>
                    <li>Mindig tudni fogod, hogy milyen feladatokon lesz érdemes dolgoznod, hogy a leggyorsabb ütemben haladhass</li>
                    <li>Megszabadulhatsz a feladatoktól, amelyek nem visznek előre téged</li>
                </ul>
            </div>
        </div>
        <p class="next-step"><b>Hogyan indulj el?</b><br>Nézd végig az interaktív alkalmazábemutatót az alábbi gombra kattintva!</p>
        <div class="cta-button-holder">
            <a href="" class="cta-button cta-button--intro">BEMUTATÓ</a>
        </div>
    </div>
<?php else : ?>
    <div id="first-run-greeting">
        <h1>Congratulations! You have stepped on the path of a more conscious future!</h1>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/crossroad.svg">
            </div><div class="col-lg-6 col-md-6"><p class="usp-list-header"><b>With the active use of this app, you will be able to:</b></p>
                <ul>
                    <li>Identify the areas of your life, which needs the most attention right now</li>
                    <li>Set the next steps in each area, which will help your reach a more fulfilled life</li>
                    <li>Filter tasks based on which will help you make the most progress towards your goals</li>
                    <li>Get rid of the tasks which just set you back</li>
                </ul>
            </div>
        </div>
        <p class="next-step"><b>How to get started?</b><br>Take your time to go through the interacting app introduction by clicking on the button below!</p>
        <div class="cta-button-holder">
            <a href="" class="cta-button cta-button--intro">LET'S START</a>
        </div>
    </div>
    <div id="xd-walkthrough" class="hide">
        <span class="close">X</span>
        <iframe width="1920" height="930" src="https://xd.adobe.com/embed/b607b054-931e-458c-82da-3d11e79d735c-e354/?fullscreen&hints=off" frameborder="0" allowfullscreen></iframe>
    </div>
<?php endif; ?>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        introCfg = !isHun ? { 'showProgress': true, 'showBullets': false, 'skipLabel': 'X', 'doneLabel': 'X' } : {
            'showProgress': true,
            'showBullets': false,
            'nextLabel': 'Következő',
            'prevLabel': 'Előző',
            'skipLabel': 'X',
            'doneLabel': 'X'
        };
    });
</script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/intro/onboarding.js'></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/intro/feature-showcase.js'></script>
<script src='<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/intro/daily-planning.js'></script>
