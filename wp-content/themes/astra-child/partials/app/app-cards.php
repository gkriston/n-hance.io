<?php
$nhance_options = get_user_meta(get_current_user_id(), 'nhance_options')[0];
$is_hun = false;
$args_cards = array(
    'post_type' => 'card',
    'lang' => $is_hun ? 'hu' : 'en'
);

$loop_cards = new WP_Query( $args_cards );
?>

<?php if ( $loop_cards->have_posts() ) : ?>

    <?php do_action( 'astra_template_parts_content_top' ); ?>

    <div id="card-slider">
        <?php while ( $loop_cards->have_posts() ) : ?>
            <?php $loop_cards->the_post(); ?>

            <?php if (get_field('card_back')) : ?>
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <div class="card-category">
                                <?php echo wp_get_post_terms( get_the_ID(), 'card_category')[0]->name; ?>
                            </div>
                            <h2><?php the_title(); ?></h2>
                            <?php the_content(); ?>
                            <p class="answer-info"><?php echo $is_hun ? 'Kattints a kártyára a válaszért...' : 'Click on the card to see the answer...'; ?></p>
                        </div>
                        <div class="flip-card-back">
                            <?php the_field('card_back'); ?>
                            <?php if (get_field('card_principle')) : ?>
                                <div class="card-principle">
                                    <h3><?php echo $is_hun ? 'Elv' : 'Principle'; ?></h3>
                                    <p class="card-principle-text"><?php the_field('card_principle'); ?></p>
                                    <button class="add-card-principle"><?php echo $is_hun ? 'Hozzáadás sajátokhoz' : 'Add to my principles'; ?> <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/plus-icon.svg"></button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <div class="simple-card">
                    <div class="simple-card-inner">
                        <div class="simple-card-content">
                            <div class="card-category">
                                <?php echo wp_get_post_terms( get_the_ID(), 'card_category')[0]->name; ?>
                            </div>
                            <h2><?php the_title(); ?></h2>
                            <?php the_content(); ?>
                            <?php if (get_field('card_principle')) : ?>
                                <div class="card-principle">
                                    <h3><?php echo $is_hun ? 'Elv' : 'Principle'; ?></h3>
                                    <p class="card-principle-text"><?php the_field('card_principle'); ?></p>
                                    <button class="add-card-principle"><?php echo $is_hun ? 'Hozzáadás sajátokhoz' : 'Add to my principles'; ?> <img class="svg-icon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/svg-icons/plus-icon.svg"></button>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>


        <?php endwhile; ?>
    </div>

<?php endif; ?>

<?php wp_reset_query(); ?>
