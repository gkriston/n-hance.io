<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>

</div> <!-- ast-container -->

</div><!-- #content -->

<?php astra_content_after(); ?>

<?php astra_footer_before(); ?>

<?php astra_footer(); ?>

<?php astra_footer_after(); ?>

</div><!-- #page -->

<?php astra_body_bottom(); ?>

<?php if (is_page_template('app.php')) : ?>
    <?php function app_ver() {echo '?ver=' . $GLOBALS['app_ver'];} ?>
    <script>
        var iosDragDropShim = { holdToDrag: 300 } //Adds 300ms delay before draging
    </script>
<!--    <script src="--><?php //echo get_stylesheet_directory_uri(); ?><!--/assets/js/ios-drag-drop.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intro.js/2.9.3/intro.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/listcategory.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/generate-google-calendar-url.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/hu-translation.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/logger.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/analytics.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/calendar.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/procon.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/principles/principles-slider.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/principles/mindmap/jsmind.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/principles/mindmap/jsmind.draggable.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/principles/mindmap/principles-mindmap.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/principles/principles.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/compass.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/lifewheel.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/time-tracking.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/settings.js<?php app_ver(); ?>"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-analytics.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.7.0/firebase-firestore.js"></script>
    <script src="https://d3js.org/d3.v3.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/firebase/UserDataManager.js<?php app_ver(); ?>"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/language/map.json"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/language/Translate.js<?php app_ver(); ?>"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<!--    <script src="//daybrush.com/moveable/release/latest/dist/moveable.min.js"></script>-->
<!--    <script>-->
<!--        const moveable = new Moveable(document.body, {-->
<!--            target: document.getElementById('editor-wrapper'),-->
<!--            draggable: true,-->
<!--        });-->
<!---->
<!--        moveable.on("drag",  ({ target, transform }) => {-->
<!--            target.style.transform = transform;-->
<!--        });-->
<!--    </script>-->




    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@5.10.1/main.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/introjs.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/modules/principles/principles.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/modules/principles/principles-slider.css">
<?php endif; ?>
<?php wp_footer(); ?>

<script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=document.createElement("script");r.type="text/javascript",r.async=!0,r.src="https://cdn.heapanalytics.com/js/heap-"+e+".js";var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(r,a);for(var n=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","resetIdentity","removeEventProperty","setEventProperties","track","unsetEventProperty"],o=0;o<p.length;o++)heap[p[o]]=n(p[o])};
    heap.load("1194828414");

    <?php if ( is_user_logged_in()) : ?>
    heap.identify('<?php echo  wp_get_current_user()->user_nicename; ?>');
    <?php endif; ?>
</script>

<script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="2845c781-f5b9-4cd1-bb4a-9e2be025c48e";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<style>
    .crisp-client .cc-kv6t .cc-1xry .cc-unoo {
        right: 15px!important;
        bottom: 85px!important;
    }

    @media (max-width: 559px) {
        .crisp-client .cc-kv6t[data-full-view=true] .cc-1xry .cc-unoo {
            right: 15px!important;
            bottom: 75px!important;
        }
    }
</style>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        document.querySelectorAll('.flip-card').forEach(function(card) {
            card.addEventListener('click', function(card) {
                card.currentTarget.classList.toggle('flipped')
            });
        });
    });
</script>
<script>
    if (Appcues) {
        Appcues.page()
        window.Appcues.identify(appVariables.currentUser);
    }
</script>
<script type="text/javascript">(function(u,x,t,w,e,a,k,s){a=function(v){try{u.setItem(t+e,v)}catch(e){}v=JSON.parse(v);for(k=0;k<v.length;k++){s=x.createElement("script");s.text="(function(u,x,t,w,e,a,k){a=u[e]=function(){a.q.push(arguments)};a.q=[];a.t=+new Date;a.c=w;k=x.createElement('script');k.async=1;k.src=t;x.getElementsByTagName('head')[0].appendChild(k)})(window,document,'"+v[k].u+"',"+JSON.stringify(v[k].c)+",'"+v[k].g+"')";x.getElementsByTagName("head")[0].appendChild(s)}};try{k=u.getItem(t+e)}catch(e){}if(k){return a(k)}k=new XMLHttpRequest;k.onreadystatechange=function(){if(k.readyState==4&&k.status==200)a(k.responseText)};k.open("POST",w+e);k.send(x.URL)})(sessionStorage,document,"uxt:","https://api.uxtweak.com/snippet/","27195b5d-8b7a-4e95-b808-43a3352f52bc");</script>
</body>
</html>
