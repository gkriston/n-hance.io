<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

if (!is_user_logged_in() && strpos(home_url( $wp->request ), 'app')) {
    if ($_GET['action'] == 'import') {
        wp_redirect(site_url() . '/login-needed-shared-list/');
    } else {
        wp_redirect(site_url() . '/wp-admin');
    }

    exit;
}
?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?> class="<?php echo (is_page('app') ? 'app' : '');?>">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-MJXBRLJ');</script>
    <!-- End Google Tag Manager -->
    <?php astra_head_top(); ?>

    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"">
    <?php if (is_front_page()) : ?>
        <script type="text/javascript" src="https://app.viral-loops.com/widgetsV2/core/loader.js"></script>
        <meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facebook-sharing.jpg">
    <?php endif; ?>
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">
    <?php if (is_page_template('app.php')) : ?>
        <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
        <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/quill/task_list.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/quill/note-extensions.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/app.css?ver=<?php echo $GLOBALS['app_ver']; ?>">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/jsmind.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/modules/jquery.spidergraph.js"></script>

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/ms-icon-144x144.png">
        <meta name="theme-color" content="#4d2780">

        <script src='<?php echo get_stylesheet_directory_uri(); ?>/assets/js/moment.js'></script>
        <script src="https://cdn.jsdelivr.net/npm/fullcalendar-scheduler@5.10.1/main.min.js"></script>
        <script src='<?php echo get_stylesheet_directory_uri(); ?>/assets/js/fullcalendar/interaction/main.js'></script>
        <script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
        <script src="https://unpkg.com/vue@next"></script>
    <?php endif; ?>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
    <?php astra_head_bottom(); ?>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1039036730232490');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=1039036730232490&ev=PageView&noscript=1"
        /></noscript>
    <meta name="facebook-domain-verification" content="3et33uwu3c5bi9dxesnnhxsvrwfr63" />
    <!-- End Facebook Pixel Code -->
    <script src="//fast.appcues.com/82689.js"></script>
</head>
<body <?php body_class('dx-style en board-view show-prio x-ray beta-features-off'); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MJXBRLJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php astra_body_top(); ?>
<?php wp_body_open(); ?>
<?php if (is_page('register')) : ?>
    <script>
        setTimeout(() => {
            if (document.getElementById('user_email-152')) {
                document.getElementById('user_email-152').value = '<?php if ($_POST['mc-reg-email']) {echo $_POST['mc-reg-email'];} ?>';
            }

            if (document.getElementById('user_login-152')) {
                document.getElementById('user_login-152').value = '<?php if ($_POST['mc-reg-email']) {echo explode('@',$_POST['mc-reg-email'])[0];} ?>';
            }
        }, 2000);
    </script>
    <style>
        .um-col-alt {
            display: flex;
            justify-content: center;
        }

        .um-right {
            display: none;
        }
    </style>
<?php endif; ?>
<div
    <?php
    echo astra_attr(
        'site',
        array(
            'id'    => 'page',
            'class' => 'hfeed site',
        )
    );
    ?>
>
    <a class="skip-link screen-reader-text" href="#content"><?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?></a>

    <?php astra_header_before(); ?>

    <?php astra_header(); ?>

    <?php astra_header_after(); ?>

    <?php astra_content_before(); ?>

    <div id="content" class="site-content">

        <div class="ast-container">

            <?php astra_content_top(); ?>
