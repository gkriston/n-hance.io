<?php
require(dirname(getcwd()) . '/db-conn.php');
$json = file_get_contents('php://input');
//$data = json_decode($json);

try {
    $conn = mysqli_connect("127.0.0.1", $db_conn['user'], $db_conn['password'], $db_conn['database']);
    mysqli_set_charset($conn, "utf8");
    // set the PDO error mode to exception
    // echo "Connected successfully"; 
} catch (mysqli_sql_exception $e) { 
    throw $e; 
} 

$target_table = $_COOKIE['username'] == 'creator' ? 'moduledata' : 'userdata';

$sql = "SELECT dayviewdata FROM " . $target_table . " WHERE name = '" . $_COOKIE['username'] . "'";


$result = mysqli_query($conn, $sql);

$rows = array();

while($r = mysqli_fetch_array($result)) {
  $rows[] = $r;
}

echo json_encode($rows);

/* close connection */
mysqli_close($conn);
?>