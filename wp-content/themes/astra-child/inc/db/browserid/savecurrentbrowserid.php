<?php
require(dirname(getcwd()) . '/db-conn.php');
$json = file_get_contents('php://input');
$data = json_decode($json);
$randbrowserid = $data->randbrowserid;

try {
    $conn = mysqli_connect("127.0.0.1", $db_conn['user'], $db_conn['password'], $db_conn['database']);
    mysqli_set_charset($conn, "utf8");
    // set the PDO error mode to exception
    // echo "Connected successfully"; 
} catch (mysqli_sql_exception $e) { 
    throw $e; 
} 

$target_table = $_COOKIE['username'] == 'creator' ? 'moduledata' : 'userdata';

$sql1 = "SELECT name FROM " . $target_table . " WHERE name = '" . $_COOKIE['username'] . "'";

$result1 = mysqli_query($conn, "SELECT name FROM " . $target_table . " WHERE name = '" . $_COOKIE['username'] . "'");

if ($result1->num_rows != 0) {
	$sql2 = "UPDATE " . $target_table . " SET randbrowserid='" . $randbrowserid . "' WHERE name='" . $_COOKIE['username'] . "'";

	$result = mysqli_query($conn, $sql2);

	echo 'Current browser id saved from savecurrentbrowserid.php: ' . $randbrowserid;
}

/* close connection */
mysqli_close($conn);
?>