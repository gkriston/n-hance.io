# Notes

#### The miniorange firebase authentication plugin
- Not log out from firebase auth in case of wp-logout
- Login also not working properly
- For testing purposes, the loginToFirebase(String email, String password) and logoutFromFirebase() functions created  
<b>IMPORTANT: These errors only occur when the login happened by wordpress</b>  
  In case of firebase user login the authentication, login-logout and user changing works.

## TODO
- [x] Firebase auth (login logout) binding to wp-login, wp-logout (in case of firebase user login)
- [ ] User data moving from mysql to firebase
- [x] Testing miniorange firebase authorization plugin
- [ ] Testing integrate firebase plugin (free version not enought)

