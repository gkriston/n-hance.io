## TODO on branch refactor-database-firebase

####IMPORTANT
Before any modification and testing I advise to use an other firebase in which you have admin permissions.  
In order to do that change the firebase config [in this file](wp-content/themes/astra-child/assets/js/modules/firebase/UserDataManager.js) in line 10.

####Notes
The php language setter methods removed from the [app-main.php](wp-content/themes/astra-child/partials/app/app-main.php) and replaced with [Translate.js and map.json](wp-content/themes/astra-child/assets/js/modules/language).  
Other files use the old php calls, this can cause language problems, because the correct language mode loaded in the frontend side by firebase useroption query.  
In the [app.js](wp-content/themes/astra-child/assets/js/app.js) (e.g. line 2125, 2547, ...) there are possible language setter lines, which could be refactored into the [Translate.js and map.json](wp-content/themes/astra-child/assets/js/modules/language) files.  
The `randomBrowserId` stored and managed in the firebase. The usermanagement also expanded into firebase.


####Todo

- [x] User management in firebase
- [x] `randomBrowserId` management in firebase
- [x] `userOptions` management in firebase
  - [x] theme loading
  - [x] options loading
  - [ ] language loading (partially OK)
    - [x] create js base language setter function ([Translate.js and map.json](wp-content/themes/astra-child/assets/js/modules/language))
    - [ ] refactor php and js language setters to work 100%
  - [ ] complex testing of `userOptions` loading and saving.
- [ ] Remove refactored old php base methods from code
- [ ] Final testing

###Contact
**email: david.polonkai2@gmail.com**